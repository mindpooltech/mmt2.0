(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-pdf-modal-pdf-modal-module"],{

/***/ "0JJn":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/pdf-modal/pdf-modal.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title class=\"ios title-default hydrated\">Answer Sheet PDF</ion-title>\n    <div class=\"ion-colse-btn\" size=\"small\" (click)=\"dismiss()\"><ion-icon name=\"close\"></ion-icon>\n    </div> \n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <iframe allowfullscreen frameborder=\"0\" height=\"750px\" width=\"100%\" [src]=\"Url | safe\">\n  </iframe>\n</ion-content>\n");

/***/ }),

/***/ "UBgk":
/*!*****************************************************!*\
  !*** ./src/app/pages/pdf-modal/pdf-modal.module.ts ***!
  \*****************************************************/
/*! exports provided: PdfModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PdfModalPageModule", function() { return PdfModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _pdf_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pdf-modal-routing.module */ "mi3y");
/* harmony import */ var _safe_pipe__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../safe.pipe */ "Z2+D");
/* harmony import */ var _pdf_modal_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pdf-modal.page */ "ndlx");








let PdfModalPageModule = class PdfModalPageModule {
};
PdfModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _pdf_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["PdfModalPageRoutingModule"]
        ],
        declarations: [_pdf_modal_page__WEBPACK_IMPORTED_MODULE_7__["PdfModalPage"], _safe_pipe__WEBPACK_IMPORTED_MODULE_6__["SafePipe"]]
    })
], PdfModalPageModule);



/***/ }),

/***/ "ctKr":
/*!*****************************************************!*\
  !*** ./src/app/pages/pdf-modal/pdf-modal.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-title {\n  background-color: #f27376;\n  color: #fff;\n  height: 50px;\n  font-size: 18px;\n  display: block;\n  text-align: center;\n  padding: 15px 0;\n}\n\n.ion-colse-btn {\n  position: absolute;\n  top: 15px;\n  right: 10px;\n  font-size: 27px;\n  color: #ffffff;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxwZGYtbW9kYWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQXlCO0VBQ3pCLFdBQVc7RUFDWCxZQUFZO0VBQ1osZUFBZTtFQUNmLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsZUFBZTtBQUNuQjs7QUFDQTtFQUNJLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsV0FBVztFQUNYLGVBQWU7RUFDZixjQUFjO0FBRWxCIiwiZmlsZSI6InBkZi1tb2RhbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdGl0bGUge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2YyNzM3NjtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nOiAxNXB4IDA7XHJcbn1cclxuLmlvbi1jb2xzZS1idG57XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IDE1cHg7XHJcbiAgICByaWdodDogMTBweDtcclxuICAgIGZvbnQtc2l6ZTogMjdweDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG59Il19 */");

/***/ }),

/***/ "mi3y":
/*!*************************************************************!*\
  !*** ./src/app/pages/pdf-modal/pdf-modal-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: PdfModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PdfModalPageRoutingModule", function() { return PdfModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _pdf_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pdf-modal.page */ "ndlx");




const routes = [
    {
        path: '',
        component: _pdf_modal_page__WEBPACK_IMPORTED_MODULE_3__["PdfModalPage"]
    }
];
let PdfModalPageRoutingModule = class PdfModalPageRoutingModule {
};
PdfModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PdfModalPageRoutingModule);



/***/ }),

/***/ "ndlx":
/*!***************************************************!*\
  !*** ./src/app/pages/pdf-modal/pdf-modal.page.ts ***!
  \***************************************************/
/*! exports provided: PdfModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PdfModalPage", function() { return PdfModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_pdf_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./pdf-modal.page.html */ "0JJn");
/* harmony import */ var _pdf_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./pdf-modal.page.scss */ "ctKr");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");






let PdfModalPage = class PdfModalPage {
    constructor(navParams, modalController, sanitizer) {
        this.navParams = navParams;
        this.modalController = modalController;
        this.sanitizer = sanitizer;
        this.checked_answerbook = this.navParams.data.checked_answerbook;
        // console.log('checked_answerbook',this.checked_answerbook);
        // this.Url = this.sanitizer.bypassSecurityTrustResourceUrl(this.checked_answerbook);
        this.Url = 'http://docs.google.com/gview?embedded=true&url=' + this.checked_answerbook;
    }
    ngOnInit() {
    }
    dismiss() {
        this.modalController.dismiss();
    }
};
PdfModalPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavParams"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"] }
];
PdfModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-pdf-modal',
        template: _raw_loader_pdf_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_pdf_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], PdfModalPage);



/***/ })

}]);
//# sourceMappingURL=pages-pdf-modal-pdf-modal-module.js.map