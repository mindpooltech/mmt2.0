(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-note-pdf-modal-note-pdf-modal-module"],{

/***/ "6gv4":
/*!*************************************************************!*\
  !*** ./src/app/pages/note-pdf-modal/note-pdf-modal.page.ts ***!
  \*************************************************************/
/*! exports provided: NotePdfModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotePdfModalPage", function() { return NotePdfModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_note_pdf_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./note-pdf-modal.page.html */ "aL3c");
/* harmony import */ var _note_pdf_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./note-pdf-modal.page.scss */ "BaTg");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");






let NotePdfModalPage = class NotePdfModalPage {
    constructor(navParams, modalController, sanitizer) {
        this.navParams = navParams;
        this.modalController = modalController;
        this.sanitizer = sanitizer;
        this.note_url = this.navParams.data.note_url;
        // console.log('checked_answerbook',this.checked_answerbook);
        // this.Url = this.sanitizer.bypassSecurityTrustResourceUrl(this.checked_answerbook);
        this.Url = 'http://docs.google.com/gview?embedded=true&url=' + this.note_url;
    }
    ngOnInit() {
    }
    dismiss() {
        this.modalController.dismiss();
    }
};
NotePdfModalPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavParams"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["DomSanitizer"] }
];
NotePdfModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-note-pdf-modal',
        template: _raw_loader_note_pdf_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_note_pdf_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], NotePdfModalPage);



/***/ }),

/***/ "BaTg":
/*!***************************************************************!*\
  !*** ./src/app/pages/note-pdf-modal/note-pdf-modal.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-title {\n  background-color: #f27376;\n  color: #fff;\n  height: 50px;\n  font-size: 18px;\n  display: block;\n  text-align: center;\n  padding: 15px 0;\n}\n\n.ion-colse-btn {\n  position: absolute;\n  top: 15px;\n  right: 10px;\n  font-size: 27px;\n  color: #ffffff;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxub3RlLXBkZi1tb2RhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx5QkFBeUI7RUFDekIsV0FBVztFQUNYLFlBQVk7RUFDWixlQUFlO0VBQ2YsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixlQUFlO0FBQ25COztBQUNBO0VBQ0ksa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxXQUFXO0VBQ1gsZUFBZTtFQUNmLGNBQWM7QUFFbEIiLCJmaWxlIjoibm90ZS1wZGYtbW9kYWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRpdGxlIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmMjczNzY7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgcGFkZGluZzogMTVweCAwO1xyXG59XHJcbi5pb24tY29sc2UtYnRue1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAxNXB4O1xyXG4gICAgcmlnaHQ6IDEwcHg7XHJcbiAgICBmb250LXNpemU6IDI3cHg7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxufSJdfQ== */");

/***/ }),

/***/ "MEdv":
/*!***********************************************************************!*\
  !*** ./src/app/pages/note-pdf-modal/note-pdf-modal-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: NotePdfModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotePdfModalPageRoutingModule", function() { return NotePdfModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _note_pdf_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./note-pdf-modal.page */ "6gv4");




const routes = [
    {
        path: '',
        component: _note_pdf_modal_page__WEBPACK_IMPORTED_MODULE_3__["NotePdfModalPage"]
    }
];
let NotePdfModalPageRoutingModule = class NotePdfModalPageRoutingModule {
};
NotePdfModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NotePdfModalPageRoutingModule);



/***/ }),

/***/ "RC39":
/*!***************************************************************!*\
  !*** ./src/app/pages/note-pdf-modal/note-pdf-modal.module.ts ***!
  \***************************************************************/
/*! exports provided: NotePdfModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotePdfModalPageModule", function() { return NotePdfModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _note_pdf_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./note-pdf-modal-routing.module */ "MEdv");
/* harmony import */ var _safe_pipe__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../safe.pipe */ "Z2+D");
/* harmony import */ var _note_pdf_modal_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./note-pdf-modal.page */ "6gv4");








let NotePdfModalPageModule = class NotePdfModalPageModule {
};
NotePdfModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _note_pdf_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["NotePdfModalPageRoutingModule"]
        ],
        declarations: [_note_pdf_modal_page__WEBPACK_IMPORTED_MODULE_7__["NotePdfModalPage"], _safe_pipe__WEBPACK_IMPORTED_MODULE_6__["SafePipe"]]
    })
], NotePdfModalPageModule);



/***/ }),

/***/ "aL3c":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/note-pdf-modal/note-pdf-modal.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title class=\"ios title-default hydrated\">Note</ion-title>\n    <div class=\"ion-colse-btn\" size=\"small\" (click)=\"dismiss()\"><ion-icon name=\"close\"></ion-icon>\n    </div>\n\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <iframe allowfullscreen frameborder=\"0\" height=\"750px\" width=\"100%\" [src]=\"Url | safe\">\n  </iframe>\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=pages-note-pdf-modal-note-pdf-modal-module.js.map