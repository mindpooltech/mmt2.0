(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboard-dashboard-module"],{

/***/ "/2RN":
/*!*****************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.module.ts ***!
  \*****************************************************/
/*! exports provided: DashboardPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPageModule", function() { return DashboardPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dashboard-routing.module */ "ea/W");
/* harmony import */ var _dashboard_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dashboard.page */ "6ckw");







let DashboardPageModule = class DashboardPageModule {
};
DashboardPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_5__["DashboardPageRoutingModule"]
        ],
        declarations: [_dashboard_page__WEBPACK_IMPORTED_MODULE_6__["DashboardPage"]]
    })
], DashboardPageModule);



/***/ }),

/***/ "6ckw":
/*!***************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.page.ts ***!
  \***************************************************/
/*! exports provided: DashboardPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPage", function() { return DashboardPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_dashboard_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./dashboard.page.html */ "spzo");
/* harmony import */ var _dashboard_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard.page.scss */ "ws6Y");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/event.service */ "fTLw");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");












let DashboardPage = class DashboardPage {
    constructor(router, uihelper, storageService, navCtrl, http, events) {
        this.router = router;
        this.uihelper = uihelper;
        this.storageService = storageService;
        this.navCtrl = navCtrl;
        this.http = http;
        this.events = events;
        this.url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_10__["Constant"].apiUrl;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
        });
        this.options = { headers: this.headers };
    }
    ngOnInit() {
        this.router.events.subscribe((event) => {
            this.selfPath = '';
            if (event && event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__["NavigationEnd"] && event.url) {
                this.selfPath = event.url + '/dashboard';
            }
            this.selfPath = this.router.routerState.snapshot.url;
            this.selfPath = this.uihelper.getStringBeforeSubstring(this.selfPath, 'dashboard') + 'dashboard';
        });
        // this.cartCount = localStorage.getItem('cartCount');
    }
    ionViewWillEnter() {
        this.GetDetailsOfDashBoard();
        var logedinUserDetails;
        this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_10__["Constant"].AUTH).then(res => {
            logedinUserDetails = res.user_details;
            // this.events.sendMessage({
            //   userName: logedinUserDetails.full_name
            // });
            this.http.afterLoginGet("cart_count", res.user_details.id).subscribe((res) => {
                this.cartCount = res['payload'].cart_count;
                // console.log('cart count',this.cartCount)
            });
            if (!logedinUserDetails.user_attempt_month && !logedinUserDetails.user_attempt_year) {
                this.uihelper.ShowAlert('', 'We need few information before you get started. Please update.');
                this.navCtrl.navigateRoot(['edit-user-profile']);
            }
        });
    }
    goToPackages() {
        this.navCtrl.navigateRoot([this.selfPath + '/packages']);
    }
    GetDetailsOfDashBoard() {
        this.cartCount = localStorage.getItem('cartCount');
        this.uihelper.ShowSpinner();
        this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_10__["Constant"].AUTH).then(res => {
            this.userData = res.user_details;
            this.user_name = res.user_details.full_name;
            this.http.afterLoginGet("dashboard", res.user_details.id).subscribe((res) => {
                this.dashboard_data = res['payload'];
                // console.log('dashboard_data',this.dashboard_data)
                this.daily_motivational_text = this.dashboard_data.daily_motivational_text;
                this.total_avail_tests = this.dashboard_data.total_avail_tests;
                this.pendingTestResultCount = this.dashboard_data.pendingTestResultCount;
                this.totalCompletedTestCount = this.dashboard_data.totalCompletedTestCount;
                this.student_referal_code = this.dashboard_data.student_code;
                this.student_avail_amt = this.dashboard_data.student_avail_amt;
                this.totalTestsAverage = parseFloat("" + this.dashboard_data.totalTestsAverage.toFixed(1));
                this.testOverview = parseFloat("" + this.dashboard_data.testOverview.toFixed(1)) / 100;
                this.repliesToPost = this.dashboard_data.repliesToPost;
                // let rcodetring = JSON.stringify(this.student_referal_code);
                this.events.setCreditBalance({
                    student_avail_amt: this.student_avail_amt
                });
                // this.router.navigate([this.selfPath + '/refer-and-earn'],{queryParams:{rcodetring:rcodetring}})
                this.uihelper.HideSpinner();
            }, (err) => {
                console.log('err', err);
                this.uihelper.ShowAlert("", "Something went wrong");
            });
        });
    }
    GoToCart() {
        this.router.navigate([this.selfPath + "/cart"]);
    }
    GoToUserProfile() {
        this.router.navigate([this.selfPath + "/user-profile"]);
    }
    GoToFeaturedPackages() {
        this.router.navigate([this.selfPath + "/featured-packages"]);
    }
};
DashboardPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_11__["UIHelper"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_9__["StorageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_8__["HttpService"] },
    { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_7__["EventService"] }
];
DashboardPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-dashboard',
        template: _raw_loader_dashboard_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_dashboard_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], DashboardPage);



/***/ }),

/***/ "ea/W":
/*!*************************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: DashboardPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPageRoutingModule", function() { return DashboardPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _dashboard_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard.page */ "6ckw");




const routes = [
    {
        path: '',
        component: _dashboard_page__WEBPACK_IMPORTED_MODULE_3__["DashboardPage"]
    }
];
let DashboardPageRoutingModule = class DashboardPageRoutingModule {
};
DashboardPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DashboardPageRoutingModule);



/***/ }),

/***/ "spzo":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/dashboard.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>dashboard</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row style=\"background-image:url(../assets/dashboard.svg);\" class=\"top-bg\">\r\n    <ion-col size=\"12\" class=\"header-arrow padding-0\">\r\n      <div>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <div class=\"hamburger\">\r\n              <ion-menu-button auto-hide=\"false\">\r\n                <img src=\"assets/hamburger-icon.svg\" alt=\"hamburger\">\r\n              </ion-menu-button>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <div class=\"menu-icon\">\r\n              <span class=\"shopping-cart\" (click)=\"GoToCart()\"><img src=\"assets/shopping-cart.svg\" alt=\"shopping\"><span\r\n                  class=\"cart-number\">{{cartCount}}</span></span>\r\n              <span class=\"user-icon\" slot=\"start\" (click)=\"GoToUserProfile()\">\r\n                <img src=\"assets/user-profile.png\" alt=\"user-icon\">\r\n              </span>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"student-name\">\r\n        <h4>Hi <span style=\"text-transform: capitalize;\">{{user_name}}</span></h4>\r\n        <h3>Welcome To <span class=\"text-white\">MMT</span></h3>\r\n      </div>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"motivationl-text\">\r\n        <p>{{daily_motivational_text}}</p>\r\n      </div>\r\n      <!-- <div>\r\n        <ion-button class=\"credit-btn\">MMT Credit Rs. {{student_avail_amt}}</ion-button>\r\n      </div> -->\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-row class=\"categories\">\r\n        <ion-col size=\"7\" class=\"padding-0\">\r\n          <div class=\"categories-content\">\r\n            <h3>Be the best With\r\n              <span class=\"d-block\"></span>Make My Test\r\n            </h3>\r\n            <ion-button expand=\"block\" class=\"c-btn\" (click)=\"goToPackages()\">Packages\r\n              <ion-icon name=\"arrow-forward-outline\"></ion-icon>\r\n            </ion-button>\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"5\" class=\"padding-0\">\r\n          <div class=\"c-img\">\r\n            <img src=\"assets/categories.png\">\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n\r\n\r\n\r\n  <ion-row class=\"test-overview\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n\r\n      <ion-row class=\"courses-main avg-bg bg-purple\" (click)=\"GoToFeaturedPackages()\">\r\n        <ion-col size=\"3\" class=\"padding-0\">\r\n          <div class=\"c-main-img\">\r\n            <img src=\"assets/test-board-icon/packages-taken.svg\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"8\" class=\"padding-0\">\r\n          <div class=\"c-content\">\r\n            <h4>Featured Packages</h4>\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"1\" class=\"padding-0\">\r\n          <div class=\"featured-packages-icon\">\r\n            <span><ion-icon name=\"arrow-forward-outline\"></ion-icon></span>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n\r\n      <ion-row class=\"courses-main\">\r\n        <ion-col size=\"3\" class=\"padding-0\">\r\n          <div class=\"c-main-img\">\r\n            <img src=\"assets/ca-foundation.png\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"9\">\r\n          <div class=\"c-content\">\r\n            <h4>Test Overview</h4>\r\n            <p>Total Allocated Test: <span>{{total_avail_tests}}</span></p>\r\n            <ion-progress-bar class=\"progress-bar\" value=\"{{testOverview}}\"></ion-progress-bar>\r\n            <ion-row>\r\n              <ion-col size=\"6\" class=\"completed-test\">\r\n                <p>Completed: <span>{{totalCompletedTestCount}}</span></p>\r\n              </ion-col>\r\n              <ion-col size=\"6\" class=\"due-test\">\r\n                <p>Due: <span>{{pendingTestResultCount}}</span></p>\r\n              </ion-col>\r\n            </ion-row>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"test-result-pending\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-row class=\"courses-main ca-inter-bg\">\r\n        <ion-col size=\"3\" class=\"padding-0\">\r\n          <div class=\"c-main-img\">\r\n            <img src=\"assets/ca-inter.png\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"8\" class=\"padding-0\">\r\n          <div class=\"c-content\">\r\n            <h4>Test Result Pending</h4>\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"1\" class=\"padding-0\">\r\n          <div class=\"c-count\">\r\n            <span>{{pendingTestResultCount}}</span>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"average-test-scores\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-row class=\"courses-main avg-bg\">\r\n        <ion-col size=\"3\" class=\"padding-0\">\r\n          <div class=\"c-main-img\">\r\n            <img src=\"assets/ca-final.png\">\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"8\" class=\"padding-0\">\r\n          <div class=\"c-content\">\r\n            <h4>Average Test Scores</h4>\r\n          </div>\r\n        </ion-col>\r\n        <ion-col size=\"1\" class=\"padding-0\">\r\n          <div class=\"avg-c-count\">\r\n            <span>{{totalTestsAverage}}%</span>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n\r\n\r\n</ion-content>");

/***/ }),

/***/ "ws6Y":
/*!*****************************************************!*\
  !*** ./src/app/pages/dashboard/dashboard.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.hamburger img {\n  width: 40px;\n}\n\n.hamburger {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.categories {\n  background: #fef3f3;\n  padding: 20px;\n  border-radius: 30px;\n  margin: 15px 20px 0;\n}\n\n.d-block {\n  display: block;\n}\n\n.categories-content h3 {\n  margin-top: 0px;\n}\n\nion-button {\n  --background-activated: #3f5c9b !important;\n  --background-focused: #3f5c9b !important;\n  --background-hover: #3f5c9b !important;\n  --background: #3f5c9b !important;\n  border-radius: 10px;\n  font-size: 14px;\n  font-family: \"Lato\";\n  font-weight: 500;\n  width: 150px;\n}\n\n.c-btn ion-icon {\n  position: absolute;\n  font-size: 16px;\n  right: 0px;\n}\n\n.categories-content h3 {\n  color: #305f72;\n  font-size: 22px;\n  font-weight: 600;\n  font-family: \"Lato\", sans-serif;\n}\n\n.categories-heading h3 {\n  font-size: 20px;\n  color: #305f72;\n  font-weight: 700;\n  font-family: \"Lato\", sans-serif;\n  margin-top: 36px;\n}\n\n.test-overview {\n  margin: 75px 20px 0;\n}\n\n.student-name {\n  padding: 25px 20px;\n}\n\n.student-name h4 {\n  font-size: 26px;\n  font-weight: 500;\n  font-family: \"Lato\", sans-serif;\n  color: #fff;\n  margin: 0;\n  margin-bottom: 5px;\n}\n\n.student-name h3 {\n  font-size: 26px;\n  font-weight: 700;\n  font-family: \"Lato\", sans-serif;\n  color: #305f72;\n  margin: 0;\n}\n\n.text-white {\n  color: #fff;\n}\n\n.courses-main {\n  background: #fae4f4;\n  padding: 10px 15px 6px;\n  border-radius: 25px;\n  align-items: center;\n  margin-bottom: 10px;\n}\n\n.c-content h4 {\n  font-size: 18px;\n  color: #305f72;\n  margin: 0px 0px 5px;\n}\n\n.c-content h4 span {\n  font-weight: 800;\n  font-size: 20px;\n}\n\n.c-content p {\n  font-size: 14px;\n  color: #f27376;\n  margin: 0;\n}\n\n.c-content {\n  padding-left: 8px;\n}\n\n.nxt-arrow {\n  background: #fff;\n  border-radius: 50%;\n  padding: 11px;\n}\n\nion-icon.nxt-arrow {\n  font-size: 14px;\n  color: #f27376;\n}\n\n.c-count span {\n  float: left;\n  font-size: 18px;\n  margin-top: 13px;\n  margin-right: 3px;\n  color: #ea9e24;\n  font-weight: 500;\n  text-align: center;\n  margin: 0 auto;\n}\n\n.avg-c-count span {\n  float: left;\n  font-size: 16px;\n  margin-top: 13px;\n  margin-right: 3px;\n  color: #9A00FF;\n  font-weight: 500;\n  text-align: center;\n  margin: 0 auto;\n  position: relative;\n  left: -5px;\n}\n\n.ca-inter-bg {\n  background: #fff5e5;\n}\n\n.ca-final-bg {\n  background: #fbf5ff;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 385px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n}\n\n.float-right {\n  float: right;\n}\n\n.clear {\n  clear: both;\n}\n\n.header-arrow {\n  padding: 20px 15px;\n}\n\n.c-img {\n  position: absolute;\n  bottom: 0;\n  right: -35px;\n}\n\n.motivationl-text {\n  padding: 10px;\n  background-color: #ffffff;\n  margin: 0 20px;\n  border-radius: 15px;\n  max-height: 87px;\n  min-height: 70px;\n}\n\n.motivationl-text p {\n  margin: 0;\n  font-size: 13px;\n  text-align: center;\n  color: #274a58;\n  letter-spacing: 0.3px;\n  font-weight: 500;\n  line-height: 17px;\n}\n\n.progress-bar {\n  height: 12px;\n  border-radius: 10px;\n  margin: 5px 0;\n}\n\n:host.progress {\n  background: #3f5c9b !important;\n  z-index: 2;\n}\n\n.c-content ion-progress-bar {\n  --buffer-background: #bab6d6;\n  --progress-background: #3f5c9b;\n}\n\n.completed-test {\n  padding: 0;\n}\n\n.due-test {\n  padding: 0;\n}\n\n.completed-test p {\n  font-size: 14px;\n  color: #1f1f1f;\n  margin: 0;\n  font-weight: 500;\n}\n\n.due-test p {\n  font-size: 14px;\n  color: #1f1f1f;\n  margin: 0;\n  font-weight: 500;\n  text-align: right;\n}\n\n.c-main-img img {\n  width: 100%;\n  border-radius: 11px;\n}\n\n.test-result-pending {\n  margin: 0px 20px 0;\n}\n\n.average-test-scores {\n  margin: 0px 20px 0;\n}\n\n.avg-bg {\n  background-color: #FBF5FF;\n}\n\n.package-taken {\n  border-radius: 15px !important;\n  padding: 12px;\n  text-align: center;\n  height: 90px;\n  color: #fff;\n  margin: 10px 15px 0;\n}\n\n.package-taken h6 {\n  font-size: 16px;\n  color: #305F72;\n  text-align: left;\n  padding-top: 20px;\n}\n\n.package-taken span {\n  font-size: 17px;\n  font-weight: 500;\n  color: #305F72;\n  border-radius: 50%;\n  position: relative;\n  top: 20px;\n}\n\n.package-taken img {\n  width: 58px;\n  height: 58px;\n  float: left;\n}\n\n.bg-purple {\n  background-color: #DEE8FF;\n}\n\n.featured-packages-icon span {\n  float: left;\n  font-size: 28px;\n  margin-top: 13px;\n  margin-right: 3px;\n  color: #3F5C9B;\n  font-weight: 500;\n  text-align: center;\n  margin: 0 auto;\n  position: relative;\n  left: -5px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxkYXNoYm9hcmQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7QUFDZjs7QUFDQTtFQUNJLFdBQVc7QUFFZjs7QUFBQTtFQUNJLFdBQVc7QUFHZjs7QUFEQTtFQUNJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFNBQVM7QUFJYjs7QUFGQTtFQUNJLGlCQUFpQjtBQUtyQjs7QUFIQTtFQUNJLGtCQUFrQjtBQU10Qjs7QUFKQTtFQUNJLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGVBQWU7RUFDZixZQUFZO0FBT2hCOztBQUxBO0VBQ0ksYUFBYTtBQVFqQjs7QUFOQTtFQUNJLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLG1CQUFtQjtBQVN2Qjs7QUFQQTtFQUNJLGNBQWM7QUFVbEI7O0FBUkE7RUFDSSxlQUFlO0FBV25COztBQVRBO0VBQ0ksMENBQXVCO0VBQ3ZCLHdDQUFxQjtFQUNyQixzQ0FBbUI7RUFDbkIsZ0NBQWE7RUFDYixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsWUFBWTtBQVloQjs7QUFWQTtFQUNJLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsVUFBVTtBQWFkOztBQVhBO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsK0JBQStCO0FBY25DOztBQVpBO0VBQ0ksZUFBZTtFQUNmLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsK0JBQStCO0VBQy9CLGdCQUFnQjtBQWVwQjs7QUFiQTtFQUNJLG1CQUFtQjtBQWdCdkI7O0FBZEE7RUFDSSxrQkFBa0I7QUFpQnRCOztBQWZBO0VBQ0ksZUFBZTtFQUNmLGdCQUFnQjtFQUNoQiwrQkFBK0I7RUFDL0IsV0FBVztFQUNYLFNBQVM7RUFDVCxrQkFBa0I7QUFrQnRCOztBQWhCQTtFQUNJLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsK0JBQStCO0VBQy9CLGNBQWM7RUFDZCxTQUFTO0FBbUJiOztBQWpCQTtFQUNJLFdBQVc7QUFvQmY7O0FBbEJBO0VBQ0ksbUJBQW1CO0VBQ25CLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLG1CQUFtQjtBQXFCdkI7O0FBbkJBO0VBQ0ksZUFBZTtFQUNmLGNBQWM7RUFDZCxtQkFBbUI7QUFzQnZCOztBQXBCQTtFQUNJLGdCQUFnQjtFQUNoQixlQUFlO0FBdUJuQjs7QUFyQkE7RUFDSSxlQUFlO0VBQ2YsY0FBYztFQUNkLFNBQVM7QUF3QmI7O0FBdEJBO0VBQ0ksaUJBQWlCO0FBeUJyQjs7QUF2QkE7RUFDSSxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGFBQWE7QUEwQmpCOztBQXhCQTtFQUNJLGVBQWU7RUFDZixjQUFjO0FBMkJsQjs7QUF6QkE7RUFDSSxXQUFXO0VBQ1gsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsY0FBYztBQTRCbEI7O0FBMUJBO0VBQ0ksV0FBVztFQUNYLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsVUFBVTtBQTZCZDs7QUEzQkE7RUFDSSxtQkFBbUI7QUE4QnZCOztBQTVCQTtFQUNJLG1CQUFtQjtBQStCdkI7O0FBN0JBO0VBQ0ksNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QixhQUFhO0VBQ2IsV0FBVztFQUNYLDJCQUEyQjtFQUMzQixrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLDRCQUE0QjtBQWdDaEM7O0FBOUJBO0VBQ0ksWUFBWTtBQWlDaEI7O0FBL0JBO0VBQ0ksV0FBVztBQWtDZjs7QUFoQ0E7RUFDSSxrQkFBa0I7QUFtQ3RCOztBQWhDQTtFQUNJLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsWUFBWTtBQW1DaEI7O0FBakNBO0VBQ0ksYUFBYTtFQUNiLHlCQUF5QjtFQUN6QixjQUFjO0VBQ2QsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixnQkFBZ0I7QUFvQ3BCOztBQWxDQTtFQUNJLFNBQVM7RUFDVCxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxxQkFBcUI7RUFDckIsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtBQXFDckI7O0FBbkNBO0VBQ0ksWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixhQUFhO0FBc0NqQjs7QUFwQ0E7RUFDSSw4QkFBOEI7RUFDOUIsVUFBVTtBQXVDZDs7QUFyQ0E7RUFFUSw0QkFBb0I7RUFDcEIsOEJBQXNCO0FBdUM5Qjs7QUFwQ0E7RUFDSSxVQUFVO0FBdUNkOztBQXJDQTtFQUNJLFVBQVU7QUF3Q2Q7O0FBdENBO0VBQ0ksZUFBZTtFQUNmLGNBQWM7RUFDZCxTQUFTO0VBQ1QsZ0JBQWdCO0FBeUNwQjs7QUF2Q0E7RUFDSSxlQUFlO0VBQ2YsY0FBYztFQUNkLFNBQVM7RUFDVCxnQkFBZ0I7RUFDaEIsaUJBQWlCO0FBMENyQjs7QUF4Q0E7RUFDSSxXQUFXO0VBQ1gsbUJBQW1CO0FBMkN2Qjs7QUF6Q0E7RUFDSSxrQkFBa0I7QUE0Q3RCOztBQTFDQTtFQUNJLGtCQUFrQjtBQTZDdEI7O0FBM0NBO0VBQ0kseUJBQXlCO0FBOEM3Qjs7QUE1Q0E7RUFDSSw4QkFBOEI7RUFDOUIsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osV0FBVztFQUNYLG1CQUFtQjtBQStDdkI7O0FBN0NBO0VBQ0ksZUFBZTtFQUNmLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsaUJBQWlCO0FBZ0RyQjs7QUE3Q0E7RUFDSSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFNBQVM7QUFnRGI7O0FBOUNBO0VBQ0ksV0FBVztFQUNYLFlBQVk7RUFDWixXQUFXO0FBaURmOztBQS9DQTtFQUNJLHlCQUF5QjtBQWtEN0I7O0FBaERBO0VBQ0ksV0FBVztFQUNYLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsVUFBVTtBQW1EZCIsImZpbGUiOiJkYXNoYm9hcmQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnVzZXItaWNvbiBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgd2lkdGg6IDQwcHg7XHJcbn1cclxuLmhhbWJ1cmdlciBpbWcge1xyXG4gICAgd2lkdGg6IDQwcHg7XHJcbn1cclxuLmhhbWJ1cmdlciB7XHJcbiAgICBmbG9hdDogbGVmdDtcclxufVxyXG4uc2hvcHBpbmctY2FydCBpbWcge1xyXG4gICAgd2lkdGg6IDMwcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IC04cHg7XHJcbn1cclxuLm1lbnUtaWNvbiB7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG4uc2hvcHBpbmctY2FydCB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuLmNhcnQtbnVtYmVyIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAxNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmYmIwMDtcclxuICAgIGhlaWdodDogMjBweDtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGNvbG9yOiAjM2Y1YzliO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgcGFkZGluZzogMXB4O1xyXG59XHJcbmlvbi1oZWFkZXIge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG4uY2F0ZWdvcmllcyB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmVmM2YzO1xyXG4gICAgcGFkZGluZzogMjBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgICBtYXJnaW46IDE1cHggMjBweCAwO1xyXG59XHJcbi5kLWJsb2NrIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcbi5jYXRlZ29yaWVzLWNvbnRlbnQgaDMge1xyXG4gICAgbWFyZ2luLXRvcDogMHB4O1xyXG59XHJcbmlvbi1idXR0b24ge1xyXG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYmFja2dyb3VuZC1ob3ZlcjogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgZm9udC1mYW1pbHk6IFwiTGF0b1wiO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIHdpZHRoOiAxNTBweDtcclxufVxyXG4uYy1idG4gaW9uLWljb24ge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgcmlnaHQ6IDBweDtcclxufVxyXG4uY2F0ZWdvcmllcy1jb250ZW50IGgzIHtcclxuICAgIGNvbG9yOiAjMzA1ZjcyO1xyXG4gICAgZm9udC1zaXplOiAyMnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGZvbnQtZmFtaWx5OiBcIkxhdG9cIiwgc2Fucy1zZXJpZjtcclxufVxyXG4uY2F0ZWdvcmllcy1oZWFkaW5nIGgzIHtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGNvbG9yOiAjMzA1ZjcyO1xyXG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgIGZvbnQtZmFtaWx5OiBcIkxhdG9cIiwgc2Fucy1zZXJpZjtcclxuICAgIG1hcmdpbi10b3A6IDM2cHg7XHJcbn1cclxuLnRlc3Qtb3ZlcnZpZXd7XHJcbiAgICBtYXJnaW46IDc1cHggMjBweCAwO1xyXG59XHJcbi5zdHVkZW50LW5hbWUge1xyXG4gICAgcGFkZGluZzogMjVweCAyMHB4O1xyXG59XHJcbi5zdHVkZW50LW5hbWUgaDQge1xyXG4gICAgZm9udC1zaXplOiAyNnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGZvbnQtZmFtaWx5OiBcIkxhdG9cIiwgc2Fucy1zZXJpZjtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG59XHJcbi5zdHVkZW50LW5hbWUgaDMge1xyXG4gICAgZm9udC1zaXplOiAyNnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgIGZvbnQtZmFtaWx5OiBcIkxhdG9cIiwgc2Fucy1zZXJpZjtcclxuICAgIGNvbG9yOiAjMzA1ZjcyO1xyXG4gICAgbWFyZ2luOiAwO1xyXG59XHJcbi50ZXh0LXdoaXRlIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG59XHJcbi5jb3Vyc2VzLW1haW4ge1xyXG4gICAgYmFja2dyb3VuZDogI2ZhZTRmNDtcclxuICAgIHBhZGRpbmc6IDEwcHggMTVweCA2cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbn1cclxuLmMtY29udGVudCBoNCB7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBjb2xvcjogIzMwNWY3MjtcclxuICAgIG1hcmdpbjogMHB4IDBweCA1cHg7XHJcbn1cclxuLmMtY29udGVudCBoNCBzcGFuIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbn1cclxuLmMtY29udGVudCBwIHtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIGNvbG9yOiAjZjI3Mzc2O1xyXG4gICAgbWFyZ2luOiAwO1xyXG59XHJcbi5jLWNvbnRlbnR7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDhweDtcclxufVxyXG4ubnh0LWFycm93IHtcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBwYWRkaW5nOiAxMXB4O1xyXG59XHJcbmlvbi1pY29uLm54dC1hcnJvdyB7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBjb2xvcjogI2YyNzM3NjtcclxufVxyXG4uYy1jb3VudCBzcGFuIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTNweDtcclxuICAgIG1hcmdpbi1yaWdodDogM3B4O1xyXG4gICAgY29sb3I6ICNlYTllMjQ7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbn1cclxuLmF2Zy1jLWNvdW50IHNwYW57XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIG1hcmdpbi10b3A6IDEzcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDNweDtcclxuICAgIGNvbG9yOiAjOUEwMEZGO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbGVmdDogLTVweDtcclxufVxyXG4uY2EtaW50ZXItYmcge1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjVlNTtcclxufVxyXG4uY2EtZmluYWwtYmcge1xyXG4gICAgYmFja2dyb3VuZDogI2ZiZjVmZjtcclxufVxyXG4udG9wLWJnIHtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgaGVpZ2h0OiAzODVweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMCAwIDMwcHggMzBweDtcclxufVxyXG4uZmxvYXQtcmlnaHQge1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcbi5jbGVhciB7XHJcbiAgICBjbGVhcjogYm90aDtcclxufVxyXG4uaGVhZGVyLWFycm93IHtcclxuICAgIHBhZGRpbmc6IDIwcHggMTVweDtcclxufVxyXG5cclxuLmMtaW1nIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIHJpZ2h0OiAtMzVweDtcclxufVxyXG4ubW90aXZhdGlvbmwtdGV4dCB7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIG1hcmdpbjogMCAyMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgIG1heC1oZWlnaHQ6IDg3cHg7XHJcbiAgICBtaW4taGVpZ2h0OiA3MHB4O1xyXG59XHJcbi5tb3RpdmF0aW9ubC10ZXh0IHAge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgY29sb3I6ICMyNzRhNTg7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogMC4zcHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgbGluZS1oZWlnaHQ6IDE3cHg7XHJcbn1cclxuLnByb2dyZXNzLWJhciB7XHJcbiAgICBoZWlnaHQ6IDEycHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgbWFyZ2luOiA1cHggMDtcclxufVxyXG46aG9zdC5wcm9ncmVzcyB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICB6LWluZGV4OiAyO1xyXG59XHJcbi5jLWNvbnRlbnQge1xyXG4gICAgaW9uLXByb2dyZXNzLWJhciB7XHJcbiAgICAgICAgLS1idWZmZXItYmFja2dyb3VuZDogI2JhYjZkNjtcclxuICAgICAgICAtLXByb2dyZXNzLWJhY2tncm91bmQ6ICMzZjVjOWI7XHJcbiAgICB9XHJcbn1cclxuLmNvbXBsZXRlZC10ZXN0e1xyXG4gICAgcGFkZGluZzogMDtcclxufVxyXG4uZHVlLXRlc3R7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG59XHJcbi5jb21wbGV0ZWQtdGVzdCBwe1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgY29sb3I6ICMxZjFmMWY7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG59XHJcbi5kdWUtdGVzdCBwe1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgY29sb3I6ICMxZjFmMWY7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuLmMtbWFpbi1pbWcgaW1ne1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMXB4O1xyXG59XHJcbi50ZXN0LXJlc3VsdC1wZW5kaW5ne1xyXG4gICAgbWFyZ2luOiAwcHggMjBweCAwO1xyXG59XHJcbi5hdmVyYWdlLXRlc3Qtc2NvcmVze1xyXG4gICAgbWFyZ2luOiAwcHggMjBweCAwO1xyXG59XHJcbi5hdmctYmd7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkJGNUZGO1xyXG59XHJcbi5wYWNrYWdlLXRha2Vue1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTVweCAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZzogMTJweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGhlaWdodDogOTBweDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgbWFyZ2luOiAxMHB4IDE1cHggMDtcclxufVxyXG4ucGFja2FnZS10YWtlbiBoNntcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIGNvbG9yOiAjMzA1RjcyO1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgIHBhZGRpbmctdG9wOiAyMHB4O1xyXG59XHJcblxyXG4ucGFja2FnZS10YWtlbiBzcGFuIHtcclxuICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogMjBweDtcclxufVxyXG4ucGFja2FnZS10YWtlbiBpbWd7XHJcbiAgICB3aWR0aDogNThweDtcclxuICAgIGhlaWdodDogNThweDtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcbi5iZy1wdXJwbGV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjREVFOEZGO1xyXG59XHJcbi5mZWF0dXJlZC1wYWNrYWdlcy1pY29uIHNwYW57XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIGZvbnQtc2l6ZTogMjhweDtcclxuICAgIG1hcmdpbi10b3A6IDEzcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDNweDtcclxuICAgIGNvbG9yOiAjM0Y1QzlCO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbGVmdDogLTVweDtcclxufSJdfQ== */");

/***/ })

}]);
//# sourceMappingURL=dashboard-dashboard-module.js.map