(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["new-test-new-test-module"],{

/***/ "1IZ5":
/*!***************************************************!*\
  !*** ./src/app/pages/new-test/new-test.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.back img {\n  width: 40px;\n}\n\n.back {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.header-arrow {\n  padding: 20px 15px;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 160px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n}\n\n.packages-header h2 {\n  margin: 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n}\n\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\nion-label {\n  margin: 0;\n  font-size: 16px;\n  font-weight: 500;\n  text-transform: capitalize;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n}\n\n.space-right-left {\n  margin: 0 5px;\n}\n\n.title-text {\n  color: #2a2a2a;\n  font-size: 15px;\n  font-weight: 500;\n  padding-left: 8px;\n  margin-bottom: 16px;\n}\n\n.form-cantrol {\n  border: 1px solid #b1bcd4;\n  padding-left: 15px !important;\n  margin-bottom: 25px !important;\n  padding: 3px 12px !important;\n  font-size: 15px !important;\n  margin: 0 25px;\n  border-radius: 8px;\n  --color: rgb(44, 44, 44);\n  font-weight: 400;\n}\n\n.test-ttl h5 {\n  font-size: 17px;\n  margin-bottom: 25px;\n  color: #305F72;\n  font-weight: 500;\n}\n\n.test-ttl p {\n  font-size: 16px;\n}\n\n.start-exam-btn {\n  padding: 12px 0;\n  color: #ffffff;\n  font-weight: 400;\n  font-size: 15px;\n  border-radius: 8px !important;\n  box-shadow: none !important;\n  background: #3f5c9b;\n  width: 100%;\n}\n\n.cancel-btn {\n  padding: 12px 0;\n  color: #2d363a;\n  font-weight: 400;\n  font-size: 15px;\n  border-radius: 8px !important;\n  box-shadow: none !important;\n  background: #d7d7d7;\n  width: 100%;\n}\n\n.m-30 {\n  margin: 0 30px;\n}\n\n.m-10 {\n  margin: 0 10px;\n}\n\n.test-img img {\n  height: 130px;\n  margin-top: 20px;\n  border-radius: 5px;\n  width: 100%;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxuZXctdGVzdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztBQUNmOztBQUNBO0VBQ0ksV0FBVztBQUVmOztBQUFBO0VBQ0ksV0FBVztBQUdmOztBQURBO0VBQ0ksV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsU0FBUztBQUliOztBQUZBO0VBQ0ksaUJBQWlCO0FBS3JCOztBQUhBO0VBQ0ksa0JBQWtCO0FBTXRCOztBQUpBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIsWUFBWTtFQUNaLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZUFBZTtFQUNmLFlBQVk7QUFPaEI7O0FBTEE7RUFDSSxhQUFhO0FBUWpCOztBQU5BO0VBQ0ksa0JBQWtCO0FBU3RCOztBQVBBO0VBQ0ksNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QixhQUFhO0VBQ2IsV0FBVztFQUNYLDJCQUEyQjtFQUMzQixrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLDRCQUE0QjtBQVVoQzs7QUFSQTtFQUNJLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxrQkFBa0I7QUFXdEI7O0FBVEE7RUFDSSxTQUFTO0VBQ1Qsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixjQUFjO0VBQ2QsY0FBYztFQUNkLGdCQUFnQjtBQVlwQjs7QUFWQTtFQUNJLFNBQVM7RUFDVCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLDBCQUEwQjtBQWE5Qjs7QUFYQTtFQUNJLG9CQUFnQjtFQUNoQix3QkFBb0I7QUFjeEI7O0FBWkE7RUFDSSxhQUFZO0FBZWhCOztBQWJBO0VBQ0ksY0FBYTtFQUNiLGVBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLG1CQUFrQjtBQWdCdEI7O0FBYkE7RUFDSSx5QkFBeUI7RUFDekIsNkJBQTZCO0VBQzdCLDhCQUE4QjtFQUM5Qiw0QkFBNEI7RUFDNUIsMEJBQTBCO0VBQzFCLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsd0JBQVE7RUFDUixnQkFBZ0I7QUFnQnBCOztBQWRBO0VBQ0ksZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsZ0JBQWdCO0FBaUJwQjs7QUFmQTtFQUNJLGVBQWU7QUFrQm5COztBQWhCQTtFQUNJLGVBQWU7RUFDZixjQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZiw2QkFBNEI7RUFDNUIsMkJBQXlCO0VBQ3pCLG1CQUFvQjtFQUNwQixXQUFXO0FBbUJmOztBQWpCQTtFQUNJLGVBQWU7RUFDZixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZiw2QkFBNkI7RUFDN0IsMkJBQTJCO0VBQzNCLG1CQUFtQjtFQUNuQixXQUFXO0FBb0JmOztBQWxCQTtFQUNJLGNBQWE7QUFxQmpCOztBQW5CQTtFQUNJLGNBQWE7QUFzQmpCOztBQXBCQTtFQUNJLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFdBQVc7QUF1QmYiLCJmaWxlIjoibmV3LXRlc3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnVzZXItaWNvbiBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgd2lkdGg6IDQwcHg7XHJcbn1cclxuLmJhY2sgaW1nIHtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG59XHJcbi5iYWNrIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IGltZyB7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLThweDtcclxufVxyXG4ubWVudS1pY29uIHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4uY2FydC1udW1iZXIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDE1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZiYjAwO1xyXG4gICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICMzZjVjOWI7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBwYWRkaW5nOiAxcHg7XHJcbn1cclxuaW9uLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcbi5oZWFkZXItYXJyb3cge1xyXG4gICAgcGFkZGluZzogMjBweCAxNXB4O1xyXG59XHJcbi50b3AtYmcge1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBoZWlnaHQ6IDE2MHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMzBweCAzMHB4O1xyXG59XHJcbi5wYWNrYWdlcy1oZWFkZXIgaDJ7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxufVxyXG4ucGFja2FnZXMtaGVhZGVyIHNwYW57XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG5pb24tbGFiZWwge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG59XHJcbmlvbi1pdGVtIHtcclxuICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xyXG4gICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xyXG59XHJcbi5zcGFjZS1yaWdodC1sZWZ0e1xyXG4gICAgbWFyZ2luOjAgNXB4O1xyXG59XHJcbi50aXRsZS10ZXh0e1xyXG4gICAgY29sb3I6IzJhMmEyYTtcclxuICAgIGZvbnQtc2l6ZToxNXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIHBhZGRpbmctbGVmdDogOHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbToxNnB4O1xyXG59XHJcblxyXG4uZm9ybS1jYW50cm9se1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2IxYmNkNDtcclxuICAgIHBhZGRpbmctbGVmdDogMTVweCAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjVweCAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZzogM3B4IDEycHggIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTogMTVweCAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luOiAwIDI1cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAtLWNvbG9yOiByZ2IoNDQsIDQ0LCA0NCk7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcbi50ZXN0LXR0bCBoNXtcclxuICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDI1cHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuLnRlc3QtdHRsIHB7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbn1cclxuLnN0YXJ0LWV4YW0tYnRue1xyXG4gICAgcGFkZGluZzogMTJweCAwO1xyXG4gICAgY29sb3I6I2ZmZmZmZjtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHghaW1wb3J0YW50O1xyXG4gICAgYm94LXNoYWRvdzpub25lIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQ6ICMzZjVjOWIgO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLmNhbmNlbC1idG57XHJcbiAgICBwYWRkaW5nOiAxMnB4IDA7XHJcbiAgICBjb2xvcjogIzJkMzYzYTtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHggIWltcG9ydGFudDtcclxuICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQ6ICNkN2Q3ZDc7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG4ubS0zMHtcclxuICAgIG1hcmdpbjowIDMwcHg7XHJcbn1cclxuLm0tMTB7XHJcbiAgICBtYXJnaW46MCAxMHB4O1xyXG59XHJcbi50ZXN0LWltZyBpbWd7XHJcbiAgICBoZWlnaHQ6IDEzMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59Il19 */");

/***/ }),

/***/ "5LVo":
/*!*************************************************!*\
  !*** ./src/app/pages/new-test/new-test.page.ts ***!
  \*************************************************/
/*! exports provided: NewTestPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewTestPage", function() { return NewTestPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_new_test_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./new-test.page.html */ "lLCl");
/* harmony import */ var _new_test_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./new-test.page.scss */ "1IZ5");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");












let NewTestPage = class NewTestPage {
    constructor(activatedRoute, router, http, httpService, storageService, loadingController, authService, navCtrl, uihelper) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.http = http;
        this.httpService = httpService;
        this.storageService = storageService;
        this.loadingController = loadingController;
        this.authService = authService;
        this.navCtrl = navCtrl;
        this.uihelper = uihelper;
        this.postData = {
            target_marks: ''
        };
        this.url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_10__["Constant"].apiUrl;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
        });
        this.options = { headers: this.headers };
        this.activatedRoute.queryParams.subscribe((data) => {
            this.test_id = data.test_id;
            this.total_marks = data.total_marks;
            console.log('test id', this.test_id);
        });
    }
    ngOnInit() {
        try {
            this.router.events.subscribe((event) => {
                this.selfPath = '';
                if (event && event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationEnd"] && event.url) {
                    this.selfPath = event.url + '/test-board';
                }
                this.selfPath = this.router.routerState.snapshot.url;
                // console.log(this.getStringBeforeSubstring(this.selfPath, 'viptips-details'));
                this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'test-board') + 'test-board';
            });
        }
        catch (e) {
            console.log(e);
        }
    }
    getStringBeforeSubstring(parentString, substring) {
        return parentString.substring(0, parentString.indexOf(substring));
    }
    ionViewWillEnter() {
        this.cartCount = localStorage.getItem('cartCount');
        this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_10__["Constant"].AUTH).then(res => {
            this.userData = res.user_details;
            this.user_id = res.user_details.id;
            this.institute_id = res.user_details.institute_id;
            this.httpService.afterLoginGet("cart_count", res.user_details.id).subscribe((res) => {
                this.cartCount = res['payload'].cart_count;
            });
        });
    }
    validateInputs() {
        let target_marks = this.postData.target_marks;
        return (target_marks);
    }
    startTest() {
        // console.log('total_marks',this.total_marks)
        if (this.postData.target_marks > this.total_marks) {
            this.uihelper.ShowAlert('', 'Target marks should not be greater than total marks.');
            return false;
        }
        if (this.validateInputs()) {
            let navigationExtras = {
                queryParams: {
                    target_marks: this.postData.target_marks,
                    test_id: this.test_id,
                }
            };
            this.navCtrl.navigateForward([this.selfPath + '/start-test'], navigationExtras);
        }
        else {
            // console.log('use toast validation here');
            this.uihelper.ShowAlert('', 'Please enter Target marks.');
        }
    }
    getHome() {
        this.navCtrl.pop();
    }
    GoToCart() {
        this.router.navigate([this.selfPath + "/cart"]);
    }
};
NewTestPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_5__["HttpService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_7__["StorageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] },
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_9__["AuthService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_11__["UIHelper"] }
];
NewTestPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-new-test',
        template: _raw_loader_new_test_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_new_test_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], NewTestPage);



/***/ }),

/***/ "CLqI":
/*!***********************************************************!*\
  !*** ./src/app/pages/new-test/new-test-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: NewTestPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewTestPageRoutingModule", function() { return NewTestPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _new_test_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./new-test.page */ "5LVo");




const routes = [
    {
        path: '',
        component: _new_test_page__WEBPACK_IMPORTED_MODULE_3__["NewTestPage"]
    }
];
let NewTestPageRoutingModule = class NewTestPageRoutingModule {
};
NewTestPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NewTestPageRoutingModule);



/***/ }),

/***/ "lLCl":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/new-test/new-test.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button (click)=\"getHome()\"></ion-back-button>\r\n    </ion-buttons>\r\n    <!-- <ion-buttons slot=\"primary\">\r\n      <ion-button>\r\n        <ul class=\"shopping-icon\">\r\n          <li (click)=\"GoToCart()\">\r\n            <img src=\"assets/icon/my-cart-1.png\" class=\"cart\">\r\n            <span class=\"cart_count\">{{cartCount ? cartCount : '0'}}</span>\r\n          </li>\r\n          <li routerLink=\"/user-profile\">\r\n            <img src=\"assets/icon/user.png\" >\r\n          </li>\r\n        </ul>\r\n      </ion-button>\r\n    </ion-buttons> -->\r\n    <ion-title class=\"ios title-default hydrated\">New Test</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n    <ion-col size=\"12\" class=\"header-arrow padding-0\">\r\n      <div>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <div class=\"back\" (click)=\"getHome()\">\r\n                <img src=\"assets/back.svg\" alt=\"back\">\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <div class=\"menu-icon\">\r\n              <span class=\"shopping-cart\"><img src=\"assets/shopping-cart.svg\" alt=\"shopping\"><span\r\n                  class=\"cart-number\">{{cartCount}}</span></span>\r\n              <span class=\"user-icon\" slot=\"start\">\r\n                <img src=\"assets/user-profile.png\" alt=\"user-icon\">\r\n              </span>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <div class=\"packages-header\">\r\n              <h2>New Test</h2>\r\n              <!-- <span>My Test History</span> -->\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <!-- <ion-row class=\"space-right-left\">\r\n    <ion-col class=\"padding-0\">\r\n      <h4 class=\"title-text\">Step 1 - Confirm to Start Test</h4>\r\n    </ion-col>\r\n  </ion-row>-->\r\n  <ion-row class=\"m-10\">\r\n    <ion-col class=\"text-center padding-0\">\r\n      <div class=\"test-img \">\r\n        <img src=\"assets/icon/take-test.png\">\r\n      </div>\r\n    </ion-col>\r\n  </ion-row> \r\n  <form #startTestForm=\"ngForm\" (ngSubmit)=\"startTest()\" >\r\n  <ion-row>\r\n    \r\n   <ion-col class=\"padding-0 text-center test-ttl\">\r\n      <h5>Enter your Target Marks <br>for this test</h5>\r\n      <p>Total Marks - {{total_marks}}</p>\r\n      <ion-list>\r\n        <ion-item lines=\"none\">\r\n          <ion-input type=\"number\" class=\"form-cantrol\" [(ngModel)]=\"postData.target_marks\" name=\"target_marks\" required></ion-input>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"m-30\">\r\n    <ion-col size=\"6\" class=\"text-center\">\r\n      <button ion-button full class=\"cancel-btn\" (click)=\"getHome()\" type=\"button\">Cancel</button>\r\n    </ion-col>\r\n    <ion-col size=\"6\" class=\"text-center\">\r\n      <button ion-button full class=\"start-exam-btn\" type=\"submit\">Start Exam</button>\r\n    </ion-col>\r\n  </ion-row>\r\n  \r\n</form>\r\n</ion-content>\r\n");

/***/ }),

/***/ "q7J+":
/*!***************************************************!*\
  !*** ./src/app/pages/new-test/new-test.module.ts ***!
  \***************************************************/
/*! exports provided: NewTestPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewTestPageModule", function() { return NewTestPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _new_test_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./new-test-routing.module */ "CLqI");
/* harmony import */ var _new_test_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./new-test.page */ "5LVo");







let NewTestPageModule = class NewTestPageModule {
};
NewTestPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _new_test_routing_module__WEBPACK_IMPORTED_MODULE_5__["NewTestPageRoutingModule"]
        ],
        declarations: [_new_test_page__WEBPACK_IMPORTED_MODULE_6__["NewTestPage"]]
    })
], NewTestPageModule);



/***/ })

}]);
//# sourceMappingURL=new-test-new-test-module.js.map