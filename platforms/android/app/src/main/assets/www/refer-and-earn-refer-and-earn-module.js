(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["refer-and-earn-refer-and-earn-module"],{

/***/ "Cko7":
/*!*************************************************************!*\
  !*** ./src/app/pages/refer-and-earn/refer-and-earn.page.ts ***!
  \*************************************************************/
/*! exports provided: ReferAndEarnPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferAndEarnPage", function() { return ReferAndEarnPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_refer_and_earn_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./refer-and-earn.page.html */ "D7d0");
/* harmony import */ var _refer_and_earn_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./refer-and-earn.page.scss */ "Rbac");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/social-sharing/ngx */ "/XPu");











let ReferAndEarnPage = class ReferAndEarnPage {
    constructor(activatedRoute, router, uihelper, storageService, navCtrl, http, socialSharing) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.uihelper = uihelper;
        this.storageService = storageService;
        this.navCtrl = navCtrl;
        this.http = http;
        this.socialSharing = socialSharing;
        this.cartCount = localStorage.getItem('cartCount');
        this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__["Constant"].AUTH).then(res => {
            this.userData = res.user_details;
            this.user_id = this.userData.id;
        });
    }
    ngOnInit() {
        try {
            this.router.events.subscribe((event) => {
                this.selfPath = '';
                if (event && event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__["NavigationEnd"] && event.url) {
                    this.selfPath = event.url + '/refer-and-earn';
                }
                this.selfPath = this.router.routerState.snapshot.url;
                this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'refer-and-earn') + 'refer-and-earn';
            });
        }
        catch (e) {
            console.log(e);
        }
    }
    ionViewWillEnter() {
        console.log('ionViewWillEnter user_id', this.user_id);
        this.GetDetailsOfDashBoard();
    }
    GetDetailsOfDashBoard() {
        // this.cartCount = localStorage.getItem('cartCount');
        this.uihelper.ShowSpinner();
        this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__["Constant"].AUTH).then(res => {
            this.userData = res.user_details;
            this.user_id = this.userData.id;
            this.http.afterLoginGet("cart_count", res.user_details.id).subscribe((res) => {
                this.cartCount = res['payload'].cart_count;
            });
            this.http.afterLoginGet("dashboard", this.user_id).subscribe((res) => {
                this.dashboard_data = res['payload'];
                // console.log('dashboard_data',this.dashboard_data)
                this.social_share_desc = this.dashboard_data.social_share_desc;
                if (this.dashboard_data.student_code) {
                    this.student_referal_code = this.dashboard_data.student_code.discount_code;
                }
                else {
                    this.student_referal_code = null;
                }
                // console.log('student_referal_code',this.student_referal_code)
                this.uihelper.HideSpinner();
            });
        });
    }
    getStringBeforeSubstring(parentString, substring) {
        return parentString.substring(0, parentString.indexOf(substring));
    }
    generateCode() {
        this.uihelper.ShowSpinner();
        let data = {
            course_id: localStorage.getItem('course_id'),
            user_id: this.user_id
        };
        this.http.afterLoginPost("generate-referralcode", data).subscribe((res) => {
            this.ionViewWillEnter();
            this.uihelper.HideSpinner();
        }, (err) => {
            this.uihelper.HideSpinner();
            this.uihelper.ShowAlert('Refer and Earn', 'Something Went wrong..Please try again');
            console.log(err);
        });
    }
    shareViaApps() {
        // console.log("MakeMyTest : I have become an MMTian by registering on Make My Test, best online test series for CA Students 😍 I recommend you to become an MMTian 🎓 Sign up on the website and get 5% discount on your first order by using my" + this.student_referal_code + "🤩 Website address: 👇🏻",this.student_referal_code)
        var options = {
            message: "MakeMyTest : I have become an MMTian by registering on Make My Test, best online test series for CA Students 😍 I recommend you to become an MMTian 🎓 Sign up on the website and get 5% discount on your first order by using my Referal Code " + this.student_referal_code + "🤩 Website address: 👇🏻",
            url: 'http://newdev.makemytest.in/register',
            chooserTitle: 'Makemytest',
        };
        this.socialSharing.shareWithOptions(options);
    }
    goBack() {
        this.navCtrl.pop();
    }
    GoToUserProfile() {
        this.router.navigate([this.selfPath + "/user-profile"]);
    }
    GoToCart() {
        this.router.navigate([this.selfPath + "/cart"]);
    }
};
ReferAndEarnPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_6__["UIHelper"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_7__["StorageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_8__["HttpService"] },
    { type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_10__["SocialSharing"] }
];
ReferAndEarnPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-refer-and-earn',
        template: _raw_loader_refer_and_earn_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_refer_and_earn_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ReferAndEarnPage);



/***/ }),

/***/ "D7d0":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/refer-and-earn/refer-and-earn.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>Refer and Earn</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n    <ion-col size=\"12\" class=\"header-arrow padding-0\">\r\n      <div>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <div class=\"back\" (click)=\"goBack()\">\r\n                <img src=\"assets/back.svg\" alt=\"back\">\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <div class=\"menu-icon\">\r\n              <span class=\"shopping-cart\" (click)=\"GoToCart()\"><img src=\"assets/shopping-cart.svg\" alt=\"shopping\"><span\r\n                  class=\"cart-number\">{{cartCount}}</span></span>\r\n              <span class=\"user-icon\" slot=\"start\" (click)=\"GoToUserProfile()\">\r\n                <img src=\"assets/user-profile.png\" alt=\"user-icon\">\r\n              </span>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <div class=\"packages-header\">\r\n              <h2>Refer and Earn</h2>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"referral-img\">\r\n      <p>Refer a friend and earn</p>\r\n      <img src=\"assets/referral-img.png\" alt=\"referral-img\">\r\n        <div class=\"space-3 text-center\" *ngIf = \"student_referal_code != null\">\r\n            <ion-item  lines=\"none\" class=\"text-center\">\r\n              <!-- <ion-input type=\"text\" ion-button class=\"start-btn\"  class=\"form-control\" [(ngModel)]='student_referal_code' [readonly]=\"true\"></ion-input>             -->\r\n           \r\n                <p style=\"background-color: #4ab84a; padding: 14px;\"><b>{{student_referal_code}}</b></p>\r\n\r\n            </ion-item>            \r\n            <ion-button (click)=\"shareViaApps()\">Share</ion-button>\r\n            \r\n          </div>\r\n        <div class=\"social-icon text-center\">\r\n        </div>\r\n        <ion-button *ngIf = \"student_referal_code == null\" ion-button class=\"start-btn\" (click)=\"generateCode()\">Click here to generate referral code<ion-icon name=\"chevron-forward-outline\" class=\"aerrow-icon\"></ion-icon></ion-button>\r\n        \r\n    \r\n      <div class=\"referral-work\">\r\n        <h2>How does referral work?</h2>\r\n        <p>1. Share above code to your friend<br>\r\n          2. They register and place their order with referral \r\n               code.<br>\r\n          3. They will get 5% discount on their order<br>\r\n          4. You will get 5% of that order amount in your \r\n              MMT credit </p>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "Rbac":
/*!***************************************************************!*\
  !*** ./src/app/pages/refer-and-earn/refer-and-earn.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.back img {\n  width: 40px;\n}\n\n.back {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.header-arrow {\n  padding: 20px 15px !important;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 120px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n}\n\n.packages-header h2 {\n  margin: 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n  position: relative;\n  top: -15px;\n}\n\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\nion-label {\n  margin: 0;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n}\n\nion-input {\n  --padding-top: 0px;\n  --padding-end: 0px;\n  --padding-bottom: 0px;\n}\n\n.referral-img p {\n  color: #305f72;\n  font-weight: 500;\n  font-size: 18px;\n  margin: 3px auto 10px;\n  text-align: center;\n  display: block;\n}\n\n.referral-img img {\n  padding: 12px 20px;\n}\n\n.start-btn {\n  --background: #3f5c9b !important;\n  --background-activated: #3f5c9b !important;\n  --background-focused: #3f5c9b !important;\n  --background-hover: #3f5c9b !important;\n  --border-radius: 8px;\n  height: 38px;\n  margin: 0 auto;\n  width: 90%;\n  text-align: center;\n  font-size: 14px;\n  font-weight: 400;\n  display: block;\n  padding: 0px 0;\n}\n\n.aerrow-icon {\n  margin-left: 30px;\n}\n\n.social-icon ul {\n  padding: 0;\n  margin: 0 auto;\n  text-align: center;\n}\n\n.social-icon li {\n  padding: 0;\n  display: inline-block;\n  margin: 22px 6px 12px 6px;\n}\n\n.social-icon li img {\n  padding: 0;\n  width: 34px;\n  height: 34px;\n}\n\n.referral-work h2 {\n  color: #305f72;\n  font-weight: 500;\n  font-size: 18px;\n  margin: 5px 15px 10px 15px;\n  text-align: left;\n  display: block;\n}\n\n.referral-work p {\n  color: #305f72;\n  font-weight: 400;\n  font-size: 15px;\n  margin: 5px 15px 5px 15px;\n  text-align: left;\n  display: inline-block;\n  line-height: 1.5;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxyZWZlci1hbmQtZWFybi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztBQUNmOztBQUNBO0VBQ0ksV0FBVztBQUVmOztBQUFBO0VBQ0ksV0FBVztBQUdmOztBQURBO0VBQ0ksV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsU0FBUztBQUliOztBQUZBO0VBQ0ksaUJBQWlCO0FBS3JCOztBQUhBO0VBQ0ksa0JBQWtCO0FBTXRCOztBQUpBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIsWUFBWTtFQUNaLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZUFBZTtFQUNmLFlBQVk7QUFPaEI7O0FBTEE7RUFDSSxhQUFhO0FBUWpCOztBQU5BO0VBQ0ksNkJBQTZCO0FBU2pDOztBQVBBO0VBQ0ksNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QixhQUFhO0VBQ2IsV0FBVztFQUNYLDJCQUEyQjtFQUMzQixrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLDRCQUE0QjtBQVVoQzs7QUFSQTtFQUNJLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFVBQVU7QUFXZDs7QUFUQTtFQUNJLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxjQUFjO0VBQ2QsZ0JBQWdCO0FBWXBCOztBQVZBO0VBQ0ksU0FBUztBQWFiOztBQVhBO0VBQ0ksb0JBQWdCO0VBQ2hCLHdCQUFvQjtBQWN4Qjs7QUFaQTtFQUNJLGtCQUFjO0VBQ2Qsa0JBQWM7RUFDZCxxQkFBaUI7QUFlckI7O0FBVEE7RUFDSSxjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixxQkFBcUI7RUFDckIsa0JBQWtCO0VBQ2xCLGNBQWM7QUFZbEI7O0FBVkE7RUFDSSxrQkFBa0I7QUFhdEI7O0FBWEE7RUFDSSxnQ0FBYTtFQUNiLDBDQUF1QjtFQUN2Qix3Q0FBcUI7RUFDckIsc0NBQW1CO0VBQ25CLG9CQUFnQjtFQUNoQixZQUFZO0VBQ1osY0FBYztFQUNkLFVBQVU7RUFDVixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsY0FBYztBQWNsQjs7QUFaQTtFQUNJLGlCQUFpQjtBQWVyQjs7QUFiQTtFQUNJLFVBQVU7RUFDVixjQUFjO0VBQ2Qsa0JBQWtCO0FBZ0J0Qjs7QUFkQTtFQUNJLFVBQVU7RUFDVixxQkFBcUI7RUFDckIseUJBQXlCO0FBaUI3Qjs7QUFmQTtFQUNJLFVBQVU7RUFDVixXQUFXO0VBQ1gsWUFBWTtBQWtCaEI7O0FBaEJBO0VBQ0ksY0FBYztFQUNkLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsMEJBQTBCO0VBQzFCLGdCQUFnQjtFQUNoQixjQUFjO0FBbUJsQjs7QUFqQkE7RUFDSSxjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsZ0JBQWdCO0VBQ2hCLHFCQUFxQjtFQUNyQixnQkFBZ0I7QUFvQnBCIiwiZmlsZSI6InJlZmVyLWFuZC1lYXJuLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi51c2VyLWljb24gaW1nIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG59XHJcbi5iYWNrIGltZyB7XHJcbiAgICB3aWR0aDogNDBweDtcclxufVxyXG4uYmFjayB7XHJcbiAgICBmbG9hdDogbGVmdDtcclxufVxyXG4uc2hvcHBpbmctY2FydCBpbWcge1xyXG4gICAgd2lkdGg6IDMwcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IC04cHg7XHJcbn1cclxuLm1lbnUtaWNvbiB7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG4uc2hvcHBpbmctY2FydCB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuLmNhcnQtbnVtYmVyIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAxNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmYmIwMDtcclxuICAgIGhlaWdodDogMjBweDtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGNvbG9yOiAjM2Y1YzliO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgcGFkZGluZzogMXB4O1xyXG59XHJcbmlvbi1oZWFkZXIge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG4uaGVhZGVyLWFycm93IHtcclxuICAgIHBhZGRpbmc6IDIwcHggMTVweCAhaW1wb3J0YW50O1xyXG59XHJcbi50b3AtYmcge1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBoZWlnaHQ6IDEyMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMzBweCAzMHB4O1xyXG59XHJcbi5wYWNrYWdlcy1oZWFkZXIgaDJ7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLTE1cHg7XHJcbn1cclxuLnBhY2thZ2VzLWhlYWRlciBzcGFue1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgY29sb3I6ICMzMDVGNzI7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuaW9uLWxhYmVsIHtcclxuICAgIG1hcmdpbjogMDtcclxufVxyXG5pb24taXRlbSB7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcclxuICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcclxufVxyXG5pb24taW5wdXQge1xyXG4gICAgLS1wYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgLS1wYWRkaW5nLWVuZDogMHB4O1xyXG4gICAgLS1wYWRkaW5nLWJvdHRvbTogMHB4O1xyXG59XHJcblxyXG4vLyAucmVmZXJyYWwtaW1ne1xyXG5cclxuLy8gfVxyXG4ucmVmZXJyYWwtaW1nIHB7XHJcbiAgICBjb2xvcjogIzMwNWY3MjtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBtYXJnaW46IDNweCBhdXRvIDEwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufVxyXG4ucmVmZXJyYWwtaW1nIGltZ3tcclxuICAgIHBhZGRpbmc6IDEycHggMjBweDtcclxufVxyXG4uc3RhcnQtYnRuIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYmFja2dyb3VuZC1ob3ZlcjogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgLS1ib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICBoZWlnaHQ6IDM4cHg7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBwYWRkaW5nOiAwcHggMDtcclxufVxyXG4uYWVycm93LWljb257XHJcbiAgICBtYXJnaW4tbGVmdDogMzBweDtcclxufVxyXG4uc29jaWFsLWljb24gdWx7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLnNvY2lhbC1pY29uIGxpe1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIG1hcmdpbjogMjJweCA2cHggMTJweCA2cHg7XHJcbn1cclxuLnNvY2lhbC1pY29uIGxpIGltZ3tcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICB3aWR0aDogMzRweDtcclxuICAgIGhlaWdodDogMzRweDtcclxufVxyXG4ucmVmZXJyYWwtd29yayBoMntcclxuICAgIGNvbG9yOiAjMzA1ZjcyO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIG1hcmdpbjogNXB4IDE1cHggMTBweCAxNXB4O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcbi5yZWZlcnJhbC13b3JrIHB7XHJcbiAgICBjb2xvcjogIzMwNWY3MjtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBtYXJnaW46IDVweCAxNXB4IDVweCAxNXB4O1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbn0iXX0= */");

/***/ }),

/***/ "tjE2":
/*!***************************************************************!*\
  !*** ./src/app/pages/refer-and-earn/refer-and-earn.module.ts ***!
  \***************************************************************/
/*! exports provided: ReferAndEarnPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferAndEarnPageModule", function() { return ReferAndEarnPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _refer_and_earn_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./refer-and-earn-routing.module */ "w1ZV");
/* harmony import */ var _refer_and_earn_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./refer-and-earn.page */ "Cko7");







let ReferAndEarnPageModule = class ReferAndEarnPageModule {
};
ReferAndEarnPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _refer_and_earn_routing_module__WEBPACK_IMPORTED_MODULE_5__["ReferAndEarnPageRoutingModule"]
        ],
        declarations: [_refer_and_earn_page__WEBPACK_IMPORTED_MODULE_6__["ReferAndEarnPage"]]
    })
], ReferAndEarnPageModule);



/***/ }),

/***/ "w1ZV":
/*!***********************************************************************!*\
  !*** ./src/app/pages/refer-and-earn/refer-and-earn-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: ReferAndEarnPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferAndEarnPageRoutingModule", function() { return ReferAndEarnPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _refer_and_earn_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./refer-and-earn.page */ "Cko7");




const routes = [
    {
        path: '',
        component: _refer_and_earn_page__WEBPACK_IMPORTED_MODULE_3__["ReferAndEarnPage"]
    }
];
let ReferAndEarnPageRoutingModule = class ReferAndEarnPageRoutingModule {
};
ReferAndEarnPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ReferAndEarnPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=refer-and-earn-refer-and-earn-module.js.map