(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["my-ratings-my-ratings-module"],{

/***/ "7pOl":
/*!***************************************************************!*\
  !*** ./src/app/pages/my-ratings/my-ratings-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: MyRatingsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyRatingsPageRoutingModule", function() { return MyRatingsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _my_ratings_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./my-ratings.page */ "xvdr");




const routes = [
    {
        path: '',
        component: _my_ratings_page__WEBPACK_IMPORTED_MODULE_3__["MyRatingsPage"]
    }
];
let MyRatingsPageRoutingModule = class MyRatingsPageRoutingModule {
};
MyRatingsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MyRatingsPageRoutingModule);



/***/ }),

/***/ "DT1v":
/*!*******************************************************!*\
  !*** ./src/app/pages/my-ratings/my-ratings.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.back img {\n  width: 40px;\n}\n\n.back {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.header-arrow {\n  padding: 20px 15px !important;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 120px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n}\n\n.packages-header h2 {\n  margin: 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n  position: relative;\n  top: -15px;\n}\n\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\nion-label {\n  margin: 0;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n}\n\n.title-text {\n  padding-left: 10px;\n  font-weight: 500;\n  font-size: 20px;\n  color: #305F72;\n}\n\n.m-10 {\n  margin: 0 10px;\n}\n\n.mt-16 {\n  margin-top: 16px;\n}\n\n.tab-list-view {\n  border-radius: 20px !important;\n  background-color: #ffffff;\n  margin: 0px 10px 10px;\n  border: 1px solid #b1bcd440;\n  padding: 10px;\n  box-shadow: 0px 1px 12px -4px #37242442;\n}\n\n.tab-list-content h2 {\n  font-size: 14px;\n  font-weight: 400;\n  color: #444444;\n  margin-bottom: 0px;\n  margin-top: 5px;\n  color: #F27376;\n}\n\n.tab-list-content strong {\n  font-size: 15px;\n  font-weight: 500;\n  color: #305f72;\n}\n\n.tab-list-content h3 {\n  font-size: 14px;\n  font-weight: 400;\n  color: #444444;\n  margin-bottom: 0px;\n  margin-top: 5px;\n}\n\n.tab-list-content h4 {\n  font-size: 14px;\n  font-weight: 600;\n  color: #444444;\n  margin-bottom: 0px;\n  margin-top: 5px;\n}\n\n.mb-0 {\n  margin-bottom: 0px;\n}\n\n/* component */\n\n.star-rating {\n  display: flex;\n  flex-direction: row-reverse;\n  font-size: 1.5em;\n  justify-content: space-around;\n  text-align: center;\n}\n\n.star-rating input {\n  display: none;\n}\n\n.star-rating label {\n  color: #ccc;\n  cursor: pointer;\n}\n\n.star-rating :checked ~ label {\n  color: #deb217;\n}\n\n.my-rating-text p {\n  font-size: 14px;\n  margin: 5px 0 5px 0;\n  text-transform: capitalize;\n}\n\n.queries-content img {\n  width: 250px;\n  margin: 0 auto;\n  text-align: center;\n  display: block;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxteS1yYXRpbmdzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFrQjtFQUNsQixXQUFXO0FBQ2Y7O0FBQ0E7RUFDSSxXQUFXO0FBRWY7O0FBQUE7RUFDSSxXQUFXO0FBR2Y7O0FBREE7RUFDSSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixTQUFTO0FBSWI7O0FBRkE7RUFDSSxpQkFBaUI7QUFLckI7O0FBSEE7RUFDSSxrQkFBa0I7QUFNdEI7O0FBSkE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxlQUFlO0VBQ2YsWUFBWTtBQU9oQjs7QUFMQTtFQUNJLGFBQWE7QUFRakI7O0FBTkE7RUFDSSw2QkFBNkI7QUFTakM7O0FBUEE7RUFDSSw0QkFBNEI7RUFDNUIsc0JBQXNCO0VBQ3RCLGFBQWE7RUFDYixXQUFXO0VBQ1gsMkJBQTJCO0VBQzNCLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sNEJBQTRCO0FBVWhDOztBQVJBO0VBQ0ksU0FBUztFQUNULGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsVUFBVTtBQVdkOztBQVRBO0VBQ0ksU0FBUztFQUNULGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsY0FBYztFQUNkLGNBQWM7RUFDZCxnQkFBZ0I7QUFZcEI7O0FBVkE7RUFDSSxTQUFTO0FBYWI7O0FBWEE7RUFDSSxvQkFBZ0I7RUFDaEIsd0JBQW9CO0FBY3hCOztBQVpBO0VBQ0ksa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsY0FBYztBQWVsQjs7QUFiQTtFQUNJLGNBQWM7QUFnQmxCOztBQWRBO0VBQ0ksZ0JBQWU7QUFpQm5COztBQWZBO0VBQ0ksOEJBQThCO0VBQzlCLHlCQUF5QjtFQUN6QixxQkFBcUI7RUFDckIsMkJBQTJCO0VBQzNCLGFBQWE7RUFDYix1Q0FBdUM7QUFrQjNDOztBQWhCQTtFQUNJLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixlQUFjO0VBQ2QsY0FBYztBQW1CbEI7O0FBakJBO0VBQ0ksZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixjQUFjO0FBb0JsQjs7QUFsQkE7RUFDSSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsZUFBYztBQXFCbEI7O0FBbkJBO0VBQ0ksZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGVBQWM7QUFzQmxCOztBQXBCQTtFQUNJLGtCQUFpQjtBQXVCckI7O0FBckJBLGNBQUE7O0FBRUE7RUFDSSxhQUFZO0VBQ1osMkJBQTJCO0VBQzNCLGdCQUFlO0VBQ2YsNkJBQTRCO0VBQzVCLGtCQUFpQjtBQXVCckI7O0FBbkJFO0VBQ0UsYUFBWTtBQXNCaEI7O0FBbkJFO0VBQ0UsV0FBVTtFQUNWLGVBQWM7QUFzQmxCOztBQW5CRTtFQUNFLGNBQWE7QUFzQmpCOztBQWZFO0VBQ0ksZUFBYztFQUNkLG1CQUFtQjtFQUNuQiwwQkFBMEI7QUFrQmhDOztBQWhCRTtFQUNFLFlBQVk7RUFDWixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGNBQWM7QUFtQmxCIiwiZmlsZSI6Im15LXJhdGluZ3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnVzZXItaWNvbiBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgd2lkdGg6IDQwcHg7XHJcbn1cclxuLmJhY2sgaW1nIHtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG59XHJcbi5iYWNrIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IGltZyB7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLThweDtcclxufVxyXG4ubWVudS1pY29uIHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4uY2FydC1udW1iZXIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDE1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZiYjAwO1xyXG4gICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICMzZjVjOWI7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBwYWRkaW5nOiAxcHg7XHJcbn1cclxuaW9uLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcbi5oZWFkZXItYXJyb3cge1xyXG4gICAgcGFkZGluZzogMjBweCAxNXB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLnRvcC1iZyB7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGhlaWdodDogMTIwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAgMCAzMHB4IDMwcHg7XHJcbn1cclxuLnBhY2thZ2VzLWhlYWRlciBoMntcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAtMTVweDtcclxufVxyXG4ucGFja2FnZXMtaGVhZGVyIHNwYW57XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG5pb24tbGFiZWwge1xyXG4gICAgbWFyZ2luOiAwO1xyXG59XHJcbmlvbi1pdGVtIHtcclxuICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xyXG4gICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xyXG59XHJcbi50aXRsZS10ZXh0e1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGNvbG9yOiAjMzA1RjcyO1xyXG59XHJcbi5tLTEwe1xyXG4gICAgbWFyZ2luOiAwIDEwcHg7XHJcbn1cclxuLm10LTE2e1xyXG4gICAgbWFyZ2luLXRvcDoxNnB4O1xyXG59XHJcbi50YWItbGlzdC12aWV3e1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweCAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIG1hcmdpbjogMHB4IDEwcHggMTBweDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNiMWJjZDQ0MDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3gtc2hhZG93OiAwcHggMXB4IDEycHggLTRweCAjMzcyNDI0NDI7XHJcbn1cclxuLnRhYi1saXN0LWNvbnRlbnQgaDJ7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgY29sb3I6ICM0NDQ0NDQ7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICBtYXJnaW4tdG9wOjVweDtcclxuICAgIGNvbG9yOiAjRjI3Mzc2O1xyXG59XHJcbi50YWItbGlzdC1jb250ZW50IHN0cm9uZ3tcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogIzMwNWY3MjtcclxufVxyXG4udGFiLWxpc3QtY29udGVudCBoMyB7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgY29sb3I6ICM0NDQ0NDQ7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgICBtYXJnaW4tdG9wOjVweDtcclxufVxyXG4udGFiLWxpc3QtY29udGVudCAgaDQge1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGNvbG9yOiAjNDQ0NDQ0O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gICAgbWFyZ2luLXRvcDo1cHg7XHJcbn1cclxuLm1iLTB7XHJcbiAgICBtYXJnaW4tYm90dG9tOjBweDtcclxufVxyXG4vKiBjb21wb25lbnQgKi9cclxuXHJcbi5zdGFyLXJhdGluZyB7XHJcbiAgICBkaXNwbGF5OmZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XHJcbiAgICBmb250LXNpemU6MS41ZW07XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6c3BhY2UtYXJvdW5kO1xyXG4gICAgdGV4dC1hbGlnbjpjZW50ZXI7XHJcbiAgICAvLyB3aWR0aDo1ZW07XHJcbiAgfVxyXG4gIFxyXG4gIC5zdGFyLXJhdGluZyBpbnB1dCB7XHJcbiAgICBkaXNwbGF5Om5vbmU7XHJcbiAgfVxyXG4gIFxyXG4gIC5zdGFyLXJhdGluZyBsYWJlbCB7XHJcbiAgICBjb2xvcjojY2NjO1xyXG4gICAgY3Vyc29yOnBvaW50ZXI7XHJcbiAgfVxyXG4gIFxyXG4gIC5zdGFyLXJhdGluZyA6Y2hlY2tlZCB+IGxhYmVsIHtcclxuICAgIGNvbG9yOiNkZWIyMTc7XHJcbiAgfVxyXG4gIFxyXG4vLyAgIC5zdGFyLXJhdGluZyBsYWJlbDpob3ZlcixcclxuLy8gICAuc3Rhci1yYXRpbmcgbGFiZWw6aG92ZXIgfiBsYWJlbCB7XHJcbi8vICAgICBjb2xvcjojZGViMjE3O1xyXG4vLyAgIH1cclxuICAubXktcmF0aW5nLXRleHQgcHtcclxuICAgICAgZm9udC1zaXplOjE0cHg7XHJcbiAgICAgIG1hcmdpbjogNXB4IDAgNXB4IDA7XHJcbiAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gIH1cclxuICAucXVlcmllcy1jb250ZW50IGltZ3tcclxuICAgIHdpZHRoOiAyNTBweDtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn0iXX0= */");

/***/ }),

/***/ "rzzE":
/*!*******************************************************!*\
  !*** ./src/app/pages/my-ratings/my-ratings.module.ts ***!
  \*******************************************************/
/*! exports provided: MyRatingsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyRatingsPageModule", function() { return MyRatingsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _my_ratings_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./my-ratings-routing.module */ "7pOl");
/* harmony import */ var _my_ratings_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./my-ratings.page */ "xvdr");







let MyRatingsPageModule = class MyRatingsPageModule {
};
MyRatingsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _my_ratings_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyRatingsPageRoutingModule"]
        ],
        declarations: [_my_ratings_page__WEBPACK_IMPORTED_MODULE_6__["MyRatingsPage"]]
    })
], MyRatingsPageModule);



/***/ }),

/***/ "xvdr":
/*!*****************************************************!*\
  !*** ./src/app/pages/my-ratings/my-ratings.page.ts ***!
  \*****************************************************/
/*! exports provided: MyRatingsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyRatingsPage", function() { return MyRatingsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_my_ratings_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./my-ratings.page.html */ "yppM");
/* harmony import */ var _my_ratings_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./my-ratings.page.scss */ "DT1v");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");











// import { LaunchReview } from '@ionic-native/launch-review/ngx';
let MyRatingsPage = class MyRatingsPage {
    constructor(router, http, httpService, storageService, uihelper, navCtrl) {
        this.router = router;
        this.http = http;
        this.httpService = httpService;
        this.storageService = storageService;
        this.uihelper = uihelper;
        this.navCtrl = navCtrl;
        this.url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__["Constant"].apiUrl;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
        });
        this.options = { headers: this.headers };
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                this.router.events.subscribe((event) => {
                    this.selfPath = '';
                    if (event && event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__["NavigationEnd"] && event.url) {
                        this.selfPath = event.url + '/my-ratings';
                    }
                    this.selfPath = this.router.routerState.snapshot.url;
                    this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'my-ratings') + 'my-ratings';
                });
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    getStringBeforeSubstring(parentString, substring) {
        return parentString.substring(0, parentString.indexOf(substring));
    }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            //   this.launchReview.launch()
            // .then(() => console.log('Successfully launched store app'));
            this.cartCount = localStorage.getItem('cartCount');
            this.uihelper.ShowSpinner();
            this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__["Constant"].AUTH).then(res => {
                this.userData = res.user_details;
                let user_id = res.user_details.id;
                this.httpService.afterLoginGet("cart_count", res.user_details.id).subscribe((res) => {
                    this.cartCount = res['payload'].cart_count;
                });
                this.http.get(this.url + 'student-test-ratings/' + user_id, this.options).subscribe((res) => {
                    this.ratings = res['payload'];
                    // console.log(this.ratings)
                    this.ratings.forEach((value, i) => {
                        this.ratings[i]['created_at'] = value.created_at.substring(0, 10);
                        this.ratings[i]['index'] = i;
                    });
                    this.uihelper.HideSpinner();
                }, (err) => {
                    this.uihelper.HideSpinner();
                    // console.log('ratings not found', err)
                    this.uihelper.urlAlert('My Ratings', "We hope you liked out Application.Please Rate us on play store");
                    // this.uihelper.playStoreAlert()
                });
            });
        });
    }
    goBack() {
        this.navCtrl.pop();
    }
    GoToCart() {
        this.router.navigate([this.selfPath + "/cart"]);
    }
};
MyRatingsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_7__["HttpService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_8__["StorageService"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_10__["UIHelper"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] }
];
MyRatingsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-my-ratings',
        template: _raw_loader_my_ratings_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_my_ratings_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MyRatingsPage);



/***/ }),

/***/ "yppM":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-ratings/my-ratings.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <!-- <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button (click)=\"getHome()\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-buttons slot=\"primary\">\r\n      <ion-button>\r\n        <ul class=\"shopping-icon\">\r\n          <li (click)=\"GoToCart()\">\r\n            <img src=\"assets/icon/my-cart-1.png\" class=\"cart\">\r\n            <span class=\"cart_count\">{{cartCount ? cartCount : '0'}}</span>\r\n          </li>\r\n          <li routerLink=\"/user-profile\">\r\n            <img src=\"assets/icon/user.png\" >\r\n          </li>\r\n        </ul>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ios title-default hydrated\">My Ratings</ion-title>\r\n  </ion-toolbar> -->\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n    <ion-col size=\"12\" class=\"header-arrow padding-0\">\r\n      <div>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <div class=\"back\" (click)=\"goBack()\">\r\n                <img src=\"assets/back.svg\" alt=\"back\">\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <div class=\"menu-icon\">\r\n              <span class=\"shopping-cart\"><img src=\"assets/shopping-cart.svg\" alt=\"shopping\"><span\r\n                  class=\"cart-number\">{{cartCount}}</span></span>\r\n              <span class=\"user-icon\" slot=\"start\">\r\n                <img src=\"assets/user-profile.png\" alt=\"user-icon\">\r\n              </span>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <div class=\"packages-header\">\r\n              <h2>My Ratings</h2>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"m-10\">\r\n    <ion-col size=\"8\" class=\"padding-0\">\r\n      <h4 class=\"title-text mb-0\">My Rating</h4>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"m-10 mt-16\">\r\n    <ion-col size=\"12\" class=\"padding-0\" *ngIf=\"ratings != null\">\r\n      <div class=\"tab-list-view \" *ngFor=\"let rating of ratings\">\r\n        <ion-row >\r\n          <ion-col size=\"8\" class=\"padding-0\">\r\n            <div class=\"tab-list-content\">\r\n              <h2><strong>Package </strong> - {{rating.test.package.name}}</h2>\r\n              <h2><strong>Test Name </strong> - {{rating.test.title}}</h2>\r\n              <h3><strong>Review Posted For </strong> - {{rating.test.title}}</h3>\r\n              <h4><strong>Review Posted On </strong> - {{rating.created_at}}</h4>\r\n            </div>\r\n          </ion-col>\r\n\r\n          <ion-col size=\"4\" class=\"padding-0\">\r\n            <div class=\"star-rating\">\r\n              <input type=\"radio\" id=\"5-stars\" name=\"rating{{rating.index}}\" value=\"5\" checked=\"{{rating.rating == 5 ? 'checked' : ''}}\" readonly/>\r\n              <label class=\"star\">&#9733;</label>\r\n\r\n              <input type=\"radio\" id=\"4-stars\" name=\"rating{{rating.index}}\" value=\"4\" checked=\"{{rating.rating == 4 ? 'checked' : ''}}\" readonly/>\r\n              <label class=\"star\">&#9733;</label>\r\n\r\n              <input type=\"radio\" id=\"3-stars\" name=\"rating{{rating.index}}\" value=\"3\" checked=\"{{rating.rating == 3 ? 'checked' : ''}}\" readonly/>\r\n              <label class=\"star\">&#9733;</label>\r\n\r\n              <input type=\"radio\" id=\"2-stars\" name=\"rating{{rating.index}}\" value=\"2\" checked=\"{{rating.rating == 2 ? 'checked' : ''}}\" readonly/>\r\n              <label class=\"star\">&#9733;</label>\r\n\r\n              <input type=\"radio\" id=\"1-star\" name=\"rating{{rating.index}}\" value=\"1\" checked=\"{{rating.rating == 1 ? 'checked' : ''}}\" readonly/>\r\n              <label class=\"star\">&#9733;</label>\r\n            </div>\r\n            \r\n            <div class=\"my-rating-text\">\r\n              <p>{{rating.comment}}</p>\r\n            </div>\r\n\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n<ion-row>\r\n    <ion-col *ngIf=\"ratings == null\">\r\n      <div class=\"queries-content\">\r\n        <img src=\"assets/icon/no-result.png\" alt=\"no-packages-found\">\r\n      </div>\r\n    </ion-col>\r\n</ion-row>\r\n\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=my-ratings-my-ratings-module.js.map