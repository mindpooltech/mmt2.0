(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["test-history-test-history-module"],{

/***/ "/KFI":
/*!*******************************************************************!*\
  !*** ./src/app/pages/test-history/test-history-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: TestHistoryPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestHistoryPageRoutingModule", function() { return TestHistoryPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _test_history_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./test-history.page */ "GrbS");




const routes = [
    {
        path: '',
        component: _test_history_page__WEBPACK_IMPORTED_MODULE_3__["TestHistoryPage"]
    }
];
let TestHistoryPageRoutingModule = class TestHistoryPageRoutingModule {
};
TestHistoryPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TestHistoryPageRoutingModule);



/***/ }),

/***/ "25Uy":
/*!***********************************************************!*\
  !*** ./src/app/pages/test-history/test-history.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.back img {\n  width: 40px;\n}\n\n.back {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.header-arrow {\n  padding: 20px 15px;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 160px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n}\n\n.packages-header h2 {\n  margin: 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n}\n\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\nion-label {\n  margin: 0;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n}\n\n.space-right-left {\n  margin: 0 5px;\n}\n\n.title-text {\n  color: #2a2a2a;\n  font-size: 15px;\n  font-weight: 500;\n  padding-left: 5px;\n}\n\n.test-history-btn button {\n  border: none;\n  padding: 12px 40px;\n  font-size: 15px;\n  font-weight: 400;\n  background-color: #3f5c9b;\n  color: white;\n  border-radius: 8px;\n  outline: none;\n  margin: -8px auto 0;\n  text-align: center;\n  display: block;\n  transition: 0.3s;\n}\n\n.font-size-1 {\n  font-size: 13px;\n  margin: 0;\n}\n\nion-select {\n  width: 34px;\n  padding: 5px;\n  padding-left: 0;\n}\n\nion-item {\n  --padding-start: 12px;\n  --min-height: 35px;\n}\n\n.form-control-cart {\n  border: 1px solid #2f3a755c !important;\n  font-size: 14px !important;\n  font-weight: 400;\n  border-radius: 8px;\n  --color: rgb(48 95 114);\n  font-weight: 400;\n  padding: 0;\n  margin-right: 4px;\n}\n\n.margin-10 {\n  margin: 0 12px 0 15px;\n}\n\nbutton {\n  padding: 0;\n}\n\n.mt-16 {\n  margin-top: 16px;\n}\n\n.m-0 {\n  margin: 0px;\n}\n\n.tab-list-view {\n  margin-bottom: 15px;\n  margin: 0px 15px 10px;\n  border-radius: 20px;\n  border: 1px solid #b1bcd440;\n  padding: 10px;\n  box-shadow: 0px 1px 12px -4px #37242442;\n}\n\n.tab-list-content h2 {\n  font-size: 13px;\n  font-weight: 400;\n  margin-top: 0;\n  margin-bottom: 5px;\n}\n\n.tab-list-content h3 strong {\n  font-weight: 500;\n  color: #305F72;\n}\n\n.tab-list-content h3 {\n  margin: 10px 0;\n  font-size: 14px;\n  font-weight: 400;\n  color: #F27376;\n}\n\n.tab-list-content a {\n  font-size: 14px;\n  text-decoration: underline;\n  font-weight: 500;\n  color: #3F5C9B;\n  padding: 8px 0;\n  display: block;\n}\n\n.center-text {\n  display: flex;\n  align-items: center;\n  padding-left: 9px;\n}\n\n.center-text span {\n  font-size: 13px;\n}\n\n.complete-btn {\n  padding: 10px 18px;\n  font-weight: 500;\n  font-size: 12px;\n  border-radius: 8px !important;\n  box-shadow: none !important;\n  background: #4bbd42;\n  color: #ffffff;\n  position: absolute;\n  bottom: 5px;\n}\n\n.submited-btn {\n  background: #f9a52a;\n  margin-top: 18px;\n  padding: 10px 18px;\n  font-weight: 500;\n  font-size: 12px;\n  border-radius: 8px !important;\n  box-shadow: none !important;\n  color: #ffffff;\n  position: absolute;\n  bottom: 5px;\n}\n\n.pending-status {\n  padding: 10px 15px;\n  color: #2a2a2a !important;\n  font-weight: 400;\n  font-size: 13px !important;\n  border-radius: 50px !important;\n  box-shadow: none !important;\n  background: #d7d7d7;\n  margin-top: 5px;\n  text-decoration: none;\n  display: inline-block;\n}\n\n.not_found {\n  padding: 22px;\n  font-size: 20px;\n}\n\n.queries-content span {\n  font-size: 15px;\n  color: #696969;\n}\n\n.select-icon-inner {\n  left: 0px !important;\n}\n\n.dates-sam strong {\n  font-weight: 500;\n  color: #305F72;\n  font-size: 14px;\n  margin-top: 8px;\n  display: block;\n}\n\n.dates-sam span {\n  font-size: 14px;\n}\n\n.queries-content img {\n  width: 250px;\n  margin: 0 auto;\n  text-align: center;\n  display: block;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFx0ZXN0LWhpc3RvcnkucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7QUFDZjs7QUFDQTtFQUNJLFdBQVc7QUFFZjs7QUFBQTtFQUNJLFdBQVc7QUFHZjs7QUFEQTtFQUNJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFNBQVM7QUFJYjs7QUFGQTtFQUNJLGlCQUFpQjtBQUtyQjs7QUFIQTtFQUNJLGtCQUFrQjtBQU10Qjs7QUFKQTtFQUNJLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGVBQWU7RUFDZixZQUFZO0FBT2hCOztBQUxBO0VBQ0ksYUFBYTtBQVFqQjs7QUFOQTtFQUNJLGtCQUFrQjtBQVN0Qjs7QUFQQTtFQUNJLDRCQUE0QjtFQUM1QixzQkFBc0I7RUFDdEIsYUFBYTtFQUNiLFdBQVc7RUFDWCwyQkFBMkI7RUFDM0Isa0JBQWtCO0VBQ2xCLE1BQU07RUFDTiw0QkFBNEI7QUFVaEM7O0FBUkE7RUFDSSxTQUFTO0VBQ1Qsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixjQUFjO0VBQ2Qsa0JBQWtCO0FBV3RCOztBQVRBO0VBQ0ksU0FBUztFQUNULGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsY0FBYztFQUNkLGNBQWM7RUFDZCxnQkFBZ0I7QUFZcEI7O0FBVkE7RUFDSSxTQUFTO0FBYWI7O0FBWEE7RUFDSSxvQkFBZ0I7RUFDaEIsd0JBQW9CO0FBY3hCOztBQVpBO0VBQ0ksYUFBWTtBQWVoQjs7QUFiQTtFQUNJLGNBQWE7RUFDYixlQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtBQWdCckI7O0FBZEE7RUFDSSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGdCQUFnQjtBQWlCcEI7O0FBZkE7RUFDSSxlQUFjO0VBQ2QsU0FBUTtBQWtCWjs7QUFoQkE7RUFDSSxXQUFVO0VBQ1YsWUFBVztFQUNYLGVBQWM7QUFtQmxCOztBQWpCQTtFQUNJLHFCQUFnQjtFQUNoQixrQkFBYTtBQW9CakI7O0FBbEJBO0VBQ0ksc0NBQXNDO0VBQ3RDLDBCQUEwQjtFQUMxQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLHVCQUFRO0VBQ1IsZ0JBQWdCO0VBQ2hCLFVBQVU7RUFDVixpQkFBaUI7QUFxQnJCOztBQW5CQTtFQUNJLHFCQUFxQjtBQXNCekI7O0FBcEJBO0VBQ0ksVUFBUztBQXVCYjs7QUFyQkE7RUFDSSxnQkFBZTtBQXdCbkI7O0FBdEJBO0VBQ0ksV0FBVTtBQXlCZDs7QUF2QkE7RUFDSSxtQkFBbUI7RUFDbkIscUJBQXFCO0VBQ3JCLG1CQUFtQjtFQUNuQiwyQkFBMkI7RUFDM0IsYUFBYTtFQUNiLHVDQUF1QztBQTBCM0M7O0FBeEJBO0VBQ0ksZUFBYztFQUNkLGdCQUFlO0VBQ2YsYUFBWTtFQUNaLGtCQUFrQjtBQTJCdEI7O0FBekJBO0VBQ0ksZ0JBQWdCO0VBQ2hCLGNBQWM7QUE0QmxCOztBQTFCQTtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7QUE2QmxCOztBQTNCQTtFQUNJLGVBQWU7RUFDZiwwQkFBMEI7RUFDMUIsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxjQUFjO0VBQ2QsY0FBYztBQThCbEI7O0FBNUJBO0VBQ0ksYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixpQkFBZ0I7QUErQnBCOztBQTdCQTtFQUNJLGVBQWU7QUFnQ25COztBQTlCQTtFQUNJLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLDZCQUE2QjtFQUM3QiwyQkFBMkI7RUFDM0IsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsV0FBVztBQWlDZjs7QUEvQkE7RUFDSSxtQkFBbUI7RUFDbkIsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLDZCQUE2QjtFQUM3QiwyQkFBMkI7RUFDM0IsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixXQUFXO0FBa0NmOztBQWhDQTtFQUNJLGtCQUFpQjtFQUNqQix5QkFBdUI7RUFDdkIsZ0JBQWdCO0VBQ2hCLDBCQUF3QjtFQUN4Qiw4QkFBNEI7RUFDNUIsMkJBQXlCO0VBQ3pCLG1CQUFtQjtFQUNuQixlQUFjO0VBQ2QscUJBQW9CO0VBQ3BCLHFCQUFxQjtBQW1DekI7O0FBakNBO0VBQ0ksYUFBYTtFQUNiLGVBQWM7QUFvQ2xCOztBQWxDQTtFQUNJLGVBQWM7RUFDZCxjQUFhO0FBcUNqQjs7QUFuQ0E7RUFDSSxvQkFBb0I7QUFzQ3hCOztBQXBDQTtFQUNJLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZUFBZTtFQUNmLGVBQWU7RUFDZixjQUFjO0FBdUNsQjs7QUFyQ0E7RUFDSSxlQUFlO0FBd0NuQjs7QUF0Q0E7RUFDSSxZQUFZO0VBQ1osY0FBYztFQUNkLGtCQUFrQjtFQUNsQixjQUFjO0FBeUNsQiIsImZpbGUiOiJ0ZXN0LWhpc3RvcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnVzZXItaWNvbiBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgd2lkdGg6IDQwcHg7XHJcbn1cclxuLmJhY2sgaW1nIHtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG59XHJcbi5iYWNrIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IGltZyB7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLThweDtcclxufVxyXG4ubWVudS1pY29uIHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4uY2FydC1udW1iZXIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDE1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZiYjAwO1xyXG4gICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICMzZjVjOWI7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBwYWRkaW5nOiAxcHg7XHJcbn1cclxuaW9uLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcbi5oZWFkZXItYXJyb3cge1xyXG4gICAgcGFkZGluZzogMjBweCAxNXB4O1xyXG59XHJcbi50b3AtYmcge1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBoZWlnaHQ6IDE2MHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMzBweCAzMHB4O1xyXG59XHJcbi5wYWNrYWdlcy1oZWFkZXIgaDJ7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxufVxyXG4ucGFja2FnZXMtaGVhZGVyIHNwYW57XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG5pb24tbGFiZWwge1xyXG4gICAgbWFyZ2luOiAwO1xyXG59XHJcbmlvbi1pdGVtIHtcclxuICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xyXG4gICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xyXG59XHJcbi5zcGFjZS1yaWdodC1sZWZ0e1xyXG4gICAgbWFyZ2luOjAgNXB4O1xyXG59XHJcbi50aXRsZS10ZXh0e1xyXG4gICAgY29sb3I6IzJhMmEyYTtcclxuICAgIGZvbnQtc2l6ZToxNXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG59XHJcbi50ZXN0LWhpc3RvcnktYnRuIGJ1dHRvbntcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIHBhZGRpbmc6IDEycHggNDBweDtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2Y1YzliO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIG1hcmdpbjogLThweCBhdXRvIDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHRyYW5zaXRpb246IDAuM3M7XHJcbn1cclxuLmZvbnQtc2l6ZS0xe1xyXG4gICAgZm9udC1zaXplOjEzcHg7XHJcbiAgICBtYXJnaW46MDtcclxufVxyXG5pb24tc2VsZWN0e1xyXG4gICAgd2lkdGg6MzRweDtcclxuICAgIHBhZGRpbmc6NXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OjA7XHJcbn1cclxuaW9uLWl0ZW17XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDEycHg7XHJcbiAgICAtLW1pbi1oZWlnaHQ6IDM1cHg7XHJcbn1cclxuLmZvcm0tY29udHJvbC1jYXJ0IHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICMyZjNhNzU1YyAhaW1wb3J0YW50O1xyXG4gICAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgLS1jb2xvcjogcmdiKDQ4IDk1IDExNCk7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIG1hcmdpbi1yaWdodDogNHB4O1xyXG59XHJcbi5tYXJnaW4tMTB7XHJcbiAgICBtYXJnaW46IDAgMTJweCAwIDE1cHg7XHJcbn1cclxuYnV0dG9ue1xyXG4gICAgcGFkZGluZzowO1xyXG59XHJcbi5tdC0xNntcclxuICAgIG1hcmdpbi10b3A6MTZweDtcclxufVxyXG4ubS0we1xyXG4gICAgbWFyZ2luOjBweDtcclxufVxyXG4udGFiLWxpc3Qtdmlld3tcclxuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICBtYXJnaW46IDBweCAxNXB4IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2IxYmNkNDQwO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJveC1zaGFkb3c6IDBweCAxcHggMTJweCAtNHB4ICMzNzI0MjQ0MjtcclxufVxyXG4udGFiLWxpc3QtY29udGVudCBoMntcclxuICAgIGZvbnQtc2l6ZToxM3B4O1xyXG4gICAgZm9udC13ZWlnaHQ6NDAwO1xyXG4gICAgbWFyZ2luLXRvcDowO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG59XHJcbi50YWItbGlzdC1jb250ZW50IGgzIHN0cm9uZ3tcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxufVxyXG4udGFiLWxpc3QtY29udGVudCBoM3tcclxuICAgIG1hcmdpbjogMTBweCAwO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIGNvbG9yOiAjRjI3Mzc2O1xyXG59XHJcbi50YWItbGlzdC1jb250ZW50IGF7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogIzNGNUM5QjtcclxuICAgIHBhZGRpbmc6IDhweCAwO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuLmNlbnRlci10ZXh0e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nLWxlZnQ6OXB4O1xyXG59XHJcbi5jZW50ZXItdGV4dCBzcGFue1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcbi5jb21wbGV0ZS1idG57XHJcbiAgICBwYWRkaW5nOiAxMHB4IDE4cHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICBiYWNrZ3JvdW5kOiAjNGJiZDQyO1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBib3R0b206IDVweDtcclxufVxyXG4uc3VibWl0ZWQtYnRue1xyXG4gICAgYmFja2dyb3VuZDogI2Y5YTUyYTtcclxuICAgIG1hcmdpbi10b3A6IDE4cHg7XHJcbiAgICBwYWRkaW5nOiAxMHB4IDE4cHg7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJvdHRvbTogNXB4O1xyXG59XHJcbi5wZW5kaW5nLXN0YXR1c3tcclxuICAgIHBhZGRpbmc6MTBweCAxNXB4O1xyXG4gICAgY29sb3I6IzJhMmEyYSFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgZm9udC1zaXplOjEzcHghaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLXJhZGl1czo1MHB4IWltcG9ydGFudDtcclxuICAgIGJveC1zaGFkb3c6bm9uZSFpbXBvcnRhbnQ7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZDdkN2Q3O1xyXG4gICAgbWFyZ2luLXRvcDo1cHg7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246bm9uZTtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxufVxyXG4ubm90X2ZvdW5ke1xyXG4gICAgcGFkZGluZzogMjJweDtcclxuICAgIGZvbnQtc2l6ZToyMHB4O1xyXG59XHJcbi5xdWVyaWVzLWNvbnRlbnQgc3BhbntcclxuICAgIGZvbnQtc2l6ZToxNXB4O1xyXG4gICAgY29sb3I6IzY5Njk2OTtcclxufVxyXG4uc2VsZWN0LWljb24taW5uZXIge1xyXG4gICAgbGVmdDogMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLmRhdGVzLXNhbSBzdHJvbmd7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICMzMDVGNzI7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBtYXJnaW4tdG9wOiA4cHg7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufVxyXG4uZGF0ZXMtc2FtIHNwYW57XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbn1cclxuLnF1ZXJpZXMtY29udGVudCBpbWd7XHJcbiAgICB3aWR0aDogMjUwcHg7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59Il19 */");

/***/ }),

/***/ "BK5u":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/test-history/test-history.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button (click)=\"getHome()\"></ion-back-button>\r\n    </ion-buttons>\r\n    <!-- <ion-buttons slot=\"primary\">\r\n      <ion-button>\r\n        <ul class=\"shopping-icon\">\r\n          <li (click)=\"GoToCart()\">\r\n            <img src=\"assets/icon/my-cart-1.png\" class=\"cart\">\r\n            <span class=\"cart_count\">{{cartCount ? cartCount : '0'}}</span>\r\n          </li>\r\n          <li routerLink=\"/user-profile\">\r\n            <img src=\"assets/icon/user.png\" >\r\n          </li>\r\n        </ul>\r\n      </ion-button>\r\n    </ion-buttons> -->\r\n    <ion-title class=\"ios title-default hydrated\">Test History</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n    <ion-col size=\"12\" class=\"header-arrow padding-0\">\r\n      <div>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <div class=\"back\" (click)=\"getHome()\">\r\n                <img src=\"assets/back.svg\" alt=\"back\">\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <div class=\"menu-icon\">\r\n              <span class=\"shopping-cart\" (click)=\"GoToCart()\"><img src=\"assets/shopping-cart.svg\" alt=\"shopping\"><span\r\n                  class=\"cart-number\">{{cartCount}}</span></span>\r\n              <span class=\"user-icon\" slot=\"start\" (click)=\"GoToUserProfile()\">\r\n                <img src=\"assets/user-profile.png\" alt=\"user-icon\">\r\n              </span>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <div class=\"packages-header\">\r\n              <h2>Test History</h2>\r\n              <span>My Test History</span>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"margin-10 mt-16\">\r\n    <ion-col size=\"4\" class=\"padding-0\" >\r\n      <ion-list class=\"form-control-cart\" >\r\n        <ion-item lines=\"none\" style=\"border-radius: 8px;\">\r\n          <ion-label class=\"text-gray font-size-1\">Subjects</ion-label>\r\n          <ion-select value=\"\" okText=\"Ok\" (ionChange)=\"getTestPackages($event)\">\r\n            <ion-select-option *ngFor=\"let list of subjectList\" value=\"{{list.id}}\">{{list.subject_name}}</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n    <ion-col size=\"4\" class=\"padding-0\">\r\n      <ion-list class=\"form-control-cart\">\r\n        <ion-item lines=\"none\" style=\"border-radius: 8px;\">\r\n          <ion-label class=\"text-gray font-size-1\">Packages</ion-label>\r\n          <ion-select value=\"\" okText=\"Ok\" (ionChange)=\"getTestData($event)\">\r\n            <ion-select-option *ngFor=\"let list of packageList\" value=\"{{list.id}}\">{{list.name}}</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n    <ion-col size=\"4\" class=\"padding-0\">\r\n      <ion-list class=\"form-control-cart\">\r\n        <ion-item lines=\"none\" style=\"border-radius: 8px;\">\r\n          <ion-label class=\"text-gray font-size-1\">Test</ion-label>\r\n          <ion-select value=\"\" okText=\"Ok\" (ionChange)=\"selectTest($event)\">\r\n            <ion-select-option *ngFor=\"let tests of package_test\" value=\"{{tests.id}}\">{{tests.title}}</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row class=\"margin-10 mt-16 space-right-left\">\r\n\r\n    <ion-col size=\"4\" class=\"padding-0\">\r\n      <ion-list class=\"form-control-cart\">\r\n        <ion-item lines=\"none\" style=\"border-radius: 8px;\">\r\n          <ion-label class=\"text-gray font-size-1\">Status</ion-label>\r\n          <ion-select value=\"\" okText=\"Ok\" (ionChange)=\"selectStatus($event)\">\r\n            <ion-select-option value=\"submitted\">Submitted</ion-select-option>\r\n            <ion-select-option value=\"completed\">Completed</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"8\">\r\n      <div class=\"test-history-btn\">\r\n        <button ion-button type=\"submit\" (click)=\"submit_filter()\" >Search</button>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"mt-16\" *ngIf=\"testHistory != null\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"tab-list-view\" *ngFor = \"let test of testHistory\">\r\n        <ion-row >\r\n          <ion-col size=\"8\" class=\"padding-0\">\r\n            <div class=\"tab-list-content\">\r\n              <h3>\r\n                <strong>Package </strong>\r\n                 - {{test.package.name}}\r\n              </h3>\r\n              <h3>\r\n                <strong>Test Name </strong> - \r\n                {{test.examtest.title}}\r\n              </h3>\r\n              <h3>\r\n                <strong>Test Marks </strong> \r\n                - {{test.examtest.total_marks}}\r\n              </h3>\r\n              <!-- <h3>\r\n                <strong>Marks Obtained </strong>\r\n                 = 70%\r\n              </h3> -->\r\n              <a (click)=\"viewDetails(test.id,test.test_id,test.obtained_marks,test.package.price)\">View Details</a>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"4\" class=\"padding-0\">\r\n           <div class=\"dates-sam\">\r\n             <!-- <img src=\"assets/icon/date.png\"> -->\r\n             <strong>Ended at - </strong> \r\n             <span>{{test.updated_at}}</span>\r\n\r\n           </div>\r\n           <div class=\"complete-btn-main\">\r\n            <button ion-button full class=\"{{test.status == 'completed' ? 'complete-btn' : 'submited-btn'}} \">{{test.status == 'completed' ? 'Completed' : 'Submitted'}}</button>\r\n          </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"mt-16\" *ngIf=\"testHistory == null\">\r\n    <!-- <p class=\"not_found\"><b>Not Result Found</b> </p> -->\r\n    <ion-col size=\"12\" class=\"padding-0\" >\r\n      <div class=\"queries-content\">\r\n        <!-- <span>No Packages Found!!</span> -->\r\n        <img src=\"assets/icon/no-result.png\" alt=\"no-packages-found\">\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n\r\n</ion-content>\r\n");

/***/ }),

/***/ "Crn+":
/*!***********************************************************!*\
  !*** ./src/app/pages/test-history/test-history.module.ts ***!
  \***********************************************************/
/*! exports provided: TestHistoryPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestHistoryPageModule", function() { return TestHistoryPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _test_history_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./test-history-routing.module */ "/KFI");
/* harmony import */ var _test_history_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./test-history.page */ "GrbS");







let TestHistoryPageModule = class TestHistoryPageModule {
};
TestHistoryPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _test_history_routing_module__WEBPACK_IMPORTED_MODULE_5__["TestHistoryPageRoutingModule"]
        ],
        declarations: [_test_history_page__WEBPACK_IMPORTED_MODULE_6__["TestHistoryPage"]]
    })
], TestHistoryPageModule);



/***/ }),

/***/ "GrbS":
/*!*********************************************************!*\
  !*** ./src/app/pages/test-history/test-history.page.ts ***!
  \*********************************************************/
/*! exports provided: TestHistoryPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestHistoryPage", function() { return TestHistoryPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_test_history_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./test-history.page.html */ "BK5u");
/* harmony import */ var _test_history_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./test-history.page.scss */ "25Uy");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");











let TestHistoryPage = class TestHistoryPage {
    constructor(router, http, httpService, storageService, navCtrl, uihelper) {
        this.router = router;
        this.http = http;
        this.httpService = httpService;
        this.storageService = storageService;
        this.navCtrl = navCtrl;
        this.uihelper = uihelper;
        this.url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__["Constant"].apiUrl;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpHeaders"]({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
        });
        this.options = { headers: this.headers };
        this.cartCount = localStorage.getItem('cartCount');
    }
    ngOnInit() {
        try {
            this.router.events.subscribe((event) => {
                this.selfPath = '';
                if (event && event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_8__["NavigationEnd"] && event.url) {
                    this.selfPath = event.url + '/test-history';
                }
                this.selfPath = this.router.routerState.snapshot.url;
                // console.log(this.getStringBeforeSubstring(this.selfPath, 'viptips-details'));
                this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'test-history') + 'test-history';
            });
        }
        catch (e) {
            console.log(e);
        }
    }
    getStringBeforeSubstring(parentString, substring) {
        return parentString.substring(0, parentString.indexOf(substring));
    }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // this.cartCount = localStorage.getItem('cartCount')
            this.uihelper.ShowSpinner();
            this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__["Constant"].AUTH).then(res => {
                this.user_id = res.user_details.id;
                //   this.http.get(this.url + 'cart-count/' + this.user_id, this.options).subscribe((res)=>{   
                //     this.cartCount = res['payload']
                // });
            });
            this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__["Constant"].AUTH).then((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                this.userData = res.user_details;
                this.user_id = res.user_details.id;
                this.course_id = res.user_details.course_id;
                this.httpService.afterLoginGet("cart_count", res.user_details.id).subscribe((res) => {
                    this.cartCount = res['payload'].cart_count;
                });
                let data = {
                    user_id: this.user_id,
                    course_id: this.course_id
                };
                this.httpService.afterLoginPost('test-history', data).subscribe((res) => {
                    this.uihelper.HideSpinner();
                    console.log('test history', res);
                    this.testHistory = res.payload.all_test;
                    this.subjectList = res.payload.subjects;
                    this.testHistory.forEach((value, i) => {
                        this.testHistory[i]['obtained_marks'] = parseFloat(value.mcq_marks) + parseFloat(value.theory_marks);
                        this.testHistory[i]['updated_at'] = value.updated_at.substring(0, 11);
                    });
                }, (err) => {
                    this.uihelper.HideSpinner();
                    console.log(err);
                    this.uihelper.ShowAlert('Test History', 'Not Found');
                });
            }));
        });
    }
    getTestData(event) {
        this.package_id = event.target.value;
        this.http.get(this.url + 'package-tests/' + this.package_id, this.options).subscribe((res) => {
            this.package_test = res['payload'];
            // console.log('package test',this.package_test);
        });
    }
    getTestPackages(event) {
        this.subject_id = event.target.value;
        let subject_data = {
            id: this.user_id,
            subject_id: this.subject_id
        };
        this.httpService.afterLoginPost('get_test_taken_packages', subject_data).subscribe((res) => {
            console.log('subject wise packages', res);
            this.packageList = res.payload.packages;
        });
    }
    selectTest(event) {
        this.test_id = event.target.value;
    }
    selectStatus(event) {
        this.status = event.target.value;
    }
    submit_filter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.uihelper.ShowSpinner();
            let form_data = new FormData();
            form_data.append('subject_id', this.subject_id ? this.subject_id : '');
            form_data.append('package_id', this.package_id ? this.package_id : '');
            form_data.append('test_id', this.test_id ? this.test_id : '');
            form_data.append('status', this.status ? this.status : '');
            form_data.append('user_id', this.user_id);
            form_data.append('course_id', localStorage.getItem('course_id'));
            this.httpService.afterLoginPost('test-history', form_data).subscribe((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                // console.log('after search filter',res); 
                this.uihelper.HideSpinner();
                this.testHistory = res.payload.all_test;
                this.subjectList = res.payload.subjects;
            }), (err) => {
                this.uihelper.HideSpinner();
                this.uihelper.ShowAlert('Test History', 'Not Found');
                console.log('error', err);
            });
        });
    }
    viewDetails(exam_id, test_id, obtained_marks, price) {
        console.log('price', price);
        this.router.navigate([this.selfPath + '/test-evaluation'], { queryParams: { exam_id: exam_id, test_id: test_id, obtained_marks: obtained_marks, price: price } });
    }
    getHome() {
        this.navCtrl.pop();
    }
    GoToCart() {
        this.router.navigate([this.selfPath + "/cart"]);
    }
    GoToUserProfile() {
        this.router.navigate([this.selfPath + "/user-profile"]);
    }
};
TestHistoryPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_5__["HttpService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_4__["StorageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_10__["UIHelper"] }
];
TestHistoryPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-test-history',
        template: _raw_loader_test_history_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_test_history_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TestHistoryPage);



/***/ })

}]);
//# sourceMappingURL=test-history-test-history-module.js.map