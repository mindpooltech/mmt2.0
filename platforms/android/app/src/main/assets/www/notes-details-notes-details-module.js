(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["notes-details-notes-details-module"],{

/***/ "TGPP":
/*!***********************************************************!*\
  !*** ./src/app/pages/notes-details/notes-details.page.ts ***!
  \***********************************************************/
/*! exports provided: NotesDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotesDetailsPage", function() { return NotesDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_notes_details_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./notes-details.page.html */ "bWVC");
/* harmony import */ var _notes_details_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./notes-details.page.scss */ "f9PN");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var _notes_popover_notes_popover_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../notes-popover/notes-popover.page */ "Uhq3");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "m/P+");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "B7Rs");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/file/ngx */ "FAH8");
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "WOgW");


















let NotesDetailsPage = class NotesDetailsPage {
    constructor(router, http, httpService, storageService, navCtrl, uihelper, iab, popovercontroller, platform, modalController, transfer, file, androidPermissions) {
        this.router = router;
        this.http = http;
        this.httpService = httpService;
        this.storageService = storageService;
        this.navCtrl = navCtrl;
        this.uihelper = uihelper;
        this.iab = iab;
        this.popovercontroller = popovercontroller;
        this.platform = platform;
        this.modalController = modalController;
        this.transfer = transfer;
        this.file = file;
        this.androidPermissions = androidPermissions;
        this.course_id = {
            course_id: localStorage.getItem('course_id')
        };
        this.fileTransfer = this.transfer.create();
    }
    ngOnInit() {
        try {
            this.router.events.subscribe((event) => {
                this.selfPath = '';
                if (event && event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationEnd"] && event.url) {
                    this.selfPath = event.url + '/notes-details';
                }
                this.selfPath = this.router.routerState.snapshot.url;
                this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'notes-details') + 'notes-details';
            });
        }
        catch (e) {
            console.log(e);
        }
    }
    getStringBeforeSubstring(parentString, substring) {
        return parentString.substring(0, parentString.indexOf(substring));
    }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.uihelper.ShowAlert('', 'Please select Subject.');
            this.uihelper.ShowSpinner();
            this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_11__["Constant"].AUTH).then(res => {
                this.userData = res.user_details;
                this.course_name = this.userData.course_name;
                this.institute_id = res.user_details.institute_id;
                this.user_id = res.user_details.id;
                this.httpService.afterLoginGet("cart_count", res.user_details.id).subscribe((res) => {
                    this.cartCount = res['payload'].cart_count;
                });
                this.uihelper.HideSpinner();
            });
            this.httpService.afterLoginPost('subject-list', this.course_id).subscribe((res) => {
                this.subjectList = res.payload;
                this.uihelper.HideSpinner();
                // console.log('subjectList',this.subjectList)
            });
        });
    }
    getNotes(event) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.uihelper.ShowSpinner();
            let data = {
                institute_id: this.institute_id,
                course_id: localStorage.getItem('course_id'),
                subject_id: event.target.value
            };
            this.httpService.afterLoginPost('subject-wise-notes', data).subscribe((res) => {
                // console.log('notes res',res)
                this.note_list = res.payload.notes;
                this.document_path = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_11__["Constant"].adminUrl + res.payload.document_path;
                this.uihelper.HideSpinner();
            }, (err) => {
                this.uihelper.HideSpinner();
                this.uihelper.ShowAlert('', "Not Found");
                this.note_list = [];
                console.log('error', err);
            });
        });
    }
    openPopover(notes_id, ev) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // console.log('notes_id',notes_id)
            const popover = yield this.popovercontroller.create({
                component: _notes_popover_notes_popover_page__WEBPACK_IMPORTED_MODULE_8__["NotesPopoverPage"],
                // cssClass: 'my-custom-class',
                componentProps: {
                    "notes_id": notes_id,
                },
            });
            return yield popover.present();
        });
    }
    goBack() {
        this.router.navigate(['/tabs']);
    }
    GoToCart() {
        this.router.navigate([this.selfPath + "/cart"]);
    }
    GoToUserProfile() {
        this.router.navigate([this.selfPath + "/user-profile"]);
    }
    openNotes(document) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const url = this.document_path + '/' + document;
            // this.note_url = this.document_path +'/'+ document
            // console.log('note_url',this.note_url);
            this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
                .then(status => {
                if (status.hasPermission) {
                    this.fileTransfer.download(url, this.file.externalRootDirectory + '/Download/' + 'my_notes.pdf').then((entry) => {
                        console.log('download complete 1: ' + entry.toURL());
                        this.uihelper.presentToast('Download complete..please check your download folder');
                    }, (error) => {
                        // handle error
                        console.log(error);
                    });
                }
                else {
                    this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
                        .then(status => {
                        if (status.hasPermission) {
                            this.fileTransfer.download(url, this.file.externalRootDirectory + '/Download/' + 'my_notes.pdf').then((entry) => {
                                console.log('download complete 2: ' + entry.toURL());
                                this.uihelper.presentToast('Download complete..please check your download folder');
                            }, (error) => {
                                // handle error
                                console.log(error);
                            });
                        }
                    });
                }
            });
        });
    }
};
NotesDetailsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClient"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_6__["HttpService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_7__["StorageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_10__["UIHelper"] },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_12__["InAppBrowser"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] },
    { type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_13__["FileTransfer"] },
    { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_14__["File"] },
    { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_15__["AndroidPermissions"] }
];
NotesDetailsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-notes-details',
        template: _raw_loader_notes_details_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_notes_details_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], NotesDetailsPage);



/***/ }),

/***/ "TpMJ":
/*!*************************************************************!*\
  !*** ./src/app/pages/notes-details/notes-details.module.ts ***!
  \*************************************************************/
/*! exports provided: NotesDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotesDetailsPageModule", function() { return NotesDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _notes_details_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./notes-details-routing.module */ "XdoU");
/* harmony import */ var _notes_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notes-details.page */ "TGPP");
/* harmony import */ var _notes_popover_notes_popover_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../notes-popover/notes-popover.page */ "Uhq3");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "m/P+");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "B7Rs");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/file/ngx */ "FAH8");
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "WOgW");












let NotesDetailsPageModule = class NotesDetailsPageModule {
};
NotesDetailsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _notes_details_routing_module__WEBPACK_IMPORTED_MODULE_5__["NotesDetailsPageRoutingModule"],
        ],
        providers: [_ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_8__["InAppBrowser"], _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_9__["FileTransfer"], _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_11__["AndroidPermissions"]],
        declarations: [_notes_details_page__WEBPACK_IMPORTED_MODULE_6__["NotesDetailsPage"], _notes_popover_notes_popover_page__WEBPACK_IMPORTED_MODULE_7__["NotesPopoverPage"]]
    })
], NotesDetailsPageModule);



/***/ }),

/***/ "Uhq3":
/*!***********************************************************!*\
  !*** ./src/app/pages/notes-popover/notes-popover.page.ts ***!
  \***********************************************************/
/*! exports provided: NotesPopoverPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotesPopoverPage", function() { return NotesPopoverPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_notes_popover_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./notes-popover.page.html */ "ZEEN");
/* harmony import */ var _notes_popover_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./notes-popover.page.scss */ "zn3K");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");









let NotesPopoverPage = class NotesPopoverPage {
    constructor(navParams, popovercontroller, http, httpService, uihelper) {
        this.navParams = navParams;
        this.popovercontroller = popovercontroller;
        this.http = http;
        this.httpService = httpService;
        this.uihelper = uihelper;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
        });
        this.url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_7__["Constant"].apiUrl;
        this.options = { headers: this.headers };
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.uihelper.ShowSpinner();
            this.notes_id = this.navParams.data.notes_id;
            this.http.get(this.url + 'notes-chapters/' + this.notes_id, this.options).subscribe((res) => {
                this.chapter_list = res['payload'].chapters;
                this.uihelper.HideSpinner();
            });
        });
    }
};
NotesPopoverPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavParams"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["PopoverController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_6__["HttpService"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_8__["UIHelper"] }
];
NotesPopoverPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-notes-popover',
        template: _raw_loader_notes_popover_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_notes_popover_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], NotesPopoverPage);



/***/ }),

/***/ "XdoU":
/*!*********************************************************************!*\
  !*** ./src/app/pages/notes-details/notes-details-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: NotesDetailsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotesDetailsPageRoutingModule", function() { return NotesDetailsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _notes_details_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./notes-details.page */ "TGPP");




const routes = [
    {
        path: '',
        component: _notes_details_page__WEBPACK_IMPORTED_MODULE_3__["NotesDetailsPage"]
    }
];
let NotesDetailsPageRoutingModule = class NotesDetailsPageRoutingModule {
};
NotesDetailsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NotesDetailsPageRoutingModule);



/***/ }),

/***/ "ZEEN":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/notes-popover/notes-popover.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>notes-popover</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content> -->\r\n\r\n<div class=\"popover-scroll\">\r\n  <div class=\"popover-main-holder\">\r\n    <h4 class=\"text-center\">Chapter List</h4>\r\n    <ul class=\"list\" *ngFor=\"let chapters of chapter_list\">\r\n      <li class=\"item\">{{chapters.title}}</li>\r\n    </ul>\r\n  </div>\r\n  </div>\r\n  ");

/***/ }),

/***/ "bWVC":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/notes-details/notes-details.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-buttons slot=\"primary\">\r\n      <ion-button>\r\n        <ul class=\"shopping-icon\">\r\n          <!-- <li (click)=\"GoToCart()\">\r\n            <img src=\"assets/icon/my-cart-1.png\" class=\"cart\">\r\n            <span class=\"cart_count\">{{cartCount ? cartCount : '0'}}</span>\r\n          </li> -->\r\n          <!-- <li>\r\n            <img src=\"assets/icon/notification.png\" class=\"cart\">\r\n          </li> -->\r\n          <li routerLink=\"/user-profile\">\r\n            <img src=\"assets/icon/user.png\" >\r\n          </li>\r\n        </ul>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ios title-default hydrated\">Notes</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n    <ion-col size=\"12\" class=\"header-arrow padding-0\">\r\n      <div>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <div class=\"back\" (click)=\"goBack()\">\r\n                <img src=\"assets/back.svg\" alt=\"back\">\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <div class=\"menu-icon\">\r\n              <span class=\"shopping-cart\" (click)=\"GoToCart()\"><img src=\"assets/shopping-cart.svg\" alt=\"shopping\"><span\r\n                  class=\"cart-number\">{{cartCount}}</span></span>\r\n              <span class=\"user-icon\" slot=\"start\" (click)=\"GoToUserProfile()\">\r\n                <img src=\"assets/user-profile.png\" alt=\"user-icon\">\r\n              </span>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <div class=\"packages-header\">\r\n              <h2>Notes</h2>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"mt-16\">\r\n    <ion-col size=\"12\" class=\"course-name-heading\">\r\n        <ion-label>{{course_name}}</ion-label>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-list class=\"border space-m10 outer-border\">\r\n        <ion-item lines=\"none\">\r\n          <ion-label class=\"m-0 choose-subject-new\">Choose Subject</ion-label>\r\n          <ion-select (ionChange)=\"getNotes($event)\">\r\n            <ion-select-option *ngFor=\"let list of subjectList\" [value]=\"list.id\">{{list.subject_name}}</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n    </ion-list>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0\" *ngFor=\"let notes of note_list, let i = index\">\r\n      <div class=\"tab-list-view\" >\r\n        <ion-row >\r\n          <ion-col size=\"4\" class=\"padding-0\">\r\n            <div class=\"tab-list-content\">\r\n              <h6 class=\"m-0\">Desription</h6>\r\n              <span class=\"m-0 d-block\">{{notes.description}}</span>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"4\" class=\"padding-0\">\r\n            <div class=\"test-count text-center\">\r\n              <a (click)=\"openPopover(notes.id,$event)\" value=\"notes.id\">View chapters</a>\r\n             \r\n            </div>\r\n          </ion-col>          \r\n          <ion-col size=\"4\" class=\"padding-0\">\r\n            <div class=\"test-cart text-right\">\r\n              <!-- <button ion-button full class=\"cart-btn\">Add to Cart</button> -->\r\n              <!-- <a full class=\"cart-btn\" target=\"_blank\" href=\"{{document_path + '/' + notes.document_name}}\">Download</a> -->\r\n              <a full class=\"cart-btn\" target=\"_blank\" (click)=\"openNotes(notes.document_name)\" >View</a>\r\n            </div>\r\n          </ion-col>\r\n\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>");

/***/ }),

/***/ "f9PN":
/*!*************************************************************!*\
  !*** ./src/app/pages/notes-details/notes-details.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("::slotted(ion-label) {\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n\n.user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.back img {\n  width: 40px;\n}\n\n.back {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.header-arrow {\n  padding: 20px 15px !important;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 120px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n}\n\n.packages-header h2 {\n  margin: 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n  position: relative;\n  top: -15px;\n}\n\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\nion-label {\n  margin: 0;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n}\n\n.course-name-heading ion-label {\n  color: #305f72;\n  font-weight: 500;\n  font-size: 18px;\n  margin: 10px 15px;\n}\n\n.padding-0 {\n  padding: 0;\n}\n\n.mt-16 {\n  margin-top: 16px;\n}\n\n.m-0 {\n  margin: 0;\n}\n\n.text-white {\n  color: #fff;\n}\n\n.outer-border {\n  font-size: 14px !important;\n  margin: 10px 20px;\n  --color: rgb(44, 44, 44);\n  font-weight: 400;\n  padding: 0;\n  border: 1px solid #b1bcd4;\n  border-radius: 10px;\n  color: #8a959e;\n}\n\n.choose-subject-new {\n  font-size: 16px;\n  padding: 10px;\n}\n\n.tab-list-view {\n  border: 1px solid #b1bcd440;\n  box-shadow: 0px 1px 12px -4px #37242442;\n  border-radius: 20px;\n  padding: 15px 15px;\n  margin: 10px 20px;\n}\n\n.tab-list-content span {\n  display: block;\n  font-size: 14px;\n  font-weight: 400;\n  text-transform: capitalize;\n}\n\n.tab-list-content a {\n  font-size: 13px;\n  font-weight: 600;\n  color: #020551;\n}\n\n.test-count a {\n  color: #f27376;\n  font-size: 15px;\n  text-align: center;\n  margin: 0 auto;\n  display: block;\n  font-weight: 500;\n}\n\n.tab-list-content h6 {\n  color: #305f72;\n  text-transform: capitalize;\n  padding: 3px 0;\n}\n\n.test-count h6 {\n  font-size: 14px;\n  font-weight: 500;\n  margin-bottom: 5px;\n}\n\n.test-count span {\n  font-size: 18px;\n}\n\n.test-cart {\n  font-size: 15px;\n  font-weight: 400;\n}\n\n.text-right {\n  text-align: right;\n}\n\n.cart-btn {\n  padding: 8px 12px;\n  color: #ffffff;\n  font-weight: 400;\n  font-size: 15px;\n  border-radius: 50px !important;\n  box-shadow: none !important;\n  background: #51C3D1;\n  margin-top: 15px;\n}\n\n.test-count {\n  padding-left: 7px;\n}\n\n.courses-tab h6 {\n  font-weight: 400;\n  font-size: 14px;\n}\n\n.courses-tab {\n  padding-left: 10px;\n}\n\nion-tab-button {\n  --background-color:transparent;\n}\n\nion-tab-bar,\nion-tab-button {\n  background: rgba(0, 0, 0, 0);\n  color: #ffffff;\n  margin: -15px 0;\n}\n\n.active:after {\n  content: \"\";\n  width: 30px;\n  height: 2px;\n  display: block;\n  background: #fff;\n}\n\n.text-left {\n  text-align: left;\n}\n\nion-label {\n  font-weight: 400;\n}\n\nbutton:focus {\n  outline: none;\n}\n\nul.shopping-icon {\n  list-style-type: none;\n}\n\n.menu-icon button {\n  padding-top: 5px;\n}\n\n.menu-icon button:focus {\n  outline: none;\n}\n\n.nav-title h6 {\n  margin: 10px 0 0 0;\n  color: #fff;\n  font-weight: 400;\n  text-align: center;\n}\n\n.cart-btn {\n  border-radius: 5px !important;\n  box-shadow: none !important;\n  --background-activated: #3f5c9b !important;\n  --background-focused: #3f5c9b !important;\n  --background-hover: #3f5c9b !important;\n  background: #3f5c9b !important;\n  font-size: 13px;\n  font-family: \"Lato\";\n  font-weight: 500;\n  display: block;\n  text-align: center;\n  text-decoration: none;\n  width: 90px;\n  margin: 5px auto;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxub3Rlcy1kZXRhaWxzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQWM7RUFDZCxrQkFBaUI7QUFDckI7O0FBQ0E7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztBQUVmOztBQUFBO0VBQ0ksV0FBVztBQUdmOztBQURBO0VBQ0ksV0FBVztBQUlmOztBQUZBO0VBQ0ksV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsU0FBUztBQUtiOztBQUhBO0VBQ0ksaUJBQWlCO0FBTXJCOztBQUpBO0VBQ0ksa0JBQWtCO0FBT3RCOztBQUxBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIsWUFBWTtFQUNaLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZUFBZTtFQUNmLFlBQVk7QUFRaEI7O0FBTkE7RUFDSSxhQUFhO0FBU2pCOztBQVBBO0VBQ0ksNkJBQTZCO0FBVWpDOztBQVJBO0VBQ0ksNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QixhQUFhO0VBQ2IsV0FBVztFQUNYLDJCQUEyQjtFQUMzQixrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLDRCQUE0QjtBQVdoQzs7QUFUQTtFQUNJLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFVBQVU7QUFZZDs7QUFWQTtFQUNJLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxjQUFjO0VBQ2QsZ0JBQWdCO0FBYXBCOztBQVhBO0VBQ0ksU0FBUztBQWNiOztBQVpBO0VBQ0ksb0JBQWdCO0VBQ2hCLHdCQUFvQjtBQWV4Qjs7QUFiQTtFQUNJLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGlCQUFpQjtBQWdCckI7O0FBZEE7RUFDSSxVQUFTO0FBaUJiOztBQWZBO0VBQ0ksZ0JBQWU7QUFrQm5COztBQWhCQTtFQUNJLFNBQVM7QUFtQmI7O0FBakJBO0VBQ0ksV0FBVTtBQW9CZDs7QUFsQkE7RUFDSSwwQkFBMEI7RUFDMUIsaUJBQWlCO0VBQ2pCLHdCQUFRO0VBQ1IsZ0JBQWdCO0VBQ2hCLFVBQVU7RUFDVix5QkFBeUI7RUFDekIsbUJBQW1CO0VBQ25CLGNBQWM7QUFxQmxCOztBQW5CQTtFQUNJLGVBQWU7RUFDZixhQUFhO0FBc0JqQjs7QUFwQkE7RUFDSSwyQkFBMkI7RUFDM0IsdUNBQXVDO0VBQ3ZDLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsaUJBQWlCO0FBdUJyQjs7QUFyQkE7RUFDSSxjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQiwwQkFBMEI7QUF3QjlCOztBQXRCQTtFQUNJLGVBQWM7RUFDZCxnQkFBZTtFQUNmLGNBQWE7QUF5QmpCOztBQXZCQTtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxjQUFjO0VBQ2QsZ0JBQWdCO0FBMEJwQjs7QUF4QkE7RUFDSSxjQUFjO0VBQ2QsMEJBQTBCO0VBQzFCLGNBQWM7QUEyQmxCOztBQXpCQTtFQUNJLGVBQWM7RUFDZCxnQkFBZTtFQUNmLGtCQUFrQjtBQTRCdEI7O0FBMUJBO0VBQ0ksZUFBYztBQTZCbEI7O0FBM0JBO0VBQ0ksZUFBYztFQUNkLGdCQUFnQjtBQThCcEI7O0FBNUJBO0VBQ0ksaUJBQWlCO0FBK0JyQjs7QUE3QkE7RUFDSSxpQkFBZ0I7RUFDaEIsY0FBYTtFQUNiLGdCQUFnQjtFQUNoQixlQUFjO0VBQ2QsOEJBQTRCO0VBQzVCLDJCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsZ0JBQWU7QUFnQ25COztBQTlCQTtFQUNJLGlCQUFnQjtBQWlDcEI7O0FBL0JBO0VBQ0ksZ0JBQWU7RUFDZixlQUFjO0FBa0NsQjs7QUFoQ0E7RUFDSSxrQkFBaUI7QUFtQ3JCOztBQWpDQTtFQUNJLDhCQUFtQjtBQW9DdkI7O0FBbENBOztFQUVJLDRCQUF5QjtFQUN6QixjQUFjO0VBQ2QsZUFBYztBQXFDbEI7O0FBbkNBO0VBQ0ksV0FBVTtFQUNWLFdBQVU7RUFDVixXQUFVO0VBQ1YsY0FBYztFQUNkLGdCQUFnQjtBQXNDcEI7O0FBcENBO0VBQ0ksZ0JBQWdCO0FBdUNwQjs7QUFyQ0E7RUFDSSxnQkFBZTtBQXdDbkI7O0FBdENBO0VBQ0ksYUFBWTtBQXlDaEI7O0FBdkNBO0VBQ0kscUJBQXFCO0FBMEN6Qjs7QUF4Q0E7RUFDSSxnQkFBZ0I7QUEyQ3BCOztBQXpDQTtFQUNJLGFBQWE7QUE0Q2pCOztBQTFDQTtFQUNJLGtCQUFrQjtFQUNsQixXQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtBQTZDdEI7O0FBM0NBO0VBQ0ksNkJBQTZCO0VBQzdCLDJCQUEyQjtFQUMzQiwwQ0FBdUI7RUFDdkIsd0NBQXFCO0VBQ3JCLHNDQUFtQjtFQUNuQiw4QkFBOEI7RUFDOUIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsV0FBVztFQUNYLGdCQUFnQjtBQThDcEIiLCJmaWxlIjoibm90ZXMtZGV0YWlscy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6OnNsb3R0ZWQoaW9uLWxhYmVsKSB7XHJcbiAgICBtYXJnaW4tdG9wOjBweDtcclxuICAgIG1hcmdpbi1ib3R0b206MHB4O1xyXG59XHJcbi51c2VyLWljb24gaW1nIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG59XHJcbi5iYWNrIGltZyB7XHJcbiAgICB3aWR0aDogNDBweDtcclxufVxyXG4uYmFjayB7XHJcbiAgICBmbG9hdDogbGVmdDtcclxufVxyXG4uc2hvcHBpbmctY2FydCBpbWcge1xyXG4gICAgd2lkdGg6IDMwcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IC04cHg7XHJcbn1cclxuLm1lbnUtaWNvbiB7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG4uc2hvcHBpbmctY2FydCB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuLmNhcnQtbnVtYmVyIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAxNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmYmIwMDtcclxuICAgIGhlaWdodDogMjBweDtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGNvbG9yOiAjM2Y1YzliO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgcGFkZGluZzogMXB4O1xyXG59XHJcbmlvbi1oZWFkZXIge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG4uaGVhZGVyLWFycm93IHtcclxuICAgIHBhZGRpbmc6IDIwcHggMTVweCAhaW1wb3J0YW50O1xyXG59XHJcbi50b3AtYmcge1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBoZWlnaHQ6IDEyMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMzBweCAzMHB4O1xyXG59XHJcbi5wYWNrYWdlcy1oZWFkZXIgaDJ7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLTE1cHg7XHJcbn1cclxuLnBhY2thZ2VzLWhlYWRlciBzcGFue1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgY29sb3I6ICMzMDVGNzI7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuaW9uLWxhYmVsIHtcclxuICAgIG1hcmdpbjogMDtcclxufVxyXG5pb24taXRlbSB7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcclxuICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcclxufVxyXG4uY291cnNlLW5hbWUtaGVhZGluZyBpb24tbGFiZWx7XHJcbiAgICBjb2xvcjogIzMwNWY3MjtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBtYXJnaW46IDEwcHggMTVweDtcclxufVxyXG4ucGFkZGluZy0we1xyXG4gICAgcGFkZGluZzowO1xyXG59XHJcbi5tdC0xNntcclxuICAgIG1hcmdpbi10b3A6MTZweDtcclxufVxyXG4ubS0we1xyXG4gICAgbWFyZ2luOiAwO1xyXG59XHJcbi50ZXh0LXdoaXRle1xyXG4gICAgY29sb3I6I2ZmZjtcclxufVxyXG4ub3V0ZXItYm9yZGVye1xyXG4gICAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBtYXJnaW46IDEwcHggMjBweDtcclxuICAgIC0tY29sb3I6IHJnYig0NCwgNDQsIDQ0KTtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2IxYmNkNDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICBjb2xvcjogIzhhOTU5ZTtcclxufVxyXG4uY2hvb3NlLXN1YmplY3QtbmV3e1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxufVxyXG4udGFiLWxpc3Qtdmlld3tcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNiMWJjZDQ0MDtcclxuICAgIGJveC1zaGFkb3c6IDBweCAxcHggMTJweCAtNHB4ICMzNzI0MjQ0MjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICBwYWRkaW5nOiAxNXB4IDE1cHg7XHJcbiAgICBtYXJnaW46IDEwcHggMjBweDtcclxufVxyXG4udGFiLWxpc3QtY29udGVudCBzcGFue1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbn1cclxuLnRhYi1saXN0LWNvbnRlbnQgYXtcclxuICAgIGZvbnQtc2l6ZToxM3B4O1xyXG4gICAgZm9udC13ZWlnaHQ6NjAwO1xyXG4gICAgY29sb3I6IzAyMDU1MTtcclxufVxyXG4udGVzdC1jb3VudCBhe1xyXG4gICAgY29sb3I6ICNmMjczNzY7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG4udGFiLWxpc3QtY29udGVudCBoNntcclxuICAgIGNvbG9yOiAjMzA1ZjcyO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICBwYWRkaW5nOiAzcHggMDtcclxufVxyXG4udGVzdC1jb3VudCBoNntcclxuICAgIGZvbnQtc2l6ZToxNHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6NTAwO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG59XHJcbi50ZXN0LWNvdW50IHNwYW57XHJcbiAgICBmb250LXNpemU6MThweDtcclxufVxyXG4udGVzdC1jYXJ0e1xyXG4gICAgZm9udC1zaXplOjE1cHg7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcbi50ZXh0LXJpZ2h0e1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuLmNhcnQtYnRue1xyXG4gICAgcGFkZGluZzo4cHggMTJweDtcclxuICAgIGNvbG9yOiNmZmZmZmY7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgZm9udC1zaXplOjE1cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOjUwcHghaW1wb3J0YW50O1xyXG4gICAgYm94LXNoYWRvdzpub25lIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQ6ICM1MUMzRDE7XHJcbiAgICBtYXJnaW4tdG9wOjE1cHg7XHJcbn1cclxuLnRlc3QtY291bnR7XHJcbiAgICBwYWRkaW5nLWxlZnQ6N3B4O1xyXG59XHJcbi5jb3Vyc2VzLXRhYiBoNntcclxuICAgIGZvbnQtd2VpZ2h0OjQwMDtcclxuICAgIGZvbnQtc2l6ZToxNHB4O1xyXG59XHJcbi5jb3Vyc2VzLXRhYntcclxuICAgIHBhZGRpbmctbGVmdDoxMHB4O1xyXG59XHJcbmlvbi10YWItYnV0dG9ue1xyXG4gICAgLS1iYWNrZ3JvdW5kLWNvbG9yOnRyYW5zcGFyZW50O1xyXG59XHJcbmlvbi10YWItYmFyLFxyXG5pb24tdGFiLWJ1dHRvbiB7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsMCwwLDApO1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBtYXJnaW46LTE1cHggMDtcclxufVxyXG4uYWN0aXZlOmFmdGVye1xyXG4gICAgY29udGVudDpcIlwiO1xyXG4gICAgd2lkdGg6MzBweDtcclxuICAgIGhlaWdodDoycHg7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbn1cclxuLnRleHQtbGVmdHtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbn1cclxuaW9uLWxhYmVse1xyXG4gICAgZm9udC13ZWlnaHQ6NDAwO1xyXG59XHJcbmJ1dHRvbjpmb2N1c3tcclxuICAgIG91dGxpbmU6bm9uZTtcclxufVxyXG51bC5zaG9wcGluZy1pY29ue1xyXG4gICAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xyXG59XHJcbi5tZW51LWljb24gYnV0dG9ue1xyXG4gICAgcGFkZGluZy10b3A6IDVweDtcclxufVxyXG4ubWVudS1pY29uIGJ1dHRvbjpmb2N1c3tcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbn1cclxuLm5hdi10aXRsZSBoNntcclxuICAgIG1hcmdpbjogMTBweCAwIDAgMDtcclxuICAgIGNvbG9yOiNmZmY7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5jYXJ0LWJ0bntcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xyXG4gICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYmFja2dyb3VuZC1ob3ZlcjogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZDogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgZm9udC1mYW1pbHk6IFwiTGF0b1wiO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgd2lkdGg6IDkwcHg7XHJcbiAgICBtYXJnaW46IDVweCBhdXRvO1xyXG59Il19 */");

/***/ }),

/***/ "zn3K":
/*!*************************************************************!*\
  !*** ./src/app/pages/notes-popover/notes-popover.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".popover-main-holder {\n  min-height: 150px;\n}\n\n.popover-main-holder h4 {\n  margin: 0px 0px 10px 0px;\n  padding: 15px 0px 15px 0px;\n  font-size: 16px;\n  color: #ffffff;\n  font-weight: 500;\n  text-align: center;\n  border-bottom: 1px solid #d0d0d0;\n  background-color: #df6a6d;\n}\n\nul.list {\n  margin: 5px 0;\n}\n\n.list li {\n  padding: 7px 0 7px 0;\n  font-size: 15px;\n  color: #2c5869;\n}\n\n.popover-scroll {\n  overflow: scroll;\n  max-height: 300px;\n  min-width: 250px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxub3Rlcy1wb3BvdmVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQTtFQUVJLGlCQUFpQjtBQUhyQjs7QUFLQTtFQUNJLHdCQUF3QjtFQUN4QiwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGdDQUFnQztFQUNoQyx5QkFBeUI7QUFGN0I7O0FBSUE7RUFDSSxhQUFhO0FBRGpCOztBQUdBO0VBQ0ksb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZixjQUFjO0FBQWxCOztBQUdBO0VBQ0ksZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixnQkFBZ0I7QUFBcEIiLCJmaWxlIjoibm90ZXMtcG9wb3Zlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBhcHAtcG9wb3ZlcntcclxuLy8gICAgIC0td2lkdGg6MjUwcHghaW1wb3J0YW50O1xyXG4vLyB9XHJcbi5wb3BvdmVyLW1haW4taG9sZGVyIHtcclxuICAgIC8vIHBhZGRpbmc6MTBweCAwO1xyXG4gICAgbWluLWhlaWdodDogMTUwcHg7XHJcbn1cclxuLnBvcG92ZXItbWFpbi1ob2xkZXIgaDQge1xyXG4gICAgbWFyZ2luOiAwcHggMHB4IDEwcHggMHB4O1xyXG4gICAgcGFkZGluZzogMTVweCAwcHggMTVweCAwcHg7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2QwZDBkMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNkZjZhNmQ7XHJcbn1cclxudWwubGlzdCB7XHJcbiAgICBtYXJnaW46IDVweCAwO1xyXG59XHJcbi5saXN0IGxpIHtcclxuICAgIHBhZGRpbmc6IDdweCAwIDdweCAwO1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgY29sb3I6ICMyYzU4Njk7XHJcbn1cclxuXHJcbi5wb3BvdmVyLXNjcm9sbCB7XHJcbiAgICBvdmVyZmxvdzogc2Nyb2xsO1xyXG4gICAgbWF4LWhlaWdodDogMzAwcHg7XHJcbiAgICBtaW4td2lkdGg6IDI1MHB4O1xyXG59XHJcbiJdfQ== */");

/***/ })

}]);
//# sourceMappingURL=notes-details-notes-details-module.js.map