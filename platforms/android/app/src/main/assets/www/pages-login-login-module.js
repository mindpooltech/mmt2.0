(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "F4UR":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "aTZN");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "bP1B");







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "H+1c":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".intro-text p {\n  font-size: 16px;\n  line-height: 21px;\n  color: #000000;\n  font-weight: 500;\n  font-family: \"Lato\", sans-serif;\n  margin-bottom: 25px;\n  margin-top: 25px;\n}\n\nion-header {\n  display: none;\n  color: #4f689e;\n}\n\n.form-control {\n  border: 1px solid #b1bcd4;\n  border-radius: 10px;\n  margin-bottom: 22px;\n  font-size: 15px;\n  padding: 15px !important;\n  color: #585858;\n  --font-family: 500;\n}\n\nion-input {\n  --padding-top: 0px;\n  --padding-end: 0px;\n  --padding-bottom: 0px;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n  padding: 0 30px;\n}\n\n.space-m-50 {\n  margin: 0 82px;\n}\n\nion-button {\n  --background: #3f5c9b !important;\n  border-radius: 10px;\n}\n\n.forget-link {\n  background: transparent;\n  color: #3f5c9b;\n  font-size: 15px;\n  font-weight: 500;\n}\n\n.login-btn {\n  margin-bottom: 18px;\n  --background-activated: #3f5c9b !important;\n  --background-focused: #3f5c9b !important;\n  --background-hover: #3f5c9b !important;\n  --border-radius: 10px;\n  --min-height: 51px;\n  height: 51px;\n  text-align: center;\n  font-size: 18px;\n  font-weight: 400;\n}\n\n.no-account {\n  font-size: 15px;\n  color: #5b5b5b;\n  font-weight: 500;\n}\n\n.sign-up-text {\n  color: #f27376;\n  font-weight: 500;\n}\n\n.top-img {\n  width: 100%;\n  border-radius: 0 0 40px 40px;\n}\n\n.back-arrow ion-icon {\n  font-size: 22px;\n  color: #fff;\n}\n\n.menu-icon ion-icon {\n  font-size: 22px;\n  color: #fff;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxsb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsK0JBQStCO0VBQy9CLG1CQUFtQjtFQUNuQixnQkFBZ0I7QUFDcEI7O0FBQ0E7RUFDSSxhQUFhO0VBQ2IsY0FBYztBQUVsQjs7QUFBQTtFQUNJLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZix3QkFBd0I7RUFDeEIsY0FBYztFQUNkLGtCQUFjO0FBR2xCOztBQURBO0VBQ0ksa0JBQWM7RUFDZCxrQkFBYztFQUNkLHFCQUFpQjtBQUlyQjs7QUFGQTtFQUNJLG9CQUFnQjtFQUNoQix3QkFBb0I7RUFDcEIsZUFBZTtBQUtuQjs7QUFIQTtFQUNJLGNBQWM7QUFNbEI7O0FBSkE7RUFDSSxnQ0FBYTtFQUNiLG1CQUFtQjtBQU92Qjs7QUFMQTtFQUNJLHVCQUF1QjtFQUN2QixjQUFjO0VBQ2QsZUFBZTtFQUNmLGdCQUFnQjtBQVFwQjs7QUFOQTtFQUNJLG1CQUFtQjtFQUNuQiwwQ0FBdUI7RUFDdkIsd0NBQXFCO0VBQ3JCLHNDQUFtQjtFQUNuQixxQkFBZ0I7RUFDaEIsa0JBQWE7RUFDYixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixnQkFBZ0I7QUFTcEI7O0FBUEE7RUFDSSxlQUFlO0VBQ2YsY0FBYztFQUNkLGdCQUFnQjtBQVVwQjs7QUFSQTtFQUNJLGNBQWM7RUFDZCxnQkFBZ0I7QUFXcEI7O0FBVEE7RUFDSSxXQUFXO0VBQ1gsNEJBQTRCO0FBWWhDOztBQVZBO0VBQ0ksZUFBZTtFQUNmLFdBQVc7QUFhZjs7QUFYQTtFQUNJLGVBQWU7RUFDZixXQUFXO0FBY2YiLCJmaWxlIjoibG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmludHJvLXRleHQgcCB7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICBsaW5lLWhlaWdodDogMjFweDtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGZvbnQtZmFtaWx5OiBcIkxhdG9cIiwgc2Fucy1zZXJpZjtcclxuICAgIG1hcmdpbi1ib3R0b206IDI1cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAyNXB4O1xyXG59XHJcbmlvbi1oZWFkZXIge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICAgIGNvbG9yOiAjNGY2ODllO1xyXG59XHJcbi5mb3JtLWNvbnRyb2wge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2IxYmNkNDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMnB4O1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgcGFkZGluZzogMTVweCAhaW1wb3J0YW50O1xyXG4gICAgY29sb3I6ICM1ODU4NTg7XHJcbiAgICAtLWZvbnQtZmFtaWx5OiA1MDA7XHJcbn1cclxuaW9uLWlucHV0IHtcclxuICAgIC0tcGFkZGluZy10b3A6IDBweDtcclxuICAgIC0tcGFkZGluZy1lbmQ6IDBweDtcclxuICAgIC0tcGFkZGluZy1ib3R0b206IDBweDtcclxufVxyXG5pb24taXRlbSB7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcclxuICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcclxuICAgIHBhZGRpbmc6IDAgMzBweDtcclxufVxyXG4uc3BhY2UtbS01MCB7XHJcbiAgICBtYXJnaW46IDAgODJweDtcclxufVxyXG5pb24tYnV0dG9uIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG4uZm9yZ2V0LWxpbmsge1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICBjb2xvcjogIzNmNWM5YjtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuLmxvZ2luLWJ0biB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxOHB4O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYmFja2dyb3VuZC1ob3ZlcjogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgLS1ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgLS1taW4taGVpZ2h0OiA1MXB4O1xyXG4gICAgaGVpZ2h0OiA1MXB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxufVxyXG4ubm8tYWNjb3VudCB7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBjb2xvcjogIzViNWI1YjtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuLnNpZ24tdXAtdGV4dCB7XHJcbiAgICBjb2xvcjogI2YyNzM3NjtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuLnRvcC1pbWcge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgNDBweCA0MHB4O1xyXG59XHJcbi5iYWNrLWFycm93IGlvbi1pY29uIHtcclxuICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG59XHJcbi5tZW51LWljb24gaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAyMnB4O1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "TuYN":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>login</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <img src=\"assets/mmt-img.svg\" class=\"top-img\">\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"10\" offset=\"1\" class=\"padding-0 \">\r\n      <div class=\"intro-text text-center\">\r\n        <p>Hey There MMTian!\r\n          Start Off Your CA Journey with Make My Test! \r\n          Login to Your MakeMyTest Account.</p>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <form #loginForm=\"ngForm\" (ngSubmit)=\"loginAction()\">\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n        <ion-item lines=\"none\">\r\n          <ion-input autocomplete=\"off\" type=\"email\" placeholder=\"Enter Email Id\" class=\"form-control\" [(ngModel)]='postData.email' name=\"email\"></ion-input>\r\n        </ion-item>\r\n\r\n        <ion-item lines=\"none\">\r\n          <ion-input autocomplete=\"off\" type=\"password\" placeholder=\"Enter Password\" class=\"form-control\" [(ngModel)]='postData.password' name=\"password\"></ion-input>\r\n        </ion-item>\r\n\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"space-m-50\">\r\n        <ion-button expand=\"block\" class=\"login-btn\" type=\"submit\">Login</ion-button>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n</form>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0 text-center\">\r\n      <button ion-button clear class=\"forget-link\" routerLink='/forget-password'>Forgot Password?</button>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0 text-center\">\r\n     <p class=\"no-account\">Don't Have an Account?<span class=\"sign-up-text\" routerLink='/signup'>Sign Up</span></p>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n</ion-content>");

/***/ }),

/***/ "aTZN":
/*!*****************************************************!*\
  !*** ./src/app/pages/login/login-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "bP1B");




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ "bP1B":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./login.page.html */ "TuYN");
/* harmony import */ var _login_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.page.scss */ "H+1c");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");










let LoginPage = class LoginPage {
    constructor(router, authService, uihelper, navCtrl, storageService) {
        this.router = router;
        this.authService = authService;
        this.uihelper = uihelper;
        this.navCtrl = navCtrl;
        this.storageService = storageService;
        this.postData = {
            email: '',
            password: '',
        };
    }
    ngOnInit() {
    }
    validateInputs() {
        let email = this.postData.email.trim();
        let password = this.postData.password.trim();
        return (this.postData.email && this.postData.password && email.length > 0 && password.length > 0);
    }
    loginAction() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.validateInputs()) {
                this.uihelper.ShowSpinner();
                this.authService.login(this.postData).subscribe((res) => {
                    console.log('login res', res);
                    if (!!res) {
                        // console.log('user details month year', res.payload.user_details.user_attempt_month, res.payload.user_details.user_attempt_year);
                        if (res.status == 200) {
                            this.uihelper.HideSpinner();
                            localStorage.setItem("cartCount", res.payload.user_details.cartCount);
                            localStorage.setItem("userToken", res.payload.token);
                            localStorage.setItem("course_id", res.payload.user_details.course_id);
                            // this.storageService.store(Constant.AUTH, res.payload);
                            this.storageService.store(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_8__["Constant"].AUTH, res.payload).then(result => {
                                console.log('Data is saved');
                            }).catch(e => {
                                console.log("error: " + e);
                            });
                            localStorage.setItem('IsLogin', 'Yes');
                            if (!res.payload.user_details.user_attempt_month && !res.payload.user_details.user_attempt_year) {
                                this.uihelper.ShowAlert('Login', 'We need few information before you get started. Please update.');
                                this.navCtrl.navigateRoot(['edit-user-profile']);
                            }
                            else {
                                this.navCtrl.navigateRoot('tabs');
                                // this.navCtrl.navigateRoot(['dashboard']);
                            }
                        }
                        else {
                            this.uihelper.HideSpinner();
                            this.uihelper.ShowAlert("Login", res["error_description"]);
                        }
                    }
                    else {
                        this.uihelper.HideSpinner();
                        this.uihelper.ShowAlert("Login", "User not found");
                    }
                }, (err) => {
                    this.uihelper.HideSpinner();
                    // this.uihelper.ShowAlert("Login", 'Invalid Credentials');
                    this.uihelper.ShowAlert("Login", Object.values(err.error.payload));
                    // console.log('failure error',Object.values(err.error.payload)[0][0]);
                    console.log('failure error', err);
                });
            }
            else {
                this.uihelper.ShowAlert("Login", 'Please Enter Email and Password');
            }
        });
    }
    SignUpOnClick() {
        this.router.navigate(['/signup']);
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_9__["UIHelper"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_7__["StorageService"] }
];
LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-login',
        template: _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module.js.map