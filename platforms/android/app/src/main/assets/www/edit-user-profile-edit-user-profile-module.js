(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["edit-user-profile-edit-user-profile-module"],{

/***/ "7dkd":
/*!*********************************************************************!*\
  !*** ./src/app/pages/edit-user-profile/edit-user-profile.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.back img {\n  width: 40px;\n}\n\n.back {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.header-arrow {\n  padding: 20px 15px !important;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 120px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n}\n\n.packages-header h2 {\n  margin: 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n  position: relative;\n  top: -15px;\n}\n\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\nion-label {\n  margin: 0;\n}\n\nion-item {\n  --padding-start: 0px;\n}\n\n.user-details-info h5 {\n  font-weight: 500;\n  font-size: 14px;\n  color: #305f72;\n  margin-bottom: 0px;\n}\n\n.user-profile {\n  margin-top: 20px;\n}\n\n.user-profile p {\n  margin-top: 0px;\n  font-size: 14px;\n}\n\n.user-profile h5 {\n  font-size: 20px;\n  margin: 5px 0;\n  color: #305f72;\n}\n\n.m-0 {\n  margin: 0;\n}\n\n.margin-5 {\n  margin: 0 5px 0 0;\n}\n\n.margin-10 {\n  margin: 0 10px;\n}\n\n.padding-0 {\n  padding: 0;\n}\n\n.user-details-info {\n  border-bottom: 1px solid #415f7782;\n  text-align: left;\n  margin: 0 10px 5px;\n  padding: 0px 0px;\n}\n\n.form-control {\n  font-weight: 400;\n  margin: 0;\n  --padding: 0;\n}\n\nion-item {\n  --padding-start: 0;\n  --inner-padding-bottom: 0;\n  --inner-padding-end: 0;\n  --inner-padding-start: 0;\n  --inner-padding-top: 0;\n}\n\n.mt-15 {\n  margin-top: 12px;\n}\n\n.padding-5 {\n  padding: 12px 0px !important;\n}\n\n.user-details-info span {\n  font-weight: 400;\n  font-size: 14px;\n  color: #606060;\n  display: block;\n  padding: 10px 0;\n}\n\n.update-button {\n  border-radius: 8px !important;\n  padding: 13px;\n  color: #ffffff;\n  box-shadow: none !important;\n  margin-top: 15px;\n  margin-bottom: 30px;\n  display: inline-block;\n  --background-activated: #3f5c9b !important;\n  --background-focused: #3f5c9b !important;\n  --background-hover: #3f5c9b !important;\n  background: #3f5c9b !important;\n  font-size: 16px;\n  font-family: \"Lato\";\n  font-weight: 500;\n  width: 160px;\n}\n\nion-icon {\n  font-size: 22px;\n  margin: -1px 9px 0 !important;\n  color: #fff;\n}\n\nion-back-button ion-icon {\n  font-size: 22px;\n  margin: -1px 9px 0 !important;\n  color: #fff;\n}\n\n.remove-text ion-icon {\n  font-size: 22px;\n  margin: -1px 9px 0 !important;\n  color: #fff;\n}\n\nbutton:focus {\n  outline: none;\n}\n\n.choose-file-btn {\n  padding: 9px 35px;\n  color: #ffffff;\n  font-weight: 400;\n  font-size: 16px;\n  border-radius: 50px !important;\n  box-shadow: none !important;\n  background: #51C3D1;\n  margin-top: 30px;\n}\n\n.invalid {\n  border: 1px solid #ea6153;\n}\n\n.alert_msg {\n  color: red;\n  font-size: 14px;\n}\n\n.user-profile img {\n  border-radius: 50%;\n  width: 120px;\n  height: 120px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxlZGl0LXVzZXItcHJvZmlsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztBQUNmOztBQUNBO0VBQ0ksV0FBVztBQUVmOztBQUFBO0VBQ0ksV0FBVztBQUdmOztBQURBO0VBQ0ksV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsU0FBUztBQUliOztBQUZBO0VBQ0ksaUJBQWlCO0FBS3JCOztBQUhBO0VBQ0ksa0JBQWtCO0FBTXRCOztBQUpBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIsWUFBWTtFQUNaLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZUFBZTtFQUNmLFlBQVk7QUFPaEI7O0FBTEE7RUFDSSxhQUFhO0FBUWpCOztBQU5BO0VBQ0ksNkJBQTZCO0FBU2pDOztBQVBBO0VBQ0ksNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QixhQUFhO0VBQ2IsV0FBVztFQUNYLDJCQUEyQjtFQUMzQixrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLDRCQUE0QjtBQVVoQzs7QUFSQTtFQUNJLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFVBQVU7QUFXZDs7QUFUQTtFQUNJLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxjQUFjO0VBQ2QsZ0JBQWdCO0FBWXBCOztBQVZBO0VBQ0ksU0FBUztBQWFiOztBQVhBO0VBQ0ksb0JBQWdCO0FBY3BCOztBQVpBO0VBQ0ksZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixjQUFjO0VBQ2Qsa0JBQWtCO0FBZXRCOztBQWJBO0VBQ0ksZ0JBQWdCO0FBZ0JwQjs7QUFkQTtFQUNJLGVBQWM7RUFDZCxlQUFjO0FBaUJsQjs7QUFmQTtFQUNJLGVBQWU7RUFDZixhQUFhO0VBQ2IsY0FBYztBQWtCbEI7O0FBaEJBO0VBQ0ksU0FBUTtBQW1CWjs7QUFqQkE7RUFDSSxpQkFBZ0I7QUFvQnBCOztBQWxCQTtFQUNJLGNBQWM7QUFxQmxCOztBQW5CQTtFQUNJLFVBQVM7QUFzQmI7O0FBcEJBO0VBQ0ksa0NBQWtDO0VBQ2xDLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsZ0JBQWdCO0FBdUJwQjs7QUFyQkE7RUFDSSxnQkFBZ0I7RUFDaEIsU0FBUztFQUNULFlBQVU7QUF3QmQ7O0FBdEJBO0VBQ0ksa0JBQWdCO0VBQ2hCLHlCQUF1QjtFQUN2QixzQkFBb0I7RUFDcEIsd0JBQXNCO0VBQ3RCLHNCQUFvQjtBQXlCeEI7O0FBdkJBO0VBQ0ksZ0JBQWU7QUEwQm5COztBQXhCQTtFQUNJLDRCQUE0QjtBQTJCaEM7O0FBekJBO0VBQ0ksZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixjQUFjO0VBQ2QsY0FBYztFQUNkLGVBQ0o7QUEyQkE7O0FBMUJBO0VBQ0ksNkJBQTZCO0VBQzdCLGFBQWE7RUFDYixjQUFjO0VBQ2QsMkJBQTJCO0VBQzNCLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIscUJBQXFCO0VBQ3JCLDBDQUF1QjtFQUN2Qix3Q0FBcUI7RUFDckIsc0NBQW1CO0VBQ25CLDhCQUE4QjtFQUM5QixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixZQUFZO0FBNkJoQjs7QUEzQkE7RUFDSSxlQUFlO0VBQ2YsNkJBQTZCO0VBQzdCLFdBQVU7QUE4QmQ7O0FBNUJBO0VBQ0ksZUFBZTtFQUNmLDZCQUE2QjtFQUM3QixXQUFVO0FBK0JkOztBQTdCQTtFQUNJLGVBQWU7RUFDZiw2QkFBNkI7RUFDN0IsV0FBVTtBQWdDZDs7QUE5QkE7RUFDSSxhQUFhO0FBaUNqQjs7QUEvQkE7RUFDSSxpQkFBZ0I7RUFDaEIsY0FBYTtFQUNiLGdCQUFnQjtFQUNoQixlQUFjO0VBQ2QsOEJBQTRCO0VBQzVCLDJCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsZ0JBQWU7QUFrQ25COztBQWhDQTtFQUNJLHlCQUF5QjtBQW1DN0I7O0FBakNBO0VBQ0ksVUFBVTtFQUNWLGVBQWU7QUFvQ25COztBQWxDQTtFQUNJLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osYUFBYTtBQXFDakIiLCJmaWxlIjoiZWRpdC11c2VyLXByb2ZpbGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnVzZXItaWNvbiBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgd2lkdGg6IDQwcHg7XHJcbn1cclxuLmJhY2sgaW1nIHtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG59XHJcbi5iYWNrIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IGltZyB7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLThweDtcclxufVxyXG4ubWVudS1pY29uIHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4uY2FydC1udW1iZXIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDE1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZiYjAwO1xyXG4gICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICMzZjVjOWI7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBwYWRkaW5nOiAxcHg7XHJcbn1cclxuaW9uLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcbi5oZWFkZXItYXJyb3cge1xyXG4gICAgcGFkZGluZzogMjBweCAxNXB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLnRvcC1iZyB7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGhlaWdodDogMTIwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAgMCAzMHB4IDMwcHg7XHJcbn1cclxuLnBhY2thZ2VzLWhlYWRlciBoMntcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAtMTVweDtcclxufVxyXG4ucGFja2FnZXMtaGVhZGVyIHNwYW57XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG5pb24tbGFiZWwge1xyXG4gICAgbWFyZ2luOiAwO1xyXG59XHJcbmlvbi1pdGVtIHtcclxuICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xyXG59XHJcbi51c2VyLWRldGFpbHMtaW5mbyBoNXtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBjb2xvcjogIzMwNWY3MjtcclxuICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxufVxyXG4udXNlci1wcm9maWxle1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxufVxyXG4udXNlci1wcm9maWxlIHB7XHJcbiAgICBtYXJnaW4tdG9wOjBweDtcclxuICAgIGZvbnQtc2l6ZToxNHB4O1xyXG59XHJcbi51c2VyLXByb2ZpbGUgaDV7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBtYXJnaW46IDVweCAwO1xyXG4gICAgY29sb3I6ICMzMDVmNzI7XHJcbn1cclxuLm0tMHtcclxuICAgIG1hcmdpbjowO1xyXG59XHJcbi5tYXJnaW4tNXtcclxuICAgIG1hcmdpbjowIDVweCAwIDA7XHJcbn1cclxuLm1hcmdpbi0xMHtcclxuICAgIG1hcmdpbjogMCAxMHB4O1xyXG59XHJcbi5wYWRkaW5nLTB7XHJcbiAgICBwYWRkaW5nOjA7XHJcbn1cclxuLnVzZXItZGV0YWlscy1pbmZve1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICM0MTVmNzc4MjtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICBtYXJnaW46IDAgMTBweCA1cHg7XHJcbiAgICBwYWRkaW5nOiAwcHggMHB4O1xyXG59XHJcbi5mb3JtLWNvbnRyb2x7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgLS1wYWRkaW5nOiAwO1xyXG59XHJcbmlvbi1pdGVtIHtcclxuICAgIC0tcGFkZGluZy1zdGFydDogMDtcclxuICAgIC0taW5uZXItcGFkZGluZy1ib3R0b206IDA7XHJcbiAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwO1xyXG4gICAgLS1pbm5lci1wYWRkaW5nLXN0YXJ0OiAwO1xyXG4gICAgLS1pbm5lci1wYWRkaW5nLXRvcDogMDtcclxufVxyXG4ubXQtMTV7XHJcbiAgICBtYXJnaW4tdG9wOjEycHg7XHJcbn1cclxuLnBhZGRpbmctNXtcclxuICAgIHBhZGRpbmc6IDEycHggMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLnVzZXItZGV0YWlscy1pbmZvIHNwYW57XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgY29sb3I6ICM2MDYwNjA7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHBhZGRpbmc6IDEwcHggMFxyXG59XHJcbi51cGRhdGUtYnV0dG9ue1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nOiAxM3B4O1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJhY2tncm91bmQtaG92ZXI6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIGZvbnQtZmFtaWx5OiBcIkxhdG9cIjtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB3aWR0aDogMTYwcHg7XHJcbn1cclxuaW9uLWljb257XHJcbiAgICBmb250LXNpemU6IDIycHg7XHJcbiAgICBtYXJnaW46IC0xcHggOXB4IDAgIWltcG9ydGFudDtcclxuICAgIGNvbG9yOiNmZmY7XHJcbn1cclxuaW9uLWJhY2stYnV0dG9uIGlvbi1pY29ue1xyXG4gICAgZm9udC1zaXplOiAyMnB4O1xyXG4gICAgbWFyZ2luOiAtMXB4IDlweCAwICFpbXBvcnRhbnQ7XHJcbiAgICBjb2xvcjojZmZmO1xyXG59XHJcbi5yZW1vdmUtdGV4dCBpb24taWNvbntcclxuICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgIG1hcmdpbjogLTFweCA5cHggMCAhaW1wb3J0YW50O1xyXG4gICAgY29sb3I6I2ZmZjtcclxufVxyXG5idXR0b246Zm9jdXN7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG59XHJcbi5jaG9vc2UtZmlsZS1idG57XHJcbiAgICBwYWRkaW5nOjlweCAzNXB4O1xyXG4gICAgY29sb3I6I2ZmZmZmZjtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBmb250LXNpemU6MTZweDtcclxuICAgIGJvcmRlci1yYWRpdXM6NTBweCFpbXBvcnRhbnQ7XHJcbiAgICBib3gtc2hhZG93Om5vbmUhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZDogIzUxQzNEMTtcclxuICAgIG1hcmdpbi10b3A6MzBweDtcclxufVxyXG4uaW52YWxpZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZWE2MTUzO1xyXG59XHJcbi5hbGVydF9tc2d7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG59XHJcbi51c2VyLXByb2ZpbGUgaW1nIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHdpZHRoOiAxMjBweDtcclxuICAgIGhlaWdodDogMTIwcHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "F3LV":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/edit-user-profile/edit-user-profile.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" class=\"remove-text\">\r\n      <ion-back-button ></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-title>Edit Profile</ion-title>\r\n  </ion-toolbar>\r\n</ion-header> -->\r\n\r\n<ion-content>\r\n  <ion-row style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n    <ion-col size=\"12\" class=\"header-arrow \">\r\n      <div>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <div class=\"back\" (click)=\"goBack()\">\r\n                <img src=\"assets/back.svg\" alt=\"back\">\r\n            </div>\r\n          </ion-col>\r\n          <!-- <ion-col size=\"6\">\r\n            <div class=\"menu-icon\">\r\n              <span class=\"shopping-cart\"><img src=\"assets/shopping-cart.svg\" alt=\"shopping\"><span\r\n                  class=\"cart-number\">{{cartCount}}</span></span>\r\n              <span class=\"user-icon\" slot=\"start\">\r\n                <img src=\"assets/user-profile.png\" alt=\"user-icon\">\r\n              </span>\r\n            </div>\r\n          </ion-col> -->\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <div class=\"packages-header\">\r\n              <h2>Edit Profile</h2>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"\">\r\n      <div class=\"user-profile text-center\">\r\n        <!-- <img src=\"{{avatar}}\"> -->\r\n        <img src=\"{{avatar}}\" alt=\"user-icon\">\r\n        <h5 class=\"m-0\">{{userData?.full_name}}</h5>\r\n        <p>{{userData?.email}}</p>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n  <!-- <p *ngIf=\"submitAttempt\" style=\"color: #ea6153;\">Please fill out all details accurately.</p> -->\r\n\r\n  <form [formGroup]=\"editProfile\" (ngSubmit)=\"editProfileAction()\">\r\n\r\n  <ion-row class=\"mt-30\">\r\n\r\n    <ion-col size=\"12\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">First Name</h5>\r\n        <ion-list>\r\n          <ion-item lines=\"none\">\r\n            <ion-input type=\"text\" formControlName=\"first_name\" class=\"form-control\"  [(ngModel)]=\"first_name\"></ion-input>\r\n          </ion-item>\r\n          <ion-item *ngIf=\"!editProfile.controls.first_name.valid  && (editProfile.controls.first_name.dirty || submitAttempt)\">\r\n            <p class=\"alert_msg\">Please enter first name.</p>\r\n        </ion-item>\r\n        </ion-list>\r\n      </div>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Last Name</h5>\r\n        <ion-list>\r\n          <ion-item lines=\"none\">\r\n            <ion-input type=\"text\" formControlName=\"last_name\" class=\"form-control\" [(ngModel)]=\"last_name\"></ion-input>\r\n          </ion-item>\r\n          <ion-item *ngIf=\"!editProfile.controls.last_name.valid  && (editProfile.controls.last_name.dirty || submitAttempt)\">\r\n            <p class=\"alert_msg\">Please enter last name.</p>\r\n        </ion-item>\r\n        </ion-list>\r\n      </div>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Mobile Number</h5>\r\n        <ion-list>\r\n          <ion-item lines=\"none\">\r\n            <ion-input type=\"tel\" maxlength=\"10\" formControlName=\"phone_number1\" class=\"form-control\" [(ngModel)]=\"phone_number1\"></ion-input>\r\n          </ion-item>\r\n          <ion-item *ngIf=\"!editProfile.controls.phone_number1.valid  && (editProfile.controls.phone_number1.dirty || submitAttempt)\">\r\n            <p class=\"alert_msg\">Please enter contact number.</p>\r\n        </ion-item>\r\n        </ion-list>\r\n      </div>\r\n    </ion-col>\r\n    \r\n    <ion-col size=\"12\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Alternate Number</h5>\r\n        <ion-list>\r\n          <ion-item lines=\"none\">\r\n            <ion-input type=\"tel\" maxlength=\"10\" formControlName=\"phone_number2\" class=\"form-control\" [(ngModel)]=\"phone_number2\"></ion-input>\r\n          </ion-item>\r\n          <ion-item *ngIf=\"!editProfile.controls.phone_number2.valid  && (editProfile.controls.phone_number2.dirty || submitAttempt)\">\r\n            <p class=\"alert_msg\">Please enter Alternate number.</p>\r\n        </ion-item>\r\n        </ion-list>\r\n      </div>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Parents Mobile Number</h5>\r\n        <ion-list>\r\n          <ion-item lines=\"none\">\r\n            <ion-input type=\"tel\" maxlength=\"10\" formControlName=\"parents_phone_no\" class=\"form-control\" [(ngModel)]=\"parents_phone_no\"></ion-input>\r\n          </ion-item>\r\n          <ion-item *ngIf=\"!editProfile.controls.parents_phone_no.valid  && (editProfile.controls.parents_phone_no.dirty || submitAttempt)\">\r\n            <p class=\"alert_msg\">Please enter contact number.</p>\r\n        </ion-item>\r\n        </ion-list>\r\n      </div>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Parents Email id</h5>\r\n        <ion-list>\r\n          <ion-item lines=\"none\">\r\n            <ion-input type=\"text\" formControlName=\"parents_email_id\" class=\"form-control\" [(ngModel)]=\"parents_email_id\"></ion-input>\r\n          </ion-item>\r\n          <ion-item *ngIf=\"!editProfile.controls.parents_email_id.valid  && (editProfile.controls.parents_email_id.dirty || submitAttempt)\">\r\n            <p class=\"alert_msg\">Please enter correct email id.</p>\r\n        </ion-item>\r\n        </ion-list>\r\n      </div>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Attempt Month*</h5>\r\n        <ion-list>\r\n          <ion-item lines=\"none\">\r\n            <ion-label class=\"m-0\">Choose Month</ion-label>\r\n            <ion-select formControlName=\"user_attempt_month\" [(ngModel)]=\"user_attempt_month\" >\r\n              <ion-select-option value=\"1\">January</ion-select-option>\r\n              <ion-select-option value=\"5\">May</ion-select-option>\r\n              <ion-select-option value=\"11\">November</ion-select-option>\r\n            </ion-select>\r\n          </ion-item>\r\n          <ion-item *ngIf=\"!editProfile.controls.user_attempt_month.valid  && (editProfile.controls.user_attempt_month.dirty || submitAttempt)\">\r\n            <p class=\"alert_msg\">Please select attempt month.</p>\r\n        </ion-item>\r\n        </ion-list>\r\n      </div>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Attempt Year*</h5>\r\n        <ion-list>\r\n          <ion-item lines=\"none\">\r\n            <ion-label class=\"m-0\">Choose Year</ion-label>\r\n            <ion-select formControlName=\"user_attempt_year\" [(ngModel)]=\"user_attempt_year\" >\r\n              <ion-select-option *ngFor=\"let year of years\" value=\"{{year.value}}\">{{year.value}}</ion-select-option>\r\n              <!-- <ion-select-option >2022</ion-select-option>\r\n              <ion-select-option >2023</ion-select-option>\r\n              <ion-select-option >2024</ion-select-option>\r\n              <ion-select-option >2025</ion-select-option>\r\n              <ion-select-option >2026</ion-select-option> -->\r\n            </ion-select>\r\n          </ion-item>\r\n          <ion-item *ngIf=\"!editProfile.controls.user_attempt_year.valid  && (editProfile.controls.user_attempt_year.dirty || submitAttempt)\">\r\n            <p class=\"alert_msg\">Please select attempt year.</p>\r\n        </ion-item>\r\n        </ion-list>\r\n      </div>\r\n    </ion-col>\r\n\r\n  <!-- <ion-row  class=\"mt-15 margin-10\">\r\n    <ion-col size=\"12\" class=\"\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Address1</h5>\r\n        <ion-list>\r\n          <ion-item lines=\"none\">\r\n            <ion-textarea class=\"form-control\" formControlName=\"address1\" [(ngModel)]=\"address1\"> </ion-textarea>\r\n          </ion-item>\r\n          <ion-item *ngIf=\"!editProfile.controls.address1.valid  && (editProfile.controls.address1.dirty || submitAttempt)\">\r\n            <p class=\"alert_msg\">Please enter Address.</p>\r\n        </ion-item>\r\n        </ion-list>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"mt-15 margin-10\">\r\n    <ion-col size=\"12\" class=\"\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Address2</h5>\r\n        <ion-list>\r\n          <ion-item lines=\"none\">\r\n            <ion-textarea class=\"form-control\" formControlName=\"address2\" [(ngModel)]=\"address2\"></ion-textarea>\r\n           \r\n          </ion-item>\r\n          <ion-item *ngIf=\"!editProfile.controls.address2.valid  && (editProfile.controls.address2.dirty || submitAttempt)\">\r\n            <p class=\"alert_msg\">Please enter Alternate Address.</p>\r\n          </ion-item>\r\n        </ion-list>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row> -->\r\n\r\n    <ion-col size=\"12\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Courses Opted For</h5>\r\n        <span>{{userData?.course_name}}</span>\r\n      </div>\r\n    </ion-col>\r\n\r\n     <ion-col size=\"12\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Avatar (only Jpg,Jpeg,Png)</h5>\r\n        <input type=\"hidden\" name=\"avatar_type\" value=\"storage\">\r\n        <input class=\"form-control padding-5 bg-color\" type=\"file\" name=\"avatar_location\" id=\"avatar_location\" (change)=\"upload($event)\">\r\n      </div>\r\n     </ion-col>\r\n\r\n    <ion-col>\r\n      <div class=\"text-center\">\r\n        <button ion-button full class=\"update-button\" type=\"submit\">Update Info</button>\r\n      </div>\r\n    </ion-col>\r\n    \r\n  </ion-row>\r\n</form>\r\n  </ion-content>\r\n");

/***/ }),

/***/ "IbmE":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/edit-user-profile/edit-user-profile-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: EditUserProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditUserProfilePageRoutingModule", function() { return EditUserProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _edit_user_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./edit-user-profile.page */ "bJwz");




const routes = [
    {
        path: '',
        component: _edit_user_profile_page__WEBPACK_IMPORTED_MODULE_3__["EditUserProfilePage"]
    }
];
let EditUserProfilePageRoutingModule = class EditUserProfilePageRoutingModule {
};
EditUserProfilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EditUserProfilePageRoutingModule);



/***/ }),

/***/ "bJwz":
/*!*******************************************************************!*\
  !*** ./src/app/pages/edit-user-profile/edit-user-profile.page.ts ***!
  \*******************************************************************/
/*! exports provided: EditUserProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditUserProfilePage", function() { return EditUserProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_edit_user_profile_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./edit-user-profile.page.html */ "F3LV");
/* harmony import */ var _edit_user_profile_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./edit-user-profile.page.scss */ "7dkd");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/event.service */ "fTLw");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");














let EditUserProfilePage = class EditUserProfilePage {
    constructor(http, navCtrl, events, router, authService, storageService, loadingController, alertController, formBuilder, uihelper) {
        this.http = http;
        this.navCtrl = navCtrl;
        this.events = events;
        this.router = router;
        this.authService = authService;
        this.storageService = storageService;
        this.loadingController = loadingController;
        this.alertController = alertController;
        this.formBuilder = formBuilder;
        this.uihelper = uihelper;
        this.url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_11__["Constant"].apiUrl;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
        });
        this.options = { headers: this.headers };
        this.submitAttempt = false;
        this.editProfile = formBuilder.group({
            first_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required],
            last_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required],
            phone_number1: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required],
            phone_number2: [''],
            parents_phone_no: [''],
            parents_email_id: [''],
            user_attempt_month: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required],
            user_attempt_year: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_9__["Validators"].required],
            avatar_type: ['']
        });
    }
    ngOnInit() {
        this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_11__["Constant"].AUTH).then(res => {
            var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
            this.userToken = localStorage.getItem("userToken");
            // console.log('user details', res)
            this.userData = res.user_details;
            this.user_id = res.user_details.id;
            this.email = res.user_details.email;
            this.first_name = (_a = this.userData) === null || _a === void 0 ? void 0 : _a.first_name;
            this.last_name = (_b = this.userData) === null || _b === void 0 ? void 0 : _b.last_name;
            this.phone_number1 = (_c = this.userData) === null || _c === void 0 ? void 0 : _c.phone_number;
            this.phone_number2 = (_d = this.userData) === null || _d === void 0 ? void 0 : _d.phone_number2;
            this.parents_phone_no = (_e = this.userData) === null || _e === void 0 ? void 0 : _e.parents_phone_no;
            this.parents_email_id = (_f = this.userData) === null || _f === void 0 ? void 0 : _f.parents_email_id;
            this.user_attempt_month = (_g = this.userData) === null || _g === void 0 ? void 0 : _g.user_attempt_month;
            this.user_attempt_year = (_h = this.userData) === null || _h === void 0 ? void 0 : _h.user_attempt_year;
            this.address1 = (_j = this.userData) === null || _j === void 0 ? void 0 : _j.address1;
            this.address2 = (_k = this.userData) === null || _k === void 0 ? void 0 : _k.address2;
            this.avatar = res.user_details.avatar_location ? src_Helper_Constant__WEBPACK_IMPORTED_MODULE_11__["Constant"].adminUrl + res.user_details.avatar_location : 'assets/icon/user-profile.png';
        });
        let current_year = new Date().getFullYear();
        this.years = [];
        for (var i = current_year; i < current_year + 6; i++) {
            this.years.push({
                value: i
            });
            // console.log('years',this.years)
        }
    }
    upload(str) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.avatar_type = str.target.files[0];
            // console.log('avatar_type', this.avatar_type);
        });
    }
    editProfileAction() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // this.uihelper.ShowSpinner();
            // console.log('phone number', this.editProfile.value.phone_number1, this.editProfile.value.parents_phone_no)
            if (this.editProfile.value.parents_email_id != '' && this.editProfile.value.parents_email_id == this.email) {
                this.uihelper.ShowAlert('', 'Students and Parents Email id cannot be same');
                return false;
            }
            if (this.editProfile.value.parents_phone_no != '' && this.editProfile.value.phone_number1 == this.editProfile.value.parents_phone_no) {
                this.uihelper.ShowAlert('', 'Number and Parents number cannot be same');
                return false;
            }
            else {
                this.uihelper.ShowSpinner();
                this.submitAttempt = true;
                let formData = new FormData();
                formData.append('id', this.user_id);
                formData.append('first_name', this.editProfile.value.first_name);
                formData.append('last_name', this.editProfile.value.last_name);
                formData.append('phone_number1', this.editProfile.value.phone_number1);
                formData.append('phone_number2', this.editProfile.value.phone_number2 == null ? "" : this.editProfile.value.phone_number2);
                formData.append('address1', this.editProfile.value.address1);
                formData.append('address2', this.editProfile.value.address2);
                formData.append('parents_phone_no', this.editProfile.value.parents_phone_no);
                formData.append('parents_email_id', this.editProfile.value.parents_email_id);
                formData.append('user_attempt_month', this.editProfile.value.user_attempt_month == null ? "" : this.editProfile.value.user_attempt_month);
                formData.append('user_attempt_year', this.editProfile.value.user_attempt_year == null ? "" : this.editProfile.value.user_attempt_year);
                formData.append('avatar', this.avatar_type);
                formData.append('avatar_type', "storage");
                // console.log('post data', formData)
                this.http.post(this.url + 'profile-update', formData, this.options).subscribe((res) => {
                    // console.log('success',res);
                    this.storageService.store(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_11__["Constant"].AUTH, res.payload);
                    var logedinUserDetails;
                    this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_11__["Constant"].AUTH).then(res => {
                        // console.log('Data', res)
                        logedinUserDetails = res.user_details;
                        this.events.sendMessage({
                            userName: logedinUserDetails.full_name
                        });
                    });
                    this.uihelper.HideSpinner();
                    this.uihelper.ShowAlert('', 'Profile updated succesfully');
                    this.router.navigate(['/tabs']);
                }, (err) => {
                    this.uihelper.HideSpinner();
                    console.log('failure', err.error);
                    // this.toastService.presentToast(res.error.payload.password_confirmation_reg.join('#'))
                    // var err = Object.values(err.error.payload)[0][0];        
                    // this.toastService.presentToast('something went wrong. Please try again')
                    this.uihelper.ShowAlert('', 'Please fill all details correctly');
                });
            }
        });
    }
    goBack() {
        this.navCtrl.pop();
    }
};
EditUserProfilePage.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] },
    { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_10__["EventService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_4__["StorageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormBuilder"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_12__["UIHelper"] }
];
EditUserProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-edit-user-profile',
        template: _raw_loader_edit_user_profile_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_edit_user_profile_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], EditUserProfilePage);



/***/ }),

/***/ "qRg9":
/*!*********************************************************************!*\
  !*** ./src/app/pages/edit-user-profile/edit-user-profile.module.ts ***!
  \*********************************************************************/
/*! exports provided: EditUserProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditUserProfilePageModule", function() { return EditUserProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _edit_user_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit-user-profile-routing.module */ "IbmE");
/* harmony import */ var _edit_user_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./edit-user-profile.page */ "bJwz");







let EditUserProfilePageModule = class EditUserProfilePageModule {
};
EditUserProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _edit_user_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditUserProfilePageRoutingModule"]
        ],
        declarations: [_edit_user_profile_page__WEBPACK_IMPORTED_MODULE_6__["EditUserProfilePage"]]
    })
], EditUserProfilePageModule);



/***/ })

}]);
//# sourceMappingURL=edit-user-profile-edit-user-profile-module.js.map