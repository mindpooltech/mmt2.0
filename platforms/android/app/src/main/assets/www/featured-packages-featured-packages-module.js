(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["featured-packages-featured-packages-module"],{

/***/ "2+cQ":
/*!*********************************************************************!*\
  !*** ./src/app/pages/featured-packages/featured-packages.page.scss ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.back img {\n  width: 40px;\n}\n\n.back {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.header-arrow {\n  padding: 20px 15px !important;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 120px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n}\n\n.packages-header h2 {\n  margin: 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n  position: relative;\n  top: -15px;\n}\n\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\nion-label {\n  margin: 0;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n}\n\nion-input {\n  --padding-top: 0px;\n  --padding-end: 0px;\n  --padding-bottom: 0px;\n}\n\n.chapter-box {\n  margin: 0px 20px 10px;\n  border-radius: 20px;\n  border: 1px solid #b1bcd440;\n  padding: 10px;\n  box-shadow: 0px 1px 12px -4px #37242442;\n}\n\n.chapter-test-heading h3 {\n  font-weight: 500;\n  margin: 0;\n  font-size: 20px;\n  color: #305F72;\n}\n\n.price-chapter span {\n  font-weight: 400;\n  font-size: 18px;\n  color: #305F72;\n  text-align: right;\n  margin: 0 auto;\n  display: block;\n}\n\n.test-available-chapter p {\n  margin: 2px 0;\n  font-size: 14px;\n  font-weight: 400;\n  color: #305F72;\n}\n\n.view-more-btn span {\n  font-size: 14px;\n  text-decoration: underline;\n  font-weight: 500;\n  color: #3F5C9B;\n  padding: 8px 0;\n  display: block;\n}\n\nion-button {\n  --background: #3f5c9b !important;\n  border-radius: 10px;\n  margin-top: 60px;\n}\n\n.start-btn {\n  background: #3f5c9b !important;\n  --background-activated: #3f5c9b !important;\n  --background-focused: #3f5c9b !important;\n  --background-hover: #3f5c9b !important;\n  --border-radius: 8px;\n  height: 33px;\n  margin: 0;\n  width: auto;\n  text-align: center;\n  font-size: 12px;\n  font-weight: 400;\n  float: right;\n}\n\n.aerrow-btn {\n  padding-left: 5px;\n}\n\n.group-text {\n  margin: 10px 0;\n  padding: 0 20px;\n  font-size: 20px;\n  color: #f27376;\n  font-weight: 500;\n  display: block;\n  width: 100%;\n}\n\n.no-featured-packages {\n  margin: 10px 0;\n  padding: 0 20px;\n  font-size: 16px;\n  color: #7c7c7c;\n  font-weight: 400;\n  display: block;\n  width: 100%;\n}\n\n.m-20 {\n  margin: 20px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxmZWF0dXJlZC1wYWNrYWdlcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztBQUNmOztBQUNBO0VBQ0ksV0FBVztBQUVmOztBQUFBO0VBQ0ksV0FBVztBQUdmOztBQURBO0VBQ0ksV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsU0FBUztBQUliOztBQUZBO0VBQ0ksaUJBQWlCO0FBS3JCOztBQUhBO0VBQ0ksa0JBQWtCO0FBTXRCOztBQUpBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIsWUFBWTtFQUNaLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZUFBZTtFQUNmLFlBQVk7QUFPaEI7O0FBTEE7RUFDSSxhQUFhO0FBUWpCOztBQU5BO0VBQ0ksNkJBQTZCO0FBU2pDOztBQVBBO0VBQ0ksNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QixhQUFhO0VBQ2IsV0FBVztFQUNYLDJCQUEyQjtFQUMzQixrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLDRCQUE0QjtBQVVoQzs7QUFSQTtFQUNJLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFVBQVU7QUFXZDs7QUFUQTtFQUNJLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxjQUFjO0VBQ2QsZ0JBQWdCO0FBWXBCOztBQVZBO0VBQ0ksU0FBUztBQWFiOztBQVhBO0VBQ0ksb0JBQWdCO0VBQ2hCLHdCQUFvQjtBQWN4Qjs7QUFaQTtFQUNJLGtCQUFjO0VBQ2Qsa0JBQWM7RUFDZCxxQkFBaUI7QUFlckI7O0FBYkE7RUFDSSxxQkFBcUI7RUFDckIsbUJBQW1CO0VBQ25CLDJCQUEyQjtFQUMzQixhQUFhO0VBQ2IsdUNBQXVDO0FBZ0IzQzs7QUFkQTtFQUNJLGdCQUFnQjtFQUNoQixTQUFTO0VBQ1QsZUFBZTtFQUNmLGNBQWM7QUFpQmxCOztBQWZBO0VBQ0ksZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGNBQWM7RUFDZCxjQUFjO0FBa0JsQjs7QUFoQkE7RUFDSSxhQUFhO0VBQ2IsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixjQUFjO0FBbUJsQjs7QUFqQkE7RUFDSSxlQUFlO0VBQ2YsMEJBQTBCO0VBQzFCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsY0FBYztFQUNkLGNBQWM7QUFvQmxCOztBQWxCQTtFQUNJLGdDQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLGdCQUFnQjtBQXFCcEI7O0FBbkJBO0VBQ0ksOEJBQThCO0VBQzlCLDBDQUF1QjtFQUN2Qix3Q0FBcUI7RUFDckIsc0NBQW1CO0VBQ25CLG9CQUFnQjtFQUNoQixZQUFZO0VBQ1osU0FBUztFQUNULFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixZQUFZO0FBc0JoQjs7QUFwQkE7RUFDSSxpQkFBaUI7QUF1QnJCOztBQXJCQTtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsZUFBZTtFQUNmLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLFdBQVc7QUF3QmY7O0FBdEJBO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixlQUFlO0VBQ2YsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsV0FBVztBQXlCZjs7QUF2QkE7RUFDSSxZQUFZO0FBMEJoQiIsImZpbGUiOiJmZWF0dXJlZC1wYWNrYWdlcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudXNlci1pY29uIGltZyB7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICB3aWR0aDogNDBweDtcclxufVxyXG4uYmFjayBpbWcge1xyXG4gICAgd2lkdGg6IDQwcHg7XHJcbn1cclxuLmJhY2sge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuLnNob3BwaW5nLWNhcnQgaW1nIHtcclxuICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAtOHB4O1xyXG59XHJcbi5tZW51LWljb24ge1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuLnNob3BwaW5nLWNhcnQge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcbi5jYXJ0LW51bWJlciB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICByaWdodDogMTVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmJiMDA7XHJcbiAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICB3aWR0aDogMjBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogIzNmNWM5YjtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIHBhZGRpbmc6IDFweDtcclxufVxyXG5pb24taGVhZGVyIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuLmhlYWRlci1hcnJvdyB7XHJcbiAgICBwYWRkaW5nOiAyMHB4IDE1cHggIWltcG9ydGFudDtcclxufVxyXG4udG9wLWJnIHtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgaGVpZ2h0OiAxMjBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMCAwIDMwcHggMzBweDtcclxufVxyXG4ucGFja2FnZXMtaGVhZGVyIGgye1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IC0xNXB4O1xyXG59XHJcbi5wYWNrYWdlcy1oZWFkZXIgc3BhbntcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGNvbG9yOiAjMzA1RjcyO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG59XHJcbmlvbi1sYWJlbCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbn1cclxuaW9uLWl0ZW0ge1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XHJcbiAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XHJcbn1cclxuaW9uLWlucHV0IHtcclxuICAgIC0tcGFkZGluZy10b3A6IDBweDtcclxuICAgIC0tcGFkZGluZy1lbmQ6IDBweDtcclxuICAgIC0tcGFkZGluZy1ib3R0b206IDBweDtcclxufVxyXG4uY2hhcHRlci1ib3h7XHJcbiAgICBtYXJnaW46IDBweCAyMHB4IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2IxYmNkNDQwO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJveC1zaGFkb3c6IDBweCAxcHggMTJweCAtNHB4ICMzNzI0MjQ0MjtcclxufVxyXG4uY2hhcHRlci10ZXN0LWhlYWRpbmcgaDN7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgY29sb3I6ICMzMDVGNzI7XHJcbn1cclxuLnByaWNlLWNoYXB0ZXIgc3BhbntcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufVxyXG4udGVzdC1hdmFpbGFibGUtY2hhcHRlciBwe1xyXG4gICAgbWFyZ2luOiAycHggMDtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxufVxyXG4udmlldy1tb3JlLWJ0biBzcGFue1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICMzRjVDOUI7XHJcbiAgICBwYWRkaW5nOiA4cHggMDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcbmlvbi1idXR0b24ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogNjBweDtcclxufVxyXG4uc3RhcnQtYnRuIHtcclxuICAgIGJhY2tncm91bmQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJhY2tncm91bmQtaG92ZXI6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgaGVpZ2h0OiAzM3B4O1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgd2lkdGg6IGF1dG87XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcbi5hZXJyb3ctYnRue1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbn1cclxuLmdyb3VwLXRleHR7XHJcbiAgICBtYXJnaW46IDEwcHggMDtcclxuICAgIHBhZGRpbmc6IDAgMjBweDtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGNvbG9yOiAjZjI3Mzc2O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLm5vLWZlYXR1cmVkLXBhY2thZ2Vze1xyXG4gICAgbWFyZ2luOiAxMHB4IDA7XHJcbiAgICBwYWRkaW5nOiAwIDIwcHg7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICBjb2xvcjogIzdjN2M3YztcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcbi5tLTIwe1xyXG4gICAgbWFyZ2luOiAyMHB4O1xyXG59Il19 */");

/***/ }),

/***/ "Ef8F":
/*!*******************************************************************!*\
  !*** ./src/app/pages/featured-packages/featured-packages.page.ts ***!
  \*******************************************************************/
/*! exports provided: FeaturedPackagesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeaturedPackagesPage", function() { return FeaturedPackagesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_featured_packages_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./featured-packages.page.html */ "u093");
/* harmony import */ var _featured_packages_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./featured-packages.page.scss */ "2+cQ");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");









let FeaturedPackagesPage = class FeaturedPackagesPage {
    constructor(uihelper, storageService, http, router) {
        this.uihelper = uihelper;
        this.storageService = storageService;
        this.http = http;
        this.router = router;
    }
    ngOnInit() {
        this.router.events.subscribe((event) => {
            this.selfPath = '';
            if (event && event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_4__["NavigationEnd"] && event.url) {
                this.selfPath = event.url + '/featured-packages';
            }
            this.selfPath = this.router.routerState.snapshot.url;
            this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'featured-packages') + 'featured-packages';
        });
    }
    catch(e) {
        console.log(e);
    }
    getStringBeforeSubstring(parentString, substring) {
        return parentString.substring(0, parentString.indexOf(substring));
    }
    ionViewWillEnter() {
        this.GetDetailsOfDashBoard();
    }
    GetDetailsOfDashBoard() {
        // this.cartCount = localStorage.getItem('cartCount');
        this.uihelper.ShowSpinner();
        this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_7__["Constant"].AUTH).then(res => {
            this.userData = res.user_details;
            this.user_name = res.user_details.full_name;
            this.course_id = res.user_details.course_id;
            this.user_id = res.user_details.id;
            this.http.afterLoginGet("dashboard", res.user_details.id).subscribe((res) => {
                this.dashboard_data = res['payload'];
                // console.log('dashboard_data',this.dashboard_data)
                // console.log('course_id',this.course_id)
                if (this.course_id == 1) {
                    this.isFoundation = true;
                    this.featured_packages_caf = this.dashboard_data.featured_packages_caf;
                }
                else {
                    this.featured_packages_caf = [];
                }
                // console.log('featured_packages_caf',this.featured_packages_caf)
                this.featured_packages_group1 = this.dashboard_data.featured_packages_group1;
                this.featured_packages_group2 = this.dashboard_data.featured_packages_group2;
                this.http.afterLoginGet("cart_count", this.userData.id).subscribe((res) => {
                    this.cartCount = res['payload'].cart_count;
                });
                this.uihelper.HideSpinner();
            }, (err) => {
                console.log('err', err);
                this.uihelper.ShowAlert("", "Something went wrong");
            });
        });
    }
    addToCart(id, index, group_type, package_price) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.uihelper.ShowSpinner();
            let cart_data = {
                pkg_id: id,
                user_id: this.user_id
            };
            this.http.afterLoginPost('add-to-cart', cart_data).subscribe((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                // console.log('add to cart',res);
                if (res['payload'].mycart == 'mycart') {
                    localStorage.setItem("cartCount", res['payload'].cartCount);
                    this.cartCount = localStorage.getItem('cartCount');
                }
                if (res) {
                    if (group_type == 'caf') {
                        this.featured_packages_caf[index]['isadded'] = true;
                    }
                    else if (group_type == 'group1') {
                        this.featured_packages_group1[index]['isadded'] = true;
                    }
                    else {
                        this.featured_packages_group2[index]['isadded'] = true;
                    }
                    if (package_price == 0) {
                        this.uihelper.showToast('Course succesfully added, now you can go to my test board and attempt your test', 1);
                    }
                    this.uihelper.HideSpinner();
                }
            }));
        });
    }
    goBack() {
        this.router.navigate(['/tabs']);
    }
    GoToCart() {
        this.router.navigate([this.selfPath + "/cart"]);
    }
    GoToUserProfile() {
        this.router.navigate([this.selfPath + "/user-profile"]);
    }
};
FeaturedPackagesPage.ctorParameters = () => [
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_8__["UIHelper"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_6__["StorageService"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_5__["HttpService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
FeaturedPackagesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-featured-packages',
        template: _raw_loader_featured_packages_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_featured_packages_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], FeaturedPackagesPage);



/***/ }),

/***/ "ZNBK":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/featured-packages/featured-packages-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: FeaturedPackagesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeaturedPackagesPageRoutingModule", function() { return FeaturedPackagesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _featured_packages_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./featured-packages.page */ "Ef8F");




const routes = [
    {
        path: '',
        component: _featured_packages_page__WEBPACK_IMPORTED_MODULE_3__["FeaturedPackagesPage"]
    }
];
let FeaturedPackagesPageRoutingModule = class FeaturedPackagesPageRoutingModule {
};
FeaturedPackagesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], FeaturedPackagesPageRoutingModule);



/***/ }),

/***/ "eIpb":
/*!*********************************************************************!*\
  !*** ./src/app/pages/featured-packages/featured-packages.module.ts ***!
  \*********************************************************************/
/*! exports provided: FeaturedPackagesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FeaturedPackagesPageModule", function() { return FeaturedPackagesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _featured_packages_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./featured-packages-routing.module */ "ZNBK");
/* harmony import */ var _featured_packages_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./featured-packages.page */ "Ef8F");







let FeaturedPackagesPageModule = class FeaturedPackagesPageModule {
};
FeaturedPackagesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _featured_packages_routing_module__WEBPACK_IMPORTED_MODULE_5__["FeaturedPackagesPageRoutingModule"],
        ],
        declarations: [_featured_packages_page__WEBPACK_IMPORTED_MODULE_6__["FeaturedPackagesPage"]]
    })
], FeaturedPackagesPageModule);



/***/ }),

/***/ "u093":
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/featured-packages/featured-packages.page.html ***!
  \***********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\r\n  <ion-row style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n    <ion-col size=\"12\" class=\"header-arrow padding-0\">\r\n      <div>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <div class=\"back\" (click)=\"goBack()\">\r\n                <img src=\"assets/back.svg\" alt=\"back\">\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <div class=\"menu-icon\">\r\n              <span class=\"shopping-cart\" (click)=\"GoToCart()\"><img src=\"assets/shopping-cart.svg\" alt=\"shopping\"><span\r\n                  class=\"cart-number\">{{cartCount}}</span></span>\r\n              <span class=\"user-icon\" slot=\"start\" (click)=\"GoToUserProfile()\">\r\n                <img src=\"assets/user-profile.png\" alt=\"user-icon\">\r\n              </span>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <div class=\"packages-header\">\r\n              <h2>Featured Packages</h2>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n<ion-row *ngIf=\"isFoundation\">  \r\n  <div class=\"tab-list-view\" *ngIf=\"featured_packages_caf\">\r\n    <ion-row class=\"chapter-box\" *ngFor=\"let packages of featured_packages_caf, let i = index\">\r\n      <ion-col size=\"8\" class=\"chapter-test-heading\">\r\n        <h3>{{packages.name}}</h3>\r\n      </ion-col>\r\n      <ion-col size=\"4\" class=\"price-chapter\">\r\n        <span>INR {{packages.price}}/-</span>\r\n      </ion-col>\r\n      <ion-col size=\"7\" class=\"test-available-chapter\">\r\n        <p>{{packages.description}}</p>\r\n      </ion-col>\r\n      <ion-col size=\"7\" class=\"test-available-chapter\">\r\n        <b>Subject:</b><p>{{packages.subject.subject_name}}</p>\r\n      </ion-col>\r\n      <ion-col size=\"5\" class=\"view-more-btn\">\r\n        <div class=\"test-cart text-right\">\r\n          <!-- <ion-button  ion-button full class=\"start-btn\" >Add to Cart<ion-icon name=\"arrow-forward-outline\" class=\"aerrow-btn\"></ion-icon></ion-button> -->\r\n          <ion-button *ngIf=\"packages.price != '0'\" ion-button full class=\"start-btn\" (click)=\"addToCart(packages.id, i, 'caf', packages.price)\" [disabled]=\"packages.cart_item || packages.isadded\">{{packages.cart_item || packages.isadded ? 'Added' : 'Add to Cart'}} <ion-icon name=\"arrow-forward-outline\" class=\"aerrow-btn\"></ion-icon></ion-button>\r\n          <ion-button *ngIf=\"packages.price == '0'\" ion-button full class=\"start-btn\" (click)=\"addToCart(packages.id, i, 'caf', packages.price)\" [disabled]=\"packages.cart_item || packages.isadded\">{{packages.cart_item || packages.isadded? 'Added' : 'Add to Packages'}} <ion-icon name=\"arrow-forward-outline\" class=\"aerrow-btn\"></ion-icon></ion-button>\r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n  </div>\r\n</ion-row>\r\n\r\n  <ion-row *ngIf=\"!isFoundation\">\r\n    <h2 class=\"group-text\">Group 1</h2>\r\n    <div class=\"tab-list-view\" *ngIf=\"featured_packages_group1\">\r\n      <ion-row class=\"chapter-box\" *ngFor=\"let packages of featured_packages_group1, let i = index\">\r\n        <ion-col size=\"8\" class=\"chapter-test-heading\">\r\n          <h3>{{packages.name}}</h3>\r\n        </ion-col>\r\n        <ion-col size=\"4\" class=\"price-chapter\">\r\n          <span>INR {{packages.price}}/-</span>\r\n        </ion-col>\r\n        <ion-col size=\"7\" class=\"test-available-chapter\">\r\n          <p>{{packages.description}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"7\" class=\"test-available-chapter\">\r\n          <b>Subject:</b><p>{{packages.subject.subject_name}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"5\" class=\"view-more-btn\">\r\n          <div class=\"test-cart text-right\">\r\n            <!-- <ion-button  ion-button full class=\"start-btn\" >Add to Cart<ion-icon name=\"arrow-forward-outline\" class=\"aerrow-btn\"></ion-icon></ion-button> -->\r\n            <ion-button *ngIf=\"packages.price != '0'\" ion-button full class=\"start-btn\" (click)=\"addToCart(packages.id, i, 'group1', packages.price)\" [disabled]=\"packages.cart_item || packages.isadded\">{{packages.cart_item || packages.isadded ? 'Added' : 'Add to Cart'}} <ion-icon name=\"arrow-forward-outline\" class=\"aerrow-btn\"></ion-icon></ion-button>\r\n            <ion-button *ngIf=\"packages.price == '0'\" ion-button full class=\"start-btn\" (click)=\"addToCart(packages.id, i, 'group1', packages.price)\" [disabled]=\"packages.cart_item || packages.isadded\">{{packages.cart_item || packages.isadded? 'Added' : 'Add to Packages'}} <ion-icon name=\"arrow-forward-outline\" class=\"aerrow-btn\"></ion-icon></ion-button>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </div>\r\n    <div class=\"tab-list-view\" *ngIf = \"featured_packages_group1 == ''\">\r\n      <p class=\"m-20\">No Featured packages</p>\r\n    </div>\r\n  </ion-row>\r\n\r\n  <ion-row *ngIf=\"!isFoundation\">\r\n    <h2 class=\"group-text\">Group 2</h2>\r\n    <div class=\"tab-list-view\" *ngIf=\"featured_packages_group2\">\r\n      <ion-row class=\"chapter-box\" *ngFor=\"let packages of featured_packages_group2, let i = index\">\r\n        <ion-col size=\"8\" class=\"chapter-test-heading\">\r\n          <h3>{{packages.name}}</h3>\r\n        </ion-col>\r\n        <ion-col size=\"4\" class=\"price-chapter\">\r\n          <span>INR {{packages.price}}/-</span>\r\n        </ion-col>\r\n        <ion-col size=\"7\" class=\"test-available-chapter\">\r\n          <p>{{packages.description}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"7\" class=\"test-available-chapter\">\r\n          <b>Subject:</b><p>{{packages.subject.subject_name}}</p>\r\n        </ion-col>\r\n        <ion-col size=\"5\" class=\"view-more-btn\">\r\n          <div class=\"test-cart text-right\">\r\n            <!-- <ion-button  ion-button full class=\"start-btn\" >Add to Cart\r\n              <ion-icon name=\"arrow-forward-outline\" class=\"aerrow-btn\"></ion-icon>\r\n            </ion-button> -->\r\n            <ion-button *ngIf=\"packages.price != '0'\" ion-button full class=\"start-btn\" (click)=\"addToCart(packages.id, i, 'group2', packages.price)\" [disabled]=\"packages.cart_item || packages.isadded\">{{packages.cart_item || packages.isadded ? 'Added' : 'Add to Cart'}}<ion-icon name=\"arrow-forward-outline\" class=\"aerrow-btn\"></ion-icon></ion-button>\r\n            <ion-button *ngIf=\"packages.price == '0'\" ion-button full class=\"start-btn\" (click)=\"addToCart(packages.id, i, 'group2', packages.price)\" [disabled]=\"packages.cart_item || packages.isadded\">{{packages.cart_item || packages.isadded? 'Added' : 'Add to Packages'}}<ion-icon name=\"arrow-forward-outline\" class=\"aerrow-btn\"></ion-icon></ion-button>\r\n   \r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </div>\r\n    <div class=\"tab-list-view\" *ngIf = \"featured_packages_group2 == ''\">\r\n      <p class=\"m-20\">No Featured packages</p>\r\n    </div>\r\n  </ion-row>\r\n  ");

/***/ })

}]);
//# sourceMappingURL=featured-packages-featured-packages-module.js.map