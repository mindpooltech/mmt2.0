(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["my-packages-my-packages-module"],{

/***/ "6n3N":
/*!*******************************************************!*\
  !*** ./src/app/pages/my-packages/my-packages.page.ts ***!
  \*******************************************************/
/*! exports provided: MyPackagesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyPackagesPage", function() { return MyPackagesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_my_packages_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./my-packages.page.html */ "9bHh");
/* harmony import */ var _my_packages_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./my-packages.page.scss */ "FKxu");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");











let MyPackagesPage = class MyPackagesPage {
    constructor(router, http, httpService, storageService, loadingController, navCtrl, uihelper) {
        this.router = router;
        this.http = http;
        this.httpService = httpService;
        this.storageService = storageService;
        this.loadingController = loadingController;
        this.navCtrl = navCtrl;
        this.uihelper = uihelper;
        this.url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_10__["Constant"].apiUrl;
        this.course_id = {
            course_id: localStorage.getItem('course_id')
        };
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpHeaders"]({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
        });
        this.options = { headers: this.headers };
    }
    ngOnInit() {
        try {
            this.router.events.subscribe((event) => {
                this.selfPath = '';
                if (event && event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_8__["NavigationEnd"] && event.url) {
                    this.selfPath = event.url + '/my-packages';
                }
                this.selfPath = this.router.routerState.snapshot.url;
                // this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'packages') + 'packages';
            });
        }
        catch (e) {
            console.log(e);
        }
    }
    ionViewWillEnter() {
        this.cartCount = localStorage.getItem('cartCount');
        this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_10__["Constant"].AUTH).then((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.userData = res.user_details;
            this.course_name = this.userData.course_name;
            this.user_id = res.user_details.id;
            this.course_id = res.user_details.course_id;
            this.uihelper.ShowSpinner();
            //   this.http.get(this.url + 'cart-count/' + this.user_id, this.options).subscribe((res)=>{   
            //     this.cartCount = res['payload']
            // });
            this.httpService.afterLoginGet("cart_count", res.user_details.id).subscribe((res) => {
                this.cartCount = res['payload'].cart_count;
            });
            this.http.get(this.url + 'my-all-packages/' + this.user_id, this.options).subscribe((res) => {
                this.myAllPackageList = res['payload'];
                console.log('aaaa', res);
                // this.myPackages = this.myAllPackageList.map((data)=>{
                //   return data.package
                // })
                // console.log('aaaa',this.myPackages);   
                this.uihelper.HideSpinner();
            }, (err) => {
                this.uihelper.HideSpinner();
                this.uihelper.ShowAlert('My Packages', "Packages Not Found");
            });
        }));
        this.httpService.afterLoginPost('subject-list', this.course_id).subscribe((res) => {
            this.subjectList = res.payload;
        });
    }
    getPackages(event) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.uihelper.ShowSpinner();
            let subject_data = {
                subject_id: event.target.value,
                user_id: this.user_id
            };
            this.httpService.afterLoginPost('my-packages', subject_data).subscribe((res) => {
                this.myAllPackageList = res['payload'];
                this.uihelper.HideSpinner();
            }, (err) => {
                this.uihelper.HideSpinner();
                // var err = Object.values(err.error.payload)[0][0];        
                this.uihelper.ShowAlert('', err.error.error.message);
            });
        });
    }
    getHome() {
        this.navCtrl.pop();
    }
    GoToCart() {
        this.router.navigate([this.selfPath + "/cart"]);
    }
    GoToUserProfile() {
        this.router.navigate([this.selfPath + "/user-profile"]);
    }
    GoToTestBoard() {
        this.router.navigate([this.selfPath + "/test-board"]);
    }
};
MyPackagesPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_7__["HttpService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_4__["StorageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_9__["UIHelper"] }
];
MyPackagesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-my-packages',
        template: _raw_loader_my_packages_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_my_packages_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MyPackagesPage);



/***/ }),

/***/ "9bHh":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-packages/my-packages.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>My Packages</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n    <ion-col size=\"12\" class=\"header-arrow padding-0\">\r\n      <div>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <div class=\"back\" (click)=\"getHome()\">\r\n                <img src=\"assets/back.svg\" alt=\"back\">\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <div class=\"menu-icon\">\r\n              <span class=\"shopping-cart\" (click)=\"GoToCart()\"><img src=\"assets/shopping-cart.svg\" alt=\"shopping\"><span\r\n                  class=\"cart-number\">{{cartCount ? cartCount : '0'}}</span></span>\r\n              <span class=\"user-icon\" slot=\"start\" (click)=\"GoToUserProfile()\">\r\n                <img src=\"assets/user-profile.png\" alt=\"user-icon\">\r\n              </span>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <div class=\"packages-header\">\r\n              <h2>My Packages</h2>\r\n              <span>{{course_name}}</span>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"space-3\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-list class=\"border-all\">\r\n        <ion-item lines=\"none\">\r\n          <ion-label class=\"reg-num\">Choose Subject</ion-label>\r\n          <ion-select (ionChange)=\"getPackages($event)\">\r\n            <ion-select-option *ngFor=\"let list of subjectList\" [value]=\"list.id\">{{list.subject_name}}</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <div class=\"tab-list-view\" *ngFor=\"let packages of myAllPackageList, let i = index\">\r\n      <ion-row class=\"chapter-box\">\r\n        <ion-col size=\"8\" class=\"chapter-test-heading\">\r\n          <h3>{{packages.package.name}}</h3>\r\n        </ion-col>\r\n        <ion-col size=\"4\" class=\"price-chapter\">\r\n          <span>INR {{packages.package.price}}/-</span>\r\n        </ion-col>\r\n        <ion-col size=\"5\" class=\"test-available-chapter\">\r\n          <p>Tests Available:</p>\r\n        </ion-col>\r\n        <ion-col size=\"7\" class=\"test-available-chapter\">\r\n          <span>{{packages.package.package_tests.length}}</span>\r\n        </ion-col>\r\n        <ion-col size=\"5\" class=\"test-available-chapter\">\r\n          <p>Subject Name:</p>\r\n        </ion-col>\r\n        <ion-col size=\"7\" class=\"test-available-chapter\">\r\n          <span>Principles and Practices of Accounting</span>\r\n        </ion-col>\r\n        <ion-col size=\"4\" class=\"view-more-btn\">\r\n          <!-- <span>View More</span> -->\r\n        </ion-col>\r\n        <ion-col size=\"8\" class=\"view-more-btn\">\r\n          <ion-button class=\"start-btn\" (click)=\"GoToTestBoard()\">Go To Testboard\r\n            <ion-icon name=\"arrow-forward-outline\" class=\"aerrow-btn\"></ion-icon>\r\n          </ion-button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </div>\r\n  </ion-row>\r\n</ion-content>");

/***/ }),

/***/ "FKxu":
/*!*********************************************************!*\
  !*** ./src/app/pages/my-packages/my-packages.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.back img {\n  width: 40px;\n}\n\n.back {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.header-arrow {\n  padding: 20px 15px;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 160px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n}\n\n.packages-header h2 {\n  margin: 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n}\n\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\n.border-all {\n  border: 1px solid #b1bcd4;\n  border-radius: 10px;\n  margin-bottom: 22px;\n  color: #8a959e;\n}\n\n.reg-num {\n  font-size: 15px;\n  font-weight: 300;\n  color: #8a959e;\n  padding-left: 15px;\n}\n\n.space-3 {\n  margin: 25px 20px 0px;\n}\n\nion-label {\n  margin: 0;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n}\n\n.chapter-box {\n  margin: 0px 20px 10px;\n  border-radius: 20px;\n  border: 1px solid #b1bcd440;\n  padding: 10px;\n  box-shadow: 0px 1px 12px -4px #37242442;\n}\n\n.chapter-test-heading h3 {\n  font-weight: 500;\n  margin: 0;\n  font-size: 20px;\n  color: #305F72;\n}\n\n.price-chapter span {\n  font-weight: 400;\n  font-size: 18px;\n  color: #305F72;\n  text-align: right;\n  margin: 0 auto;\n  display: block;\n}\n\n.test-available-chapter p {\n  margin: 2px 0;\n  font-size: 14px;\n  font-weight: 400;\n  color: #305F72;\n}\n\n.test-available-chapter span {\n  font-size: 15px;\n  font-weight: 400;\n  color: #F27376;\n}\n\n.view-more-btn span {\n  font-size: 14px;\n  text-decoration: underline;\n  font-weight: 500;\n  color: #3F5C9B;\n  padding: 8px 0;\n  display: block;\n}\n\n.start-btn {\n  background: #3f5c9b !important;\n  --background-activated: #3f5c9b !important;\n  --background-focused: #3f5c9b !important;\n  --background-hover: #3f5c9b !important;\n  --border-radius: 8px;\n  height: 33px;\n  margin: 0;\n  width: auto;\n  text-align: center;\n  font-size: 13px;\n  font-weight: 400;\n  float: right;\n}\n\nion-button {\n  --background: #3f5c9b !important;\n  border-radius: 10px;\n  margin-top: 60px;\n}\n\n.aerrow-btn {\n  padding-left: 5px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxteS1wYWNrYWdlcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztBQUNmOztBQUNBO0VBQ0ksV0FBVztBQUVmOztBQUFBO0VBQ0ksV0FBVztBQUdmOztBQURBO0VBQ0ksV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsU0FBUztBQUliOztBQUZBO0VBQ0ksaUJBQWlCO0FBS3JCOztBQUhBO0VBQ0ksa0JBQWtCO0FBTXRCOztBQUpBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIsWUFBWTtFQUNaLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZUFBZTtFQUNmLFlBQVk7QUFPaEI7O0FBTEE7RUFDSSxhQUFhO0FBUWpCOztBQU5BO0VBQ0ksa0JBQWtCO0FBU3RCOztBQVBBO0VBQ0ksNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QixhQUFhO0VBQ2IsV0FBVztFQUNYLDJCQUEyQjtFQUMzQixrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLDRCQUE0QjtBQVVoQzs7QUFSQTtFQUNJLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxrQkFBa0I7QUFXdEI7O0FBVEE7RUFDSSxTQUFTO0VBQ1Qsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixjQUFjO0VBQ2QsY0FBYztFQUNkLGdCQUFnQjtBQVlwQjs7QUFWQTtFQUNJLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLGNBQWM7QUFhbEI7O0FBWEE7RUFDSSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxrQkFBa0I7QUFjdEI7O0FBWkE7RUFDSSxxQkFBcUI7QUFlekI7O0FBYkE7RUFDSSxTQUFTO0FBZ0JiOztBQWRBO0VBQ0ksb0JBQWdCO0VBQ2hCLHdCQUFvQjtBQWlCeEI7O0FBZkE7RUFDSSxxQkFBcUI7RUFDckIsbUJBQW1CO0VBQ25CLDJCQUEyQjtFQUMzQixhQUFhO0VBQ2IsdUNBQXVDO0FBa0IzQzs7QUFoQkE7RUFDSSxnQkFBZ0I7RUFDaEIsU0FBUztFQUNULGVBQWU7RUFDZixjQUFjO0FBbUJsQjs7QUFqQkE7RUFDSSxnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsY0FBYztFQUNkLGNBQWM7QUFvQmxCOztBQWxCQTtFQUNJLGFBQWE7RUFDYixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7QUFxQmxCOztBQW5CQTtFQUNJLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsY0FBYztBQXNCbEI7O0FBcEJBO0VBQ0ksZUFBZTtFQUNmLDBCQUEwQjtFQUMxQixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGNBQWM7RUFDZCxjQUFjO0FBdUJsQjs7QUFyQkE7RUFDSSw4QkFBOEI7RUFDOUIsMENBQXVCO0VBQ3ZCLHdDQUFxQjtFQUNyQixzQ0FBbUI7RUFDbkIsb0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixTQUFTO0VBQ1QsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLFlBQVk7QUF3QmhCOztBQXRCQTtFQUNJLGdDQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLGdCQUFnQjtBQXlCcEI7O0FBdkJBO0VBQ0ksaUJBQWlCO0FBMEJyQiIsImZpbGUiOiJteS1wYWNrYWdlcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudXNlci1pY29uIGltZyB7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICB3aWR0aDogNDBweDtcclxufVxyXG4uYmFjayBpbWcge1xyXG4gICAgd2lkdGg6IDQwcHg7XHJcbn1cclxuLmJhY2sge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuLnNob3BwaW5nLWNhcnQgaW1nIHtcclxuICAgIHdpZHRoOiAzMHB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAtOHB4O1xyXG59XHJcbi5tZW51LWljb24ge1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuLnNob3BwaW5nLWNhcnQge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcbi5jYXJ0LW51bWJlciB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICByaWdodDogMTVweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmJiMDA7XHJcbiAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICB3aWR0aDogMjBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogIzNmNWM5YjtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIHBhZGRpbmc6IDFweDtcclxufVxyXG5pb24taGVhZGVyIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuLmhlYWRlci1hcnJvdyB7XHJcbiAgICBwYWRkaW5nOiAyMHB4IDE1cHg7XHJcbn1cclxuLnRvcC1iZyB7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGhlaWdodDogMTYwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAgMCAzMHB4IDMwcHg7XHJcbn1cclxuLnBhY2thZ2VzLWhlYWRlciBoMntcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG59XHJcbi5wYWNrYWdlcy1oZWFkZXIgc3BhbntcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGNvbG9yOiAjMzA1RjcyO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG59XHJcbi5ib3JkZXItYWxsIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNiMWJjZDQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjJweDtcclxuICAgIGNvbG9yOiAjOGE5NTllO1xyXG59XHJcbi5yZWctbnVtIHtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XHJcbiAgICBjb2xvcjogIzhhOTU5ZTtcclxuICAgIHBhZGRpbmctbGVmdDogMTVweDtcclxufVxyXG4uc3BhY2UtMyB7XHJcbiAgICBtYXJnaW46IDI1cHggMjBweCAwcHg7XHJcbn1cclxuaW9uLWxhYmVsIHtcclxuICAgIG1hcmdpbjogMDtcclxufVxyXG5pb24taXRlbSB7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcclxuICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcclxufVxyXG4uY2hhcHRlci1ib3h7XHJcbiAgICBtYXJnaW46IDBweCAyMHB4IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2IxYmNkNDQwO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJveC1zaGFkb3c6IDBweCAxcHggMTJweCAtNHB4ICMzNzI0MjQ0MjtcclxufVxyXG4uY2hhcHRlci10ZXN0LWhlYWRpbmcgaDN7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgY29sb3I6ICMzMDVGNzI7XHJcbn1cclxuLnByaWNlLWNoYXB0ZXIgc3BhbntcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufVxyXG4udGVzdC1hdmFpbGFibGUtY2hhcHRlciBwe1xyXG4gICAgbWFyZ2luOiAycHggMDtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxufVxyXG4udGVzdC1hdmFpbGFibGUtY2hhcHRlciBzcGFue1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIGNvbG9yOiAjRjI3Mzc2O1xyXG59XHJcbi52aWV3LW1vcmUtYnRuIHNwYW57XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogIzNGNUM5QjtcclxuICAgIHBhZGRpbmc6IDhweCAwO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuLnN0YXJ0LWJ0biB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWhvdmVyOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgIGhlaWdodDogMzNweDtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIGZsb2F0OiByaWdodDtcclxufVxyXG5pb24tYnV0dG9uIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIG1hcmdpbi10b3A6IDYwcHg7XHJcbn1cclxuLmFlcnJvdy1idG57XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxufSJdfQ== */");

/***/ }),

/***/ "pgHy":
/*!*********************************************************!*\
  !*** ./src/app/pages/my-packages/my-packages.module.ts ***!
  \*********************************************************/
/*! exports provided: MyPackagesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyPackagesPageModule", function() { return MyPackagesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _my_packages_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./my-packages-routing.module */ "tw9F");
/* harmony import */ var _my_packages_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./my-packages.page */ "6n3N");







let MyPackagesPageModule = class MyPackagesPageModule {
};
MyPackagesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _my_packages_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyPackagesPageRoutingModule"]
        ],
        declarations: [_my_packages_page__WEBPACK_IMPORTED_MODULE_6__["MyPackagesPage"]]
    })
], MyPackagesPageModule);



/***/ }),

/***/ "tw9F":
/*!*****************************************************************!*\
  !*** ./src/app/pages/my-packages/my-packages-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: MyPackagesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyPackagesPageRoutingModule", function() { return MyPackagesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _my_packages_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./my-packages.page */ "6n3N");




const routes = [
    {
        path: '',
        component: _my_packages_page__WEBPACK_IMPORTED_MODULE_3__["MyPackagesPage"]
    }
];
let MyPackagesPageRoutingModule = class MyPackagesPageRoutingModule {
};
MyPackagesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MyPackagesPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=my-packages-my-packages-module.js.map