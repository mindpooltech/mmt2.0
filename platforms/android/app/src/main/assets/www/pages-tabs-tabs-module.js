(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-tabs-tabs-module"],{

/***/ "1vg1":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/tabs/tabs.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-tabs>\r\n\r\n  <ion-tab-bar slot=\"bottom\">\r\n    <ion-tab-button tab=\"test-board\">\r\n      <ion-icon class=\"test-board\" name=\"settings-outline\"></ion-icon>\r\n    </ion-tab-button>\r\n\r\n\r\n    <ion-tab-button tab=\"dashboard\">\r\n      <ion-icon class=\"dashboard\" name=\"easel-outline\"></ion-icon>\r\n    </ion-tab-button>\r\n\r\n    <ion-tab-button tab=\"packages\">\r\n      <ion-icon class=\"packages\" name=\"person-circle-outline\"></ion-icon>\r\n    </ion-tab-button>\r\n\r\n  </ion-tab-bar>\r\n\r\n</ion-tabs>");

/***/ }),

/***/ "Gg5j":
/*!*******************************************!*\
  !*** ./src/app/pages/tabs/tabs.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-tab-bar {\n  height: 60px;\n  border-radius: 30px 30px 0 0;\n  border: 1px solid #B9C9EE;\n  box-shadow: 0px 3px 12px 0px #9a9a9a66;\n  border-bottom: none;\n}\n\n.tab-selected .test-board {\n  background-image: url('test-board-active.svg');\n  background-repeat: no-repeat;\n  background-size: cover;\n  width: 27px;\n  height: 30px;\n}\n\n.test-board:before {\n  content: \"\\f448\";\n  font-size: 0;\n}\n\n.test-board {\n  background-image: url('test-board.svg');\n  background-repeat: no-repeat;\n  background-size: cover;\n  width: 22px;\n  height: 25px;\n}\n\n.test-board:before {\n  content: \"\\f447\";\n  font-size: 0;\n}\n\n.tab-selected .dashboard {\n  background-image: url('dashboard-active.svg');\n  background-repeat: no-repeat;\n  background-size: cover;\n  width: 30px;\n  height: 30px;\n}\n\n.dashboard:before {\n  content: \"\\f448\";\n  font-size: 0;\n}\n\n.dashboard {\n  background-image: url('dashboard.svg');\n  background-repeat: no-repeat;\n  background-size: cover;\n  width: 24px;\n  height: 24px;\n}\n\n.dashboard:before {\n  content: \"\\f447\";\n  font-size: 0;\n}\n\n.tab-selected .packages {\n  background-image: url('packages-active.svg');\n  background-repeat: no-repeat;\n  background-size: cover;\n  width: 30px;\n  height: 25px;\n}\n\n.packages:before {\n  content: \"\\f448\";\n  font-size: 0;\n}\n\n.packages {\n  background-image: url('packages.svg');\n  background-repeat: no-repeat;\n  background-size: cover;\n  width: 24px;\n  height: 19px;\n}\n\n.packages:before {\n  content: \"\\f447\";\n  font-size: 0;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFx0YWJzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQVk7RUFDWiw0QkFBNEI7RUFDNUIseUJBQXlCO0VBQ3pCLHNDQUFzQztFQUN0QyxtQkFBbUI7QUFDdkI7O0FBR0E7RUFDSSw4Q0FBcUU7RUFDckUsNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QixXQUFXO0VBQ1gsWUFBWTtBQUFoQjs7QUFFQTtFQUNJLGdCQUFnQjtFQUNoQixZQUFZO0FBQ2hCOztBQUNBO0VBQ0ksdUNBQThEO0VBQzlELDRCQUE0QjtFQUM1QixzQkFBc0I7RUFDdEIsV0FBVztFQUNYLFlBQVk7QUFFaEI7O0FBQUE7RUFDSSxnQkFBZ0I7RUFDaEIsWUFBWTtBQUdoQjs7QUFDQTtFQUNJLDZDQUFvRTtFQUNwRSw0QkFBNEI7RUFDNUIsc0JBQXNCO0VBQ3RCLFdBQVc7RUFDWCxZQUFZO0FBRWhCOztBQUFBO0VBQ0ksZ0JBQWdCO0VBQ2hCLFlBQVk7QUFHaEI7O0FBREE7RUFDSSxzQ0FBNkQ7RUFDN0QsNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QixXQUFXO0VBQ1gsWUFBWTtBQUloQjs7QUFGQTtFQUNJLGdCQUFnQjtFQUNoQixZQUFZO0FBS2hCOztBQURBO0VBQ0ksNENBQW1FO0VBQ25FLDRCQUE0QjtFQUM1QixzQkFBc0I7RUFDdEIsV0FBVztFQUNYLFlBQVk7QUFJaEI7O0FBRkE7RUFDSSxnQkFBZ0I7RUFDaEIsWUFBWTtBQUtoQjs7QUFIQTtFQUNJLHFDQUE0RDtFQUM1RCw0QkFBNEI7RUFDNUIsc0JBQXNCO0VBQ3RCLFdBQVc7RUFDWCxZQUFZO0FBTWhCOztBQUpBO0VBQ0ksZ0JBQWdCO0VBQ2hCLFlBQVk7QUFPaEIiLCJmaWxlIjoidGFicy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdGFiLWJhcntcclxuICAgIGhlaWdodDogNjBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDMwcHggMzBweCAwIDA7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjQjlDOUVFO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDNweCAxMnB4IDBweCAjOWE5YTlhNjY7XHJcbiAgICBib3JkZXItYm90dG9tOiBub25lO1xyXG59XHJcblxyXG4vLyB0ZXN0LWJvYXJkXHJcbi50YWItc2VsZWN0ZWQgLnRlc3QtYm9hcmQgIHtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi8uLi9hc3NldHMvdGFiLW1lbnUvdGVzdC1ib2FyZC1hY3RpdmUuc3ZnKTtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgd2lkdGg6IDI3cHg7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbn1cclxuLnRlc3QtYm9hcmQ6YmVmb3JlIHtcclxuICAgIGNvbnRlbnQ6IFwiXFxmNDQ4XCI7XHJcbiAgICBmb250LXNpemU6IDA7XHJcbn1cclxuLnRlc3QtYm9hcmQge1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uLy4uL2Fzc2V0cy90YWItbWVudS90ZXN0LWJvYXJkLnN2Zyk7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIHdpZHRoOiAyMnB4O1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG59XHJcbi50ZXN0LWJvYXJkOmJlZm9yZSB7XHJcbiAgICBjb250ZW50OiBcIlxcZjQ0N1wiO1xyXG4gICAgZm9udC1zaXplOiAwO1xyXG59XHJcblxyXG4vLyBkYXNoYm9hcmRcclxuLnRhYi1zZWxlY3RlZCAuZGFzaGJvYXJkIHtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi8uLi9hc3NldHMvdGFiLW1lbnUvZGFzaGJvYXJkLWFjdGl2ZS5zdmcpO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIGhlaWdodDogMzBweDtcclxufVxyXG4uZGFzaGJvYXJkOmJlZm9yZSB7XHJcbiAgICBjb250ZW50OiBcIlxcZjQ0OFwiO1xyXG4gICAgZm9udC1zaXplOiAwO1xyXG59XHJcbi5kYXNoYm9hcmQge1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uLy4uL2Fzc2V0cy90YWItbWVudS9kYXNoYm9hcmQuc3ZnKTtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgd2lkdGg6IDI0cHg7XHJcbiAgICBoZWlnaHQ6IDI0cHg7XHJcbn1cclxuLmRhc2hib2FyZDpiZWZvcmUge1xyXG4gICAgY29udGVudDogXCJcXGY0NDdcIjtcclxuICAgIGZvbnQtc2l6ZTogMDtcclxufVxyXG5cclxuLy8gcGFja2FnZXNcclxuLnRhYi1zZWxlY3RlZCAucGFja2FnZXMge1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uLy4uL2Fzc2V0cy90YWItbWVudS9wYWNrYWdlcy1hY3RpdmUuc3ZnKTtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgd2lkdGg6IDMwcHg7XHJcbiAgICBoZWlnaHQ6IDI1cHg7XHJcbn1cclxuLnBhY2thZ2VzOmJlZm9yZSB7XHJcbiAgICBjb250ZW50OiBcIlxcZjQ0OFwiO1xyXG4gICAgZm9udC1zaXplOiAwO1xyXG59XHJcbi5wYWNrYWdlcyB7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vLi4vYXNzZXRzL3RhYi1tZW51L3BhY2thZ2VzLnN2Zyk7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIHdpZHRoOiAyNHB4O1xyXG4gICAgaGVpZ2h0OiAxOXB4O1xyXG59XHJcbi5wYWNrYWdlczpiZWZvcmUge1xyXG4gICAgY29udGVudDogXCJcXGY0NDdcIjtcclxuICAgIGZvbnQtc2l6ZTogMDtcclxufSJdfQ== */");

/***/ }),

/***/ "MM9G":
/*!***************************************************!*\
  !*** ./src/app/pages/tabs/tabs-routing.module.ts ***!
  \***************************************************/
/*! exports provided: TabsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageRoutingModule", function() { return TabsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tabs.page */ "TA0h");




const routes = [
    {
        path: '',
        component: _tabs_page__WEBPACK_IMPORTED_MODULE_3__["TabsPage"],
        children: [
            {
                path: 'dashboard',
                children: [
                    {
                        path: '',
                        loadChildren: () => __webpack_require__.e(/*! import() | dashboard-dashboard-module */ "dashboard-dashboard-module").then(__webpack_require__.bind(null, /*! ../dashboard/dashboard.module */ "/2RN")).then(m => m.DashboardPageModule)
                    },
                    {
                        path: 'refer-and-earn',
                        children: [
                            {
                                path: '',
                                loadChildren: () => __webpack_require__.e(/*! import() | refer-and-earn-refer-and-earn-module */ "refer-and-earn-refer-and-earn-module").then(__webpack_require__.bind(null, /*! ../refer-and-earn/refer-and-earn.module */ "tjE2")).then(m => m.ReferAndEarnPageModule)
                            },
                            {
                                path: 'cart',
                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                            },
                            {
                                path: 'user-profile',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                    },
                                    {
                                        path: 'edit-user-profile',
                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                    }
                                ]
                            },
                        ]
                    },
                    {
                        path: 'packages',
                        children: [
                            {
                                path: '',
                                loadChildren: () => Promise.all(/*! import() | packages-packages-module */[__webpack_require__.e("common"), __webpack_require__.e("packages-packages-module")]).then(__webpack_require__.bind(null, /*! ../packages/packages.module */ "8D2i")).then(m => m.PackagesPageModule)
                            },
                            {
                                path: 'cart',
                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                            },
                            {
                                path: 'user-profile',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                    },
                                    {
                                        path: 'edit-user-profile',
                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                    }
                                ]
                            },
                        ]
                    },
                    {
                        path: 'featured-packages',
                        children: [
                            {
                                path: '',
                                loadChildren: () => __webpack_require__.e(/*! import() | featured-packages-featured-packages-module */ "featured-packages-featured-packages-module").then(__webpack_require__.bind(null, /*! ../featured-packages/featured-packages.module */ "eIpb")).then(m => m.FeaturedPackagesPageModule)
                            },
                            {
                                path: 'cart',
                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                            },
                            {
                                path: 'user-profile',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                    },
                                    {
                                        path: 'edit-user-profile',
                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                    }
                                ]
                            },
                        ]
                    },
                    {
                        path: 'cart',
                        loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                    },
                    {
                        path: 'group-packages',
                        children: [
                            {
                                path: '',
                                loadChildren: () => Promise.all(/*! import() | group-packages-group-packages-module */[__webpack_require__.e("common"), __webpack_require__.e("group-packages-group-packages-module")]).then(__webpack_require__.bind(null, /*! ../group-packages/group-packages.module */ "7Upg")).then(m => m.GroupPackagesPageModule)
                            },
                            {
                                path: 'cart',
                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                            },
                            {
                                path: 'user-profile',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                    },
                                    {
                                        path: 'edit-user-profile',
                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                    }
                                ]
                            },
                        ]
                    },
                    {
                        path: 'user-profile',
                        children: [
                            {
                                path: '',
                                loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                            },
                            {
                                path: 'edit-user-profile',
                                loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                            }
                        ]
                    },
                    {
                        path: 'my-ratings',
                        children: [
                            {
                                path: '',
                                loadChildren: () => __webpack_require__.e(/*! import() | my-ratings-my-ratings-module */ "my-ratings-my-ratings-module").then(__webpack_require__.bind(null, /*! ../my-ratings/my-ratings.module */ "rzzE")).then(m => m.MyRatingsPageModule)
                            },
                            {
                                path: 'cart',
                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                            },
                            {
                                path: 'user-profile',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                    },
                                    {
                                        path: 'edit-user-profile',
                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                    }
                                ]
                            },
                        ]
                    },
                    {
                        path: 'notes-details',
                        children: [
                            {
                                path: '',
                                loadChildren: () => Promise.all(/*! import() | notes-details-notes-details-module */[__webpack_require__.e("default~notes-details-notes-details-module~test-evaluation-test-evaluation-module"), __webpack_require__.e("notes-details-notes-details-module")]).then(__webpack_require__.bind(null, /*! ../notes-details/notes-details.module */ "TpMJ")).then(m => m.NotesDetailsPageModule)
                            }, {
                                path: 'cart',
                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                            },
                            {
                                path: 'user-profile',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                    },
                                    {
                                        path: 'edit-user-profile',
                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        path: 'my-videos',
                        children: [
                            {
                                path: '',
                                loadChildren: () => Promise.all(/*! import() | my-videos-my-videos-module */[__webpack_require__.e("common"), __webpack_require__.e("my-videos-my-videos-module")]).then(__webpack_require__.bind(null, /*! ../my-videos/my-videos.module */ "ix0U")).then(m => m.MyVideosPageModule)
                            },
                            {
                                path: 'cart',
                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                            },
                            {
                                path: 'user-profile',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                    },
                                    {
                                        path: 'edit-user-profile',
                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                    }
                                ]
                            },
                        ]
                    },
                    {
                        path: 'my-queries',
                        children: [
                            {
                                path: '',
                                loadChildren: () => __webpack_require__.e(/*! import() | my-queries-my-queries-module */ "my-queries-my-queries-module").then(__webpack_require__.bind(null, /*! ../my-queries/my-queries.module */ "5q53")).then(m => m.MyQueriesPageModule)
                            },
                            {
                                path: 'cart',
                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                            },
                            {
                                path: 'user-profile',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                    },
                                    {
                                        path: 'edit-user-profile',
                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                    }
                                ]
                            },
                        ]
                    },
                    {
                        path: 'my-packages',
                        children: [
                            {
                                path: '',
                                loadChildren: () => __webpack_require__.e(/*! import() | my-packages-my-packages-module */ "my-packages-my-packages-module").then(__webpack_require__.bind(null, /*! ../my-packages/my-packages.module */ "pgHy")).then(m => m.MyPackagesPageModule)
                            },
                            {
                                path: 'cart',
                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                            },
                            {
                                path: 'user-profile',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                    },
                                    {
                                        path: 'edit-user-profile',
                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                    }
                                ]
                            },
                            {
                                path: 'test-board',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | test-board-test-board-module */ "test-board-test-board-module").then(__webpack_require__.bind(null, /*! ../test-board/test-board.module */ "URWB")).then(m => m.TestBoardPageModule)
                                    },
                                    {
                                        path: 'cart',
                                        loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                                    },
                                    {
                                        path: 'user-profile',
                                        children: [
                                            {
                                                path: '',
                                                loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                            },
                                            {
                                                path: 'edit-user-profile',
                                                loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                            }
                                        ]
                                    },
                                    {
                                        path: 'test-history',
                                        loadChildren: () => __webpack_require__.e(/*! import() | test-history-test-history-module */ "test-history-test-history-module").then(__webpack_require__.bind(null, /*! ../test-history/test-history.module */ "Crn+")).then(m => m.TestHistoryPageModule)
                                    },
                                    {
                                        path: 'new-test',
                                        loadChildren: () => __webpack_require__.e(/*! import() | new-test-new-test-module */ "new-test-new-test-module").then(__webpack_require__.bind(null, /*! ../new-test/new-test.module */ "q7J+")).then(m => m.NewTestPageModule)
                                    }, {
                                        path: 'start-test',
                                        children: [
                                            {
                                                path: '',
                                                loadChildren: () => Promise.all(/*! import() | start-test-start-test-module */[__webpack_require__.e("common"), __webpack_require__.e("start-test-start-test-module")]).then(__webpack_require__.bind(null, /*! ../start-test/start-test.module */ "VTbd")).then(m => m.StartTestPageModule)
                                            }, {
                                                path: 'cart',
                                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                                            },
                                            {
                                                path: 'user-profile',
                                                children: [
                                                    {
                                                        path: '',
                                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                                    },
                                                    {
                                                        path: 'edit-user-profile',
                                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                                    }
                                                ]
                                            }, {
                                                path: 'end-test',
                                                loadChildren: () => __webpack_require__.e(/*! import() | end-test-end-test-module */ "end-test-end-test-module").then(__webpack_require__.bind(null, /*! ../end-test/end-test.module */ "eBAB")).then(m => m.EndTestPageModule)
                                            }
                                        ]
                                    }, {
                                        path: 'test-history',
                                        children: [
                                            {
                                                path: '',
                                                loadChildren: () => __webpack_require__.e(/*! import() | test-history-test-history-module */ "test-history-test-history-module").then(__webpack_require__.bind(null, /*! ../test-history/test-history.module */ "Crn+")).then(m => m.TestHistoryPageModule)
                                            }, {
                                                path: 'cart',
                                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                                            },
                                            {
                                                path: 'user-profile',
                                                children: [
                                                    {
                                                        path: '',
                                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                                    },
                                                    {
                                                        path: 'edit-user-profile',
                                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                                    }
                                                ]
                                            },
                                            {
                                                path: 'test-evaluation',
                                                children: [
                                                    {
                                                        path: '',
                                                        loadChildren: () => Promise.all(/*! import() | test-evaluation-test-evaluation-module */[__webpack_require__.e("default~notes-details-notes-details-module~test-evaluation-test-evaluation-module"), __webpack_require__.e("common"), __webpack_require__.e("test-evaluation-test-evaluation-module")]).then(__webpack_require__.bind(null, /*! ../test-evaluation/test-evaluation.module */ "lKpJ")).then(m => m.TestEvaluationPageModule)
                                                    }, {
                                                        path: 'cart',
                                                        loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                                                    },
                                                    {
                                                        path: 'user-profile',
                                                        children: [
                                                            {
                                                                path: '',
                                                                loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                                            },
                                                            {
                                                                path: 'edit-user-profile',
                                                                loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        path: 'test-evaluation-scorecard',
                                                        loadChildren: () => __webpack_require__.e(/*! import() | test-evaluation-scorecard-test-evaluation-scorecard-module */ "test-evaluation-scorecard-test-evaluation-scorecard-module").then(__webpack_require__.bind(null, /*! ../test-evaluation-scorecard/test-evaluation-scorecard.module */ "mebM")).then(m => m.TestEvaluationScorecardPageModule)
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                ]
                            }
                        ]
                    },
                    {
                        path: 'test-history',
                        children: [
                            {
                                path: '',
                                loadChildren: () => __webpack_require__.e(/*! import() | test-history-test-history-module */ "test-history-test-history-module").then(__webpack_require__.bind(null, /*! ../test-history/test-history.module */ "Crn+")).then(m => m.TestHistoryPageModule)
                            },
                            {
                                path: 'cart',
                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                            },
                            {
                                path: 'user-profile',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                    },
                                    {
                                        path: 'edit-user-profile',
                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                    }
                                ]
                            },
                            {
                                path: 'test-evaluation',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => Promise.all(/*! import() | test-evaluation-test-evaluation-module */[__webpack_require__.e("default~notes-details-notes-details-module~test-evaluation-test-evaluation-module"), __webpack_require__.e("common"), __webpack_require__.e("test-evaluation-test-evaluation-module")]).then(__webpack_require__.bind(null, /*! ../test-evaluation/test-evaluation.module */ "lKpJ")).then(m => m.TestEvaluationPageModule)
                                    }, {
                                        path: 'cart',
                                        loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                                    },
                                    {
                                        path: 'user-profile',
                                        children: [
                                            {
                                                path: '',
                                                loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                            },
                                            {
                                                path: 'edit-user-profile',
                                                loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                            }
                                        ]
                                    },
                                    {
                                        path: 'test-evaluation-scorecard',
                                        loadChildren: () => __webpack_require__.e(/*! import() | test-evaluation-scorecard-test-evaluation-scorecard-module */ "test-evaluation-scorecard-test-evaluation-scorecard-module").then(__webpack_require__.bind(null, /*! ../test-evaluation-scorecard/test-evaluation-scorecard.module */ "mebM")).then(m => m.TestEvaluationScorecardPageModule)
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        path: 'test-board',
                        children: [
                            {
                                path: '',
                                loadChildren: () => __webpack_require__.e(/*! import() | test-board-test-board-module */ "test-board-test-board-module").then(__webpack_require__.bind(null, /*! ../test-board/test-board.module */ "URWB")).then(m => m.TestBoardPageModule)
                            },
                            {
                                path: 'cart',
                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                            },
                            {
                                path: 'user-profile',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                    },
                                    {
                                        path: 'edit-user-profile',
                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                    }
                                ]
                            },
                            {
                                path: 'new-test',
                                loadChildren: () => __webpack_require__.e(/*! import() | new-test-new-test-module */ "new-test-new-test-module").then(__webpack_require__.bind(null, /*! ../new-test/new-test.module */ "q7J+")).then(m => m.NewTestPageModule)
                            },
                            {
                                path: 'start-test',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => Promise.all(/*! import() | start-test-start-test-module */[__webpack_require__.e("common"), __webpack_require__.e("start-test-start-test-module")]).then(__webpack_require__.bind(null, /*! ../start-test/start-test.module */ "VTbd")).then(m => m.StartTestPageModule)
                                    }, {
                                        path: 'cart',
                                        loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                                    },
                                    {
                                        path: 'user-profile',
                                        children: [
                                            {
                                                path: '',
                                                loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                            },
                                            {
                                                path: 'edit-user-profile',
                                                loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                            }
                                        ]
                                    }, {
                                        path: 'end-test',
                                        loadChildren: () => __webpack_require__.e(/*! import() | end-test-end-test-module */ "end-test-end-test-module").then(__webpack_require__.bind(null, /*! ../end-test/end-test.module */ "eBAB")).then(m => m.EndTestPageModule)
                                    }
                                ]
                            },
                            {
                                path: 'test-history',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | test-history-test-history-module */ "test-history-test-history-module").then(__webpack_require__.bind(null, /*! ../test-history/test-history.module */ "Crn+")).then(m => m.TestHistoryPageModule)
                                    },
                                    {
                                        path: 'cart',
                                        loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                                    },
                                    {
                                        path: 'user-profile',
                                        children: [
                                            {
                                                path: '',
                                                loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                            },
                                            {
                                                path: 'edit-user-profile',
                                                loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                            }
                                        ]
                                    },
                                    {
                                        path: 'test-evaluation',
                                        children: [
                                            {
                                                path: '',
                                                loadChildren: () => Promise.all(/*! import() | test-evaluation-test-evaluation-module */[__webpack_require__.e("default~notes-details-notes-details-module~test-evaluation-test-evaluation-module"), __webpack_require__.e("common"), __webpack_require__.e("test-evaluation-test-evaluation-module")]).then(__webpack_require__.bind(null, /*! ../test-evaluation/test-evaluation.module */ "lKpJ")).then(m => m.TestEvaluationPageModule)
                                            },
                                            {
                                                path: 'test-evaluation-scorecard',
                                                loadChildren: () => __webpack_require__.e(/*! import() | test-evaluation-scorecard-test-evaluation-scorecard-module */ "test-evaluation-scorecard-test-evaluation-scorecard-module").then(__webpack_require__.bind(null, /*! ../test-evaluation-scorecard/test-evaluation-scorecard.module */ "mebM")).then(m => m.TestEvaluationScorecardPageModule)
                                            },
                                            {
                                                path: 'cart',
                                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                                            },
                                            {
                                                path: 'user-profile',
                                                children: [
                                                    {
                                                        path: '',
                                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                                    },
                                                    {
                                                        path: 'edit-user-profile',
                                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                                    }
                                                ]
                                            },
                                        ]
                                    },
                                ]
                            },
                            {
                                path: 'new-test',
                                loadChildren: () => __webpack_require__.e(/*! import() | new-test-new-test-module */ "new-test-new-test-module").then(__webpack_require__.bind(null, /*! ../new-test/new-test.module */ "q7J+")).then(m => m.NewTestPageModule)
                            },
                            {
                                path: 'start-test',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => Promise.all(/*! import() | start-test-start-test-module */[__webpack_require__.e("common"), __webpack_require__.e("start-test-start-test-module")]).then(__webpack_require__.bind(null, /*! ../start-test/start-test.module */ "VTbd")).then(m => m.StartTestPageModule)
                                    },
                                    // {
                                    //   path: 'cart',
                                    //   loadChildren: () =>
                                    //     import('../pages/cart/cart.module').then(m => m.CartPageModule)
                                    // },
                                    {
                                        path: 'end-test',
                                        loadChildren: () => __webpack_require__.e(/*! import() | end-test-end-test-module */ "end-test-end-test-module").then(__webpack_require__.bind(null, /*! ../end-test/end-test.module */ "eBAB")).then(m => m.EndTestPageModule)
                                    }
                                ]
                            }
                        ],
                    }
                ],
            },
            {
                path: 'packages',
                children: [
                    {
                        path: '',
                        loadChildren: () => Promise.all(/*! import() | packages-packages-module */[__webpack_require__.e("common"), __webpack_require__.e("packages-packages-module")]).then(__webpack_require__.bind(null, /*! ../packages/packages.module */ "8D2i")).then(m => m.PackagesPageModule)
                    },
                    {
                        path: 'cart',
                        loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                    },
                    {
                        path: 'user-profile',
                        children: [
                            {
                                path: '',
                                loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                            },
                            {
                                path: 'edit-user-profile',
                                loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                            }
                        ]
                    },
                ],
            },
            {
                path: 'test-board',
                children: [
                    {
                        path: '',
                        loadChildren: () => __webpack_require__.e(/*! import() | test-board-test-board-module */ "test-board-test-board-module").then(__webpack_require__.bind(null, /*! ../test-board/test-board.module */ "URWB")).then(m => m.TestBoardPageModule)
                    },
                    {
                        path: 'cart',
                        loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                    },
                    {
                        path: 'new-test',
                        loadChildren: () => __webpack_require__.e(/*! import() | new-test-new-test-module */ "new-test-new-test-module").then(__webpack_require__.bind(null, /*! ../new-test/new-test.module */ "q7J+")).then(m => m.NewTestPageModule)
                    },
                    {
                        path: 'start-test',
                        children: [
                            {
                                path: '',
                                loadChildren: () => Promise.all(/*! import() | start-test-start-test-module */[__webpack_require__.e("common"), __webpack_require__.e("start-test-start-test-module")]).then(__webpack_require__.bind(null, /*! ../start-test/start-test.module */ "VTbd")).then(m => m.StartTestPageModule)
                            }, {
                                path: 'cart',
                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                            },
                            {
                                path: 'user-profile',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                    },
                                    {
                                        path: 'edit-user-profile',
                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                    }
                                ]
                            }, {
                                path: 'end-test',
                                loadChildren: () => __webpack_require__.e(/*! import() | end-test-end-test-module */ "end-test-end-test-module").then(__webpack_require__.bind(null, /*! ../end-test/end-test.module */ "eBAB")).then(m => m.EndTestPageModule)
                            }
                        ]
                    },
                    {
                        path: 'user-profile',
                        children: [
                            {
                                path: '',
                                loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                            },
                            {
                                path: 'edit-user-profile',
                                loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                            }
                        ]
                    },
                    {
                        path: 'test-history',
                        children: [
                            {
                                path: '',
                                loadChildren: () => __webpack_require__.e(/*! import() | test-history-test-history-module */ "test-history-test-history-module").then(__webpack_require__.bind(null, /*! ../test-history/test-history.module */ "Crn+")).then(m => m.TestHistoryPageModule)
                            },
                            {
                                path: 'cart',
                                loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                            },
                            {
                                path: 'user-profile',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                    },
                                    {
                                        path: 'edit-user-profile',
                                        loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                    }
                                ]
                            },
                            {
                                path: 'test-evaluation',
                                children: [
                                    {
                                        path: '',
                                        loadChildren: () => Promise.all(/*! import() | test-evaluation-test-evaluation-module */[__webpack_require__.e("default~notes-details-notes-details-module~test-evaluation-test-evaluation-module"), __webpack_require__.e("common"), __webpack_require__.e("test-evaluation-test-evaluation-module")]).then(__webpack_require__.bind(null, /*! ../test-evaluation/test-evaluation.module */ "lKpJ")).then(m => m.TestEvaluationPageModule)
                                    }, {
                                        path: 'cart',
                                        loadChildren: () => Promise.all(/*! import() | cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ../cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
                                    },
                                    {
                                        path: 'user-profile',
                                        children: [
                                            {
                                                path: '',
                                                loadChildren: () => __webpack_require__.e(/*! import() | user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ../user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
                                            },
                                            {
                                                path: 'edit-user-profile',
                                                loadChildren: () => __webpack_require__.e(/*! import() | edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ../edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
                                            }
                                        ]
                                    },
                                    {
                                        path: 'test-evaluation-scorecard',
                                        loadChildren: () => __webpack_require__.e(/*! import() | test-evaluation-scorecard-test-evaluation-scorecard-module */ "test-evaluation-scorecard-test-evaluation-scorecard-module").then(__webpack_require__.bind(null, /*! ../test-evaluation-scorecard/test-evaluation-scorecard.module */ "mebM")).then(m => m.TestEvaluationScorecardPageModule)
                                    }
                                ]
                            }
                        ]
                    }
                ],
            },
            {
                path: '',
                redirectTo: '/tabs/dashboard',
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '',
        redirectTo: '/tabs/dashboard',
        pathMatch: 'full'
    }
];
let TabsPageRoutingModule = class TabsPageRoutingModule {
};
TabsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TabsPageRoutingModule);



/***/ }),

/***/ "TA0h":
/*!*****************************************!*\
  !*** ./src/app/pages/tabs/tabs.page.ts ***!
  \*****************************************/
/*! exports provided: TabsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPage", function() { return TabsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_tabs_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./tabs.page.html */ "1vg1");
/* harmony import */ var _tabs_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tabs.page.scss */ "Gg5j");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let TabsPage = class TabsPage {
    constructor() { }
    ngOnInit() {
    }
};
TabsPage.ctorParameters = () => [];
TabsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-tabs',
        template: _raw_loader_tabs_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_tabs_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TabsPage);



/***/ }),

/***/ "qiIP":
/*!*******************************************!*\
  !*** ./src/app/pages/tabs/tabs.module.ts ***!
  \*******************************************/
/*! exports provided: TabsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabsPageModule", function() { return TabsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _tabs_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tabs-routing.module */ "MM9G");
/* harmony import */ var _tabs_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tabs.page */ "TA0h");







let TabsPageModule = class TabsPageModule {
};
TabsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _tabs_routing_module__WEBPACK_IMPORTED_MODULE_5__["TabsPageRoutingModule"]
        ],
        declarations: [_tabs_page__WEBPACK_IMPORTED_MODULE_6__["TabsPage"]]
    })
], TabsPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-tabs-tabs-module.js.map