import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';
// import { LaunchReview } from '@ionic-native/launch-review/ngx';

@Component({
  selector: 'app-my-ratings',
  templateUrl: './my-ratings.page.html',
  styleUrls: ['./my-ratings.page.scss'],
})
export class MyRatingsPage implements OnInit {

  userData: any;
  url = Constant.apiUrl
  headers = new HttpHeaders({
    'Accept':  'application/json',
    'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
  });
  options = { headers: this.headers}
  ratings: any;
  cartCount: any;
  selfPath: string;
  constructor(private router: Router,private http: HttpClient,private httpService: HttpService,private storageService:StorageService,private uihelper: UIHelper, private navCtrl:NavController) {
  
   }

   async ngOnInit() {

    try {
      this.router.events.subscribe((event: RouterEvent) => {
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          this.selfPath = event.url + '/my-ratings';
        }
        this.selfPath = this.router.routerState.snapshot.url;
        this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'my-ratings') + 'my-ratings';
      });

    } catch (e) {
      console.log(e);
    }
  }

  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }

  async ionViewWillEnter()
  {
  //   this.launchReview.launch()
  // .then(() => console.log('Successfully launched store app'));

    this.cartCount = localStorage.getItem('cartCount')
    this.uihelper.ShowSpinner();
    this.storageService.get(Constant.AUTH).then(res => {
      this.userData= res.user_details;
      let user_id = res.user_details.id;
      this.httpService.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
        this.cartCount = res['payload'].cart_count        
      })
      this.http.get(this.url + 'student-test-ratings/' + user_id, this.options).subscribe((res)=>{ 
     
        this.ratings = res['payload'];
        // console.log(this.ratings)
        this.ratings.forEach((value,i)=>{
          this.ratings[i]['created_at'] = value.created_at.substring(0, 10)
          this.ratings[i]['index'] = i
          })
          this.uihelper.HideSpinner();

      },
      (err)=>{
        this.uihelper.HideSpinner();
        // console.log('ratings not found', err)
        this.uihelper.urlAlert('My Ratings',"We hope you liked out Application.Please Rate us on play store");
        // this.uihelper.playStoreAlert()
      });
    })
  }

  
  goBack()
  {
    this.navCtrl.pop();
  }

  GoToCart(){
    this.router.navigate([ this.selfPath + "/cart"]);
  }

}
