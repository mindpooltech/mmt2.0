import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyRatingsPage } from './my-ratings.page';

const routes: Routes = [
  {
    path: '',
    component: MyRatingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyRatingsPageRoutingModule {}
