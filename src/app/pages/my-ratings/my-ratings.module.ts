import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyRatingsPageRoutingModule } from './my-ratings-routing.module';

import { MyRatingsPage } from './my-ratings.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyRatingsPageRoutingModule
  ],
  declarations: [MyRatingsPage]
})
export class MyRatingsPageModule {}
