import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { LoadingController, NavController, PopoverController } from '@ionic/angular';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';
import { VideoPopoverPage } from '../video-popover/video-popover.page';

@Component({
  selector: 'app-my-videos',
  templateUrl: './my-videos.page.html',
  styleUrls: ['./my-videos.page.scss'],
})
export class MyVideosPage implements OnInit {

  cartCount: string;
  selfPath: string;
  userData: any;
  course_name: any;
  institute_id: any;
  user_id: any;
  course_id = {
    course_id: localStorage.getItem('course_id')
  }
  subjectList: any;
  video_list: any;
  constructor(private router: Router,
    private http: HttpClient,
    private httpService: HttpService,
    private storageService: StorageService,
    private loadingController: LoadingController,
    private navCtrl: NavController,
    private uihelper: UIHelper,
    private popovercontroller: PopoverController) {
    
  }

  ngOnInit() {
    try {
      this.router.events.subscribe((event: RouterEvent) => {
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          this.selfPath = event.url + '/my-videos';
        }
        this.selfPath = this.router.routerState.snapshot.url;

        this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'my-videos') + 'my-videos';
      });

    } catch (e) {
      console.log(e);
    }
  }

  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }

  async ionViewWillEnter() {
    this.cartCount = localStorage.getItem('cartCount');
    this.uihelper.ShowAlert('','Please select Subject.')
    this.uihelper.ShowSpinner();
    this.storageService.get(Constant.AUTH).then(res => {
      this.userData = res.user_details;
      this.course_name = this.userData.course_name
      this.institute_id = res.user_details.institute_id;
      this.user_id = res.user_details.id;
      this.httpService.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
        this.cartCount = res['payload'].cart_count        
      })
      this.uihelper.HideSpinner(); 
    });    
    this.httpService.afterLoginPost('subject-list', this.course_id).subscribe((res: any) => {
      this.subjectList = res.payload;
      // console.log('subjectList',this.subjectList)
      this.uihelper.HideSpinner(); 
    });
  }

  async getVideos(event)
  {
    this.uihelper.ShowSpinner();
    let data = {
      institute_id: this.institute_id,
      course_id: this.course_id,
      subject_id: event.target.value
    }
    this.httpService.afterLoginPost('subject-wise-videos', data).subscribe((res: any) => {

      this.video_list = res.payload.videos
      this.uihelper.HideSpinner();     
      // console.log('video_list',this.video_list)
       },
       (err:any)=>{
        this.uihelper.HideSpinner(); 
        this.uihelper.ShowAlert('',"Not Found");
        this.video_list = []
       })
  }

  async openPopover(video_id, ev:any)
  {
    const popover = await this.popovercontroller.create({
      component: VideoPopoverPage,
      // cssClass: 'my-custom-class',
      
      componentProps: {
        "video_id": video_id,
      },
    });
    return await popover.present(); 
  }

  goBack() {
    this.router.navigate([ '/tabs']);
  }

  GoToCart(){
    this.router.navigate([ this.selfPath + "/cart"]);
  }

  GoToUserProfile() {
    this.router.navigate([this.selfPath + "/user-profile"]);
  }

}
