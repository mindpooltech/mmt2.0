import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StartTestPage } from './start-test.page';

const routes: Routes = [
  {
    path: '',
    component: StartTestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StartTestPageRoutingModule {}
