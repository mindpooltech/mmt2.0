import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StartTestPageRoutingModule } from './start-test-routing.module';

import { StartTestPage } from './start-test.page';
import { RemovehtmltagsPipe } from '../../removehtmltags.pipe';
import { ApplicationPipesModule } from 'src/app/application-pipes/application-pipes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StartTestPageRoutingModule,
    ApplicationPipesModule
  ],
  declarations: [StartTestPage, ]
})
export class StartTestPageModule {}
