import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { HttpService } from 'src/app/services/http.service';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { isEmptyExpression } from '@angular/compiler';
import { DatePipe, formatDate } from '@angular/common';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';
import * as moment from 'moment';
import { jsPDF } from 'jspdf';
// import jspdf from 'jspdf';
import domtoimage from 'dom-to-image';
import { File, IWriteOptions } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import html2canvas from 'html2canvas'; 

declare var cordova: any;

@Component({
  selector: 'app-start-test',
  templateUrl: './start-test.page.html',
  styleUrls: ['./start-test.page.scss'],
})
export class StartTestPage implements OnInit {

  testdetails: any;
  questionDetails: any;
  test_id: any;
  testResponse: any;
  hour: string;
  minute: string;
  second: string;
  final_timer: string;
  ans_sheet: any;
  mcqAns: any;
  userData: any;
  user_id: any;
  exist_theory_que: any;
  url = Constant.apiUrl
  headers = new HttpHeaders({
    'Accept': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
  });
  options = { headers: this.headers }
  cartCount: any;
  selfPath: string;
  instructions: any;
  objLength: number;
  questionDetailsLength: number;
  target_marks: any;
  institute_id: any;
  toggle: boolean = true; 

  @ViewChild("uploader") uploader: ElementRef;
  isTheory: any;
  isTheoryCount: any = 0;
  last_row: any;
  caseStudies: any;
  studentSelectedCSOption: any;
  isSelectedMcqExist: any;
  user_number: any;
  user_fullname: any;
  alert: any;
  price: any;

  popFileChooser() {
  
    this.uploader.nativeElement.click();
    // document.getElementById('uploader').click();

  }

  constructor(private router: Router,
    public alertController: AlertController,
    private storageService: StorageService,
    private activatedRoute: ActivatedRoute,
    private httpService: HttpService,
    private http: HttpClient,
    private datePipe: DatePipe,
    private navCtrl: NavController,
    private uihelper: UIHelper,
    private file: File,
    private fileOpener: FileOpener) {
    this.cartCount = localStorage.getItem('cartCount')
    this.activatedRoute.queryParams.subscribe((data) => {
      this.test_id = data.test_id
    })
    this.activatedRoute.queryParams.subscribe(params => {
      this.target_marks = params["target_marks"];
      this.test_id = params["test_id"];
    });

  }


    async ngOnInit() {
    try {
      this.router.events.subscribe((event: RouterEvent) => {
        // console.log('event url',event.url);
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          this.selfPath = event.url + '/start-test';
        }
        this.selfPath = this.router.routerState.snapshot.url;
        // console.log("ngOnInit", this.selfPath);

        this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'start-test') + 'start-test';
      });

    } catch (e) {
      console.log(e);
    }

  }

  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }

  async ionViewWillEnter() {

    // this.cartCount = localStorage.getItem('cartCount')
    this.uihelper.ShowSpinner();
    this.storageService.get(Constant.AUTH).then(res => {
      this.user_id = res.user_details.id;
      this.user_number = res.user_details.phone_number
      this.user_fullname = res.user_details.full_name
      this.institute_id = res.user_details.institute_id
      this.httpService.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
        this.cartCount = res['payload'].cart_count        
      })
      let startExamData = {
        target_marks: this.target_marks,
        test_id: this.test_id,
        user_id: this.user_id,
        institute_id: this.institute_id
      }
      // console.log('startExamData', startExamData)
      this.httpService.afterLoginPost('start-test', startExamData).subscribe((res: any) => {
        this.testResponse = res.payload
        console.log('test data response', res.payload)

        this.instructions = res.payload.instructions
        this.testdetails = res.payload['test']
        this.price = this.testdetails.package.price
        
        this.questionDetails = res.payload.sub_que
        this.studentSelectedCSOption = res.payload.studentSelectedCSOption
        this.isSelectedMcqExist = this.questionDetails.filter((e)=>{
          return e.student_selected_mcq_option && e.student_selected_mcq_option != null 
        })
        console.log('isSelectedMcqExist',this.isSelectedMcqExist)
        this.last_row = res.payload.last_row
        this.exist_theory_que = this.testResponse.exist_theory_que
        // console.log(this.testResponse)
        this.uihelper.HideSpinner();
        this.testTimer(this.testResponse)
        this.questionDetails.forEach((value)=>{
          if(value.q_type == "Theory")
          {
            this.isTheoryCount += 1
          }
        })
       
        if(this.questionDetails.length == this.isTheoryCount)
        {
          this.isTheory = true
        }

      });
    });
 
  }

  getDataDiff(startDate, endDate) {
    var diff = endDate.getTime() - startDate.getTime();
    var days = Math.floor(diff / (60 * 60 * 24 * 1000));
    var hours = Math.floor(diff / (60 * 60 * 1000)) - (days * 24);
    var minutes = Math.floor(diff / (60 * 1000)) - ((days * 24 * 60) + (hours * 60));
    var seconds = Math.floor(diff / 1000) - ((days * 24 * 60 * 60) + (hours * 60 * 60) + (minutes * 60));
    return { day: days, hour: hours, minute: minutes, second: seconds };
}

testTimer(testResponse) {
  // console.log(testResponse)
  let exam_duration_time = testResponse.get_tot_test_time
  let a = exam_duration_time.split(':');
  let temp = (parseInt(a[0]) * 3600) + (parseInt(a[1]) * 60) + (parseInt(a[2]));


  var start_time = moment(testResponse.start_time).format('yyyy-MM-DD hh:mm:ss');

  let current = new Date();
  const current_time = moment(current).format('yyyy-MM-DD hh:mm:ss');

  // console.log(Date.parse(current_time.toString()), Date.parse(start_time.toString()))

  // console.log(moment(current_time).diff(moment(start_time)));
  let twoDateDiff = moment(current_time).diff(moment(start_time))
  temp = temp - ( twoDateDiff / 1000);

  // console.log(moment(start_time).fromNow());

  let temp2 = temp - 1;
  // console.log('time2', temp2)

  setTimeout(() => {
    // console.log('test variable',this.storageService.getVariable())

    if (temp2 < 0 || temp2 == NaN) {
      // Do Something
      // this.miscHelper.showAlert("times Up !! Please submit your answer!!");
      this.showTimesUpAlert();
    }

    // if (this.storageService.getVariable()) {
    //   this.miscHelper.showAlert("times Up !! Please submit your answer!!");
    // }
    this.questionDetails = [];
    this.storageService.setVariable(true)
    //  this.storageService.removeStorageItem(this.test_id)
    // console.log('time3', set_interval)

    clearInterval(set_interval)
  }, temp * 1000);

  var set_interval = setInterval(() => {
    var converttohour = secondsToHms(temp2);
    // console.log('wfwfw',secondsToHms(temp2));
    this.hour = ('0' + +converttohour[0]).slice(-2)
    this.minute = ('0' + +converttohour[1]).slice(-2)
    this.second = ('0' + +converttohour[2]).slice(-2)
    this.final_timer = this.hour + ':' + this.minute + ':' + this.second
    temp2 = temp2 - 1;

    if(this.hour == '00' && this.minute == '00' && this.second == '01') { 

      console.log('time end', this.final_timer)
      this.showTimesUpAlert();

    }
    
    // console.log('time', this.final_timer)
  }, 1000);

  function secondsToHms(d) {
    d = Number(d);
    // console.log("dddddd",d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? "" : "") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? "" : "") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? "" : "") : "";
    return [hDisplay, mDisplay, sDisplay];
  }

}

async showTimesUpAlert() {
  let student_exam_id = {
    student_exam_id : this.last_row.id
  }
  this.httpService.afterLoginPost('move-checked-mcqque-options', student_exam_id).subscribe(async (res: any) => {
    console.log('auto mcq submit',res)
    console.log('exist_theory_que',this.exist_theory_que)
    if(this.exist_theory_que)
    {
    this.uihelper.ShowAlert('',"Oops.... your time's up. Don't worry! Your MCQ have been submitted, you just need to upload the pdf.")
    let alert = this.alertController.create({
      header: "times Up !!",
      subHeader: "Please submit your answer sheet!!",
      cssClass: 'alertDanger',
      inputs: [
      ],
      buttons: [
        {
          text: 'Ok',
          role: 'ok',
          cssClass: 'secondary',
          handler: () => {
            //console.log('Confirm Cancel');
            this.popFileChooser();
          }
        },
        {
          text: 'Dismiss',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            //console.log('Confirm Cancel');
            this.getHome();
          }
        }
      ]
    });
  
    (await alert).present();
    }
    else{
      this.uihelper.ShowAlert('',"Pinch yourself but it’s true 😍 Your test has been evaluated automatically and your result is declared. Click on Below button and Go to My Test History to view your performance. ")
    }
    
  });
  
}

mcqAnswer(qid,marks,q_type,test_id, event) {
  this.finalMcqAns[qid] = event.detail.value

  // console.log('qid',qid)

    let mcq_data = {
      'test_id' : test_id,
      'student_exam_id' : this.last_row.id,
      'qid' : qid,
      'question_marks' : marks,
      'submitted_answer' : event.detail.value,
      'question_type' :  q_type,
      'user_id' :  this.user_id,
    }

  // let mcq_data = new FormData();
  // mcq_data.append('test_id', test_id);
  // mcq_data.append('student_exam_id', this.last_row.id);
  // mcq_data.append('qid', qid);
  // mcq_data.append('question_marks', marks);
  // mcq_data.append('submitted_answer', event.detail.value);
  // mcq_data.append('question_type', q_type);
  // mcq_data.append('user_id', this.user_id);
    let mcq_data1 = new FormData();
    mcq_data1.append('mcq_data', JSON.stringify(mcq_data));
    mcq_data1.append('user_id', this.user_id);
  console.log('mcq_data',mcq_data)
  this.httpService.afterLoginPost('save-mcqque-checked-options', mcq_data).subscribe(async (res: any) => {
    console.log('auto mcq submit',res)
  });
}

finalMcqAns = {
}

async upload(str: any) {
  this.ans_sheet = str.target.files[0];
  console.log('answersheet', this.ans_sheet);
  this.uihelper.ShowAlert('Uploaded Answersheet',this.ans_sheet.name)

}

async submit_test() {
  // console.log('questionDetails',this.questionDetails)
 
  if (!this.ans_sheet && this.exist_theory_que) {
    this.uihelper.ShowAlert('','Upload Answer sheet first and only PDF is allowed.')
    return false
  }

  this.objLength = Object.keys(this.finalMcqAns).length
  this.questionDetailsLength = Object.keys(this.questionDetails).length

  if (!this.isTheory && this.questionDetails != 0 && this.objLength == 0 && this.isSelectedMcqExist == null && this.studentSelectedCSOption == null) {
    this.uihelper.ShowAlert('','Atleast select one MCQ.')
    
    return false
  }
  this.uihelper.ShowSpinner();

  let formData = new FormData();
  formData.append('files', this.ans_sheet);
  formData.append('que_data', JSON.stringify(this.finalMcqAns));
  formData.append('test_id', this.test_id);
  formData.append('user_id', this.user_id);
  this.httpService.afterLoginPost('save-mcq-answers', formData).subscribe(async (res: any) => {
    // console.log('after uploading image',res.status);
    this.uihelper.HideSpinner();
    this.uihelper.urlAlert('','We hope you liked our Application.Please rate us on play store.')
    this.router.navigate([this.selfPath + '/end-test'],{queryParams: {price:this.price,exist_theory_que:this.exist_theory_que}})
    this.storageService.removeStorageItem(this.test_id)
    this.storageService.setVariable(false)
    // console.log('test variable 2',this.storageService.getVariable())
  },
    (err: any) => {
      this.uihelper.HideSpinner();
      this.uihelper.ShowAlert('',"Upload Answer sheet first and only PDF is allowed.");

    });
}

getHome() {
  this.navCtrl.pop();
}

GoToCart() {
  this.router.navigate([this.selfPath + "/cart"]);
}

changeWidth()
{
  this.toggle = !this.toggle;
}

addWaterMark(doc) {
  doc.setTextColor(64, 61, 61);
  doc.setFontSize(14);
  
  doc.text(doc.internal.pageSize.width - 90, doc.internal.pageSize.height - 40, 'Makemytest');
  doc.text(doc.internal.pageSize.width - 90,doc.internal.pageSize.height - 25, this.user_fullname)
  doc.text(doc.internal.pageSize.width - 90,doc.internal.pageSize.height - 10, this.user_number)
  return doc;
}

exportPdf() 
{
  const alert = this.uihelper.ShowAlert('Test PDF',`Dear Student, you are exporting this question to PDF form, the following features have been disabled.
   (1) You will not be able to make use of MMT Timer which we have added since you are going out of application.
  (2) For MCQ Questions you have to attempt the questions in the app itself, hence only Theory Ques have been exported, You need to go back and submit your MCQ Questions`).then(res => {
  
  this.uihelper.ShowSpinner();

  const div = document.getElementById("question_paper");
  const options = { background: "white", height: div.scrollHeight, width:div.scrollWidth  };
  domtoimage.toPng(div, options).then((dataUrl)=> {
    //Initialize JSPDF
    var doc = new jsPDF("p","px",[div.scrollHeight, div.scrollWidth]);
    //Add image Url to PDF
    doc.addImage(dataUrl, 'PNG', 0, 0, doc.internal.pageSize.getWidth(), doc.internal.pageSize.getHeight());
    doc = this.addWaterMark(doc);
    // doc.save('pdfDocument.pdf');

    let pdfOutput = doc.output();
    // using ArrayBuffer will allow you to put image inside PDF
    let buffer = new ArrayBuffer(pdfOutput.length);
    let array = new Uint8Array(buffer);
    for (var i = 0; i < pdfOutput.length; i++) {
        array[i] = pdfOutput.charCodeAt(i);
    }

    // This is where the PDF file will stored , you can change it as you like
    // for more information please visit https://ionicframework.com/docs/native/file/
    const directory = cordova.file.dataDirectory ;
    const fileName = "question_paper.pdf";
    let options: IWriteOptions = { replace: true };
    this.file.checkFile(directory, fileName).then((success)=> {
      //Writing File to Device
      // this.uihelper.HideSpinner();

      this.file.writeFile(directory,fileName,buffer, options)
      .then((success)=> {
        this.uihelper.HideSpinner();
        console.log("File created Succesfully" + JSON.stringify(success));
        this.fileOpener.open(this.file.dataDirectory + fileName, 'application/pdf')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error opening file', e));
      })
      .catch((error)=> {
        this.uihelper.HideSpinner();
        console.log("Cannot Create File " +JSON.stringify(error));
      });
    })
    .catch((error)=> {
      //Writing File to Device
        this.uihelper.HideSpinner();
      this.file.writeFile(directory,fileName,buffer)
      .then((success)=> {
        console.log("File created Succesfully" + JSON.stringify(success));
        this.fileOpener.open(this.file.dataDirectory + fileName, 'application/pdf')
          .then(() => console.log('File is opened'))
          .catch(e => console.log('Error opening file', e));
      })
      .catch((error)=> {
        this.uihelper.HideSpinner();
        console.log("Cannot Create File " +JSON.stringify(error));
      });
    });
  })
  .catch(function (error) {
    // this.uihelper.HideSpinner();
    console.error('oops, something went wrong!', error);
  });
  
})
}

}
