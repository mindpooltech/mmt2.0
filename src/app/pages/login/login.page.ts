import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  postData = {
    email : '',
    password : '',
  }

  constructor(
    private router: Router,
    private authService: AuthService,
    private uihelper: UIHelper,
    private navCtrl:NavController,
    private storageService:StorageService,
  ) { }

  ngOnInit() {
  }

  validateInputs() {
    let email = this.postData.email.trim();
    let password = this.postData.password.trim();
    return (
    this.postData.email && this.postData.password && email.length > 0 && password.length > 0
    );
    }

  async loginAction(){
      
    if(this.validateInputs())
    {
      this.uihelper.ShowSpinner();
      this.authService.login(this.postData).subscribe((res: any) => {
        console.log('login res',res);
        
        if(!!res)
        {
          // console.log('user details month year', res.payload.user_details.user_attempt_month, res.payload.user_details.user_attempt_year);
          if(res.status == 200)
          {
            this.uihelper.HideSpinner();
            localStorage.setItem("cartCount", res.payload.user_details.cartCount);
            localStorage.setItem("userToken", res.payload.token);
            localStorage.setItem("course_id", res.payload.user_details.course_id);
            // this.storageService.store(Constant.AUTH, res.payload);
            this.storageService.store(Constant.AUTH, res.payload).then(result => {
              console.log('Data is saved');
              }).catch(e => {
              console.log("error: " + e);
              });
            localStorage.setItem('IsLogin', 'Yes');
            if(!res.payload.user_details.user_attempt_month && !res.payload.user_details.user_attempt_year)
            {           
              this.uihelper.ShowAlert('Login','We need few information before you get started. Please update.')
              this.navCtrl.navigateRoot(['edit-user-profile']);
            }
            else{
            this.navCtrl.navigateRoot('tabs');
              // this.navCtrl.navigateRoot(['dashboard']);
            }
          }
          else {
            this.uihelper.HideSpinner();
            this.uihelper.ShowAlert("Login", res["error_description"]);
          }
        }
        else{
          this.uihelper.HideSpinner();
          this.uihelper.ShowAlert("Login", "User not found");
        }   
      },
      (err: any) => {
        this.uihelper.HideSpinner();
        // this.uihelper.ShowAlert("Login", 'Invalid Credentials');
        this.uihelper.ShowAlert("Login", Object.values(err.error.payload));
        // console.log('failure error',Object.values(err.error.payload)[0][0]);
        console.log('failure error',err)
      }
      );
    }
    else{
      this.uihelper.ShowAlert("Login", 'Please Enter Email and Password');
    }
  }

  SignUpOnClick(){
    this.router.navigate(['/signup'])
  }

}
