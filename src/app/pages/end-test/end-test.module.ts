import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EndTestPageRoutingModule } from './end-test-routing.module';

import { EndTestPage } from './end-test.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EndTestPageRoutingModule
  ],
  declarations: [EndTestPage]
})
export class EndTestPageModule {}
