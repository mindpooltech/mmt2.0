import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EndTestPage } from './end-test.page';

const routes: Routes = [
  {
    path: '',
    component: EndTestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EndTestPageRoutingModule {}
