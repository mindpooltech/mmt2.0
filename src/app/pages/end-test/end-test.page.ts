import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-end-test',
  templateUrl: './end-test.page.html',
  styleUrls: ['./end-test.page.scss'],
})
export class EndTestPage implements OnInit {
  price: any;
  exist_theory_que: boolean;

  constructor(private router:Router,
    private activatedRoute: ActivatedRoute,) { 
    this.activatedRoute.queryParams.subscribe(params => {
      this.price = params["price"];
      this.exist_theory_que = params["exist_theory_que"];
      console.log('exist_theory_que',this.exist_theory_que);
      
    });
  }

  ngOnInit() {
  }

  goToDashboard()
{
  this.router.navigate(['/tabs']);
}

}
