import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { NavController, PopoverController, ModalController } from '@ionic/angular';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';
import { PopoverPage } from '../popover/popover.page';

@Component({
  selector: 'app-test-board',
  templateUrl: './test-board.page.html',
  styleUrls: ['./test-board.page.scss'],
})
export class TestBoardPage implements OnInit {
  
  userData: any;
  url = Constant.apiUrl
  headers = new HttpHeaders({
    'Accept':  'application/json',
    'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
  });
  options = { headers: this.headers}
  total_test_pending: any;
  testBoardPackages: any;
  tempObj:any[] = [];
  dashboard_data: any;
  daily_motivational_text: any;
  total_avail_tests: any;
  pendingTestResultCount: any;
  totalCompletedTestCount: any;
  totalTestsAverage: any;
  repliesToPost: any;
  avg_percantage: any;
  totalPackages: any;
  marks_obtained: any;
  subject_id: any;
  cartCount: any;
  selfPath: string;
  testBoardData: any;

  constructor(private router: Router,
    private http: HttpClient,
    private httpService: HttpService,
    private storageService:StorageService,
    private navCtrl: NavController,
    private uihelper: UIHelper,
    private popovercontroller: PopoverController,
    private modalController:ModalController
    ) { }

  async ngOnInit() {
    try {
      this.router.events.subscribe((event: RouterEvent) => {
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          this.selfPath = event.url + '/test-board';
        }
        this.selfPath = this.router.routerState.snapshot.url;

        this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'test-board') + 'test-board';
      });

    } catch (e) {
      console.log(e);
    }    
  }

  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }

  async ionViewWillEnter() {
    // this.cartCount = localStorage.getItem('cartCount');
    this.uihelper.ShowSpinner();
    this.storageService.get(Constant.AUTH).then(res => {
      this.userData= res.user_details;
      let user_id = res.user_details.id;
      this.httpService.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
        this.cartCount = res['payload'].cart_count        
      })
      this.http.get(this.url + 'test-board/' + user_id, this.options).subscribe((res)=>{ 
        // console.log('test board',res['payload'])        
        this.testBoardData = res['payload'];
        this.testBoardPackages = res['payload'].packages;
        this.totalPackages = this.testBoardPackages.length
        this.total_avail_tests = res['payload'].total_avail_tests
        this.total_test_pending = res['payload'].total_test_pending
        this.avg_percantage = parseFloat("" + res['payload'].avg_percantage.toFixed(1))
        this.subject_id = res['payload'].subject_id
        this.uihelper.HideSpinner();
            this.tempObj.length = 0;
            this.testBoardPackages.forEach((value)=>{ 
            value.package.package_tests.forEach((value2,index)=>{
              const testData = {...value2};
              testData.name = !index ? value.package.name : ''
              testData.subject_name = !index ? value.package.subject.subject_name : ''
              this.tempObj.push(testData)
            });
            // console.log('tempObj',this.tempObj)  
            this.tempObj.forEach((value3,i)=>{
             
              if(value3.exam)
              {
                if(value3.exam.mcq_marks || value3.exam.theory_marks)
                {
                  this.tempObj[i]['marks_obtained'] =  parseFloat(value3.exam?.mcq_marks || 0) + parseFloat(value3.exam?.theory_marks || 0)
                  this.tempObj[i]['marks_obtained'] = this.tempObj[i]['marks_obtained'] < 0 ? '0' : this.tempObj[i]['marks_obtained']
                  // console.log('marks_obtained',this.tempObj[i]['marks_obtained']);
                   
                }
                else{
                  this.tempObj[i]['marks_obtained'] = '0'  
                }
              }
              else{
                  this.tempObj[i]['marks_obtained'] = 'N/A' 
              }
              })
        });
        // console.log('temp obj check',this.tempObj)

      }); 

    this.http.get(this.url + "dashboard/" + user_id, this.options).subscribe((res)=>{
      this.dashboard_data = res['payload'];
      this.daily_motivational_text = this.dashboard_data.daily_motivational_text
      this.total_avail_tests = this.dashboard_data.total_avail_tests
      this.pendingTestResultCount = this.dashboard_data.pendingTestResultCount
      this.totalCompletedTestCount = this.dashboard_data.totalCompletedTestCount
      this.totalTestsAverage = this.dashboard_data.totalTestsAverage
      this.repliesToPost = this.dashboard_data.repliesToPost

    })
    
  });
  }

  async openPopover(test_id, ev:any)
  {
    this.uihelper.ShowSpinner();
    this.http.get(this.url + 'test-chapters/' + test_id, this.options).subscribe(async (res)=>{   
      let chapter_list = res['payload']
      // console.log('popover chapters', chapter_list)
      this.uihelper.HideSpinner();

      const popover = await this.modalController.create({
        component: PopoverPage,
        componentProps: {
          "chapter_list": chapter_list,
        },
      });
      return await popover.present();

    });

  }


  takeTest(test_id,total_marks)
  {
  
    this.router.navigate([this.selfPath + '/new-test'],{ queryParams: { test_id:test_id,total_marks:total_marks }})
  }

  startTestPage(test_id)
  {
    this.router.navigate([ this.selfPath + '/start-test'],{queryParams: {test_id:test_id}});
  }

  GoToHistoryPage()
  {   
    this.router.navigate([ this.selfPath + "/test-history"]);
  }

  goBack() {
    this.router.navigate([ '/tabs']);
  }

  GoToCart(){
    this.router.navigate([ this.selfPath + "/cart"]);
  }

  GoToUserProfile() {
    this.router.navigate([this.selfPath + "/user-profile"]);
  }
  
}
