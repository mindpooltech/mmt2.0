import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestBoardPage } from './test-board.page';

const routes: Routes = [
  {
    path: '',
    component: TestBoardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestBoardPageRoutingModule {}
