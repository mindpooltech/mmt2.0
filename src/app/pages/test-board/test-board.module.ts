import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestBoardPageRoutingModule } from './test-board-routing.module';

import { TestBoardPage } from './test-board.page';
import { PopoverPage } from '../popover/popover.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestBoardPageRoutingModule
  ],
  declarations: [TestBoardPage,PopoverPage]
})
export class TestBoardPageModule {}
