import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-pdf-modal',
  templateUrl: './pdf-modal.page.html',
  styleUrls: ['./pdf-modal.page.scss'],
})
export class PdfModalPage implements OnInit {
  checked_answerbook: any;
  Url: any;

  constructor(private navParams: NavParams,
    public modalController: ModalController,
    private sanitizer: DomSanitizer) {
    this.checked_answerbook = this.navParams.data.checked_answerbook
    // console.log('checked_answerbook',this.checked_answerbook);
    // this.Url = this.sanitizer.bypassSecurityTrustResourceUrl(this.checked_answerbook);
    this.Url = 'http://docs.google.com/gview?embedded=true&url='+ this.checked_answerbook;
   }

  ngOnInit() {
     
  }

  dismiss() {
    this.modalController.dismiss();

  }

}
