import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestEvaluationPageRoutingModule } from './test-evaluation-routing.module';

import { TestEvaluationPage } from './test-evaluation.page';
import { RemovehtmltagsPipe } from '../../removehtmltags.pipe';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ApplicationPipesModule } from 'src/app/application-pipes/application-pipes.module';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestEvaluationPageRoutingModule,
    ApplicationPipesModule,
    PdfViewerModule
  ],
  providers:[InAppBrowser,FileTransfer,File,AndroidPermissions],
  declarations: [TestEvaluationPage]
})
export class TestEvaluationPageModule {}
