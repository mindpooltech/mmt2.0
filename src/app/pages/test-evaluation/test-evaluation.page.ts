import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { environment } from 'src/environments/environment';
import { StorageService } from 'src/app/services/storage.service';
// import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoadingController, ModalController, NavController } from '@ionic/angular';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';
import { File, IWriteOptions } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { DocumentViewer,DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Platform } from '@ionic/angular';
import { DomSanitizer} from '@angular/platform-browser';
import { PdfModalPage } from '../pdf-modal/pdf-modal.page'
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@Component({
  selector: 'app-test-evaluation',
  templateUrl: './test-evaluation.page.html',
  styleUrls: ['./test-evaluation.page.scss'],
})
export class TestEvaluationPage implements OnInit {
  pdfLink:any
  exam_id: any;
  test_id: any;
  questionDetails: any;
  checked_answerbook: any;
  file_path: any;
  total_marks: any;
  dataObj: any;
  student_exam_id: any;
  obtained_marks: any;
  status: any;
  exist_theory_que: any;
  url = Constant.apiUrl
  headers = new HttpHeaders({
    'Accept':  'application/json',
    'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
  });
  options = { headers: this.headers}
  cartCount: any;
  user_id: any;
  selfPath: string;
  toggle: boolean = true; 
  document_path: any;
  price: any;
  tempObj: { option_1: string; option_2: string; option_3: string; option_4: string; };

  // public pdfSrc: string = 'https://vadimdez.github.io/ng2-pdf-viewer/pdf-test.pdf';
  constructor(private router:Router,private activatedRoute:ActivatedRoute,private httpService:HttpService, private storageService:StorageService,private http:HttpClient,private navCtrl: NavController,private uihelper: UIHelper,  private iab: InAppBrowser,public platform: Platform,private sanitizer: DomSanitizer,public modalController: ModalController,private transfer: FileTransfer,
    private file: File,
    private androidPermissions: AndroidPermissions) {
    this.cartCount = localStorage.getItem('cartCount')
    this.activatedRoute.queryParams.subscribe((data)=>{
      
      this.exam_id = data.exam_id;
      this.test_id = data.test_id;
      this.price = data.price;
      this.obtained_marks = data.obtained_marks;

      this.obtained_marks =  this.obtained_marks == "NaN" || this.obtained_marks < 0 ? "0" : this.obtained_marks
    })
   }
  
  fileTransfer: FileTransferObject = this.transfer.create();


   async ngOnInit() {
    try {
      this.router.events.subscribe((event: RouterEvent) => {
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          // console.log('inside event url',event.url);
          this.selfPath = event.url + '/test-evaluation';
        }
        this.selfPath = this.router.routerState.snapshot.url;
        
        this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'test-evaluation') + 'test-evaluation';
      });

    } catch (e) {
      console.log(e);
    }
    
  }

  async ionViewWillEnter()
  {
    // this.cartCount = localStorage.getItem('cartCount')
    let postData = {
      test_id : this.test_id,
      exam_id : this.exam_id
    }
    this.uihelper.ShowSpinner();
    this.storageService.get(Constant.AUTH).then(res => {
      this.user_id= res.user_details.id;
      this.httpService.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
        this.cartCount = res['payload'].cart_count        
      })
    //   this.http.get(this.url + 'cart-count/' + this.user_id, this.options).subscribe((res)=>{   
    //     this.cartCount = res['payload']
    // });
    });
    this.httpService.afterLoginPost('test-evaluation-detail',postData).subscribe((res:any)=>{
      // console.log('evaluation detail',res)
     
      this.dataObj = res
      this.questionDetails = res.payload.sub_que
      this.total_marks = res.payload.studentExam.test_question_data.total_marks
      this.status = res.payload.studentExam.status
      this.exist_theory_que = res.payload.exist_theory_que
      this.file_path = res.payload.checked_answerbook_basepath
      this.checked_answerbook = Constant.adminUrl+this.file_path+res.payload.studentExam.checked_answerbook
      
      let new_url = 'http://docs.google.com/gview?embedded=true&url='+ this.checked_answerbook
      this.tempObj = {
        option_1: 'A',
        option_2: 'B',
        option_3: 'C',
        option_4: 'D',
      }
      this.uihelper.HideSpinner();   
           
   });
  }

  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }

  goToScoreCard()
  {
    let dataString = JSON.stringify(this.dataObj);
    this.storageService.setParamData(dataString)
    this.router.navigate([this.selfPath + '/test-evaluation-scorecard'],{queryParams:{exam_id:this.exam_id,test_id:this.test_id}})
  }

  getHome() {
    this.navCtrl.pop();
  }

  // GoToCart(){
  //   this.router.navigate([ this.selfPath + "/cart"]);
  // }

  changeWidth()
  {
    this.toggle = !this.toggle;
  }

  async viewAnswerSheet()
  {
  //   const modal = await this.modalController.create({
  //   component: PdfModalPage,
  //   cssClass: 'my-custom-class',
  //   componentProps: {
  //     "checked_answerbook": this.checked_answerbook,
  //   }
  // });

  // return await modal.present();
  this.uihelper.ShowSpinner();

  const url = this.checked_answerbook
  this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
    .then(status => {
      if (status.hasPermission) {
        this.fileTransfer.download(url, this.file.externalRootDirectory  + '/Download/' + 'answersheet.pdf').then((entry) => {
          this.uihelper.HideSpinner(); 
          console.log('download complete 1: ' + entry.toURL());
          this.uihelper.presentToast('Download complete..please check your download folder')
        }, (error) => {
          // handle error
          console.log(error);
          
        });
      } 
      else {
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
          .then(status => {
            if(status.hasPermission) {
              this.fileTransfer.download(url, this.file.externalRootDirectory  + '/Download/' + 'answersheet.pdf').then((entry) => {
                this.uihelper.HideSpinner(); 
                console.log('download complete 2: ' + entry.toURL());
                this.uihelper.presentToast('Download complete..please check your download folder')
                
              }, (error) => {
                // handle error
                console.log(error);
                
              });
            }
          });
      }
    });

  }
}
