import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestEvaluationPage } from './test-evaluation.page';

const routes: Routes = [
  {
    path: '',
    component: TestEvaluationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestEvaluationPageRoutingModule {}
