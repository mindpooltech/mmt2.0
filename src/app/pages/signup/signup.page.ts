import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';
import { LoadingController, NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  postData = {
    registration_group : '',
    registration_number : '',
    first_name : '',
    last_name : '',
    email : '',
    phone_number : '',
    password_reg : '',
    password_confirmation_reg : '',
    city : '',
    state : '',
    country : '',
    course_id : '',
    tnc : false,
  }


  constructor(private router: Router, private authService: AuthService, private storageService: StorageService, private loadingController: LoadingController,private alertController: AlertController,private uihelper: UIHelper,private navCtrl: NavController) { }

  ngOnInit() {
  }

  validateInputs() {

    return (
    this.postData.registration_group && this.postData.registration_number  && this.postData.first_name  && this.postData.last_name && this.postData.email && this.postData.phone_number && this.postData.password_reg && this.postData.password_confirmation_reg  && this.postData.city && this.postData.state && this.postData.country && this.postData.course_id && this.postData.tnc
    );
    }

    async signupAction()
    {
      if(this.validateInputs())
      {
        this.uihelper.ShowSpinner();
        this.authService.signup(this.postData).subscribe((res:any)=>{
          console.log('success',res);
          this.uihelper.HideSpinner();
          this.uihelper.presentAlert();
        },
        
        (err:any)=>{
          this.uihelper.HideSpinner();
          console.log('failure',err.error)
          // this.toastService.presentToast(res.error.payload.password_confirmation_reg.join('#'))
          var err = Object.values(err.error.payload)[0][0];        
          this.uihelper.ShowAlert('',err)


        });
      }
      else{
        this.uihelper.ShowAlert('','Please Enter All Details Correctly.')

      }
     }

     getHome() {
      this.navCtrl.pop();
    }
}
