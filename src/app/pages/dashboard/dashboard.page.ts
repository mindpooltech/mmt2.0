import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { NavController } from '@ionic/angular';
import { EventService } from 'src/app/services/event.service';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  selfPath: string;
  cartCount: string;
  userData: any;
  user_id: any;
  dashboard_data: any;
  daily_motivational_text: any;
  total_avail_tests: any;
  pendingTestResultCount: any;
  totalCompletedTestCount: any;
  totalTestsAverage: number;
  testOverview: number;
  repliesToPost: any;
  url = Constant.apiUrl
  headers = new HttpHeaders({
    'Accept': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
  });
  options = { headers: this.headers }
  user_name: any;
  student_code: any;
  student_referal_code: any;
  student_avail_amt: any;
  constructor(
    private router: Router ,
    private uihelper: UIHelper,
    private storageService: StorageService,
    private navCtrl: NavController,
    private http: HttpService,
    private events: EventService,
  ) { }

  ngOnInit() {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selfPath = '';
      if (event && event instanceof NavigationEnd && event.url) {
        this.selfPath = event.url + '/dashboard';
      }
      this.selfPath = this.router.routerState.snapshot.url;
      this.selfPath = this.uihelper.getStringBeforeSubstring(this.selfPath, 'dashboard') + 'dashboard';
    });

    // this.cartCount = localStorage.getItem('cartCount');


  }

  ionViewWillEnter() {
    this.GetDetailsOfDashBoard();
    var logedinUserDetails: any;
    this.storageService.get(Constant.AUTH).then(res => {
      logedinUserDetails = res.user_details
      // this.events.sendMessage({
      //   userName: logedinUserDetails.full_name
      // });
    this.http.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
      this.cartCount = res['payload'].cart_count
      // console.log('cart count',this.cartCount)

    })

      if(!logedinUserDetails.user_attempt_month && !logedinUserDetails.user_attempt_year)
      {           
        this.uihelper.ShowAlert('','We need few information before you get started. Please update.')
        this.navCtrl.navigateRoot(['edit-user-profile']);
      }
    })

  }

  goToPackages()
  {
    this.navCtrl.navigateRoot([this.selfPath + '/packages']);
  }

  GetDetailsOfDashBoard() {
    this.cartCount = localStorage.getItem('cartCount');
    this.uihelper.ShowSpinner();
    this.storageService.get(Constant.AUTH).then(res => {
      this.userData = res.user_details;
      this.user_name = res.user_details.full_name
      this.http.afterLoginGet( "dashboard" , res.user_details.id).subscribe((res) => {
        this.dashboard_data = res['payload'];
        // console.log('dashboard_data',this.dashboard_data)
        
        this.daily_motivational_text = this.dashboard_data.daily_motivational_text
        this.total_avail_tests = this.dashboard_data.total_avail_tests
        this.pendingTestResultCount = this.dashboard_data.pendingTestResultCount
        this.totalCompletedTestCount = this.dashboard_data.totalCompletedTestCount
        this.student_referal_code = this.dashboard_data.student_code
        this.student_avail_amt = this.dashboard_data.student_avail_amt
        
        this.totalTestsAverage = parseFloat("" + this.dashboard_data.totalTestsAverage.toFixed(1))
        this.testOverview = parseFloat("" + this.dashboard_data.testOverview.toFixed(1)) / 100
        this.repliesToPost = this.dashboard_data.repliesToPost
        // let rcodetring = JSON.stringify(this.student_referal_code);
        this.events.setCreditBalance({
          student_avail_amt: this.student_avail_amt
        })
        // this.router.navigate([this.selfPath + '/refer-and-earn'],{queryParams:{rcodetring:rcodetring}})
        this.uihelper.HideSpinner();
      },
      (err)=>{
        console.log('err',err)
        this.uihelper.ShowAlert("","Something went wrong");

      });
    });

  }

  GoToCart(){
    this.router.navigate([ this.selfPath + "/cart"]);
  }

  GoToUserProfile() {
    this.router.navigate([this.selfPath + "/user-profile"]);
  }

  GoToFeaturedPackages()
  {
    this.router.navigate([this.selfPath + "/featured-packages"]);
  }

}
