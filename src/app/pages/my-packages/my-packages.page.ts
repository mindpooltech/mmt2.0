import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { LoadingController, NavController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpService } from 'src/app/services/http.service';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { environment } from 'src/environments/environment';
import { UIHelper } from 'src/Helper/UIHelper';
import { Constant } from 'src/Helper/Constant';

@Component({
  selector: 'app-my-packages',
  templateUrl: './my-packages.page.html',
  styleUrls: ['./my-packages.page.scss'],
})
export class MyPackagesPage implements OnInit {

  url = Constant.apiUrl
  course_id= {
    course_id:localStorage.getItem('course_id')
  }
  user_id: any;
  userData: any;
  headers = new HttpHeaders({
    'Accept':  'application/json',
    'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
  });
  options = { headers: this.headers}
  myAllPackageList: any;
  myPackages: any;
  subjectList: any;
  institute_id: any;
  packageList: any;
  course_name: any;
  cartCount: any;
  selfPath: string;

  constructor(private router: Router,private http: HttpClient,private httpService: HttpService, private storageService:StorageService,private loadingController: LoadingController, private navCtrl: NavController, private uihelper: UIHelper) {
   
   }

  ngOnInit() {
    try {
      this.router.events.subscribe((event: RouterEvent) => {
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          this.selfPath = event.url + '/my-packages';
        }
        this.selfPath = this.router.routerState.snapshot.url;
        // this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'packages') + 'packages';
      });

    } catch (e) {
      console.log(e);
    }
  }

  ionViewWillEnter()
  { 
    this.cartCount = localStorage.getItem('cartCount')
    this.storageService.get(Constant.AUTH).then(async res => {
      this.userData= res.user_details;
      this.course_name = this.userData.course_name
      this.user_id = res.user_details.id;
      this.course_id = res.user_details.course_id;
      this.uihelper.ShowSpinner();
    //   this.http.get(this.url + 'cart-count/' + this.user_id, this.options).subscribe((res)=>{   
    //     this.cartCount = res['payload']
    // });
    this.httpService.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
      this.cartCount = res['payload'].cart_count        
    })
      this.http.get(this.url + 'my-all-packages/' + this.user_id, this.options).subscribe((res)=>{     
     
        this.myAllPackageList = res['payload'];
        console.log('aaaa',res)
        // this.myPackages = this.myAllPackageList.map((data)=>{
        //   return data.package
        // })
        // console.log('aaaa',this.myPackages);   
        
      this.uihelper.HideSpinner();
  },
  (err)=>{
    this.uihelper.HideSpinner();
    this.uihelper.ShowAlert('My Packages',"Packages Not Found")
    
  });
  });
    this.httpService.afterLoginPost('subject-list',this.course_id).subscribe((res:any)=>{
    this.subjectList = res.payload;
    });
  }

  async getPackages(event)
  {
    this.uihelper.ShowSpinner();
    let subject_data = {
      subject_id : event.target.value,
      user_id : this.user_id
    }
    this.httpService.afterLoginPost('my-packages', subject_data).subscribe((res)=>{
      this.myAllPackageList = res['payload'];
      this.uihelper.HideSpinner();
      
    },        
    (err:any)=>{
      this.uihelper.HideSpinner(); 
      // var err = Object.values(err.error.payload)[0][0];        

      this.uihelper.ShowAlert('',err.error.error.message)

    });
    
  }

  getHome() {
    this.navCtrl.pop();
  }

  GoToCart(){
    this.router.navigate([ this.selfPath + "/cart"]);
  }

  GoToUserProfile() {
    this.router.navigate([this.selfPath + "/user-profile"]);
  }

  GoToTestBoard(){
    this.router.navigate([ this.selfPath + "/test-board"]);
  }

}
