import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { NavController, PopoverController } from '@ionic/angular';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';
import { SearchPackagesPopoverPage } from '../search-packages-popover/search-packages-popover.page';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.page.html',
  styleUrls: ['./packages.page.scss'],
})
export class PackagesPage implements OnInit {
  cartCount: string;
  userData: any;
  course_name: any;
  institute_id: any;
  user_id: any;
  subjectList: any[] = [];
  course_id = {
    course_id: localStorage.getItem('course_id')
  }
  packageList: any;
  selfPath: string;
  constructor(
    private navCtrl:NavController,
    private router: Router,
    private uihelper: UIHelper,
    private storageService: StorageService,
    private httpService: HttpService,
    private popovercontroller: PopoverController,
  ) { }

  ngOnInit() {
    try {
      this.router.events.subscribe((event: RouterEvent) => {
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          this.selfPath = event.url + '/packages';
        }
        this.selfPath = this.router.routerState.snapshot.url;

        this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'packages') + 'packages';
      });

    } catch (e) {
      console.log(e);
    }

  }

  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }

  async ionViewWillEnter() {
     this.uihelper.ShowSpinner();
    this.storageService.get(Constant.AUTH).then(res => {
      this.userData = res.user_details;
      this.course_name = this.userData.course_name
      this.institute_id = res.user_details.institute_id;
      this.user_id = res.user_details.id;
      this.httpService.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
        this.cartCount = res['payload'].cart_count        
      })
      this.get_all_packages();
    });

    this.httpService.afterLoginPost('subject-list', this.course_id).subscribe((res: any) => {
      this.subjectList = res.payload;
     
    });

  }

  async get_all_packages() {
    let package_data = {
      course_id: this.course_id,
      user_id: this.user_id,
      institute_id: this.institute_id
    }
    this.httpService.afterLoginPost('all-packages', package_data).subscribe((res: any) => {
      this.packageList = res['payload'];
      this.uihelper.HideSpinner();

    });
  }

  async getPackages(event) {
    this.uihelper.ShowSpinner();
    let subject_data = {
      subject_id: event.target.value,
      pkg_type: 'all',
      institute_id: this.institute_id,
      course_id: this.course_id,
      user_id: this.user_id
    }

    this.httpService.afterLoginPost('packages', subject_data).subscribe((res) => {
      this.packageList = res['payload'];
     
      this.uihelper.HideSpinner();
    },
      (err: any) => {

        this.uihelper.HideSpinner();
        this.packageList = null
        // console.log('failure',err.error.error.message)
        // var err = Object.values(err.error.payload)[0][0];        
        this.uihelper.ShowAlert('',err.error.error.message)

      });
  }

  async addToCart(id, index) {
    this.uihelper.ShowSpinner();
    let cart_data = {
      pkg_id: id,
      user_id: this.user_id
    }

    this.httpService.afterLoginPost('add-to-cart', cart_data).subscribe(async (res) => {
      // console.log('add to cart',res);
      if(res['payload'].mycart == 'mycart')
      {
        localStorage.setItem("cartCount", res['payload'].cartCount);
        this.cartCount = localStorage.getItem('cartCount')
      }      

      if (res) {
        this.packageList[index]['isadded'] = true;
        // console.log('packageList',this.packageList)
        this.uihelper.HideSpinner();
        // this.router.navigate([this.selfPath]);
      }          
    })
  }

  async openPopover(package_id, ev:any)
  {
    const popover = await this.popovercontroller.create({
      component: SearchPackagesPopoverPage,
      // cssClass: 'my-custom-class',
      componentProps: {
        "package_id": package_id,
      },
      // event: ev

    });
    return await popover.present(); 
  }

  goBack() {
    this.router.navigate([ '/tabs']);
  }

  GoToCart(){
    this.router.navigate([ this.selfPath + "/cart"]);
  }

  GoToUserProfile() {
    this.router.navigate([this.selfPath + "/user-profile"]);
  }

}
