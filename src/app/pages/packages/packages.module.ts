import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PackagesPageRoutingModule } from './packages-routing.module';

import { PackagesPage } from './packages.page';
import { SearchPackagesPopoverPage } from '../search-packages-popover/search-packages-popover.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PackagesPageRoutingModule
  ],
  declarations: [PackagesPage,SearchPackagesPopoverPage]
})
export class PackagesPageModule {}
