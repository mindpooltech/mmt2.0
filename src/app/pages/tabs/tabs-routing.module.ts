import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'dashboard',
        children: [          
          {
            path: '',
            loadChildren: () => import('../dashboard/dashboard.module').then(m => m.DashboardPageModule)
          },
          {
            path: 'refer-and-earn',
            children: [
              {
              path: '',
              loadChildren: () => import('../refer-and-earn/refer-and-earn.module').then(m => m.ReferAndEarnPageModule)
              },
              {
                path: 'cart',
                loadChildren: () => import('../cart/cart.module').then(m => m.CartPageModule)
    
              },
              {
                path: 'user-profile',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../user-profile/user-profile.module').then(
                    m => m.UserProfilePageModule)
                  },
                  {
                    path: 'edit-user-profile',
                    loadChildren: () =>
                      import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                  }
                ]       
              },
          ]
            
          },
          {
            path: 'packages',
            children: [
              {
                path:'',
                loadChildren: () => import('../packages/packages.module').then(m => m.PackagesPageModule)
              },
              {
                path: 'cart',
                loadChildren: () => import('../cart/cart.module').then(m => m.CartPageModule)
    
              },
              {
                path: 'user-profile',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../user-profile/user-profile.module').then(
                    m => m.UserProfilePageModule)
                  },
                  {
                    path: 'edit-user-profile',
                    loadChildren: () =>
                      import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                  }
                ]       
              },
            ]

          },
          {
            path: 'featured-packages',
            children: [
              {
                path:'',
                loadChildren: () => import('../featured-packages/featured-packages.module').then(m => m.FeaturedPackagesPageModule)
              },
              {
                path: 'cart',
                loadChildren: () => import('../cart/cart.module').then(m => m.CartPageModule)
    
              },
              {
                path: 'user-profile',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../user-profile/user-profile.module').then(
                    m => m.UserProfilePageModule)
                  },
                  {
                    path: 'edit-user-profile',
                    loadChildren: () =>
                      import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                  }
                ]       
              },
            ]

          },
          {
            path: 'cart',
            loadChildren: () => import('../cart/cart.module').then(m => m.CartPageModule)

          },
          {
            path: 'group-packages',
            children: [
              {
                path:'',
                loadChildren: () => import('../group-packages/group-packages.module').then(m => m.GroupPackagesPageModule)
              },
              {
                path: 'cart',
                loadChildren: () =>
                  import('../cart/cart.module').then(m => m.CartPageModule)
              },
              {
                path: 'user-profile',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../user-profile/user-profile.module').then(
                    m => m.UserProfilePageModule)
                  },
                  {
                    path: 'edit-user-profile',
                    loadChildren: () =>
                      import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                  }
                ]       
              },
            ]

          },
          {
            path: 'user-profile',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../user-profile/user-profile.module').then(
                m => m.UserProfilePageModule)
              },
              {
                path: 'edit-user-profile',
                loadChildren: () =>
                  import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
              }
            ]            
          },
          {
            path: 'my-ratings',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../my-ratings/my-ratings.module').then(
                  m => m.MyRatingsPageModule)
              },
              {
                path: 'cart',
                loadChildren: () =>
                  import('../cart/cart.module').then(m => m.CartPageModule)
              },
              {
                path: 'user-profile',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../user-profile/user-profile.module').then(
                    m => m.UserProfilePageModule)
                  },
                  {
                    path: 'edit-user-profile',
                    loadChildren: () =>
                      import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                  }
                ]       
              },
            ]
          },
          {
            path: 'notes-details',
            children: [
              {
                path: '',
                loadChildren: () =>
                  import('../notes-details/notes-details.module').then(
                  m => m.NotesDetailsPageModule)
              },{
                path: 'cart',
                loadChildren: () =>
                  import('../cart/cart.module').then(m => m.CartPageModule)
              },
              {
                path: 'user-profile',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../user-profile/user-profile.module').then(
                    m => m.UserProfilePageModule)
                  },
                  {
                    path: 'edit-user-profile',
                    loadChildren: () =>
                      import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                  }
                ]       
              }
            ]            
          },
          {
            path: 'my-videos',
            children: [
              {
                path: '',
                loadChildren: () =>
                  import('../my-videos/my-videos.module').then(
                  m => m.MyVideosPageModule)
              },
              {
                path: 'cart',
                loadChildren: () =>
                  import('../cart/cart.module').then(m => m.CartPageModule)
              },
              {
                path: 'user-profile',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../user-profile/user-profile.module').then(
                    m => m.UserProfilePageModule)
                  },
                  {
                    path: 'edit-user-profile',
                    loadChildren: () =>
                      import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                  }
                ]       
              },
            ]            
          },
          {
            path: 'my-queries',
            children: [
              {
                path: '',
                loadChildren: () =>
                  import('../my-queries/my-queries.module').then(
                  m => m.MyQueriesPageModule)
              },
              {
                path: 'cart',
                loadChildren: () =>
                  import('../cart/cart.module').then(m => m.CartPageModule)
              },
              {
                path: 'user-profile',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../user-profile/user-profile.module').then(
                    m => m.UserProfilePageModule)
                  },
                  {
                    path: 'edit-user-profile',
                    loadChildren: () =>
                      import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                  }
                ]       
              },
            ]
            
          },
          {
            path: 'my-packages',
            children: [
              {
                path: '',
                loadChildren: () => import('../my-packages/my-packages.module').then(m => m.MyPackagesPageModule)

              },
              {
                path: 'cart',
                loadChildren: () =>
                  import('../cart/cart.module').then(m => m.CartPageModule)
              },
              {
                path: 'user-profile',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../user-profile/user-profile.module').then(
                    m => m.UserProfilePageModule)
                  },
                  {
                    path: 'edit-user-profile',
                    loadChildren: () =>
                      import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                  }
                ]       
              },
              {
                path: 'test-board',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../test-board/test-board.module').then(m => m.TestBoardPageModule)
                  },
                  {
                    path: 'cart',
                    loadChildren: () =>
                      import('../cart/cart.module').then(m => m.CartPageModule)
                  },
                  {
                    path: 'user-profile',
                    children: [
                      {
                        path: '',
                        loadChildren: () =>
                        import('../user-profile/user-profile.module').then(
                        m => m.UserProfilePageModule)
                      },
                      {
                        path: 'edit-user-profile',
                        loadChildren: () =>
                          import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                      }
                    ]       
                  },
                  {
                    path: 'test-history',
                    loadChildren: () =>
                    import('../test-history/test-history.module').then(
                      m => m.TestHistoryPageModule)
                  }
                  ,
                  {
                    path: 'new-test',
                    loadChildren: () =>
                      import('../new-test/new-test.module').then(
                        m => m.NewTestPageModule)
                  },{
                    path: 'start-test',
                    children: [
                      {
                        path: '',
                        loadChildren: () =>
                        import('../start-test/start-test.module').then(
                          m => m.StartTestPageModule)
                      },{
                        path: 'cart',
                        loadChildren: () =>
                          import('../cart/cart.module').then(m => m.CartPageModule)
                      },
                      {
                        path: 'user-profile',
                        children: [
                          {
                            path: '',
                            loadChildren: () =>
                            import('../user-profile/user-profile.module').then(
                            m => m.UserProfilePageModule)
                          },
                          {
                            path: 'edit-user-profile',
                            loadChildren: () =>
                              import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                          }
                        ]       
                      },{
                        path: 'end-test',
                        loadChildren: () =>
                          import('../end-test/end-test.module').then(
                            m => m.EndTestPageModule)
                      }                 
                    ]                
                  },{
                    path: 'test-history',
                    children: [
                      {
                        path: '',
                        loadChildren: () =>
                        import('../test-history/test-history.module').then(
                          m => m.TestHistoryPageModule)
                      },{
                        path: 'cart',
                        loadChildren: () =>
                          import('../cart/cart.module').then(m => m.CartPageModule)
                      },
                      {
                        path: 'user-profile',
                        children: [
                          {
                            path: '',
                            loadChildren: () =>
                            import('../user-profile/user-profile.module').then(
                            m => m.UserProfilePageModule)
                          },
                          {
                            path: 'edit-user-profile',
                            loadChildren: () =>
                              import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                          }
                        ]       
                      },
                      {
                        path: 'test-evaluation',
                        children: [
                          {
                            path:'',
                            loadChildren: () =>
                            import('../test-evaluation/test-evaluation.module').then(
                            m => m.TestEvaluationPageModule)
                          },{
                            path: 'cart',
                            loadChildren: () =>
                              import('../cart/cart.module').then(m => m.CartPageModule)
                          },                          
                          {
                            path: 'user-profile',
                            children: [
                              {
                                path: '',
                                loadChildren: () =>
                                import('../user-profile/user-profile.module').then(
                                m => m.UserProfilePageModule)
                              },
                              {
                                path: 'edit-user-profile',
                                loadChildren: () =>
                                  import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                              }
                            ]       
                          },
                          {
                            path: 'test-evaluation-scorecard',
                            loadChildren: () =>
                              import('../test-evaluation-scorecard/test-evaluation-scorecard.module').then(
                                m => m.TestEvaluationScorecardPageModule)
                          }
        
                        ]
                        
                      }
                    ]
                  },
                ]
              }
            ]
          },
          {
            path: 'test-history',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../test-history/test-history.module').then(
                  m => m.TestHistoryPageModule)
              },
              {
                path: 'cart',
                loadChildren: () =>
                  import('../cart/cart.module').then(m => m.CartPageModule)
              },
              {
                path: 'user-profile',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../user-profile/user-profile.module').then(
                    m => m.UserProfilePageModule)
                  },
                  {
                    path: 'edit-user-profile',
                    loadChildren: () =>
                      import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                  }
                ]       
              },
              {
                path: 'test-evaluation',
                children: [
                  {
                    path:'',
                    loadChildren: () =>
                    import('../test-evaluation/test-evaluation.module').then(
                    m => m.TestEvaluationPageModule)
                  },{
                    path: 'cart',
                    loadChildren: () =>
                      import('../cart/cart.module').then(m => m.CartPageModule)
                  },
                  {
                    path: 'user-profile',
                    children: [
                      {
                        path: '',
                        loadChildren: () =>
                        import('../user-profile/user-profile.module').then(
                        m => m.UserProfilePageModule)
                      },
                      {
                        path: 'edit-user-profile',
                        loadChildren: () =>
                          import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                      }
                    ]       
                  },
                  {
                    path: 'test-evaluation-scorecard',
                    loadChildren: () =>
                      import('../test-evaluation-scorecard/test-evaluation-scorecard.module').then(
                        m => m.TestEvaluationScorecardPageModule)
                  }
    
                ]
                
              }
            ]
          },
          {
            path: 'test-board',
            children: [          
              {
                path: '',
                loadChildren: () => import('../test-board/test-board.module').then(m => m.TestBoardPageModule)
    
              },
              {
                path: 'cart',
                loadChildren: () => import('../cart/cart.module').then(m => m.CartPageModule)
    
              },
              {
                path: 'user-profile',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../user-profile/user-profile.module').then(
                    m => m.UserProfilePageModule)
                  },
                  {
                    path: 'edit-user-profile',
                    loadChildren: () =>
                      import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                  }
                ]       
              },
              {
                path: 'new-test',
                loadChildren: () =>
                  import('../new-test/new-test.module').then(
                    m => m.NewTestPageModule)
              },
              {
                path: 'start-test',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../start-test/start-test.module').then(
                      m => m.StartTestPageModule)
                  },{
                    path: 'cart',
                    loadChildren: () =>
                      import('../cart/cart.module').then(m => m.CartPageModule)
                  },
                  {
                    path: 'user-profile',
                    children: [
                      {
                        path: '',
                        loadChildren: () =>
                        import('../user-profile/user-profile.module').then(
                        m => m.UserProfilePageModule)
                      },
                      {
                        path: 'edit-user-profile',
                        loadChildren: () =>
                          import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                      }
                    ]       
                  },{
                    path: 'end-test',
                    loadChildren: () =>
                      import('../end-test/end-test.module').then(
                        m => m.EndTestPageModule)
                  }                 
                ]                
              },
              {
                path: 'test-history',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../test-history/test-history.module').then(
                      m => m.TestHistoryPageModule)
                  },
                  {
                    path: 'cart',
                    loadChildren: () =>
                      import('../cart/cart.module').then(m => m.CartPageModule)
                  },
                  {
                    path: 'user-profile',
                    children: [
                      {
                        path: '',
                        loadChildren: () =>
                        import('../user-profile/user-profile.module').then(
                        m => m.UserProfilePageModule)
                      },
                      {
                        path: 'edit-user-profile',
                        loadChildren: () =>
                          import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                      }
                    ]       
                  },
                  {
                    path: 'test-evaluation',
                    children: [
                      {
                        path: '',
                        loadChildren: () =>
                        import('../test-evaluation/test-evaluation.module').then(
                        m => m.TestEvaluationPageModule)
                      },
                      {
                        path: 'test-evaluation-scorecard',
                        loadChildren: () =>
                        import('../test-evaluation-scorecard/test-evaluation-scorecard.module').then(
                        m => m.TestEvaluationScorecardPageModule)
                      },
                      {
                        path: 'cart',
                        loadChildren: () =>
                          import('../cart/cart.module').then(m => m.CartPageModule)
                      },
                      {
                        path: 'user-profile',
                        children: [
                          {
                            path: '',
                            loadChildren: () =>
                            import('../user-profile/user-profile.module').then(
                            m => m.UserProfilePageModule)
                          },
                          {
                            path: 'edit-user-profile',
                            loadChildren: () =>
                              import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                          }
                        ]       
                      },
                    ]
                  },
                  
                ]
              },
              {
                path: 'new-test',
                loadChildren: () =>
                  import('../new-test/new-test.module').then(
                    m => m.NewTestPageModule)
              },
              {
                path: 'start-test',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../start-test/start-test.module').then(
                      m => m.StartTestPageModule)
                  },
                  // {
                  //   path: 'cart',
                  //   loadChildren: () =>
                  //     import('../pages/cart/cart.module').then(m => m.CartPageModule)
                  // },
                  {
                    path: 'end-test',
                    loadChildren: () =>
                      import('../end-test/end-test.module').then(
                        m => m.EndTestPageModule)
                  }                 
                ]                
              }
            ],
            
          }
        ],
        
      },
      {
        path: 'packages',
        children: [          
          {
            path: '',
            loadChildren: () => import('../packages/packages.module').then(m => m.PackagesPageModule)
          },
          {
            path: 'cart',
            loadChildren: () =>
              import('../cart/cart.module').then(m => m.CartPageModule)
          },
          {
            path: 'user-profile',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../user-profile/user-profile.module').then(
                m => m.UserProfilePageModule)
              },
              {
                path: 'edit-user-profile',
                loadChildren: () =>
                  import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
              }
            ]       
          },
        ],
        
      },
      {
        path: 'test-board',
        children: [          
          {
            path: '',
            loadChildren: () => import('../test-board/test-board.module').then(m => m.TestBoardPageModule)

          },
          {
            path: 'cart',
            loadChildren: () => import('../cart/cart.module').then(m => m.CartPageModule)

          },
          {
            path: 'new-test',
            loadChildren: () =>
              import('../new-test/new-test.module').then(
                m => m.NewTestPageModule)
          },
          {
            path: 'start-test',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../start-test/start-test.module').then(
                  m => m.StartTestPageModule)
              },{
                path: 'cart',
                loadChildren: () =>
                  import('../cart/cart.module').then(m => m.CartPageModule)
              },
              {
                path: 'user-profile',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../user-profile/user-profile.module').then(
                    m => m.UserProfilePageModule)
                  },
                  {
                    path: 'edit-user-profile',
                    loadChildren: () =>
                      import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                  }
                ]       
              },{
                path: 'end-test',
                loadChildren: () =>
                  import('../end-test/end-test.module').then(
                    m => m.EndTestPageModule)
              }                 
            ]                
          },
          {
            path: 'user-profile',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../user-profile/user-profile.module').then(
                m => m.UserProfilePageModule)
              },
              {
                path: 'edit-user-profile',
                loadChildren: () =>
                  import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
              }
            ]       
          },
          {
            path: 'test-history',
            children: [
              {
                path: '',
                loadChildren: () =>
                import('../test-history/test-history.module').then(
                  m => m.TestHistoryPageModule)
              },
              {
                path: 'cart',
                loadChildren: () =>
                  import('../cart/cart.module').then(m => m.CartPageModule)
              },
              {
                path: 'user-profile',
                children: [
                  {
                    path: '',
                    loadChildren: () =>
                    import('../user-profile/user-profile.module').then(
                    m => m.UserProfilePageModule)
                  },
                  {
                    path: 'edit-user-profile',
                    loadChildren: () =>
                      import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                  }
                ]       
              },
              {
                path: 'test-evaluation',
                children: [
                  {
                    path:'',
                    loadChildren: () =>
                    import('../test-evaluation/test-evaluation.module').then(
                    m => m.TestEvaluationPageModule)
                  },{
                    path: 'cart',
                    loadChildren: () =>
                      import('../cart/cart.module').then(m => m.CartPageModule)
                  },
                  {
                    path: 'user-profile',
                    children: [
                      {
                        path: '',
                        loadChildren: () =>
                        import('../user-profile/user-profile.module').then(
                        m => m.UserProfilePageModule)
                      },
                      {
                        path: 'edit-user-profile',
                        loadChildren: () =>
                          import('../edit-user-profile/edit-user-profile.module').then(m => m.EditUserProfilePageModule)
                      }
                    ]       
                  },
                  {
                    path: 'test-evaluation-scorecard',
                    loadChildren: () =>
                      import('../test-evaluation-scorecard/test-evaluation-scorecard.module').then(
                        m => m.TestEvaluationScorecardPageModule)
                  }

                ]
                
              }
            ]
          }
        ],
        
      },
      
      {
        path: '',
        redirectTo: '/tabs/dashboard',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
