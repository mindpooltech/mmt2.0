import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { UIHelper } from 'src/Helper/UIHelper';
import { StorageService } from 'src/app/services/storage.service';
import { HttpService } from 'src/app/services/http.service';
import { Constant } from 'src/Helper/Constant';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-refer-and-earn',
  templateUrl: './refer-and-earn.page.html',
  styleUrls: ['./refer-and-earn.page.scss'],
})
export class ReferAndEarnPage implements OnInit {

  userData: any;
  avatar: any;
  selfPath: string;
  cartCount : any;
  dashboard_data: any;
  student_referal_code: any;
  user_id: any;
  social_share_desc: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router ,
    private uihelper: UIHelper,
    private storageService: StorageService,
    private navCtrl: NavController,
    private http: HttpService,
    private socialSharing: SocialSharing) {
    this.cartCount = localStorage.getItem('cartCount');
    this.storageService.get(Constant.AUTH).then(res => {
      this.userData = res.user_details;
      this.user_id = this.userData.id
    });
   }

  ngOnInit() {
    try {
      this.router.events.subscribe((event: RouterEvent) => {
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          this.selfPath = event.url + '/refer-and-earn';
        }
        this.selfPath = this.router.routerState.snapshot.url;

        this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'refer-and-earn') + 'refer-and-earn';
      });

    } catch (e) {
      console.log(e);
  }
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter user_id',this.user_id)

    this.GetDetailsOfDashBoard();
  }

  GetDetailsOfDashBoard() {
    // this.cartCount = localStorage.getItem('cartCount');
    this.uihelper.ShowSpinner();
    this.storageService.get(Constant.AUTH).then(res => {
      this.userData = res.user_details;
      this.user_id = this.userData.id
      this.http.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
        this.cartCount = res['payload'].cart_count        
      })
      this.http.afterLoginGet( "dashboard" , this.user_id).subscribe((res) => {
        this.dashboard_data = res['payload'];
        // console.log('dashboard_data',this.dashboard_data)
        this.social_share_desc = this.dashboard_data.social_share_desc
        if(this.dashboard_data.student_code)
        {
          this.student_referal_code = this.dashboard_data.student_code.discount_code
        }
        else{
          this.student_referal_code = null
        }
        // console.log('student_referal_code',this.student_referal_code)
        this.uihelper.HideSpinner();
 
    });
  });
  }
  
  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }

  generateCode(){
    this.uihelper.ShowSpinner();
    let data = {
      course_id: localStorage.getItem('course_id'),
      user_id: this.user_id
    }

    this.http.afterLoginPost( "generate-referralcode" , data).subscribe((res) => {
     
      this.ionViewWillEnter();
      this.uihelper.HideSpinner();

    },
    (err)=>{
      this.uihelper.HideSpinner();
      this.uihelper.ShowAlert('Refer and Earn','Something Went wrong..Please try again');
      console.log(err)
    })
  }

  shareViaApps(){
    // console.log("MakeMyTest : I have become an MMTian by registering on Make My Test, best online test series for CA Students 😍 I recommend you to become an MMTian 🎓 Sign up on the website and get 5% discount on your first order by using my" + this.student_referal_code + "🤩 Website address: 👇🏻",this.student_referal_code)
    var options = {
      message: "MakeMyTest : I have become an MMTian by registering on Make My Test, best online test series for CA Students 😍 I recommend you to become an MMTian 🎓 Sign up on the website and get 5% discount on your first order by using my Referal Code " + this.student_referal_code + "🤩 Website address: 👇🏻", 
      url: 'http://newdev.makemytest.in/register',
      chooserTitle: 'Makemytest', // Android only, you can override the default share sheet title
  };
    
    this.socialSharing.shareWithOptions(options);
  }

  goBack()
  {
    this.navCtrl.pop();
  }

  GoToUserProfile() {
    this.router.navigate([this.selfPath + "/user-profile"]);
  }

  GoToCart(){
    this.router.navigate([ this.selfPath + "/cart"]);
  }

}
