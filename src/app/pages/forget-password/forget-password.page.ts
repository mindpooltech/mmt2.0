import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Observable } from 'rxjs';
import { LoadingController, NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.page.html',
  styleUrls: ['./forget-password.page.scss'],
})
export class ForgetPasswordPage implements OnInit {

  emailId : '';
  postData = {
    email : ''
  }
  constructor(private authService: AuthService,private router: Router,private loadingController: LoadingController,private alertController: AlertController,private uihelper: UIHelper,private navCtrl: NavController) { }

  ngOnInit() {
  }

  async forgotPasswordAction() 
  {
    this.uihelper.ShowSpinner();
    return this.authService.forgotPassword(this.postData).subscribe((res: any) => {
      console.log('response aaa',res)
      this.uihelper.HideSpinner();
      this.presentAlert();
    },
    (err)=>{
      this.uihelper.HideSpinner();
      console.log('error', err)
      this.uihelper.ShowAlert('',err.error.error.message)
    })
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
     spinner: 'circles',
     keyboardClose: true,
     message: 'Please Wait'
   });
   return await loading.present();
 }

 async presentAlert() {
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    subHeader: 'Reset Password',
    message: 'We have e-mailed your password reset link!',
    buttons: [
    {
      text: 'Ok',
      handler: () => {
      this.router.navigate(['/login']);
      } 
    }
    ]
  });

  await alert.present();
}

getHome() {
  this.navCtrl.pop();
}
}
