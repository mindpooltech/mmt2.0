import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FeaturedPackagesPage } from './featured-packages.page';

const routes: Routes = [
  {
    path: '',
    component: FeaturedPackagesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeaturedPackagesPageRoutingModule {}
