import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FeaturedPackagesPageRoutingModule } from './featured-packages-routing.module';

import { FeaturedPackagesPage } from './featured-packages.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FeaturedPackagesPageRoutingModule,
    
  ],
  declarations: [FeaturedPackagesPage]
})
export class FeaturedPackagesPageModule {}
