import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-featured-packages',
  templateUrl: './featured-packages.page.html',
  styleUrls: ['./featured-packages.page.scss'],
})
export class FeaturedPackagesPage implements OnInit {
  selfPath: string;
  cartCount: string;
  userData: any;
  user_id: any;
  dashboard_data: any;
  user_name: any;
  featured_packages_group1: any;
  featured_packages_group2: any;
  featured_packages_caf: any;
  course_id: any;
  isFoundation: boolean;
  constructor(
    private uihelper: UIHelper,
    private storageService: StorageService,
    private http: HttpService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selfPath = '';
      if (event && event instanceof NavigationEnd && event.url) {
        this.selfPath = event.url + '/featured-packages';
      }
      this.selfPath = this.router.routerState.snapshot.url;

      this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'featured-packages') + 'featured-packages';
    });

  } catch (e) {
    console.log(e);
  }

  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }
  

  ionViewWillEnter() {
    this.GetDetailsOfDashBoard();
  }

  GetDetailsOfDashBoard() {
    // this.cartCount = localStorage.getItem('cartCount');
    this.uihelper.ShowSpinner();
    this.storageService.get(Constant.AUTH).then(res => {
      this.userData = res.user_details;
      this.user_name = res.user_details.full_name
      this.course_id = res.user_details.course_id
      this.user_id = res.user_details.id;
      this.http.afterLoginGet( "dashboard" , res.user_details.id).subscribe((res) => {
        this.dashboard_data = res['payload'];
        // console.log('dashboard_data',this.dashboard_data)
        // console.log('course_id',this.course_id)
        if(this.course_id == 1)
        {
          this.isFoundation = true
          this.featured_packages_caf = this.dashboard_data.featured_packages_caf
        }
        else{
          this.featured_packages_caf = []
        }
        // console.log('featured_packages_caf',this.featured_packages_caf)

        this.featured_packages_group1 = this.dashboard_data.featured_packages_group1
        this.featured_packages_group2 = this.dashboard_data.featured_packages_group2
        
        this.http.afterLoginGet( "cart_count" , this.userData.id).subscribe((res) => {
          this.cartCount = res['payload'].cart_count        
        })
         this.uihelper.HideSpinner();
      },
      (err)=>{
        console.log('err',err)
        this.uihelper.ShowAlert("","Something went wrong");

      });
    });

  }

  async addToCart(id, index, group_type, package_price) {
    this.uihelper.ShowSpinner();
    let cart_data = {
      pkg_id: id,
      user_id: this.user_id
    }

    this.http.afterLoginPost('add-to-cart', cart_data).subscribe(async (res) => {
      // console.log('add to cart',res);
      if(res['payload'].mycart == 'mycart')
      {
        localStorage.setItem("cartCount", res['payload'].cartCount);
        this.cartCount = localStorage.getItem('cartCount')
      }      

      if (res) {
      if(group_type == 'caf')
      {
        this.featured_packages_caf[index]['isadded'] = true;
        
      }
      else if(group_type == 'group1')
      {
        this.featured_packages_group1[index]['isadded'] = true;
      } 
      
      else{
        this.featured_packages_group2[index]['isadded'] = true;
      }
       
      if(package_price == 0)
      {
        this.uihelper.showToast('Course succesfully added, now you can go to my test board and attempt your test', 1)
      }
        this.uihelper.HideSpinner();
      }          
    })
  }

  goBack() {
    this.router.navigate([ '/tabs']);
  }

  GoToCart(){
    this.router.navigate([ this.selfPath + "/cart"]);
  }

  GoToUserProfile() {
    this.router.navigate([this.selfPath + "/user-profile"]);
  }

}
