import { Component, OnInit } from '@angular/core';
import { ModalController,NavController,NavParams  } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';
import { HttpService } from 'src/app/services/http.service';
import { Router } from '@angular/router';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';
import { NgForm }   from '@angular/forms';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {
  postData = {
    ratings : '',
    rating_comment : '',
  }
  ratings: '';
  rating_comment= '';
  test_id: any;
  exam_id: any;
  user_id: any;
  constructor(public router:Router, public modalController: ModalController,private navParams: NavParams,private storageService:StorageService,private httpService:HttpService, public navCtrl: NavController,private uihelper: UIHelper) { }

  ngOnInit() {
    // console.log(this.navParams);
    this.storageService.get(Constant.AUTH).then(res => {
      this.user_id= res.user_details.id;
  });
  }

  dismiss() {
    this.modalController.dismiss();
    return false;
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    // this.modalController.dismiss({
    //   'dismissed': true
    // });
  }

  submit_rating(form:NgForm)
  {
    this.postData['test_id'] = this.navParams.data.test_id
    this.postData['student_exam_id'] = this.navParams.data.exam_id
    this.postData['user_id'] = this.user_id
    console.log('postData',this.postData)
    this.httpService.afterLoginPost('submit-test-rating',this.postData).subscribe((res:any)=>{
      // console.log('submit rating response',res)
      this.uihelper.ShowAlert('','Thank you for your feedback.')
      
      this.dismiss();
      this.router.navigate(['/tabs'])
           
   },
   (err)=>{
     console.log('submit rating error', err)
    //  this.toastService.presentToast("Please fill all Details")
     this.uihelper.ShowAlert('','Please fill all Details')

   });
  }

  

}
