import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotePdfModalPage } from './note-pdf-modal.page';

const routes: Routes = [
  {
    path: '',
    component: NotePdfModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotePdfModalPageRoutingModule {}
