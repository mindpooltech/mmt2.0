import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotePdfModalPageRoutingModule } from './note-pdf-modal-routing.module';
import { SafePipe } from '../../safe.pipe';

import { NotePdfModalPage } from './note-pdf-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotePdfModalPageRoutingModule
  ],
  declarations: [NotePdfModalPage,SafePipe]
})
export class NotePdfModalPageModule {}
