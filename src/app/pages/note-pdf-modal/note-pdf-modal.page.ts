import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-note-pdf-modal',
  templateUrl: './note-pdf-modal.page.html',
  styleUrls: ['./note-pdf-modal.page.scss'],
})
export class NotePdfModalPage implements OnInit {
  note_url: any;
  Url: string;

  constructor(private navParams: NavParams,
    public modalController: ModalController,
    private sanitizer: DomSanitizer) {
    this.note_url = this.navParams.data.note_url
    // console.log('checked_answerbook',this.checked_answerbook);
    // this.Url = this.sanitizer.bypassSecurityTrustResourceUrl(this.checked_answerbook);
    this.Url = 'http://docs.google.com/gview?embedded=true&url='+ this.note_url;
   }

  ngOnInit() {
  }

  dismiss() {
    this.modalController.dismiss();
       
  }
}
