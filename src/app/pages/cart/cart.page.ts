import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { LoadingController, NavController, ModalController } from '@ionic/angular';
import { HttpService } from 'src/app/services/http.service';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { UIHelper } from 'src/Helper/UIHelper';
import { Constant } from 'src/Helper/Constant';
import { CouponPopupPage } from '../coupon-popup/coupon-popup.page';
// import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import * as sha512 from 'js-sha512'
import { sha256, sha224 } from 'js-sha256';

// declare module '*';
declare var RazorpayCheckout:any;

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})

export class CartPage implements OnInit {

  userData: any;
  user_id: any;
  cartList: any[] = [];
  url = Constant.apiUrl
  cart_list: any;
  course_name: any;
  public sum:string;
  headers = new HttpHeaders({
    'Accept':  'application/json',
    'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
  });
  options = { headers: this.headers}
  amount_detail: any;
  final_amount:any;
  final_total: any;
  cartCount: any;
  selfPath: string;
  coupon_detail: any;
  new_cart_list: any;
  course_id = {
    course_id: localStorage.getItem('course_id')
  }
  res: any;
  admin_coupon_detail_data: any;
  institute_coupon_detail_data: any;
  user_name: any;
  orderId: any;
  signature: any;
  referal_coupon_data: any;
  referal_coupon_credit: any;
  type: any;
  check_type: any;
  credit_detail: any;
  checked : boolean;
  referal_coupon_detail_data: any;
  
  constructor(private router: Router,
    private http: HttpClient,
    private httpService: HttpService,
    private storageService:StorageService,
    private loadingController: LoadingController, 
    private navCtrl: NavController,
    private uihelper: UIHelper,
    private modalController:ModalController,
    // private iab: InAppBrowser
    ) {
    // this.cartCount = localStorage.getItem('cartCount')

   }

  //  payWithRazorpay() {
  //    console.log('aaaaaaaaaaaaa')
  //   var options = {
  //     description: 'Credits towards consultation',
  //     image: 'https://i.imgur.com/3g7nmJC.png',
  //     currency: "INR", // your 3 letter currency code
  //     key: "rzp_test_sdQrRwyEDr4qfx", // your Key Id from Razorpay dashboard
  //     amount: 100, // Payment amount in smallest denomiation e.g. cents for USD
  //     name: 'Razorpay',
  //     prefill: {
  //       email: 'test@razorpay.com',
  //       contact: '9990009991',
  //       name: 'Razorpay'
  //     },
  //     theme: {
  //       color: '#F37254'
  //     },
  //     modal: {
  //       ondismiss: function () {
  //         alert('dismissed')
  //       }
  //     }
  //   };

  //   var successCallback = function(success) {
  //     alert('payment_id: ' + success.razorpay_payment_id)
  //     var orderId = success.razorpay_order_id
  //     var signature = success.razorpay_signature
  //   }
    
  //   var cancelCallback = function(error) {
  //     alert(error.description + ' (Error '+error.code+')')
  //   }
    
  //   RazorpayCheckout.on('payment.success', successCallback)
  //   RazorpayCheckout.on('payment.cancel', cancelCallback)
  //   RazorpayCheckout.open(options)
  // }


  ngOnInit() {
    try {
      this.router.events.subscribe((event: RouterEvent) => {
        // console.log('event url',event.url);
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          // console.log('inside event',event.url)
          this.selfPath = event.url + '/cart';
        }
        this.selfPath = this.router.routerState.snapshot.url;
        // console.log(this.getStringBeforeSubstring(this.selfPath, 'viptips-details'));
        // console.log("ngOnInit", this.selfPath);

        this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'cart') + 'cart';
      });

    } catch (e) {
      console.log(e);
    }
  }

  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }

  async ionViewWillEnter() {
    this.uihelper.ShowSpinner();
    this.storageService.get(Constant.AUTH).then(res => {
      this.userData= res.user_details;
      // console.log('userData',this.userData)
      this.user_id = res.user_details.id;
      this.course_name = res.user_details.course_name;
      this.httpService.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
        this.cartCount = res['payload'].cart_count        
      })
      this.cartListApi();    
  });
  }

  cartListApi()
  {
    this.http.get(this.url + 'cart-list/' + this.user_id, this.options).subscribe((res)=>{   
      console.log('cart page',res)
      this.cartList = Object.values(res['payload']);

      this.coupon_detail = this.cartList.pop();
      // console.log('coupon_detail',this.coupon_detail)

      this.amount_detail = this.cartList.pop();
      this.final_total = this.amount_detail.final_total
      // console.log('amount_detail',this.amount_detail)
      this.admin_coupon_detail_data = this.coupon_detail.admin_coupon_detail_data
      this.institute_coupon_detail_data = this.coupon_detail.institute_coupon_detail_data
      this.referal_coupon_detail_data = this.coupon_detail.coupon_detail
      this.referal_coupon_credit = this.coupon_detail.studentReferalCredits
      this.credit_detail = this.coupon_detail.credit_detail
      // console.log('credit_detail',this.credit_detail.length)
      if(this.credit_detail.length == 0)
      {
        this.checked = false
        // console.log('credit detail checked 1',this.checked)
      }
      else{
        this.checked = true
        // console.log('credit detail checked 2',this.checked)

      }

      this.uihelper.HideSpinner();
    },
    (err)=>{
      console.log('cart page error',err)
      this.uihelper.HideSpinner();
      this.uihelper.ShowAlert("","Cart is empty");
    })
  }

  async deleteCartItem(id)
  {
    this.uihelper.ShowSpinner();
    this.http.delete(this.url + 'delete-cart-item/' + this.user_id + '/' + id, this.options).subscribe((res)=>{
      // localStorage.setItem("cartCount", res['payload'].cartCount);
      // this.cartCount = localStorage.getItem('cartCount')
      this.httpService.afterLoginGet( "cart_count" , this.user_id).subscribe((res) => {
        this.cartCount = res['payload'].cart_count        
      })
      this.http.get(this.url + 'cart-list/' + this.user_id, this.options).subscribe((res)=>{  
        
        this.cartList = Object.values(res['payload']);
        this.coupon_detail = this.cartList.pop();
        this.amount_detail = this.cartList.pop();
        this.final_total = this.amount_detail.final_total

        this.admin_coupon_detail_data = this.coupon_detail.admin_coupon_detail_data
        this.institute_coupon_detail_data = this.coupon_detail.institute_coupon_detail_data
        this.referal_coupon_credit = this.coupon_detail.studentReferalCredits
        this.referal_coupon_detail_data = this.coupon_detail.coupon_detail
        
        localStorage.setItem("cartCount", this.cartList.length + "" );
        this.uihelper.HideSpinner();
      },
      (err)=>{
        // console.log('cart page error',err)
        this.uihelper.HideSpinner();
        this.uihelper.ShowAlert('',"Cart is empty");
        this.router.navigate(['/tabs'])
      }
      );
    });

  }

  checkReferalCredit(e)
  {
    // this.checked = !this.checked;
    this.uihelper.ShowSpinner();
  	// console.log("checked: " + e.currentTarget.checked);
    this.check_type = e.currentTarget.checked ? 'remove' : 'add'    
    let data = new FormData();
    data.append('type', this.check_type);
    this.httpService.afterLoginPost( "applycredit" , data).subscribe((res) => {
      console.log('applycredit',res)
      this.cartListApi()
      this.uihelper.HideSpinner();

    },
    (err)=>{
      this.uihelper.HideSpinner();
      this.uihelper.ShowAlert('Refer and Earn','Something Went wrong..Please try again');
      console.log(err)
    })
  }

  randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
  }

  // proceedToPay()
  // {
  //       let key = "Up5BkJWC";        //
  //       let salt = "O54v6MqUYs";
  //       let bookingId = this.randomString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
  //       let amt = this.final_total;
  //       let name = this.userData.first_name;
  //       let email = this.userData.email;
  //       let mobile = this.userData.phone_number;
  //       let productinfo = "Packages";
  //       let service_provider = "payu_paisa";
  //       let surl = "https://makemytest.in/mobileapp/success.php";
  //       let furl = "https://makemytest.in/mobileapp/failure.php";
  //       var udf1 = '';
  //       var udf2 = '';
  //       var udf3 = '';
  //       var udf4 = '';
  //       var udf5 = '';
  //       var udf6 = '';
  //       var udf7 = '';
  //       var udf8 = '';
  //       var udf9 = '';
  //       var udf10 = '';

  //         let string = key + '|' + bookingId + '|' + amt + '|' + productinfo + '|' + name + '|' + email + '|' + udf1 + '|' + udf2 + '|' + udf3 + '|' + udf4 + '|' + udf5 + '|' + udf6 + '|' + udf7 + '|' + udf8 + '|' + udf9 + '|' + udf10 + '|' + salt;
  //         let encrypttext = sha512.sha512(string);

  //         let url = "https://makemytest.in/mobileapp/payuBiz.html?amt=" + amt + "&service_provider=" + service_provider + "&name=" + name + "&surl=" + surl + "&furl=" + furl + "&mobileNo=" + mobile + "&email=" + email + "&bookingId=" + bookingId + "&productinfo=" + productinfo + "&hash=" + encrypttext + "&salt=" + salt + "&key=" + key;
              
  //         let option: InAppBrowserOptions = {
  //           location: 'yes',
  //           clearcache: 'yes',
  //           zoom: 'yes',
  //           toolbar: 'no',
  //           closebuttoncaption: 'back'
  //         };
  //         const browser: any = this.iab.create(url, '_blank', option);
  //           // console.log('iab',this.iab.create(url, '_blank', option))
  //         browser.on('loadstart').subscribe(event => {
  //           browser.executeScript({
  //             code: "(function submitForm() { var form = document.getElementById('sendParam'); var productinfo = getParameterByName('productinfo'); var amt = getParameterByName('amt'); var name = getParameterByName('name'); var mobileNo = getParameterByName('mobileNo'); var email = getParameterByName('email'); var bookingId = getParameterByName('bookingId'); var salt = getParameterByName('salt'); var key = getParameterByName('key'); document.getElementById('key').value = key; document.getElementById('amount').value = amt; document.getElementById('productinfo').value = productinfo; document.getElementById('amount').value = amt; document.getElementById('firstname').value = name; document.getElementById('phone').value = mobileNo; document.getElementById('email').value = email;document.getElementById('txnid').value = bookingId;var hash = getParameterByName('hash');document.getElementById('hash').value = hash; document.sendParam.submit(); }function getParameterByName(name) { name = name.replace(/[\[]/, '\\[' ).replace(/[\]]/, '\\]'); var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'), results = regex.exec(location.search); return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' ')); } submitForm();)()"
  //             // file: "https://makemytest.in/mobileapp/payumoneyPaymentGateway.js"
         
  //           });

  //           // if (event.url.match("https://newdev.makemytest.in/mobileapp/success.php")) {
  //             if(event.url == surl) {
  //             console.log('success url match');
  //             browser.close();
  //             this.completePayment(bookingId)
  //           }
  //           if (event.url == furl) {
  //             console.log('Payment failure');
  //             this.uihelper.ShowAlert('',"Payment Failed");
  //             browser.close();
  //           }
  //         });

  // }

  proceedToPay()
  {

    let txnid = this.randomString(20, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
    let form_data: FormData = new FormData();
    form_data.append('amount', this.final_total);
    form_data.append('currency', 'INR');
    form_data.append('txnid', txnid);
    form_data.append('user_id', this.user_id);
    this.httpService.afterLoginPost('checkout-proceed',form_data).subscribe((res:any)=>{  
      // console.log('proceedtopay',res.payload.order_id)
      if(res.payload.order_id)
      {
          var options = {
            description: 'MMT payment',
            currency: 'INR', // your 3 letter currency code
            key: 'rzp_test_sdQrRwyEDr4qfx', // your Key Id from Razorpay dashboard
            order_id: res.payload.order_id,
            amount:  this.final_total * 100, // ,  Payment amount in smallest denomiation e.g. cents for USD
            name: this.userData.full_name, // this.userData.full_name,
            prefill: {
              email:  this.userData.email, // this.userData.email,
              contact: this.userData.phone_number, // this.userData.phone_number,
              name: this.userData.full_name,  // this.userData.full_name
            },
            theme: {
              color: '#F37254'
            },
            modal: {
              ondismiss: function () {
                alert('dismissed')
              }
            }
          };
      
          var successCallback = (success)=> {
         
            this.completePayment(success.razorpay_order_id,success.razorpay_payment_id,success.razorpay_signature)

            }
      
          var cancelCallback = function (error) {
            // alert(error.description + ' (Error ' + error.code + ')');
            this.uihelper.paymentAlert("Payment Unsuccesfull.");

          };
          RazorpayCheckout.on('payment.success', successCallback)
          RazorpayCheckout.on('payment.cancel', cancelCallback)
          RazorpayCheckout.open(options)
      
          // RazorpayCheckout.open(options, successCallback, cancelCallback);
        
      }  
    },
    (err)=>{
        console.log('error',err)
    })
  }

   completePayment(order_id: any,rzp_paymentid,rzp_signature) {
    this.uihelper.ShowSpinner();
    let form_data: FormData = new FormData();
    form_data.append('total_amount', this.final_total);
    form_data.append('status', 'success');
    form_data.append('txnid', order_id);
    form_data.append('rzp_paymentid', rzp_paymentid);
    form_data.append('rzp_signature', rzp_signature);

    this.httpService.afterLoginPost('payumoney/response',form_data).subscribe((res:any)=>{    

      if (res.status == 200) {
        // console.log('after payment success')
        localStorage.setItem("cartCount", res['payload'].cartCount);
        this.uihelper.HideSpinner(); 
        this.uihelper.paymentAlert("Payment Successfull. Go to Dashboard");
        
      }
      else {
        this.uihelper.HideSpinner(); 
        this.uihelper.ShowAlert('',"Invalid Data");
      }
    },
    (err)=>{
        this.uihelper.HideSpinner(); 
        this.uihelper.ShowAlert('',"Something went wrong");
        console.log('error',err)
    });

  }


  // async completePayment(txid: string) {
  //   this.uihelper.ShowSpinner();
  //   let form_data: FormData = new FormData();
  //   form_data.append('total_amount', this.final_total);
  //   form_data.append('status', 'success');
  //   form_data.append('unmappedstatus', 'captured');
  //   form_data.append('txnid', txid);

  //   this.httpService.afterLoginPost('payumoney/response',form_data).subscribe((res:any)=>{    
  //     // console.log('response')
  //     // console.log(res.toString())
  //     // console.log(JSON.stringify(res))

  //     if (res.status == 200) {
  //       // console.log('after payment success')
  //       localStorage.setItem("cartCount", res['payload'].cartCount);
  //       this.uihelper.HideSpinner(); 
  //       this.uihelper.paymentAlert("Payment Successfull. Go to Dashboard");
        
  //     }
  //     else {
  //       this.uihelper.HideSpinner(); 
  //       this.uihelper.ShowAlert('',"Invalid Data");
  //     }
  //   },
  //   (err)=>{
  //       console.log('error',err)
  //   });

  // }

  async openPopover()
  {
     // this.uihelper.HideSpinner();
      let data = new FormData();
      data.append('course_id', this.course_id.course_id);
      data.append('user_id', this.user_id);

      const popover = await this.modalController.create({
        component: CouponPopupPage,
        componentProps: {
          "coupons": "true",
          'data': data,
        },
        
      });
      popover.onWillDismiss().then(() => {
        console.log('modal dismiss')
        this.cartListApi();
      });
      
      return await popover.present();

  }

  // deleteCouponCode()
  // {
  //   let coupon_type = this.coupon_detail.admin_coupon_detail_data.coupon_code_type
  //   console.log(' delete coupon',coupon_type)
  //   let data = new FormData();
  //   data.append('coupontype', 'admin');
  //   data.append('student_id', this.user_id);
  //   this.httpService.afterLoginPost('removecoupon',data).subscribe((res:any)=>{    
  //     console.log('remove coupon res',res)
    
  //   });
  // }


  TestAction(){
    this.navCtrl.pop();
  }

}
