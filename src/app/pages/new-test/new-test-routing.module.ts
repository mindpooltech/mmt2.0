import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewTestPage } from './new-test.page';

const routes: Routes = [
  {
    path: '',
    component: NewTestPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewTestPageRoutingModule {}
