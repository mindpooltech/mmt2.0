import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras, RouterEvent, NavigationEnd } from '@angular/router';
import { HttpService } from 'src/app/services/http.service';
import { LoadingController, NavController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JsonPipe } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { environment } from 'src/environments/environment';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-new-test',
  templateUrl: './new-test.page.html',
  styleUrls: ['./new-test.page.scss'],
})
export class NewTestPage implements OnInit {

  test_id: any;
  postData = {
    target_marks: ''
  }
  userData: any;
  user_id: any;
  institute_id: any;
  url = Constant.apiUrl
  headers = new HttpHeaders({
    'Accept':  'application/json',
    'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
  });
  options = { headers: this.headers}
  cartCount: any;
  selfPath: string;
  total_marks: any;
  constructor(private activatedRoute:ActivatedRoute,private router: Router,private http: HttpClient,private httpService: HttpService,private storageService:StorageService,private loadingController: LoadingController,private authService: AuthService, private navCtrl: NavController, private uihelper: UIHelper) { 
   
    this.activatedRoute.queryParams.subscribe((data)=>{
      this.test_id = data.test_id
      this.total_marks = data.total_marks
      console.log('test id', this.test_id)
    })
  }

  ngOnInit() {
    try {
      this.router.events.subscribe((event: RouterEvent) => {
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          this.selfPath = event.url + '/test-board';
        }
        this.selfPath = this.router.routerState.snapshot.url;
        // console.log(this.getStringBeforeSubstring(this.selfPath, 'viptips-details'));

        this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'test-board') + 'test-board';
      });

    } catch (e) {
      console.log(e);
    } 
  }

  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }

  ionViewWillEnter()
  {
    this.cartCount = localStorage.getItem('cartCount')
    this.storageService.get(Constant.AUTH).then(res => {
      this.userData= res.user_details;
      this.user_id = res.user_details.id;
      this.institute_id = res.user_details.institute_id
      this.httpService.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
        this.cartCount = res['payload'].cart_count        
      })
    });
  }

  validateInputs()
  {
    let target_marks = this.postData.target_marks;    
      return (target_marks);     
  }

  startTest()
  {
    // console.log('total_marks',this.total_marks)
    if( this.postData.target_marks > this.total_marks)
    {
      this.uihelper.ShowAlert('','Target marks should not be greater than total marks.')
      return false
    }

    if(this.validateInputs())
    {
      let navigationExtras: NavigationExtras = {
        queryParams: {
          target_marks: this.postData.target_marks,
          test_id : this.test_id,
        }
      };
      this.navCtrl.navigateForward([this.selfPath + '/start-test'],  navigationExtras);

    }
    else{
      // console.log('use toast validation here');
      this.uihelper.ShowAlert('','Please enter Target marks.')

    }
  }

  getHome() {
    this.navCtrl.pop();
  }

  GoToCart(){
    this.router.navigate([ this.selfPath + "/cart"]);
  }

}
