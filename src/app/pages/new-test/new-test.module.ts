import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewTestPageRoutingModule } from './new-test-routing.module';

import { NewTestPage } from './new-test.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewTestPageRoutingModule
  ],
  declarations: [NewTestPage]
})
export class NewTestPageModule {}
