import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyQueriesPageRoutingModule } from './my-queries-routing.module';

import { MyQueriesPage } from './my-queries.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyQueriesPageRoutingModule
  ],
  declarations: [MyQueriesPage]
})
export class MyQueriesPageModule {}
