import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyQueriesPage } from './my-queries.page';

const routes: Routes = [
  {
    path: '',
    component: MyQueriesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyQueriesPageRoutingModule {}
