import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { LoadingController, NavController } from '@ionic/angular';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { environment } from 'src/environments/environment';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-my-queries',
  templateUrl: './my-queries.page.html',
  styleUrls: ['./my-queries.page.scss'],
})
export class MyQueriesPage implements OnInit {

  userData: any;
  url = Constant.apiUrl
  headers = new HttpHeaders({
    'Accept':  'application/json',
    'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
  });
  options = { headers: this.headers}
  queries: any;
  packageList: any;
  package_id: any;
  package_test: any;
  test_id: any;
  status: any;
  user_id: any;
  cartCount: any;
  selfPath: string;
  constructor(private router: Router,private http: HttpClient,private httpService: HttpService,private storageService:StorageService,private uihelper: UIHelper,private navCtrl:NavController) { 
    
  }

  async ngOnInit() {
    try {
      this.router.events.subscribe((event: RouterEvent) => {
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          this.selfPath = event.url + '/my-queries';
        }
        this.selfPath = this.router.routerState.snapshot.url;
        this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'my-queries') + 'my-queries';
      });

    } catch (e) {
      console.log(e);
    }    
  }

  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }

  async ionViewWillEnter()
  {
    this.cartCount = localStorage.getItem('cartCount')
    this.uihelper.ShowSpinner();
    this.storageService.get(Constant.AUTH).then(res => {
      this.userData= res.user_details;
      this.user_id = res.user_details.id;
      this.httpService.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
        this.cartCount = res['payload'].cart_count        
      })
      let data = {
        user_id : this.user_id
      }
      this.httpService.afterLoginPost('student-test-queries',data).subscribe((res:any)=>{ 
        this.queries = res['payload'].studentquery;
        this.packageList = res['payload'].packages;
        // console.log('queries',this.queries);
        this.queries.forEach((value,i)=>{
        this.queries[i]['created_at'] = value.created_at.substring(0, 10)
        })

        this.uihelper.HideSpinner();

      },
      (err)=>{
        console.log('queries not found')
        this.uihelper.ShowAlert('',"not found!!");

      });
    })
  }

  getTestData(event)
  {
    this.package_id = event.target.value;
    this.http.get(this.url+ 'package-tests/' +this.package_id,this.options).subscribe((res)=>{
    this.package_test = res['payload']
    })
  }

  selectTest(event)
  {
    this.test_id = event.target.value;
  }

  selectStatus(event)
  {
    this.status = event.target.value;
  }

  async submit_filter(){
    this.uihelper.ShowSpinner();
    let form_data = new FormData();
    form_data.append('package_id', this.package_id ? this.package_id : '')
    form_data.append('test_id', this.test_id ? this.test_id : '')
    form_data.append('test_status', this.status ? this.status : '')
    form_data.append('user_id', this.user_id)
    this.httpService.afterLoginPost('student-test-queries',form_data).subscribe(async (res:any)=>{      
        this.queries = res['payload'].studentquery;
        // console.log('filter queries',this.queries); 

        this.uihelper.HideSpinner();
    },
    (err:any)=>{
      this.uihelper.HideSpinner();
      console.log('error',err)
  
    });
  }

  goBack() {
    this.router.navigate([ '/tabs']);
  }

  GoToCart(){
    this.router.navigate([ this.selfPath + "/cart"]);
  }

  GoToUserProfile() {
    this.router.navigate([this.selfPath + "/user-profile"]);
  }

}
