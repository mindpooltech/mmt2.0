import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchPackagesPopoverPageRoutingModule } from './search-packages-popover-routing.module';

import { SearchPackagesPopoverPage } from './search-packages-popover.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchPackagesPopoverPageRoutingModule
  ],
  declarations: [SearchPackagesPopoverPage]
})
export class SearchPackagesPopoverPageModule {}
