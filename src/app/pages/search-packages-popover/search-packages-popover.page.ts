import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';
import { HttpService } from 'src/app/services/http.service';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-search-packages-popover',
  templateUrl: './search-packages-popover.page.html',
  styleUrls: ['./search-packages-popover.page.scss'],
})
export class SearchPackagesPopoverPage implements OnInit {

  package_id: any;
  headers = new HttpHeaders({
    'Accept':  'application/json',
    'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
  });
  url = Constant.apiUrl

  options = { headers: this.headers}
  test_chapters: any;

  constructor(private navParams: NavParams, private popovercontroller: PopoverController,private http: HttpClient,private httpService: HttpService,private uihelper: UIHelper,) { }

  async ngOnInit() {
    this.uihelper.ShowSpinner();
    this.package_id = this.navParams.data.package_id
    this.http.get(this.url + 'package-test-chapters/' + this.package_id, this.options).subscribe((res)=>{     
      // console.log('search package popover test chapters', res)
      res['payload'].forEach((value,index)=>{
          // console.log('values',value,index)
          value.chapters.forEach((value2,index2)=>{
            res['payload'][index]['chapters'][index2] = value2.title
          });
          res['payload'][index]['chapters'] = value.chapters.join(',');

      });
      this.test_chapters = res['payload']
      this.uihelper.HideSpinner();

      // console.log('final response',this.test_chapters)

  });
  }

}
