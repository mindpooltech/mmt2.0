import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchPackagesPopoverPage } from './search-packages-popover.page';

const routes: Routes = [
  {
    path: '',
    component: SearchPackagesPopoverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchPackagesPopoverPageRoutingModule {}
