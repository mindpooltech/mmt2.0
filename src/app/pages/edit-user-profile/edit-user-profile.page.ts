import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { AuthService } from 'src/app/services/auth.service';
import { AlertController, NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { EventService } from 'src/app/services/event.service';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-edit-user-profile',
  templateUrl: './edit-user-profile.page.html',
  styleUrls: ['./edit-user-profile.page.scss'],
})
export class EditUserProfilePage implements OnInit {

  userData: any;
  userToken: any;
  url = Constant.apiUrl
  headers = new HttpHeaders({
    'Accept': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
  });
  options = { headers: this.headers }
  public editProfile: FormGroup;
  postData: { id: any, first_name: any; last_name: any; phone_number1: any; phone_number2: any; user_attempt_month: any; user_attempt_year: any; address1: any; address2: any; avatar: any, avatar_type: any };
  public submitAttempt: boolean = false;
  avatar_type: any;
  user_id: any;
  avatar: any;
  first_name: any;
  address1: any;
  address2: any;
  phone_number2: any;
  phone_number1: any;
  parents_phone_no: any;
  parents_email_id:any;
  user_attempt_month:any;
  user_attempt_year:any;
  last_name: any;
  years: any[];
  attempt_month: any;
  attempt_year: any;
  email: any;

  constructor(private http: HttpClient,private navCtrl: NavController,private events: EventService,
    private router: Router, private authService: AuthService, private storageService: StorageService, private loadingController: LoadingController, private alertController: AlertController, public formBuilder: FormBuilder, private uihelper: UIHelper) {

    this.editProfile = formBuilder.group({

      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      phone_number1: ['', Validators.required],
      phone_number2: [''],
      parents_phone_no: [''],
      parents_email_id: [''],
      user_attempt_month: ['', Validators.required],
      user_attempt_year: ['', Validators.required],
      avatar_type: ['']
    });
  }

  ngOnInit() {
    this.storageService.get(Constant.AUTH).then(res => {
      this.userToken = localStorage.getItem("userToken");
      // console.log('user details', res)
      this.userData = res.user_details;
      this.user_id = res.user_details.id;
      this.email = res.user_details.email;
      this.first_name = this.userData?.first_name
      this.last_name = this.userData?.last_name
      this.phone_number1 = this.userData?.phone_number
      this.phone_number2 = this.userData?.phone_number2
      this.parents_phone_no = this.userData?.parents_phone_no
      this.parents_email_id = this.userData?.parents_email_id
      this.user_attempt_month = this.userData?.user_attempt_month
      this.user_attempt_year = this.userData?.user_attempt_year
      this.address1 = this.userData?.address1
      this.address2 = this.userData?.address2
      this.avatar = res.user_details.avatar_location ? Constant.adminUrl + res.user_details.avatar_location : 'assets/icon/user-profile.png';

    });

    let current_year = new Date().getFullYear()
    this.years = []
    for(var i = current_year; i < current_year + 6; i++)
    {
      this.years.push({
        value: i
      })
      // console.log('years',this.years)
    }
  }

  async upload(str: any) {
    this.avatar_type = str.target.files[0];
    // console.log('avatar_type', this.avatar_type);
  }

  async editProfileAction() {
  
    // this.uihelper.ShowSpinner();
    // console.log('phone number', this.editProfile.value.phone_number1, this.editProfile.value.parents_phone_no)
    if(this.editProfile.value.parents_email_id != '' && this.editProfile.value.parents_email_id == this.email)
    {
      this.uihelper.ShowAlert('','Students and Parents Email id cannot be same')
      return false
    }
    if(this.editProfile.value.parents_phone_no != '' && this.editProfile.value.phone_number1 == this.editProfile.value.parents_phone_no)
    {
      this.uihelper.ShowAlert('','Number and Parents number cannot be same')
      return false
    }
    else{
      this.uihelper.ShowSpinner();
    this.submitAttempt = true;
    let formData = new FormData();
    formData.append('id', this.user_id);
    formData.append('first_name', this.editProfile.value.first_name);
    formData.append('last_name', this.editProfile.value.last_name);
    formData.append('phone_number1', this.editProfile.value.phone_number1);
    formData.append('phone_number2', this.editProfile.value.phone_number2 == null ? "" : this.editProfile.value.phone_number2);
    formData.append('address1', this.editProfile.value.address1);
    formData.append('address2', this.editProfile.value.address2);
    formData.append('parents_phone_no', this.editProfile.value.parents_phone_no);
    formData.append('parents_email_id', this.editProfile.value.parents_email_id);
    formData.append('user_attempt_month', this.editProfile.value.user_attempt_month == null ? "" : this.editProfile.value.user_attempt_month);
    formData.append('user_attempt_year', this.editProfile.value.user_attempt_year == null ? "" : this.editProfile.value.user_attempt_year);
    formData.append('avatar', this.avatar_type);
    formData.append('avatar_type', "storage");
    // console.log('post data', formData)

    this.http.post(this.url + 'profile-update', formData, this.options).subscribe((res: any) => {
      // console.log('success',res);
      this.storageService.store(Constant.AUTH, res.payload);

      var logedinUserDetails: any;
      this.storageService.get(Constant.AUTH).then(res => {
        // console.log('Data', res)
        logedinUserDetails = res.user_details
        this.events.sendMessage({
          userName: logedinUserDetails.full_name
        });
      })

      this.uihelper.HideSpinner();
      this.uihelper.ShowAlert('','Profile updated succesfully')
      this.router.navigate(['/tabs'])
    },
      (err: any) => {
        this.uihelper.HideSpinner();
        console.log('failure', err.error)
        // this.toastService.presentToast(res.error.payload.password_confirmation_reg.join('#'))
        // var err = Object.values(err.error.payload)[0][0];        
        // this.toastService.presentToast('something went wrong. Please try again')
        this.uihelper.ShowAlert('','Please fill all details correctly')

      });
    }
  }

  goBack()
  {
    this.navCtrl.pop();
  }

}
