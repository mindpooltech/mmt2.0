import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestEvaluationScorecardPageRoutingModule } from './test-evaluation-scorecard-routing.module';

import { TestEvaluationScorecardPage } from './test-evaluation-scorecard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TestEvaluationScorecardPageRoutingModule
  ],
  declarations: [TestEvaluationScorecardPage]
})
export class TestEvaluationScorecardPageModule {}
