import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestEvaluationScorecardPage } from './test-evaluation-scorecard.page';

const routes: Routes = [
  {
    path: '',
    component: TestEvaluationScorecardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestEvaluationScorecardPageRoutingModule {}
