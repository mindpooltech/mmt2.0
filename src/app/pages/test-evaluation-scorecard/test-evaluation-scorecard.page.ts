import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { ModalController, PopoverController } from '@ionic/angular';
import { ModalPage } from '../modal/modal.page';
import { ScorecardpopoverPage } from '../scorecardpopover/scorecardpopover.page';
import { UIHelper } from 'src/Helper/UIHelper';
import { Constant } from 'src/Helper/Constant';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-test-evaluation-scorecard',
  templateUrl: './test-evaluation-scorecard.page.html',
  styleUrls: ['./test-evaluation-scorecard.page.scss'],
})
export class TestEvaluationScorecardPage implements OnInit {

  scoreCardObj: any;
  scoreCard: any;
  test_id: any;
  exam_id: any;
  exist_theory_que: any;
  tempObj: { option_1: string; option_2: string; option_3: string; option_4: string; };
  cartCount: string;
  student_testRating_exist: boolean;
  selfPath: string;
  user_id: any;

  constructor(private activatedRoute: ActivatedRoute,private storageService:StorageService,public modalController: ModalController, private popovercontroller: PopoverController, private router:Router, private uihelper:UIHelper,private httpService:HttpService) {
    // this.cartCount = localStorage.getItem('cartCount')
  
    this.activatedRoute.queryParams.subscribe((data)=>{
      // console.log('student exam id and test id',data);
      this.test_id = data.test_id
      this.exam_id = data.exam_id
    })
  }


  ngOnInit() {
    this.router.events.subscribe((event: RouterEvent) => {
      this.selfPath = '';
      if (event && event instanceof NavigationEnd && event.url) {
        this.selfPath = event.url + '/test-evaluation-scorecard';
      }
      this.selfPath = this.router.routerState.snapshot.url;
      this.selfPath = this.uihelper.getStringBeforeSubstring(this.selfPath, 'test-evaluation-scorecard') + 'test-evaluation-scorecard';
    });
    
    let paramData = this.storageService.getParamData()
    this.scoreCardObj = JSON.parse(paramData)
    this.student_testRating_exist = this.scoreCardObj.payload.student_testRating_exist
    this.scoreCard = this.scoreCardObj.payload.studentExam.test_question_data.test_sub_questions
    // console.log('scoreCardObj',this.scoreCardObj)
    this.tempObj = {
      option_1: 'A',
      option_2: 'B',
      option_3: 'C',
      option_4: 'D',
    }

  }

  async ionViewWillEnter()
  {
    this.storageService.get(Constant.AUTH).then(res => {
      this.user_id= res.user_details.id;
      this.httpService.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
        this.cartCount = res['payload'].cart_count        
      })
    });
  }

  async openPopover(question_id, ev:any)
{
  const popover = await this.popovercontroller.create({
    component: ScorecardpopoverPage,
    // cssClass: 'my-custom-class',
    
    componentProps: {
      "question_id": question_id,
    },
  });
  return await popover.present(); 
}

async presentModal() {
  const modal = await this.modalController.create({
    component: ModalPage,
    cssClass: 'my-custom-class',
    componentProps: {
      "test_id": this.test_id,
      "exam_id": this.exam_id
    }
  });
  return await modal.present();
}

goToDashboard()
{
  this.router.navigate(['/tabs']);
}


}
