import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TestEvaluationScorecardPage } from './test-evaluation-scorecard.page';

describe('TestEvaluationScorecardPage', () => {
  let component: TestEvaluationScorecardPage;
  let fixture: ComponentFixture<TestEvaluationScorecardPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TestEvaluationScorecardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TestEvaluationScorecardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
