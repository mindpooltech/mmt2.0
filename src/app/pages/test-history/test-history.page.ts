import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';
import { HttpService } from 'src/app/services/http.service';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { LoadingController, NavController } from '@ionic/angular';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-test-history',
  templateUrl: './test-history.page.html',
  styleUrls: ['./test-history.page.scss'],
})
export class TestHistoryPage implements OnInit {

  userData: any;
  user_id: any;
  course_id: any;
  testHistory: any;
  examtest: any;
  packageList: any;
  url = Constant.apiUrl
  headers = new HttpHeaders({
    'Accept':  'application/json',
    'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
  });
  options = { headers: this.headers}
  package_id: any;
  package_test: any;
  test_id: any;
  status: any;
  cartCount: any;
  selfPath: string;
  subjectList: any;
  subject_id: any;
  constructor(private router:Router,private http:HttpClient,private httpService:HttpService, private storageService:StorageService,private navCtrl: NavController,private uihelper: UIHelper,) {
    this.cartCount = localStorage.getItem('cartCount')
   }

  ngOnInit() {
    try {
      this.router.events.subscribe((event: RouterEvent) => {
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          this.selfPath = event.url + '/test-history';
        }
        this.selfPath = this.router.routerState.snapshot.url;
        // console.log(this.getStringBeforeSubstring(this.selfPath, 'viptips-details'));

        this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'test-history') + 'test-history';
      });

    } catch (e) {
      console.log(e);
    }
  }

  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }

  async ionViewWillEnter()
  {
    // this.cartCount = localStorage.getItem('cartCount')
    this.uihelper.ShowSpinner();

    this.storageService.get(Constant.AUTH).then(res => {
      this.user_id= res.user_details.id;
    //   this.http.get(this.url + 'cart-count/' + this.user_id, this.options).subscribe((res)=>{   
    //     this.cartCount = res['payload']
    // });
    });
    this.storageService.get(Constant.AUTH).then(async res => {
      this.userData= res.user_details;
      this.user_id = res.user_details.id;
      this.course_id = res.user_details.course_id;
      this.httpService.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
        this.cartCount = res['payload'].cart_count        
      })

      let data = {
        user_id : this.user_id,
        course_id : this.course_id
      }
      this.httpService.afterLoginPost('test-history',data).subscribe((res:any)=>{
        this.uihelper.HideSpinner();
      console.log('test history', res)
      this.testHistory = res.payload.all_test;       
      this.subjectList = res.payload.subjects;
      
      this.testHistory.forEach((value,i)=>
      {
        this.testHistory[i]['obtained_marks'] = parseFloat(value.mcq_marks) + parseFloat(value.theory_marks)  
        this.testHistory[i]['updated_at'] = value.updated_at.substring(0, 11)
      })

    },
    (err)=>{
      this.uihelper.HideSpinner();
      console.log(err);
      this.uihelper.ShowAlert('Test History','Not Found');
    });

  });
  }

  getTestData(event)
  {
    this.package_id = event.target.value;
    this.http.get(this.url+ 'package-tests/' +this.package_id,this.options).subscribe((res)=>{
    this.package_test = res['payload']
    // console.log('package test',this.package_test);

    })
  }

  getTestPackages(event)
  {
    this.subject_id = event.target.value;
    let subject_data = {
      id : this.user_id,
      subject_id : this.subject_id
    }
    this.httpService.afterLoginPost('get_test_taken_packages', subject_data).subscribe((res: any) => {
     console.log('subject wise packages',res);
     this.packageList = res.payload.packages;
    });
  }

  selectTest(event)
  {
    this.test_id = event.target.value;
  }

  selectStatus(event)
  {
    this.status = event.target.value;
  }

  async submit_filter(){
    this.uihelper.ShowSpinner();
    
    let form_data = new FormData();
    form_data.append('subject_id', this.subject_id ? this.subject_id : '')
    form_data.append('package_id', this.package_id ? this.package_id : '')
    form_data.append('test_id', this.test_id ? this.test_id : '')
    form_data.append('status', this.status ? this.status : '')
    form_data.append('user_id', this.user_id)
    form_data.append('course_id', localStorage.getItem('course_id'))
    this.httpService.afterLoginPost('test-history',form_data).subscribe(async (res:any)=>{      
        // console.log('after search filter',res); 
        this.uihelper.HideSpinner();
        this.testHistory = res.payload.all_test;       
        this.subjectList = res.payload.subjects;
    },
    (err:any)=>{
      this.uihelper.HideSpinner();
      this.uihelper.ShowAlert('Test History','Not Found');
      console.log('error',err)
  
    });
  }

  viewDetails(exam_id,test_id,obtained_marks,price)
  {
    console.log('price',price);
    
    this.router.navigate([this.selfPath + '/test-evaluation'],{queryParams:{exam_id:exam_id,test_id:test_id,obtained_marks:obtained_marks,price:price}})
  }

  getHome() {
    this.navCtrl.pop();
  }

  GoToCart(){
    this.router.navigate([ this.selfPath + "/cart"]);
  }

  GoToUserProfile() {
    this.router.navigate([this.selfPath + "/user-profile"]);
  }

}
