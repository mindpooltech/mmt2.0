import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { LoadingController, NavController, PopoverController } from '@ionic/angular';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { environment } from 'src/environments/environment';
import { NotesPopoverPage } from '../notes-popover/notes-popover.page';
import { PopoverPage } from '../popover/popover.page';
import { HttpClient } from '@angular/common/http';
import { UIHelper } from 'src/Helper/UIHelper';
import { Constant } from 'src/Helper/Constant';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Platform } from '@ionic/angular';
import { ModalController, NavParams } from '@ionic/angular';
import { NotePdfModalPage } from '../note-pdf-modal/note-pdf-modal.page';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@Component({
  selector: 'app-notes-details',
  templateUrl: './notes-details.page.html',
  styleUrls: ['./notes-details.page.scss'],
})
export class NotesDetailsPage implements OnInit {

  selfPath: string;
  cartCount: string;
  userData: any;
  course_name: any;
  institute_id: any;
  // course_id: any;
  user_id: any;
  subjectList: any;
  course_id = {
    course_id: localStorage.getItem('course_id')
  }
  document_path: any;
  note_list: any;
  document_file: any;
  note_url: string;
  
  constructor(private router: Router,
    private http: HttpClient,
    private httpService: HttpService,
    private storageService: StorageService,
    private navCtrl: NavController,
    private uihelper: UIHelper,
    private iab: InAppBrowser,
    private popovercontroller: PopoverController,
    public platform: Platform,
    public modalController: ModalController,
    private transfer: FileTransfer,
     private file: File,
     private androidPermissions: AndroidPermissions) {
  }

  fileTransfer: FileTransferObject = this.transfer.create();


  ngOnInit() {
    try {
      this.router.events.subscribe((event: RouterEvent) => {
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          this.selfPath = event.url + '/notes-details';
        }
        this.selfPath = this.router.routerState.snapshot.url;

        this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'notes-details') + 'notes-details';
      });

    } catch (e) {
      console.log(e);
    }
  }

  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }

  async ionViewWillEnter() {
   this.uihelper.ShowAlert('','Please select Subject.')
    this.uihelper.ShowSpinner();
    this.storageService.get(Constant.AUTH).then(res => {
      this.userData = res.user_details;
      this.course_name = this.userData.course_name
      this.institute_id = res.user_details.institute_id;
      this.user_id = res.user_details.id;
      this.httpService.afterLoginGet( "cart_count" , res.user_details.id).subscribe((res) => {
        this.cartCount = res['payload'].cart_count        
      })
      this.uihelper.HideSpinner(); 
    });    
    this.httpService.afterLoginPost('subject-list', this.course_id).subscribe((res: any) => {
      this.subjectList = res.payload;
      this.uihelper.HideSpinner(); 
      // console.log('subjectList',this.subjectList)
    });
  }

  async getNotes(event)
  {
    this.uihelper.ShowSpinner();
    let data = {
      institute_id: this.institute_id,
      course_id: localStorage.getItem('course_id'),
      subject_id: event.target.value
    }
    this.httpService.afterLoginPost('subject-wise-notes', data).subscribe((res: any) => {
    // console.log('notes res',res)
      this.note_list = res.payload.notes
      this.document_path = Constant.adminUrl + res.payload.document_path
      this.uihelper.HideSpinner();   

     },
     (err:any)=>{
      this.uihelper.HideSpinner();   
      this.uihelper.ShowAlert('',"Not Found");
      this.note_list = []
       console.log('error',err)
     })

  }

  async openPopover(notes_id, ev:any)
  {
    // console.log('notes_id',notes_id)
    const popover = await this.popovercontroller.create({
      component: NotesPopoverPage,
      // cssClass: 'my-custom-class',
      
      componentProps: {
        "notes_id": notes_id,
      },
    });
    return await popover.present(); 
  }

  goBack() {
    this.router.navigate([ '/tabs']);
  }

  GoToCart(){
    this.router.navigate([ this.selfPath + "/cart"]);
  }

  GoToUserProfile() {
    this.router.navigate([this.selfPath + "/user-profile"]);
  }

  async openNotes(document)
  {
    const url = this.document_path +'/'+ document;

    // this.note_url = this.document_path +'/'+ document
    // console.log('note_url',this.note_url);
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
    .then(status => {
      if (status.hasPermission) {
        this.fileTransfer.download(url, this.file.externalRootDirectory  + '/Download/' + 'my_notes.pdf').then((entry) => {
          console.log('download complete 1: ' + entry.toURL());
          this.uihelper.presentToast('Download complete..please check your download folder')
        }, (error) => {
          // handle error
          console.log(error);
          
        });
      } 
      else {
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
          .then(status => {
            if(status.hasPermission) {
              this.fileTransfer.download(url, this.file.externalRootDirectory  + '/Download/' + 'my_notes.pdf').then((entry) => {
                console.log('download complete 2: ' + entry.toURL());
                this.uihelper.presentToast('Download complete..please check your download folder')

              }, (error) => {
                // handle error
                console.log(error);
                
              });
            }
          });
      }
    });
   

  }


}
