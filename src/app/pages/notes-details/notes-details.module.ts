import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotesDetailsPageRoutingModule } from './notes-details-routing.module';

import { NotesDetailsPage } from './notes-details.page';
import { NotesPopoverPage } from '../notes-popover/notes-popover.page';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotesDetailsPageRoutingModule,
    
    
  ],
  providers:[InAppBrowser,FileTransfer,File,AndroidPermissions],
  declarations: [NotesDetailsPage,NotesPopoverPage]
})
export class NotesDetailsPageModule {}
