import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';
import { environment } from 'src/environments/environment';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Constant } from 'src/Helper/Constant';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {

  userData: any;
  avatar: any;
  selfPath: string;
  constructor(public navCtrl: NavController,private router:Router,private authService: AuthService, private storageService: StorageService) { }

  ngOnInit() {
    try {
      this.router.events.subscribe((event: RouterEvent) => {
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          this.selfPath = event.url + '/user-profile';
        }
        this.selfPath = this.router.routerState.snapshot.url;

        this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'user-profile') + 'user-profile';
      });

    } catch (e) {
      console.log(e);
    }
    
    this.storageService.get(Constant.AUTH).then(res => {
      this.userData= res.user_details
      this.avatar = res.user_details.avatar_location ? Constant.adminUrl+res.user_details.avatar_location : 'assets/user-profile.png';
      // console.log('user data',this.userData);

    })
  }

  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }

  EditUserPage()
  {
    this.router.navigate([this.selfPath + '/edit-user-profile'])
  }

  goBack()
  {
    this.navCtrl.pop();
  }

}
