import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GroupPackagesPageRoutingModule } from './group-packages-routing.module';

import { GroupPackagesPage } from './group-packages.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GroupPackagesPageRoutingModule
  ],
  declarations: [GroupPackagesPage]
})
export class GroupPackagesPageModule {}
