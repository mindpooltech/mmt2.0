import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { StorageService } from 'src/app/services/storage.service';
import { LoadingController, NavController, PopoverController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { UIHelper } from 'src/Helper/UIHelper';
import { Constant } from 'src/Helper/Constant';
import { SearchPackagesPopoverPage } from '../search-packages-popover/search-packages-popover.page';
// import { GroupPackagesPopoverPage } from '../group-packages-popover/group-packages-popover.page';

@Component({
  selector: 'app-group-packages',
  templateUrl: './group-packages.page.html',
  styleUrls: ['./group-packages.page.scss'],
})
export class GroupPackagesPage implements OnInit {
  selfPath: string;
  cartCount: string;
  userData: any;
  course_name: any;
  institute_id: any;
  course_id: any;
  user_id: any;
  packages: any;
  group_unique_code: any;
  disableButton: boolean = false;
  pkgidObj: any[] = [];
  group_packages: any;
  pkgid: any;
  cartFlag: any;
  discounted_price: any;
  pkgIds: string;
  
  constructor(
    private router: Router,
    private http: HttpClient,
    private httpService: HttpService,
    private storageService:StorageService,
    private popovercontroller: PopoverController,
    private navCtrl:NavController,
    private uihelper: UIHelper,
  ) { }

  ngOnInit() {
    try {
      this.router.events.subscribe((event: RouterEvent) => {
        this.selfPath = '';
        if (event && event instanceof NavigationEnd && event.url) {
          this.selfPath = event.url + '/packages';
        }
        this.selfPath = this.router.routerState.snapshot.url;
        this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'packages') + 'packages';
      });

    } catch (e) {
      console.log(e);
    }
  }

  getStringBeforeSubstring(parentString, substring) {
    return parentString.substring(0, parentString.indexOf(substring))
  }

  ionViewWillEnter()
{
      this.cartCount = localStorage.getItem('cartCount')
      // const loading = await this.miscHelper.presentLoading();
      this.uihelper.ShowAlert('','Please select group.')
      this.storageService.get(Constant.AUTH).then(res => {
        this.userData= res.user_details;
        this.course_name = this.userData.course_name
        this.institute_id = res.user_details.institute_id;
        this.course_id = res.user_details.course_id;
        this.user_id = res.user_details.id;
  
        this.get_all_groups()
    });
}

get_all_groups(){
      
  let group_data = {
    course_id : this.course_id,
    institute_id : this.institute_id
  }
  this.httpService.afterLoginPost('group-pkgs-names-list',group_data).subscribe((res:any)=>{
    this.packages = res.payload.packages
    this.uihelper.HideSpinner();
 });
}

async getGroupPackages(event)
  {
    this.disableButton = false;
    this.uihelper.ShowSpinner();
    this.group_unique_code = event.target.value
    let data = {
      group_unique_code : event.target.value,
      user_id :this.user_id
    }
    this.httpService.afterLoginPost('group-pkgs-list',data).subscribe((res:any)=>{
      console.log('res',res)
      this.pkgidObj.length = 0;
      this.group_packages = res.payload.packages.filter((value)=>{
        let package_test_length = value.package.package_tests.length
        if(package_test_length)
        {
          this.pkgid = value.package_id
          this.pkgidObj.push(this.pkgid)
        }        
        return package_test_length
      }) 
      console.log('package_test_length',this.group_packages)
        if(this.group_packages != '')
        {
          this.cartFlag = this.group_packages[0].package.cart_item
          console.log('cartFlag',this.cartFlag)
          this.discounted_price = this.group_packages[0].package_group_price
          this.uihelper.HideSpinner();

        }
        else{
          this.uihelper.HideSpinner();
          this.uihelper.ShowAlert('',"Packages not found");
        }
    
      this.uihelper.HideSpinner();
   },
   (err)=>{
    this.uihelper.HideSpinner();
      this.uihelper.ShowAlert('',"Packages not found");
   });
  }

  async addToCart()
  {
    this.pkgIds = this.pkgidObj.join(',')
    let groupData = {
      price_type : this.discounted_price == '0' ? 0 : this.discounted_price,
      group_code : this.group_unique_code,
      user_id : this.user_id,
      pkgIds : this.pkgIds,
      type: 'group'
    }
    this.uihelper.ShowSpinner();
    this.httpService.afterLoginPost('add-to-cart',groupData).subscribe((res:any)=>{
      // console.log('added to cart',res)
      localStorage.setItem("cartCount", res['payload'].cartCount);
      this.cartCount = localStorage.getItem('cartCount')
      this.uihelper.HideSpinner();
      this.uihelper.ShowAlert('',"Added to cart!!");
      this.disableButton = true;
      
      // this.router.navigate([this.selfPath]);         
    },
    (err)=>{
      this.uihelper.HideSpinner();
      this.uihelper.ShowAlert('',"Something went wrong!!");
    })
  }

  async openPopover(package_id, ev:any)
  {
    const popover = await this.popovercontroller.create({
      component: SearchPackagesPopoverPage,
      cssClass: 'my-custom-class',
      componentProps: {
        "package_id": package_id,
      },
      event: ev

    });
    return await popover.present(); 
  }

  goBack() {
    this.navCtrl.pop();
  }

  GoToCart(){
    this.router.navigate([ this.selfPath + "/cart"]);
  }

  GoToUserProfile() {
    this.router.navigate([this.selfPath + "/user-profile"]);
  }

}
