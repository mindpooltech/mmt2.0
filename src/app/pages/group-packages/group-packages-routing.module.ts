import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GroupPackagesPage } from './group-packages.page';

const routes: Routes = [
  {
    path: '',
    component: GroupPackagesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GroupPackagesPageRoutingModule {}
