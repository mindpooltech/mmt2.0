import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { HttpService } from 'src/app/services/http.service';
import { StorageService } from 'src/app/services/storage.service';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-coupon-popup',
  templateUrl: './coupon-popup.page.html',
  styleUrls: ['./coupon-popup.page.scss'],
})
export class CouponPopupPage implements OnInit {
  coupon_list: any;
  res: any;
  data: any;
  course_id = {
    course_id: localStorage.getItem('course_id')
  }
  user_id: any;
  couponCodeManual: any;
  userData: any;
  applied_coupon_id: any;
  coupons_added: any;
  coupons_added_in_cart: any;
  coupons_added_in_cart_admin: any;
  group_coupon_list: any;
  single_coupon_list: any;
  coupons_added_in_cart_institute: any;
  coupons_added_in_cart_student: any;
  constructor(
    public modalController: ModalController,
    private navParams: NavParams,
    private httpService: HttpService,
    private storageService:StorageService,
    private uihelper: UIHelper,
  ) {
    this.storageService.get(Constant.AUTH).then(res => {
      this.userData= res.user_details;
      this.user_id = res.user_details.id;
    });
   }

  ngOnInit() {
    this.data = this.navParams.get('data')
    this.uihelper.ShowSpinner();
    this.httpService.afterLoginPost('fetchadmindiscounts', this.data).subscribe((res: any) => {
      // console.log('fetchadmindiscounts',res)
      this.coupons_added_in_cart_admin = res.payload.coupons_added_in_cart.filter((element)=>{
        return element.discount_of == 'admin'
      })  
      this.coupons_added_in_cart_institute = res.payload.coupons_added_in_cart.filter((element)=>{
        return element.discount_of == 'institute'
      })
      // console.log('coupons_added_in_cart_institute',this.coupons_added_in_cart_institute)
      this.coupons_added_in_cart_student = res.payload.coupons_added_in_cart.filter((element)=>{
        return element.discount_of == 'student'
      }) 
      // console.log('coupons_added_in_cart_student',this.coupons_added_in_cart_student)

      this.coupons_added = res.payload.coupons_added_in_cart.map((element,index)=>{
        return element.id;
      })
      // console.log('coupons_added',this.coupons_added)

      this.single_coupon_list = res.payload.discounts_for_single
      this.group_coupon_list = res.payload.discounts_for_group

      this.coupon_list = this.single_coupon_list.concat(this.group_coupon_list)
      console.log('final coupon list', this.coupon_list)
      this.uihelper.HideSpinner();

    });
  }

  dismiss() {
    // console.log('dismiss')
    this.modalController.dismiss();
    return false;
    
  }

  applyCoupon(coupon_code)
  {
    if(coupon_code == undefined)
    {
      this.uihelper.HideSpinner();
      this.uihelper.ShowAlert('Coupon Code',"Please Enter Coupon Code");
      
    }
    else{
      
    this.uihelper.ShowSpinner();
    let data = new FormData();
    data.append('course_id', this.course_id.course_id);
    data.append('user_id', this.user_id);
    data.append('coupon_code', coupon_code);
    data.append('remove_credit', '0');

    this.httpService.afterLoginPost('applycoupon', data).subscribe((res: any) => {
      if(res.payload.discounted_of == 'admin_coupon')
      {
        this.uihelper.ShowAlert('Cart',"Coupon Code successfully applied..you got the discount of Rs." + res.payload.admin_code_discounted_amt);
      }
      else if(res.payload.discounted_of == 'institute_coupon')
      {
        this.uihelper.ShowAlert('Cart',"Coupon Code successfully applied..you got the discount of Rs." + res.payload.admin_code_discounted_amt);
      }
      else {
        this.uihelper.ShowAlert('Cart',"Coupon Code successfully applied..you got the discount of Rs." + res.payload.student_code_discounted_amt);
      }
      this.modalController.dismiss();
      this.uihelper.HideSpinner();
      
    },
    (err:any)=>{
      this.uihelper.HideSpinner();
      console.log(err)
      this.uihelper.ShowAlert('Apply Coupon',err.error.payload.message);

    });
  }
  }

  deleteCouponCode(coupon_type)
  {
    this.uihelper.ShowSpinner();
    // let coupon_type = this.coupon_detail.admin_coupon_detail_data.coupon_code_type
    console.log(' delete coupon',coupon_type)
    let data = new FormData();
    data.append('coupontype', coupon_type);
    data.append('student_id', this.user_id);
    this.httpService.afterLoginPost('removecoupon',data).subscribe((res:any)=>{   
      this.modalController.dismiss();
      this.uihelper.HideSpinner();
      this.uihelper.ShowAlert('Cart',"Coupon Code Removed");

    },
    (err:any)=>{
      this.uihelper.ShowAlert('Apply Coupon',"Something went wrong!! Try Again..");
    });
  }

}
