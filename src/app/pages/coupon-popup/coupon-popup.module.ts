import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CouponPopupPageRoutingModule } from './coupon-popup-routing.module';

import { CouponPopupPage } from './coupon-popup.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CouponPopupPageRoutingModule
  ],
  declarations: [CouponPopupPage]
})
export class CouponPopupPageModule {}
