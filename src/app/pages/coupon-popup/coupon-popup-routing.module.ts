import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CouponPopupPage } from './coupon-popup.page';

const routes: Routes = [
  {
    path: '',
    component: CouponPopupPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CouponPopupPageRoutingModule {}
