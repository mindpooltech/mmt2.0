import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ScorecardpopoverPageRoutingModule } from './scorecardpopover-routing.module';

import { ScorecardpopoverPage } from './scorecardpopover.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ScorecardpopoverPageRoutingModule
  ],
  declarations: [ScorecardpopoverPage]
})
export class ScorecardpopoverPageModule {}
