import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ScorecardpopoverPage } from './scorecardpopover.page';

const routes: Routes = [
  {
    path: '',
    component: ScorecardpopoverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScorecardpopoverPageRoutingModule {}
