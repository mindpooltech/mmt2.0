import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LoadingController, NavParams, PopoverController } from '@ionic/angular';
import { HttpService } from 'src/app/services/http.service';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-scorecardpopover',
  templateUrl: './scorecardpopover.page.html',
  styleUrls: ['./scorecardpopover.page.scss'],
})
export class ScorecardpopoverPage implements OnInit {

  headers = new HttpHeaders({
    'Accept':  'application/json',
    'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
  });
  url = Constant.apiUrl

  options = { headers: this.headers}
  chapter_list: any;
  question_id: any;

  constructor(private navParams: NavParams, private popovercontroller: PopoverController,private http: HttpClient,private httpService: HttpService, private loadingController: LoadingController, private uihelper: UIHelper,) { }

  async ngOnInit() {
    this.uihelper.ShowSpinner();
    this.question_id = this.navParams.data.question_id
    this.http.get(this.url + 'question-chapters/' + this.question_id, this.options).subscribe((res)=>{     
      this.uihelper.HideSpinner();  
      console.log('popover chapters', res)
      this.chapter_list = res['payload'].chapters
      console.log('chapter list',this.chapter_list)

  },
  (err)=>{
    this.uihelper.HideSpinner(); 
    this.chapter_list = []  
    // this.miscHelper.showAlert("not found!!");

  });
  }

}
