import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ScorecardpopoverPage } from './scorecardpopover.page';

describe('ScorecardpopoverPage', () => {
  let component: ScorecardpopoverPage;
  let fixture: ComponentFixture<ScorecardpopoverPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScorecardpopoverPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ScorecardpopoverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
