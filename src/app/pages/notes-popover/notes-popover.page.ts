import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { LoadingController, NavParams, PopoverController } from '@ionic/angular';
import { HttpService } from 'src/app/services/http.service';
import { environment } from 'src/environments/environment';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-notes-popover',
  templateUrl: './notes-popover.page.html',
  styleUrls: ['./notes-popover.page.scss'],
})
export class NotesPopoverPage implements OnInit {
  headers = new HttpHeaders({
    'Accept':  'application/json',
    'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
  });
  url = Constant.apiUrl

  options = { headers: this.headers}
  chapter_list: any;
  notes_id: any;
  constructor(private navParams: NavParams, private popovercontroller: PopoverController,private http: HttpClient,private httpService: HttpService, private uihelper: UIHelper) { }

  async ngOnInit() {
    this.uihelper.ShowSpinner();
    this.notes_id = this.navParams.data.notes_id
    this.http.get(this.url + 'notes-chapters/' + this.notes_id, this.options).subscribe((res)=>{  
        this.chapter_list = res['payload'].chapters
        this.uihelper.HideSpinner(); 

    });
  }

}
