import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotesPopoverPageRoutingModule } from './notes-popover-routing.module';

import { NotesPopoverPage } from './notes-popover.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotesPopoverPageRoutingModule
  ],
  declarations: [NotesPopoverPage]
})
export class NotesPopoverPageModule {}
