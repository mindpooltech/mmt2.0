import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotesPopoverPage } from './notes-popover.page';

const routes: Routes = [
  {
    path: '',
    component: NotesPopoverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotesPopoverPageRoutingModule {}
