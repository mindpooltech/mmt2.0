import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController,ModalController } from '@ionic/angular';
import { HttpService } from 'src/app/services/http.service';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage implements OnInit {
  test_id: any;
  headers = new HttpHeaders({
    'Accept':  'application/json',
    'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
  });
  url = Constant.apiUrl

  options = { headers: this.headers}
  chapter_list: any;
  constructor(private navParams: NavParams, public modalController: ModalController,private http: HttpClient,private httpService: HttpService, private uihelper: UIHelper) { }

  async ngOnInit() {
    // this.uihelper.ShowSpinner();
    // this.test_id = this.navParams.data.test_id
    // this.http.get(this.url + 'test-chapters/' + this.test_id, this.options).subscribe((res)=>{     
    //     // console.log('popover chapters', res)
    //     this.chapter_list = res['payload']
    //     this.uihelper.HideSpinner();

    // });
    this.chapter_list = this.navParams.data.chapter_list
    console.log('chapter_list',this.chapter_list)
  }

  dismiss() {
    this.modalController.dismiss();
    return false;
    
  }

}
