import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VideoPopoverPageRoutingModule } from './video-popover-routing.module';

import { VideoPopoverPage } from './video-popover.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VideoPopoverPageRoutingModule
  ],
  declarations: [VideoPopoverPage]
})
export class VideoPopoverPageModule {}
