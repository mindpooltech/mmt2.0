import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VideoPopoverPage } from './video-popover.page';

const routes: Routes = [
  {
    path: '',
    component: VideoPopoverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VideoPopoverPageRoutingModule {}
