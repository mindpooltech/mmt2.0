import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { LoadingController, NavParams, PopoverController } from '@ionic/angular';
import { HttpService } from 'src/app/services/http.service';
import { environment } from 'src/environments/environment';
import { Constant } from 'src/Helper/Constant';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-video-popover',
  templateUrl: './video-popover.page.html',
  styleUrls: ['./video-popover.page.scss'],
})
export class VideoPopoverPage implements OnInit {

  headers = new HttpHeaders({
    'Accept':  'application/json',
    'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
  });
  url = Constant.apiUrl

  options = { headers: this.headers}
  chapter_list: any;
  video_id: any;
  constructor(private navParams: NavParams, private popovercontroller: PopoverController,private http: HttpClient,private httpService: HttpService, private uihelper: UIHelper) { }

  async ngOnInit() {
    this.uihelper.ShowSpinner();
    this.video_id = this.navParams.data.video_id
    // console.log('this.video_id',this.video_id)
    this.http.get(this.url + 'video-chapters/' + this.video_id, this.options).subscribe((res)=>{  
        this.chapter_list = res['payload'].chapters
        // console.log(this.chapter_list)
        this.uihelper.HideSpinner();   

    });
  }

}
