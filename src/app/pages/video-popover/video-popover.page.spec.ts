import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VideoPopoverPage } from './video-popover.page';

describe('VideoPopoverPage', () => {
  let component: VideoPopoverPage;
  let fixture: ComponentFixture<VideoPopoverPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoPopoverPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VideoPopoverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
