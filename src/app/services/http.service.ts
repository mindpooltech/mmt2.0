import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constant } from 'src/Helper/Constant';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http:HttpClient) { }

  post(serviceName: string, data:any){
    const headers = new HttpHeaders();
    const options = { headers: headers, withCredentails: false }
    const url = Constant.apiUrl + serviceName;
    return this.http.post(url, data, options);

  }

  afterLoginPost(serviceName: string, data:any){
    const headers = new HttpHeaders({
      'Accept':  'application/json',
      'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
    });
    // console.log('http service main', serviceName, data)
    const options = { headers: headers}
    const url = Constant.apiUrl + serviceName;
    return this.http.post(url, data, options);

  }

  afterLoginGet(serviceName: string, data:any){
    const headers = new HttpHeaders({
      'Accept':  'application/json',
      'Authorization':'Bearer '+ localStorage.getItem('userToken') ,     
    });
    // console.log('http service main', serviceName, data)
    const options = { headers: headers}
    const url = Constant.apiUrl + serviceName + '/' + data;
    return this.http.get(url,  options);

  }

  // get(serviceName: string, data:any){
  //   console.log(serviceName,data)
  //   const headers = new HttpHeaders({
  //     'Accept': 'application/json',
  //     'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
  //   });
  //   const options = { headers: headers, withCredentails: false }
  //   // const url = Constant.apiUrl + serviceName;
  //   const url = Constant.apiUrl + serviceName + '/' + data;
  //   return this.http.get(url, options);
  // }

}
