import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor() { }

  
  private subject = new Subject<any>();

  sendMessage(text) {
    this.subject.next(text);
  }

  setCreditBalance(data: any) {
    this.subject.next(data);
}

  getObservable(): Observable<any> {
    return this.subject.asObservable();
  }
}
