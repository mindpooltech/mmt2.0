import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

// import { Plugins } from '@capacitor/core';
// const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  paramData: any;
  isLoadingBool: any;

  constructor(private storage: Storage) { }

  //store the value
  async store(storageKey: string, value: any) {
    
    const encryptedValue = JSON.stringify(value);
    const result = await this.storage.set(storageKey,encryptedValue);
    // console.log('sotrage result',result)
  }

  // get value
  async get(storageKey: string){
    const ret = await this.storage.get(storageKey);
    // console.log('get data',ret)
    return JSON.parse((ret));
  }

  async removeStorageItem(storageKey: string) {
    await this.storage.remove(storageKey);
    }
    
  //   // Clear storage
  //   async clear() {
  //   await Storage.clear();
  //   }

    setParamData(data){
      this.paramData = data
    }
  
    getParamData(){
      return this.paramData
    }

    setVariable(data)
    {
      this.isLoadingBool = !!data
    }

    getVariable()
    {
      return !!this.isLoadingBool
    }
}
