import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { Constant } from 'src/Helper/Constant';
import { HttpService } from './http.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpService: HttpService,
    private storageService:StorageService,
    private router:Router,
    private http: HttpClient,
    private navCtrl: NavController,
    ) { }

  login(postData: any): Observable<any>{
    return this.httpService.post('login', postData);
  }

  logout(){
    this.storageService.removeStorageItem(Constant.AUTH).then(res=>{
      localStorage.removeItem("userToken");
      localStorage.removeItem("course_id");
      localStorage.removeItem("cartCount");
      // this.storageService.removeStorageItem(testDataConstants.TESTDATA);
      // this.router.navigate(['']);
      localStorage.setItem('IsLogin', 'No');
      localStorage.clear();
      this.navCtrl.navigateRoot('login');

    });

  }

  signup(postData: any): Observable<any>{
    return this.httpService.post('register', postData);
  }

  forgotPassword(postData: any): Observable<any>{
    const url = Constant.apiUrl;
    return this.http.post(url + 'forgotpassword' , postData);
  }

}
