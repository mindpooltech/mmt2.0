import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/get-started/get-started.module').then( m => m.GetStartedPageModule)
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  // {
    // path: 'get-started',
    // loadChildren: () => import('./pages/get-started/get-started.module').then( m => m.GetStartedPageModule)
  // },
  {
    path: 'signup',
    loadChildren: () => import('./pages/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'forget-password',
    loadChildren: () => import('./pages/forget-password/forget-password.module').then( m => m.ForgetPasswordPageModule)
  },
  {
    path: 'user-profile',
    loadChildren: () => import('./pages/user-profile/user-profile.module').then( m => m.UserProfilePageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./pages/tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'packages',
    loadChildren: () => import('./pages/packages/packages.module').then( m => m.PackagesPageModule)
  },
  {
    path: 'test-board',
    loadChildren: () => import('./pages/test-board/test-board.module').then( m => m.TestBoardPageModule)
  },
  {
    path: 'group-packages',
    loadChildren: () => import('./pages/group-packages/group-packages.module').then( m => m.GroupPackagesPageModule)
  },
  {
    path: 'my-packages',
    loadChildren: () => import('./pages/my-packages/my-packages.module').then( m => m.MyPackagesPageModule)
  },
  {
    path: 'new-test',
    loadChildren: () => import('./pages/new-test/new-test.module').then( m => m.NewTestPageModule)
  },
  {
    path: 'start-test',
    loadChildren: () => import('./pages/start-test/start-test.module').then( m => m.StartTestPageModule)
  },
  {
    path: 'end-test',
    loadChildren: () => import('./pages/end-test/end-test.module').then( m => m.EndTestPageModule)
  },
  {
    path: 'test-history',
    loadChildren: () => import('./pages/test-history/test-history.module').then( m => m.TestHistoryPageModule)
  },
  {
    path: 'test-evaluation',
    loadChildren: () => import('./pages/test-evaluation/test-evaluation.module').then( m => m.TestEvaluationPageModule)
  },
  {
    path: 'cart',
    loadChildren: () => import('./pages/cart/cart.module').then( m => m.CartPageModule)
  },
  {
    path: 'edit-user-profile',
    loadChildren: () => import('./pages/edit-user-profile/edit-user-profile.module').then( m => m.EditUserProfilePageModule)
  },
  {
    path: 'my-ratings',
    loadChildren: () => import('./pages/my-ratings/my-ratings.module').then( m => m.MyRatingsPageModule)
  },
  {
    path: 'my-queries',
    loadChildren: () => import('./pages/my-queries/my-queries.module').then( m => m.MyQueriesPageModule)
  },
  {
    path: 'my-videos',
    loadChildren: () => import('./pages/my-videos/my-videos.module').then( m => m.MyVideosPageModule)
  },
  {
    path: 'notes-details',
    loadChildren: () => import('./pages/notes-details/notes-details.module').then( m => m.NotesDetailsPageModule)
  },
  {
    path: 'coupon-popup',
    loadChildren: () => import('./pages/coupon-popup/coupon-popup.module').then( m => m.CouponPopupPageModule)
  },
  {
    path: 'refer-and-earn',
    loadChildren: () => import('./pages/refer-and-earn/refer-and-earn.module').then( m => m.ReferAndEarnPageModule)
  },
  
  {
    path: 'featured-packages',
    loadChildren: () => import('./pages/featured-packages/featured-packages.module').then( m => m.FeaturedPackagesPageModule)
  },  {
    path: 'pdf-modal',
    loadChildren: () => import('./pages/pdf-modal/pdf-modal.module').then( m => m.PdfModalPageModule)
  },
  {
    path: 'note-pdf-modal',
    loadChildren: () => import('./pages/note-pdf-modal/note-pdf-modal.module').then( m => m.NotePdfModalPageModule)
  },




];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
