import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { UIHelper } from 'src/Helper/UIHelper';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { IonicStorageModule } from '@ionic/storage';
import { ModalPage } from './pages/modal/modal.page';
import { DatePipe } from '@angular/common';
import { SafePipe } from './safe.pipe';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File, IWriteOptions } from '@ionic-native/file/ngx';
import { DocumentViewer,DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { EditUserProfilePage } from './pages/edit-user-profile/edit-user-profile.page';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { MbscModule } from '@mobiscroll/angular-lite';
import { LaunchReview } from '@ionic-native/launch-review/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ScorecardpopoverPage } from './pages/scorecardpopover/scorecardpopover.page';

@NgModule({
  declarations: [AppComponent, ModalPage,ScorecardpopoverPage],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(),IonicStorageModule.forRoot(), AppRoutingModule, HttpClientModule, FormsModule,ReactiveFormsModule,MbscModule],
  providers: [
    HttpClientModule,
    UIHelper,
    DatePipe,
    FileOpener,
    DocumentViewer,
    File,
    SocialSharing,
    LaunchReview,
    InAppBrowser,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
