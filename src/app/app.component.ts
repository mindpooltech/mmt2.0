import { Component } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import { AuthService } from './services/auth.service';
import { StorageService } from './services/storage.service';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { EventService } from './services/event.service';
import { Constant } from 'src/Helper/Constant';
import { HttpService } from 'src/app/services/http.service';
import { UIHelper } from 'src/Helper/UIHelper';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  userData: any;
  selfPath: string;
  userName: string;
  isLogin: string;
  user_name: any;
  dashboard_data: any;
  student_avail_amt: any;
  student_credit_balance: any;


  constructor(
    private platform: Platform,
    private menu: MenuController,
    private authService: AuthService,
    public navCtrl: NavController,
    private storageService: StorageService,
    private router: Router,
    private events: EventService,
    private http: HttpService,
    private uihelper: UIHelper,
  ) {
    this.initializeApp();

  }

  initializeApp() {
    this.platform.ready().then(() => {

      // this.splashScreen.hide();
      let IsLogin = localStorage.getItem('IsLogin');
      if (IsLogin == 'Yes') {

        // this.rootPage = 'OwnerDashboardPage';
        this.navCtrl.navigateRoot(['tabs']);

      } else {
        this.navCtrl.navigateRoot(['login']);
      }

    });

  }

  ngOnInit()
  {
    // (<any>window).plugins.preventscreenshot.disable((a) => this.successCallback(a), (b) => this.errorCallback(b));
    this.events.getObservable().subscribe((data) => {
     
      this.student_credit_balance = data.student_avail_amt
    })


    try {
      this.router.events.subscribe((event: RouterEvent) => {
        this.selfPath = '';
        // if (event && event instanceof NavigationEnd && event.url) {
        //   console.log('inside event', event.url)
        //   this.selfPath = event.url + '/dashboard';
        // }
        this.selfPath = this.router.routerState.snapshot.url;
        // console.log(this.getStringBeforeSubstring(this.selfPath, 'viptips-details'));

      });

    } catch (e) {
      console.log(e);
    }

    this.storageService.get(Constant.AUTH).then(res => {
      this.userData = res.user_details;
      this.userName = res.user_details.full_name
    })
  }

  ionViewWillEnter()
  {
    
  }



  // successCallback(result) {
  //   console.log(result); // true - enabled, false - disabled
  // }
 
  // errorCallback(error) {
  //   console.log(error);
  // }

  logoutAction() {
    this.authService.logout();
  }
  GoToSearchPackages() {
    console.log('Test', this.selfPath);
    this.router.navigate([this.selfPath + "/packages"]);
  }
  GoToDashboard()
  {
    this.router.navigate([this.selfPath]);
  }
  GoToGroupPackages()
  {
    this.router.navigate([this.selfPath + "/group-packages"]);
  }
  GoToMyPackages()
  {
    this.router.navigate([this.selfPath + "/my-packages"]);
  }
  GoToFeaturedPackages()
  {
    this.router.navigate([this.selfPath + "/featured-packages"]);
  }
  TestBoard() {
    this.router.navigate([this.selfPath + "/test-board"]);
  }
  TestHistory()
  {
    this.router.navigate([this.selfPath + "/test-history"]);
  }
  MyRatings() {
    this.router.navigate([this.selfPath + "/my-ratings"]);
  }
  MyQueries() {
    this.router.navigate([this.selfPath + "/my-queries"]);
  }
  MyNotes()
  {
    this.router.navigate([this.selfPath + "/notes-details"]);
  }
  Myvideos()
  {
    this.router.navigate([this.selfPath + "/my-videos"]);
  }
  GoToReferral()
  {
    this.router.navigate([this.selfPath + "/refer-and-earn"]);
  }
}


