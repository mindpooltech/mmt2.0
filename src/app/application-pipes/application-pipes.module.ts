import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RemovehtmltagsPipe } from '../removehtmltags.pipe';



@NgModule({
  declarations: [RemovehtmltagsPipe],
  imports: [
    CommonModule
  ],
  exports: [
    RemovehtmltagsPipe
  ]
})
export class ApplicationPipesModule { }
