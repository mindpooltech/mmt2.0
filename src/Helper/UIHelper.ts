import { AlertController, LoadingController, NavController, IonApp, ToastController } from '@ionic/angular'
import { Injectable } from '@angular/core'
import { ACTIVITY, TOAST, NOTIFICATION } from '../Helper/Enums'
import { async } from '@angular/core/testing';
import { Router, RouterLink } from '@angular/router';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Platform } from '@ionic/angular';

@Injectable()
export class UIHelper {

    loading: any;
    loader: any;
    toastEnum = TOAST;

    constructor(public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        private alertController: AlertController,
        private toastCtrl: ToastController,
        private router: Router,
        private iab: InAppBrowser,
        public platform: Platform,
    ) { }

    Logout() {

    }
    async ShowAlert(title, subtitle) {
        let alert = this.alertController.create({
            header: title,
            subHeader: subtitle,
            buttons: ['Ok']
        });
        (await alert).present();
    }

    async urlAlert(title, subtitle) {
        let alert = this.alertController.create({
            header: title,
            subHeader: subtitle,
            buttons: [
                {
                    text: 'Ok',
                    handler: () => {
                        console.log('ok clicked');
                        if (this.platform.is('android')) {
                            console.log('play store link');
                            
                            const browser = this.iab.create("https://play.google.com/store/apps/details?id=com.MakeMyTest&hl=en_IN&gl=US", '_system', { location: 'yes' });
                
                        } else {
                            // iOS
                            console.log('applie store link');                    
                           const browser = this.iab.create("itms-apps://itunes.apple.com/app/viphomelink/id1522942035?mt=8", '_system', { location: 'yes' });
                        }
                    }
                  }
            ]
        });
        (await alert).present();
    }

    async presentAlert() {
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          subHeader: 'Sign Up successful',
          message: 'Please confirm your mail before login.',
          buttons: [
          {
            text: 'Ok',
            handler: () => {
            this.router.navigate(['/login']);
            } 
          }
          ]
        });
     
        await alert.present();
      }

      async ShowSpinner() {

        this.loadingCtrl.create({
            message: `<img src="assets/icon/loader4.gif" class="loader-old" />`,
            // message: `<div class="loader-new" /></div>`,
            spinner: null,
            translucent: true,
            cssClass: 'loading-wrapper',
            backdropDismiss: false
        }).then((res) => {
            res.present();
        });
    }
    
    async ShowSpinner_old() {

        this.loadingCtrl.create({
            message: `<img src="assets/icon/loader2.gif" class="loader-old" />`,
            spinner: null,
            translucent: true,
            cssClass: 'text-center',
            backdropDismiss: false
        }).then((res) => {
            res.present();
        });
    }

    async HideSpinner() {
        this.loadingCtrl.dismiss().then((res) => {
            console.log('Loading dismissed!', res);
        }).catch((error) => {
            console.log('error', error);
        });
    }

    async presentLoadingCustom() {
        const loading = await this.loadingCtrl.create({
            duration: 55000,
            message: '<img src="../assets/imgs/loader.svg" class="img-align"  /> ',
            spinner: null,
            // translucent: true,
            cssClass: 'custom-loader-class',
            backdropDismiss: true
        });
        await loading.present();

        const { role, data } = await loading.onDidDismiss();
        console.log('Loading dismissed with role:', role);
    }


    // Show the loader for infinite time
    ShowSpinner1() {

        this.loadingCtrl.create({
            message: '<p>Please wait...</p>',
            cssClass: 'loader-para'
        }).then((res) => {
            res.present();
        });

    }

    // Hide the loader if already created otherwise return error
    HideSpinner1() {

        this.loadingCtrl.dismiss().then((res) => {
            console.log('Loading dismissed!', res);
        }).catch((error) => {
            console.log('error', error);
        });

    }

    async playStoreAlert() {
        const alert = await this.alertController.create({
          message: 'Do you want to buy this book?',
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Ok',
              handler: () => {
                console.log('ok clicked');
                if (this.platform.is('android')) {
                    console.log('play store link');
                    
                    const browser = this.iab.create("https://play.google.com/store/apps/details?id=com.MakeMyTest&hl=en_IN&gl=US", '_system', { location: 'yes' });
        
                } else {
                    // iOS
                    console.log('applie store link');                    
                   const browser = this.iab.create("itms-apps://itunes.apple.com/app/viphomelink/id1522942035?mt=8", '_system', { location: 'yes' });
                }
              }
            }
          ]
        });
        alert.present();
      }


    async showToast(message: string, type: TOAST) {
        let cssClassName = 'successToast';

        if (type == this.toastEnum.ERROR) {
            cssClassName = 'errorToast';
        } else if (type == this.toastEnum.SUCCESS) {
            cssClassName = 'successToast';
        } else if (type == this.toastEnum.WARNING) {
            cssClassName = 'warningToast';
        } else if (type == this.toastEnum.INFO) {
            cssClassName = 'infoToast';
        }

        let toast = this.toastCtrl.create({
            message: message,
            duration: 4000,
            position: 'bottom',
            cssClass: cssClassName
        });

        (await toast).onDidDismiss();

        (await toast).present();
    }

    getStringBeforeSubstring(parentString, substring) {
        return parentString.substring(0, parentString.indexOf(substring))
    }

    async paymentAlert(msg:string) {
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          subHeader: 'MMT',
          message: msg,
          buttons: [
          {
            text: 'Ok',
            handler: () => {
            this.router.navigate(['/tabs']);
            } 
          }
          ]
        });
      
        await alert.present();
    }


    async presentToast(InfoMessage: string) {
        const toast = await this.toastCtrl.create({
          message: InfoMessage,
          duration: 2000
        });
        toast.present();
      }
}
