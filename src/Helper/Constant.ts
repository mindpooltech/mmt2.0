export class Constant {
    
    public static apiUrl:string = 'http://newdev.makemytest.in/api/';
    public static adminUrl:string = 'http://newdev.makemytest.in/';
    
    // public static apiUrl:string = 'http://127.0.0.1:8000/api/';
    // public static adminUrl:string = 'http://127.0.0.1:8000/';
    public static readonly AUTH = 'userData'
}