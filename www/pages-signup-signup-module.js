(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-signup-signup-module"],{

/***/ "UUSl":
/*!***********************************************!*\
  !*** ./src/app/pages/signup/signup.module.ts ***!
  \***********************************************/
/*! exports provided: SignupPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _signup_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./signup-routing.module */ "eOyQ");
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signup.page */ "XHxw");







let SignupPageModule = class SignupPageModule {
};
SignupPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _signup_routing_module__WEBPACK_IMPORTED_MODULE_5__["SignupPageRoutingModule"]
        ],
        declarations: [_signup_page__WEBPACK_IMPORTED_MODULE_6__["SignupPage"]]
    })
], SignupPageModule);



/***/ }),

/***/ "XHxw":
/*!*********************************************!*\
  !*** ./src/app/pages/signup/signup.page.ts ***!
  \*********************************************/
/*! exports provided: SignupPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPage", function() { return SignupPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_signup_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./signup.page.html */ "vfPX");
/* harmony import */ var _signup_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./signup.page.scss */ "bYg9");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");










let SignupPage = class SignupPage {
    constructor(router, authService, storageService, loadingController, alertController, uihelper, navCtrl) {
        this.router = router;
        this.authService = authService;
        this.storageService = storageService;
        this.loadingController = loadingController;
        this.alertController = alertController;
        this.uihelper = uihelper;
        this.navCtrl = navCtrl;
        this.postData = {
            registration_group: '',
            registration_number: '',
            first_name: '',
            last_name: '',
            email: '',
            phone_number: '',
            password_reg: '',
            password_confirmation_reg: '',
            city: '',
            state: '',
            country: '',
            course_id: '',
            tnc: false,
        };
    }
    ngOnInit() {
    }
    validateInputs() {
        return (this.postData.registration_group && this.postData.registration_number && this.postData.first_name && this.postData.last_name && this.postData.email && this.postData.phone_number && this.postData.password_reg && this.postData.password_confirmation_reg && this.postData.city && this.postData.state && this.postData.country && this.postData.course_id && this.postData.tnc);
    }
    signupAction() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.validateInputs()) {
                this.uihelper.ShowSpinner();
                this.authService.signup(this.postData).subscribe((res) => {
                    console.log('success', res);
                    this.uihelper.HideSpinner();
                    this.uihelper.presentAlert();
                }, (err) => {
                    this.uihelper.HideSpinner();
                    console.log('failure', err.error);
                    // this.toastService.presentToast(res.error.payload.password_confirmation_reg.join('#'))
                    var err = Object.values(err.error.payload)[0][0];
                    this.uihelper.ShowAlert('', err);
                });
            }
            else {
                this.uihelper.ShowAlert('', 'Please Enter All Details Correctly.');
            }
        });
    }
    getHome() {
        this.navCtrl.pop();
    }
};
SignupPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_6__["StorageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_8__["UIHelper"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"] }
];
SignupPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-signup',
        template: _raw_loader_signup_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_signup_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SignupPage);



/***/ }),

/***/ "bYg9":
/*!***********************************************!*\
  !*** ./src/app/pages/signup/signup.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  display: none;\n  color: #4f689e;\n}\n\n.header-arrow {\n  padding: 10px;\n}\n\n.back-arrow {\n  background: #f6b7b5;\n  padding: 10px;\n  border-radius: 11px;\n  width: 45px;\n  text-align: center;\n  float: left;\n}\n\n.back-arrow ion-icon {\n  font-size: 22px;\n  color: #fff;\n}\n\n.sign-up-ttl {\n  font-size: 16px;\n  line-height: 21px;\n  color: #000000;\n  font-weight: 500;\n  font-family: \"Lato\", sans-serif;\n  margin-bottom: 25px;\n  margin-top: 25px;\n}\n\n.form-control {\n  border: 1px solid #b1bcd4;\n  border-radius: 10px;\n  margin-bottom: 22px;\n  font-size: 15px;\n  padding: 15px !important;\n  color: #585858;\n  --font-family: 500;\n}\n\nion-label {\n  margin: 0;\n}\n\n.space-3 {\n  margin: 0 30px;\n}\n\n.border-all {\n  border: 1px solid #b1bcd4;\n  border-radius: 10px;\n  margin-bottom: 22px;\n  color: #8a959e;\n}\n\nion-input {\n  --padding-top: 0px;\n  --padding-end: 0px;\n  --padding-bottom: 0px;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n}\n\n.reg-num {\n  font-size: 15px;\n  font-weight: 300;\n  color: #8a959e;\n  padding-left: 15px;\n}\n\n.space-m-50 {\n  margin: 10px 82px 10px;\n}\n\nion-button {\n  --background: #3f5c9b !important;\n  --border-radius: 10px;\n}\n\n.checkbox-txt .label {\n  font-size: 14px !important;\n}\n\nion-checkbox {\n  --border-radius: 3px;\n  --size: 16px;\n  --border-color: 1px solid rgb(63 92 155) !important;\n}\n\n.checkbox-txt ion-label {\n  font-size: 15px;\n}\n\n.term-text {\n  color: #3f5c9b;\n  text-decoration: underline;\n  font-weight: 500;\n}\n\n.account {\n  font-size: 15px;\n  color: #5b5b5b;\n  margin: 5px 0 20px;\n  font-weight: 500;\n}\n\n.sign-in-text {\n  color: #f27376;\n  font-weight: 500;\n}\n\n.submit-btn {\n  --background-activated: #3f5c9b !important;\n  --background-focused: #3f5c9b !important;\n  --background-hover: #3f5c9b !important;\n  --border-radius: 10px;\n  --min-height: 51px;\n  height: 51px;\n  text-align: center;\n  font-size: 18px;\n  font-weight: 400;\n}\n\n.top-img {\n  width: 100%;\n}\n\n.checkbox-txt {\n  margin: 0 auto;\n  display: inline-block;\n}\n\n.back img {\n  position: absolute;\n  z-index: 99;\n  top: 20px;\n  left: 20px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxzaWdudXAucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBYTtFQUNiLGNBQWM7QUFDbEI7O0FBQ0E7RUFDSSxhQUFhO0FBRWpCOztBQUFBO0VBQ0ksbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixXQUFXO0FBR2Y7O0FBREE7RUFDSSxlQUFlO0VBQ2YsV0FBVztBQUlmOztBQUZBO0VBQ0ksZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLCtCQUErQjtFQUMvQixtQkFBbUI7RUFDbkIsZ0JBQWdCO0FBS3BCOztBQUhBO0VBQ0kseUJBQXlCO0VBQ3pCLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLHdCQUF3QjtFQUN4QixjQUFjO0VBQ2Qsa0JBQWM7QUFNbEI7O0FBSkE7RUFDSSxTQUFTO0FBT2I7O0FBTEE7RUFDSSxjQUFjO0FBUWxCOztBQU5BO0VBQ0kseUJBQXlCO0VBQ3pCLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsY0FBYztBQVNsQjs7QUFQQTtFQUNJLGtCQUFjO0VBQ2Qsa0JBQWM7RUFDZCxxQkFBaUI7QUFVckI7O0FBUkE7RUFDSSxvQkFBZ0I7RUFDaEIsd0JBQW9CO0FBV3hCOztBQVRBO0VBQ0ksZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2Qsa0JBQWtCO0FBWXRCOztBQVZBO0VBQ0ksc0JBQXNCO0FBYTFCOztBQVhBO0VBQ0ksZ0NBQWE7RUFDYixxQkFBZ0I7QUFjcEI7O0FBWkE7RUFDSSwwQkFBMEI7QUFlOUI7O0FBYkE7RUFDSSxvQkFBZ0I7RUFDaEIsWUFBTztFQUNQLG1EQUFlO0FBZ0JuQjs7QUFkQTtFQUNJLGVBQWU7QUFpQm5COztBQWZBO0VBQ0ksY0FBYztFQUNkLDBCQUEwQjtFQUMxQixnQkFBZ0I7QUFrQnBCOztBQWhCQTtFQUNJLGVBQWU7RUFDZixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGdCQUFnQjtBQW1CcEI7O0FBakJBO0VBQ0ksY0FBYztFQUNkLGdCQUFnQjtBQW9CcEI7O0FBbEJBO0VBQ0ksMENBQXVCO0VBQ3ZCLHdDQUFxQjtFQUNyQixzQ0FBbUI7RUFDbkIscUJBQWdCO0VBQ2hCLGtCQUFhO0VBQ2IsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0FBcUJwQjs7QUFuQkE7RUFDSSxXQUFXO0FBc0JmOztBQXBCQTtFQUNJLGNBQWM7RUFDZCxxQkFBcUI7QUF1QnpCOztBQXJCQTtFQUNJLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsU0FBUztFQUNULFVBQVU7QUF3QmQiLCJmaWxlIjoic2lnbnVwLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXIge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICAgIGNvbG9yOiAjNGY2ODllO1xyXG59XHJcbi5oZWFkZXItYXJyb3cge1xyXG4gICAgcGFkZGluZzogMTBweDtcclxufVxyXG4uYmFjay1hcnJvdyB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZjZiN2I1O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDExcHg7XHJcbiAgICB3aWR0aDogNDVweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcbi5iYWNrLWFycm93IGlvbi1pY29uIHtcclxuICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG59XHJcbi5zaWduLXVwLXR0bCB7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICBsaW5lLWhlaWdodDogMjFweDtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGZvbnQtZmFtaWx5OiBcIkxhdG9cIiwgc2Fucy1zZXJpZjtcclxuICAgIG1hcmdpbi1ib3R0b206IDI1cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAyNXB4O1xyXG59XHJcbi5mb3JtLWNvbnRyb2wge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2IxYmNkNDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMnB4O1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgcGFkZGluZzogMTVweCAhaW1wb3J0YW50O1xyXG4gICAgY29sb3I6ICM1ODU4NTg7XHJcbiAgICAtLWZvbnQtZmFtaWx5OiA1MDA7XHJcbn1cclxuaW9uLWxhYmVsIHtcclxuICAgIG1hcmdpbjogMDtcclxufVxyXG4uc3BhY2UtMyB7XHJcbiAgICBtYXJnaW46IDAgMzBweDtcclxufVxyXG4uYm9yZGVyLWFsbCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjYjFiY2Q0O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDIycHg7XHJcbiAgICBjb2xvcjogIzhhOTU5ZTtcclxufVxyXG5pb24taW5wdXQge1xyXG4gICAgLS1wYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgLS1wYWRkaW5nLWVuZDogMHB4O1xyXG4gICAgLS1wYWRkaW5nLWJvdHRvbTogMHB4O1xyXG59XHJcbmlvbi1pdGVtIHtcclxuICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xyXG4gICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xyXG59XHJcbi5yZWctbnVtIHtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XHJcbiAgICBjb2xvcjogIzhhOTU5ZTtcclxuICAgIHBhZGRpbmctbGVmdDogMTVweDtcclxufVxyXG4uc3BhY2UtbS01MCB7ICAgXHJcbiAgICBtYXJnaW46IDEwcHggODJweCAxMHB4O1xyXG59XHJcbmlvbi1idXR0b24ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn1cclxuLmNoZWNrYm94LXR4dCAubGFiZWwge1xyXG4gICAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuaW9uLWNoZWNrYm94IHtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogM3B4O1xyXG4gICAgLS1zaXplOiAxNnB4O1xyXG4gICAgLS1ib3JkZXItY29sb3I6IDFweCBzb2xpZCByZ2IoNjMgOTIgMTU1KSAhaW1wb3J0YW50O1xyXG59XHJcbi5jaGVja2JveC10eHQgaW9uLWxhYmVsIHtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG4udGVybS10ZXh0IHtcclxuICAgIGNvbG9yOiAjM2Y1YzliO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG59XHJcbi5hY2NvdW50IHtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGNvbG9yOiAjNWI1YjViO1xyXG4gICAgbWFyZ2luOiA1cHggMCAyMHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG4uc2lnbi1pbi10ZXh0IHtcclxuICAgIGNvbG9yOiAjZjI3Mzc2O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG4uc3VibWl0LWJ0biB7XHJcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWhvdmVyOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAtLW1pbi1oZWlnaHQ6IDUxcHg7XHJcbiAgICBoZWlnaHQ6IDUxcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcbi50b3AtaW1nIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcbi5jaGVja2JveC10eHQge1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuLmJhY2sgaW1ne1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgei1pbmRleDogOTk7XHJcbiAgICB0b3A6IDIwcHg7XHJcbiAgICBsZWZ0OiAyMHB4O1xyXG59Il19 */");

/***/ }),

/***/ "eOyQ":
/*!*******************************************************!*\
  !*** ./src/app/pages/signup/signup-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: SignupPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageRoutingModule", function() { return SignupPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signup.page */ "XHxw");




const routes = [
    {
        path: '',
        component: _signup_page__WEBPACK_IMPORTED_MODULE_3__["SignupPage"]
    }
];
let SignupPageRoutingModule = class SignupPageRoutingModule {
};
SignupPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SignupPageRoutingModule);



/***/ }),

/***/ "vfPX":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/signup/signup.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>signup</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row>\r\n    <div class=\"back\" (click)=\"getHome()\">\r\n      <img src=\"assets/back.svg\" alt=\"back\">\r\n    </div>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <img src=\"assets/sign-up.svg\" class=\"top-img\">\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <p class=\"text-center sign-up-ttl\"> Don't have an account? Create your account here..</p>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <form #itemForm=\"ngForm\" (ngSubmit)=\"signupAction()\">\r\n  <ion-row class=\"space-3\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-list class=\"border-all\">\r\n        <ion-item lines=\"none\">\r\n          <ion-label class=\"reg-num\">Registration Number head</ion-label>\r\n          <ion-select value=\"\" okText=\"Ok\" cancelText=\"Dismiss\" [(ngModel)]='postData.registration_group' name=\"registration_group\" required>\r\n            <ion-select-option value=\"WRO\">WRO</ion-select-option>\r\n            <ion-select-option value=\"SRO\">SRO</ion-select-option>\r\n            <ion-select-option value=\"CRO\">CRO</ion-select-option>\r\n            <ion-select-option value=\"ERO\">ERO</ion-select-option>\r\n            <ion-select-option value=\"NRO\">NRO</ion-select-option>\r\n            <ion-select-option value=\"NRO\">FRO</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"space-3\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-item lines=\"none\">\r\n        <ion-input type=\"tel\" maxlength=\"7\" placeholder=\"ICAI Registration No\" class=\"form-control\" [(ngModel)]='postData.registration_number' name=\"registration_number\"></ion-input>\r\n      </ion-item>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"space-3\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-item lines=\"none\">\r\n        <ion-input type=\"text\"placeholder=\"First Name\" class=\"form-control\" [(ngModel)]='postData.first_name' name=\"first_name\"></ion-input>\r\n      </ion-item>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"space-3\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-item lines=\"none\">\r\n        <ion-input type=\"text\" placeholder=\"Last Name\" class=\"form-control\" [(ngModel)]='postData.last_name' name=\"last_name\"></ion-input>\r\n      </ion-item>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"space-3\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-item lines=\"none\">\r\n        <ion-input type=\"email\" placeholder=\"Enter email Id\" class=\"form-control\" autocomplete=\"off\" type=\"email\" name=\"email\" [(ngModel)]='postData.email'></ion-input>\r\n      </ion-item>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"space-3\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-item lines=\"none\">\r\n        <ion-input type=\"Password\" placeholder=\"Password\" class=\"form-control\" [(ngModel)]='postData.password_reg' name=\"password_reg\"></ion-input>\r\n      </ion-item>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"space-3\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-item lines=\"none\">\r\n        <ion-input type=\"Password\" placeholder=\"Password Confirmation\" class=\"form-control\" [(ngModel)]='postData.password_confirmation_reg' name=\"password_confirmation_reg\"></ion-input>\r\n      </ion-item>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"space-3\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-item lines=\"none\">\r\n        <ion-input type=\"tel\" maxlength=\"10\" placeholder=\"Phone No\" class=\"form-control\" [(ngModel)]='postData.phone_number' name=\"phone_number\"></ion-input>\r\n      </ion-item>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"space-3\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-item lines=\"none\">\r\n        <ion-input type=\"text\" placeholder=\"City\" class=\"form-control\" [(ngModel)]='postData.city' name=\"city\"></ion-input>\r\n      </ion-item>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"space-3\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-item lines=\"none\">\r\n        <ion-input type=\"text\"  placeholder=\"State\" class=\"form-control\" [(ngModel)]='postData.state' name=\"state\"></ion-input>\r\n      </ion-item>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"space-3\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-item lines=\"none\">\r\n        <ion-input type=\"text\" placeholder=\"Country\" class=\"form-control\" [(ngModel)]='postData.country' name=\"country\"></ion-input>\r\n      </ion-item>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"space-3\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-list class=\"border-all\">\r\n        <ion-item lines=\"none\">\r\n          <ion-label class=\"reg-num\">Courses</ion-label>\r\n          <ion-select value=\"\" okText=\"Ok\" cancelText=\"Dismiss\" [(ngModel)]='postData.course_id' name=\"course_id\" required>\r\n            <ion-select-option value=\"1\">CA Foundation</ion-select-option>\r\n            <ion-select-option value=\"2\">CA Inter</ion-select-option>\r\n            <ion-select-option value=\"3\">CA Final</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"space-3\">\r\n    <ion-col size=\"12\" class=\"text-center padding-0\">\r\n      <ion-item class=\"checkbox-txt\" lines=\"none\">\r\n        <ion-checkbox color=\"\" [(ngModel)]='postData.tnc' name=\"tnc\"></ion-checkbox>&nbsp;\r\n        <ion-label class=\"padding-left-10\">  I accept these <span class=\"terms-text\"><a href=\"http://makemytest.in/terms-condition\">Terms & Condition</a></span></ion-label>\r\n      </ion-item>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"space-m-50\">\r\n        <ion-button expand=\"block\" class=\"submit-btn\" type=\"submit\">Submit</ion-button>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  </form>\r\n\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0 text-center\">\r\n      <p class=\"account\">Already Have an Account?<span class=\"sign-in-text\" routerLink='/login'> Log In</span></p>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=pages-signup-signup-module.js.map