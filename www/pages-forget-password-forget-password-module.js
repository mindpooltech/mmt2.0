(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-forget-password-forget-password-module"],{

/***/ "4VsH":
/*!***************************************************************!*\
  !*** ./src/app/pages/forget-password/forget-password.page.ts ***!
  \***************************************************************/
/*! exports provided: ForgetPasswordPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgetPasswordPage", function() { return ForgetPasswordPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_forget_password_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./forget-password.page.html */ "Mn5x");
/* harmony import */ var _forget_password_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./forget-password.page.scss */ "4cMY");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");









let ForgetPasswordPage = class ForgetPasswordPage {
    constructor(authService, router, loadingController, alertController, uihelper, navCtrl) {
        this.authService = authService;
        this.router = router;
        this.loadingController = loadingController;
        this.alertController = alertController;
        this.uihelper = uihelper;
        this.navCtrl = navCtrl;
        this.postData = {
            email: ''
        };
    }
    ngOnInit() {
    }
    forgotPasswordAction() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.uihelper.ShowSpinner();
            return this.authService.forgotPassword(this.postData).subscribe((res) => {
                console.log('response aaa', res);
                this.uihelper.HideSpinner();
                this.presentAlert();
            }, (err) => {
                this.uihelper.HideSpinner();
                console.log('error', err);
                this.uihelper.ShowAlert('', err.error.error.message);
            });
        });
    }
    presentLoading() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                spinner: 'circles',
                keyboardClose: true,
                message: 'Please Wait'
            });
            return yield loading.present();
        });
    }
    presentAlert() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                subHeader: 'Reset Password',
                message: 'We have e-mailed your password reset link!',
                buttons: [
                    {
                        text: 'Ok',
                        handler: () => {
                            this.router.navigate(['/login']);
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    getHome() {
        this.navCtrl.pop();
    }
};
ForgetPasswordPage.ctorParameters = () => [
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_7__["UIHelper"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"] }
];
ForgetPasswordPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-forget-password',
        template: _raw_loader_forget_password_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_forget_password_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ForgetPasswordPage);



/***/ }),

/***/ "4cMY":
/*!*****************************************************************!*\
  !*** ./src/app/pages/forget-password/forget-password.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".intro-text p {\n  font-size: 16px;\n  line-height: 21px;\n  color: #000000;\n  font-weight: 500;\n  font-family: \"Lato\", sans-serif;\n  margin-bottom: 25px;\n  margin-top: 25px;\n}\n\nion-header {\n  display: none;\n  color: #4f689e;\n}\n\n.form-control {\n  border: 1px solid #b1bcd4;\n  border-radius: 10px;\n  margin-bottom: 22px;\n  font-size: 15px;\n  padding: 15px !important;\n  color: #585858;\n  --font-family: 500;\n}\n\nion-input {\n  --padding-top: 0px;\n  --padding-end: 0px;\n  --padding-bottom: 0px;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n  padding: 0 30px;\n}\n\n.space-m-50 {\n  margin: 0 82px;\n}\n\nion-button {\n  --background: #3f5c9b !important;\n  border-radius: 10px;\n}\n\n.forget-link {\n  background: transparent;\n  color: #3f5c9b;\n  font-size: 15px;\n  font-weight: 500;\n}\n\n.login-btn {\n  margin-bottom: 18px;\n  --background-activated: #3f5c9b !important;\n  --background-focused: #3f5c9b !important;\n  --background-hover: #3f5c9b !important;\n  --border-radius: 10px;\n  --min-height: 51px;\n  height: 51px;\n  text-align: center;\n  font-size: 18px;\n  font-weight: 400;\n}\n\n.no-account {\n  font-size: 15px;\n  color: #5b5b5b;\n  font-weight: 500;\n}\n\n.sign-up-text {\n  color: #f27376;\n  font-weight: 500;\n}\n\n.top-img {\n  width: 100%;\n  border-radius: 0 0 40px 40px;\n}\n\n.back-arrow ion-icon {\n  font-size: 22px;\n  color: #fff;\n}\n\n.menu-icon ion-icon {\n  font-size: 22px;\n  color: #fff;\n}\n\n.back img {\n  position: absolute;\n  z-index: 99;\n  top: 20px;\n  left: 20px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxmb3JnZXQtcGFzc3dvcmQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLCtCQUErQjtFQUMvQixtQkFBbUI7RUFDbkIsZ0JBQWdCO0FBQ3BCOztBQUNBO0VBQ0ksYUFBYTtFQUNiLGNBQWM7QUFFbEI7O0FBQUE7RUFDSSx5QkFBeUI7RUFDekIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2Ysd0JBQXdCO0VBQ3hCLGNBQWM7RUFDZCxrQkFBYztBQUdsQjs7QUFEQTtFQUNJLGtCQUFjO0VBQ2Qsa0JBQWM7RUFDZCxxQkFBaUI7QUFJckI7O0FBRkE7RUFDSSxvQkFBZ0I7RUFDaEIsd0JBQW9CO0VBQ3BCLGVBQWU7QUFLbkI7O0FBSEE7RUFDSSxjQUFjO0FBTWxCOztBQUpBO0VBQ0ksZ0NBQWE7RUFDYixtQkFBbUI7QUFPdkI7O0FBTEE7RUFDSSx1QkFBdUI7RUFDdkIsY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7QUFRcEI7O0FBTkE7RUFDSSxtQkFBbUI7RUFDbkIsMENBQXVCO0VBQ3ZCLHdDQUFxQjtFQUNyQixzQ0FBbUI7RUFDbkIscUJBQWdCO0VBQ2hCLGtCQUFhO0VBQ2IsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0FBU3BCOztBQVBBO0VBQ0ksZUFBZTtFQUNmLGNBQWM7RUFDZCxnQkFBZ0I7QUFVcEI7O0FBUkE7RUFDSSxjQUFjO0VBQ2QsZ0JBQWdCO0FBV3BCOztBQVRBO0VBQ0ksV0FBVztFQUNYLDRCQUE0QjtBQVloQzs7QUFWQTtFQUNJLGVBQWU7RUFDZixXQUFXO0FBYWY7O0FBWEE7RUFDSSxlQUFlO0VBQ2YsV0FBVztBQWNmOztBQVpBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxTQUFTO0VBQ1QsVUFBVTtBQWVkIiwiZmlsZSI6ImZvcmdldC1wYXNzd29yZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW50cm8tdGV4dCBwIHtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAyMXB4O1xyXG4gICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgZm9udC1mYW1pbHk6IFwiTGF0b1wiLCBzYW5zLXNlcmlmO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjVweDtcclxuICAgIG1hcmdpbi10b3A6IDI1cHg7XHJcbn1cclxuaW9uLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgY29sb3I6ICM0ZjY4OWU7XHJcbn1cclxuLmZvcm0tY29udHJvbCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjYjFiY2Q0O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDIycHg7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBwYWRkaW5nOiAxNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICBjb2xvcjogIzU4NTg1ODtcclxuICAgIC0tZm9udC1mYW1pbHk6IDUwMDtcclxufVxyXG5pb24taW5wdXQge1xyXG4gICAgLS1wYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgLS1wYWRkaW5nLWVuZDogMHB4O1xyXG4gICAgLS1wYWRkaW5nLWJvdHRvbTogMHB4O1xyXG59XHJcbmlvbi1pdGVtIHtcclxuICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xyXG4gICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xyXG4gICAgcGFkZGluZzogMCAzMHB4O1xyXG59XHJcbi5zcGFjZS1tLTUwIHtcclxuICAgIG1hcmdpbjogMCA4MnB4O1xyXG59XHJcbmlvbi1idXR0b24ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG59XHJcbi5mb3JnZXQtbGluayB7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIGNvbG9yOiAjM2Y1YzliO1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG4ubG9naW4tYnRuIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDE4cHg7XHJcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWhvdmVyOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAtLW1pbi1oZWlnaHQ6IDUxcHg7XHJcbiAgICBoZWlnaHQ6IDUxcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG59XHJcbi5uby1hY2NvdW50IHtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGNvbG9yOiAjNWI1YjViO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG4uc2lnbi11cC10ZXh0IHtcclxuICAgIGNvbG9yOiAjZjI3Mzc2O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG4udG9wLWltZyB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAgMCA0MHB4IDQwcHg7XHJcbn1cclxuLmJhY2stYXJyb3cgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAyMnB4O1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbn1cclxuLm1lbnUtaWNvbiBpb24taWNvbiB7XHJcbiAgICBmb250LXNpemU6IDIycHg7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxufVxyXG4uYmFjayBpbWd7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB6LWluZGV4OiA5OTtcclxuICAgIHRvcDogMjBweDtcclxuICAgIGxlZnQ6IDIwcHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "Mn5x":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forget-password/forget-password.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>forget-password</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row>\r\n    <div class=\"back\" (click)=\"getHome()\">\r\n        <img src=\"assets/back.svg\" alt=\"back\">\r\n    </div>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <img src=\"assets/mmt-img.svg\" class=\"top-img\">\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"10\" offset=\"1\" class=\"padding-0 \">\r\n      <div class=\"intro-text text-center\">\r\n        <p>Reset Your Password</p>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <form #forgotPasswordForm=\"ngForm\" (ngSubmit)=\"forgotPasswordAction()\">\r\n      <ion-col size=\"12\" class=\"padding-0\">\r\n          <ion-item lines=\"none\">\r\n            <ion-input type=\"email\" placeholder=\"Enter Email Id\" class=\"form-control\" [(ngModel)]='postData.email' name=\"email\" required></ion-input>\r\n          </ion-item>\r\n      </ion-col>\r\n      <ion-col size=\"12\" class=\"padding-0\">\r\n        <div class=\"space-m-50\">\r\n          <ion-button expand=\"block\" class=\"login-btn\" type=\"submit\">Reset Password</ion-button>\r\n        </div>\r\n      </ion-col>\r\n    </form>\r\n\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <!-- <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"space-m-50\">\r\n        <ion-button expand=\"block\" class=\"login-btn\" type=\"submit\">Reset Password</ion-button>\r\n      </div>\r\n    </ion-col> -->\r\n  </ion-row>\r\n\r\n</ion-content>");

/***/ }),

/***/ "m6ud":
/*!*****************************************************************!*\
  !*** ./src/app/pages/forget-password/forget-password.module.ts ***!
  \*****************************************************************/
/*! exports provided: ForgetPasswordPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgetPasswordPageModule", function() { return ForgetPasswordPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _forget_password_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./forget-password-routing.module */ "o1cY");
/* harmony import */ var _forget_password_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./forget-password.page */ "4VsH");







let ForgetPasswordPageModule = class ForgetPasswordPageModule {
};
ForgetPasswordPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _forget_password_routing_module__WEBPACK_IMPORTED_MODULE_5__["ForgetPasswordPageRoutingModule"]
        ],
        declarations: [_forget_password_page__WEBPACK_IMPORTED_MODULE_6__["ForgetPasswordPage"]]
    })
], ForgetPasswordPageModule);



/***/ }),

/***/ "o1cY":
/*!*************************************************************************!*\
  !*** ./src/app/pages/forget-password/forget-password-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: ForgetPasswordPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgetPasswordPageRoutingModule", function() { return ForgetPasswordPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _forget_password_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./forget-password.page */ "4VsH");




const routes = [
    {
        path: '',
        component: _forget_password_page__WEBPACK_IMPORTED_MODULE_3__["ForgetPasswordPage"]
    }
];
let ForgetPasswordPageRoutingModule = class ForgetPasswordPageRoutingModule {
};
ForgetPasswordPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ForgetPasswordPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-forget-password-forget-password-module.js.map