(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-coupon-popup-coupon-popup-module"],{

/***/ "ghvq":
/*!***********************************************************!*\
  !*** ./src/app/pages/coupon-popup/coupon-popup.module.ts ***!
  \***********************************************************/
/*! exports provided: CouponPopupPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CouponPopupPageModule", function() { return CouponPopupPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _coupon_popup_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./coupon-popup-routing.module */ "iCQb");
/* harmony import */ var _coupon_popup_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./coupon-popup.page */ "Fjy8");







let CouponPopupPageModule = class CouponPopupPageModule {
};
CouponPopupPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _coupon_popup_routing_module__WEBPACK_IMPORTED_MODULE_5__["CouponPopupPageRoutingModule"]
        ],
        declarations: [_coupon_popup_page__WEBPACK_IMPORTED_MODULE_6__["CouponPopupPage"]]
    })
], CouponPopupPageModule);



/***/ }),

/***/ "iCQb":
/*!*******************************************************************!*\
  !*** ./src/app/pages/coupon-popup/coupon-popup-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: CouponPopupPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CouponPopupPageRoutingModule", function() { return CouponPopupPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _coupon_popup_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./coupon-popup.page */ "Fjy8");




const routes = [
    {
        path: '',
        component: _coupon_popup_page__WEBPACK_IMPORTED_MODULE_3__["CouponPopupPage"]
    }
];
let CouponPopupPageRoutingModule = class CouponPopupPageRoutingModule {
};
CouponPopupPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CouponPopupPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=pages-coupon-popup-coupon-popup-module.js.map