(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-get-started-get-started-module"],{

/***/ "E4F9":
/*!*******************************************************!*\
  !*** ./src/app/pages/get-started/get-started.page.ts ***!
  \*******************************************************/
/*! exports provided: GetStartedPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetStartedPage", function() { return GetStartedPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_get_started_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./get-started.page.html */ "Kj4g");
/* harmony import */ var _get_started_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./get-started.page.scss */ "dgWA");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let GetStartedPage = class GetStartedPage {
    constructor() { }
    ngOnInit() {
    }
};
GetStartedPage.ctorParameters = () => [];
GetStartedPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-get-started',
        template: _raw_loader_get_started_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_get_started_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], GetStartedPage);



/***/ }),

/***/ "Kj4g":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/get-started/get-started.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>get-started</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <img src=\"assets/mmt-img.svg\" class=\"top-img\">\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"main-ttl text-center\">\r\n        <h3>Make My Test</h3>\r\n        <p>A brand new experience to learn something new. Polish your skills with this app. Easy to use.</p>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"space-m-20\">\r\n        <ion-button expand=\"block\" class=\"start-btn\" routerLink='/login'>Lets Get Started\r\n          <ion-icon name=\"chevron-forward-outline\"></ion-icon>\r\n        </ion-button>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n</ion-content>");

/***/ }),

/***/ "LmSD":
/*!*****************************************************************!*\
  !*** ./src/app/pages/get-started/get-started-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: GetStartedPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetStartedPageRoutingModule", function() { return GetStartedPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _get_started_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./get-started.page */ "E4F9");




const routes = [
    {
        path: '',
        component: _get_started_page__WEBPACK_IMPORTED_MODULE_3__["GetStartedPage"]
    }
];
let GetStartedPageRoutingModule = class GetStartedPageRoutingModule {
};
GetStartedPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], GetStartedPageRoutingModule);



/***/ }),

/***/ "UUMU":
/*!*********************************************************!*\
  !*** ./src/app/pages/get-started/get-started.module.ts ***!
  \*********************************************************/
/*! exports provided: GetStartedPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetStartedPageModule", function() { return GetStartedPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _get_started_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./get-started-routing.module */ "LmSD");
/* harmony import */ var _get_started_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./get-started.page */ "E4F9");







let GetStartedPageModule = class GetStartedPageModule {
};
GetStartedPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _get_started_routing_module__WEBPACK_IMPORTED_MODULE_5__["GetStartedPageRoutingModule"]
        ],
        declarations: [_get_started_page__WEBPACK_IMPORTED_MODULE_6__["GetStartedPage"]]
    })
], GetStartedPageModule);



/***/ }),

/***/ "dgWA":
/*!*********************************************************!*\
  !*** ./src/app/pages/get-started/get-started.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".padding-0 {\n  padding: 0px;\n}\n\n.text-center {\n  text-align: center;\n}\n\n.main-ttl h3 {\n  font-size: 31px;\n  color: #132767;\n  font-weight: 600;\n  margin: 25px 0 20px 0;\n}\n\n.main-ttl p {\n  font-size: 16px;\n  color: #132767;\n  font-weight: 500;\n  line-height: 24px;\n  font-family: \"Lato\", sans-serif;\n  padding: 0 40px;\n}\n\nion-title {\n  background-color: #f27376;\n}\n\n.start-btn {\n  background: #3f5c9b !important;\n  --background-activated: #3f5c9b !important;\n  --background-focused: #3f5c9b !important;\n  --background-hover: #3f5c9b !important;\n  --border-radius: 10px;\n  --min-height: 51px;\n  height: 51px;\n  margin: 50px auto 0;\n  width: 260px;\n  text-align: center;\n  font-size: 18px;\n  font-weight: 400;\n}\n\nion-button {\n  --background: #3f5c9b !important;\n  border-radius: 10px;\n  margin-top: 60px;\n}\n\n.space-m-20 {\n  margin: 0 32px;\n}\n\nion-header {\n  display: none;\n}\n\n.start-btn ion-icon {\n  position: absolute;\n  font-size: 16px;\n  right: 15px;\n}\n\n.top-img {\n  width: 100%;\n  border-radius: 0 0 40px 40px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxnZXQtc3RhcnRlZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFZO0FBQ2hCOztBQUNBO0VBQ0ksa0JBQWtCO0FBRXRCOztBQUFBO0VBQ0ksZUFBZTtFQUNmLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIscUJBQXFCO0FBR3pCOztBQURBO0VBQ0ksZUFBZTtFQUNmLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLCtCQUErQjtFQUMvQixlQUFlO0FBSW5COztBQUZBO0VBQ0kseUJBQXlCO0FBSzdCOztBQUhBO0VBQ0ksOEJBQThCO0VBQzlCLDBDQUF1QjtFQUN2Qix3Q0FBcUI7RUFDckIsc0NBQW1CO0VBQ25CLHFCQUFnQjtFQUNoQixrQkFBYTtFQUNiLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0FBTXBCOztBQUpBO0VBQ0ksZ0NBQWE7RUFDYixtQkFBbUI7RUFDbkIsZ0JBQWdCO0FBT3BCOztBQUxBO0VBQ0ksY0FBYztBQVFsQjs7QUFOQTtFQUNJLGFBQWE7QUFTakI7O0FBUEE7RUFDSSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLFdBQVc7QUFVZjs7QUFSQTtFQUNJLFdBQVc7RUFDWCw0QkFBNEI7QUFXaEMiLCJmaWxlIjoiZ2V0LXN0YXJ0ZWQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBhZGRpbmctMCB7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuLnRleHQtY2VudGVyIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4ubWFpbi10dGwgaDMge1xyXG4gICAgZm9udC1zaXplOiAzMXB4O1xyXG4gICAgY29sb3I6ICMxMzI3Njc7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgbWFyZ2luOiAyNXB4IDAgMjBweCAwO1xyXG59XHJcbi5tYWluLXR0bCBwIHtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIGNvbG9yOiAjMTMyNzY3O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xyXG4gICAgZm9udC1mYW1pbHk6IFwiTGF0b1wiLCBzYW5zLXNlcmlmO1xyXG4gICAgcGFkZGluZzogMCA0MHB4O1xyXG59XHJcbmlvbi10aXRsZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjI3Mzc2O1xyXG59XHJcbi5zdGFydC1idG4ge1xyXG4gICAgYmFja2dyb3VuZDogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYmFja2dyb3VuZC1ob3ZlcjogIzNmNWM5YiAhaW1wb3J0YW50O1xyXG4gICAgLS1ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgLS1taW4taGVpZ2h0OiA1MXB4O1xyXG4gICAgaGVpZ2h0OiA1MXB4O1xyXG4gICAgbWFyZ2luOiA1MHB4IGF1dG8gMDtcclxuICAgIHdpZHRoOiAyNjBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbn1cclxuaW9uLWJ1dHRvbiB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiA2MHB4O1xyXG59XHJcbi5zcGFjZS1tLTIwIHtcclxuICAgIG1hcmdpbjogMCAzMnB4O1xyXG59XHJcbmlvbi1oZWFkZXIge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG4uc3RhcnQtYnRuIGlvbi1pY29uIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIHJpZ2h0OiAxNXB4O1xyXG59XHJcbi50b3AtaW1nIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMCAwIDQwcHggNDBweDtcclxufVxyXG4iXX0= */");

/***/ })

}]);
//# sourceMappingURL=pages-get-started-get-started-module.js.map