(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\mindpooltech\mmt2.0\src\main.ts */"zUnb");


/***/ }),

/***/ "AF6X":
/*!*******************************************************************!*\
  !*** ./src/app/pages/scorecardpopover/scorecardpopover.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".popover-scroll {\n  overflow: scroll;\n  max-height: 300px;\n  min-width: 250px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxzY29yZWNhcmRwb3BvdmVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFnQjtFQUNoQixpQkFBaUI7RUFDakIsZ0JBQWdCO0FBQ3BCIiwiZmlsZSI6InNjb3JlY2FyZHBvcG92ZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBvcG92ZXItc2Nyb2xse1xyXG4gICAgb3ZlcmZsb3c6IHNjcm9sbDtcclxuICAgIG1heC1oZWlnaHQ6IDMwMHB4O1xyXG4gICAgbWluLXdpZHRoOiAyNTBweDtcclxuICB9Il19 */");

/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "CegS":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/scorecardpopover/scorecardpopover.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>scorecardpopover</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content> -->\r\n<div class=\"popover-scroll\">\r\n<div class=\"popover-main-holder\">\r\n  <h4 class=\"text-center\">Chapter List</h4>\r\n  <ul class=\"list\" *ngFor=\"let chapters of chapter_list\">\r\n    <li class=\"item\">{{chapters.title}}</li>\r\n  </ul>\r\n  <p *ngIf=\"chapter_list == ''\" style=\"color: red;text-align: center;\">No chapters found</p>\r\n\r\n</div>\r\n</div>\r\n");

/***/ }),

/***/ "IJhC":
/*!*********************************************!*\
  !*** ./src/app/pages/modal/modal.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@charset \"UTF-8\";\n.rate {\n  height: 46px;\n  padding: 0 10px;\n  float: left;\n}\ntable {\n  width: 100% !important;\n}\n.rate:not(:checked) > input {\n  position: absolute;\n  top: -9999px;\n}\n.rate:not(:checked) > label {\n  float: right;\n  width: 1em;\n  overflow: hidden;\n  white-space: nowrap;\n  cursor: pointer;\n  font-size: 26px;\n  color: #ccc;\n}\n.rate:not(:checked) > label:before {\n  content: '★ ';\n}\n.rate > input:checked ~ label {\n  color: #deb217;\n}\n.rate > input:checked + label:hover,\n.rate > input:checked + label:hover ~ label,\n.rate > input:checked ~ label:hover,\n.rate > input:checked label:hover label,\n.rate > label:hover input:checked label {\n  color: #deb217;\n}\n.form-control {\n  padding: 10px 15px;\n  margin: 0 25px 12px;\n  border-radius: 5px;\n  box-shadow: 0 0 0.875rem 0 rgba(181, 181, 181, 0.3);\n  font-size: 14px;\n  line-height: 20px;\n  height: 100px;\n}\n.start-exam-btn {\n  padding: 9px 0;\n  color: #ffffff;\n  font-weight: 400;\n  font-size: 16px;\n  border-radius: 50px !important;\n  box-shadow: none !important;\n  background: #51C3D1;\n  width: 95%;\n}\n.cancel-btn {\n  padding: 9px 0;\n  color: #2a2a2a;\n  font-weight: 400;\n  font-size: 16px;\n  border-radius: 50px !important;\n  box-shadow: none !important;\n  background: #d7d7d7;\n  width: 95%;\n}\n.m-30 {\n  margin: 0 45px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxtb2RhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FBQWhCO0VBQ0ksWUFBWTtFQUNaLGVBQWU7RUFJZixXQUFXO0FBRGY7QUFHQTtFQUNJLHNCQUFvQjtBQUF4QjtBQUVBO0VBQ0ksa0JBQWlCO0VBQ2pCLFlBQVc7QUFDZjtBQUNBO0VBQ0ksWUFBVztFQUNYLFVBQVM7RUFDVCxnQkFBZTtFQUNmLG1CQUFrQjtFQUNsQixlQUFjO0VBQ2QsZUFBYztFQUNkLFdBQVU7QUFFZDtBQUFBO0VBQ0ksYUFBUztBQUdiO0FBREE7RUFDSSxjQUFjO0FBSWxCO0FBRkE7Ozs7O0VBS0ksY0FBYztBQUtsQjtBQUhBO0VBQ0ksa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsbURBQW1EO0VBQ25ELGVBQWU7RUFDZixpQkFBaUI7RUFDakIsYUFBYTtBQU1qQjtBQUpBO0VBQ0ksY0FBYTtFQUNiLGNBQWE7RUFDYixnQkFBZ0I7RUFDaEIsZUFBYztFQUNkLDhCQUE0QjtFQUM1QiwyQkFBeUI7RUFDekIsbUJBQW1CO0VBQ25CLFVBQVU7QUFPZDtBQUxBO0VBQ0ksY0FBYTtFQUNiLGNBQWE7RUFDYixnQkFBZ0I7RUFDaEIsZUFBYztFQUNkLDhCQUE0QjtFQUM1QiwyQkFBeUI7RUFDekIsbUJBQW1CO0VBQ25CLFVBQVM7QUFRYjtBQU5BO0VBQ0ksY0FBYztBQVNsQiIsImZpbGUiOiJtb2RhbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAY2hhcnNldCBcIlVURi04XCI7XG4ucmF0ZSB7XG4gIGhlaWdodDogNDZweDtcbiAgcGFkZGluZzogMCAxMHB4O1xuICBmbG9hdDogbGVmdDtcbn1cblxudGFibGUge1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xufVxuXG4ucmF0ZTpub3QoOmNoZWNrZWQpID4gaW5wdXQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogLTk5OTlweDtcbn1cblxuLnJhdGU6bm90KDpjaGVja2VkKSA+IGxhYmVsIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICB3aWR0aDogMWVtO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgY29sb3I6ICNjY2M7XG59XG5cbi5yYXRlOm5vdCg6Y2hlY2tlZCkgPiBsYWJlbDpiZWZvcmUge1xuICBjb250ZW50OiAn4piFICc7XG59XG5cbi5yYXRlID4gaW5wdXQ6Y2hlY2tlZCB+IGxhYmVsIHtcbiAgY29sb3I6ICNkZWIyMTc7XG59XG5cbi5yYXRlID4gaW5wdXQ6Y2hlY2tlZCArIGxhYmVsOmhvdmVyLFxuLnJhdGUgPiBpbnB1dDpjaGVja2VkICsgbGFiZWw6aG92ZXIgfiBsYWJlbCxcbi5yYXRlID4gaW5wdXQ6Y2hlY2tlZCB+IGxhYmVsOmhvdmVyLFxuLnJhdGUgPiBpbnB1dDpjaGVja2VkIGxhYmVsOmhvdmVyIGxhYmVsLFxuLnJhdGUgPiBsYWJlbDpob3ZlciBpbnB1dDpjaGVja2VkIGxhYmVsIHtcbiAgY29sb3I6ICNkZWIyMTc7XG59XG5cbi5mb3JtLWNvbnRyb2wge1xuICBwYWRkaW5nOiAxMHB4IDE1cHg7XG4gIG1hcmdpbjogMCAyNXB4IDEycHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgYm94LXNoYWRvdzogMCAwIDAuODc1cmVtIDAgcmdiYSgxODEsIDE4MSwgMTgxLCAwLjMpO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBoZWlnaHQ6IDEwMHB4O1xufVxuXG4uc3RhcnQtZXhhbS1idG4ge1xuICBwYWRkaW5nOiA5cHggMDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgYm9yZGVyLXJhZGl1czogNTBweCAhaW1wb3J0YW50O1xuICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6ICM1MUMzRDE7XG4gIHdpZHRoOiA5NSU7XG59XG5cbi5jYW5jZWwtYnRuIHtcbiAgcGFkZGluZzogOXB4IDA7XG4gIGNvbG9yOiAjMmEyYTJhO1xuICBmb250LXdlaWdodDogNDAwO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHggIWltcG9ydGFudDtcbiAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kOiAjZDdkN2Q3O1xuICB3aWR0aDogOTUlO1xufVxuXG4ubS0zMCB7XG4gIG1hcmdpbjogMCA0NXB4O1xufVxuIl19 */");

/***/ }),

/***/ "JAFY":
/*!*******************************************!*\
  !*** ./src/app/pages/modal/modal.page.ts ***!
  \*******************************************/
/*! exports provided: ModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalPage", function() { return ModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./modal.page.html */ "lg3r");
/* harmony import */ var _modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modal.page.scss */ "IJhC");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");










let ModalPage = class ModalPage {
    constructor(router, modalController, navParams, storageService, httpService, navCtrl, uihelper) {
        this.router = router;
        this.modalController = modalController;
        this.navParams = navParams;
        this.storageService = storageService;
        this.httpService = httpService;
        this.navCtrl = navCtrl;
        this.uihelper = uihelper;
        this.postData = {
            ratings: '',
            rating_comment: '',
        };
        this.rating_comment = '';
    }
    ngOnInit() {
        // console.log(this.navParams);
        this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_8__["Constant"].AUTH).then(res => {
            this.user_id = res.user_details.id;
        });
    }
    dismiss() {
        this.modalController.dismiss();
        return false;
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        // this.modalController.dismiss({
        //   'dismissed': true
        // });
    }
    submit_rating(form) {
        this.postData['test_id'] = this.navParams.data.test_id;
        this.postData['student_exam_id'] = this.navParams.data.exam_id;
        this.postData['user_id'] = this.user_id;
        console.log('postData', this.postData);
        this.httpService.afterLoginPost('submit-test-rating', this.postData).subscribe((res) => {
            // console.log('submit rating response',res)
            this.uihelper.ShowAlert('', 'Thank you for your feedback.');
            this.dismiss();
            this.router.navigate(['/tabs']);
        }, (err) => {
            console.log('submit rating error', err);
            //  this.toastService.presentToast("Please fill all Details")
            this.uihelper.ShowAlert('', 'Please fill all Details');
        });
    }
};
ModalPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavParams"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_5__["StorageService"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_6__["HttpService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_9__["UIHelper"] }
];
ModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-modal',
        template: _raw_loader_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ModalPage);



/***/ }),

/***/ "N+K7":
/*!******************************************!*\
  !*** ./src/app/services/http.service.ts ***!
  \******************************************/
/*! exports provided: HttpService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpService", function() { return HttpService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");




let HttpService = class HttpService {
    constructor(http) {
        this.http = http;
    }
    post(serviceName, data) {
        const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        const options = { headers: headers, withCredentails: false };
        const url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_3__["Constant"].apiUrl + serviceName;
        return this.http.post(url, data, options);
    }
    afterLoginPost(serviceName, data) {
        const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
        });
        // console.log('http service main', serviceName, data)
        const options = { headers: headers };
        const url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_3__["Constant"].apiUrl + serviceName;
        return this.http.post(url, data, options);
    }
    afterLoginGet(serviceName, data) {
        const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
        });
        // console.log('http service main', serviceName, data)
        const options = { headers: headers };
        const url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_3__["Constant"].apiUrl + serviceName + '/' + data;
        return this.http.get(url, options);
    }
};
HttpService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
HttpService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], HttpService);



/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./app.component.html */ "VzVu");
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component.scss */ "ynWL");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/auth.service */ "lGQG");
/* harmony import */ var _services_storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/storage.service */ "n90K");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _services_event_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/event.service */ "fTLw");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");













let AppComponent = class AppComponent {
    constructor(platform, menu, authService, navCtrl, storageService, router, events, http, uihelper) {
        this.platform = platform;
        this.menu = menu;
        this.authService = authService;
        this.navCtrl = navCtrl;
        this.storageService = storageService;
        this.router = router;
        this.events = events;
        this.http = http;
        this.uihelper = uihelper;
        this.initializeApp();
    }
    initializeApp() {
        this.platform.ready().then(() => {
            // this.splashScreen.hide();
            let IsLogin = localStorage.getItem('IsLogin');
            if (IsLogin == 'Yes') {
                // this.rootPage = 'OwnerDashboardPage';
                this.navCtrl.navigateRoot(['tabs']);
            }
            else {
                this.navCtrl.navigateRoot(['login']);
            }
        });
    }
    ngOnInit() {
        // (<any>window).plugins.preventscreenshot.disable((a) => this.successCallback(a), (b) => this.errorCallback(b));
        this.events.getObservable().subscribe((data) => {
            this.student_credit_balance = data.student_avail_amt;
        });
        try {
            this.router.events.subscribe((event) => {
                this.selfPath = '';
                // if (event && event instanceof NavigationEnd && event.url) {
                //   console.log('inside event', event.url)
                //   this.selfPath = event.url + '/dashboard';
                // }
                this.selfPath = this.router.routerState.snapshot.url;
                // console.log(this.getStringBeforeSubstring(this.selfPath, 'viptips-details'));
            });
        }
        catch (e) {
            console.log(e);
        }
        this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__["Constant"].AUTH).then(res => {
            this.userData = res.user_details;
            this.userName = res.user_details.full_name;
        });
    }
    ionViewWillEnter() {
    }
    // successCallback(result) {
    //   console.log(result); // true - enabled, false - disabled
    // }
    // errorCallback(error) {
    //   console.log(error);
    // }
    logoutAction() {
        this.authService.logout();
    }
    GoToSearchPackages() {
        console.log('Test', this.selfPath);
        this.router.navigate([this.selfPath + "/packages"]);
    }
    GoToDashboard() {
        this.router.navigate([this.selfPath]);
    }
    GoToGroupPackages() {
        this.router.navigate([this.selfPath + "/group-packages"]);
    }
    GoToMyPackages() {
        this.router.navigate([this.selfPath + "/my-packages"]);
    }
    GoToFeaturedPackages() {
        this.router.navigate([this.selfPath + "/featured-packages"]);
    }
    TestBoard() {
        this.router.navigate([this.selfPath + "/test-board"]);
    }
    TestHistory() {
        this.router.navigate([this.selfPath + "/test-history"]);
    }
    MyRatings() {
        this.router.navigate([this.selfPath + "/my-ratings"]);
    }
    MyQueries() {
        this.router.navigate([this.selfPath + "/my-queries"]);
    }
    MyNotes() {
        this.router.navigate([this.selfPath + "/notes-details"]);
    }
    Myvideos() {
        this.router.navigate([this.selfPath + "/my-videos"]);
    }
    GoToReferral() {
        this.router.navigate([this.selfPath + "/refer-and-earn"]);
    }
};
AppComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _services_storage_service__WEBPACK_IMPORTED_MODULE_6__["StorageService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] },
    { type: _services_event_service__WEBPACK_IMPORTED_MODULE_8__["EventService"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_10__["HttpService"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_11__["UIHelper"] }
];
AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], AppComponent);



/***/ }),

/***/ "VzVu":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-app>\r\n  <ion-split-pane contentId=\"main\" when=\"(min-width: 3000px)\">\r\n    <ion-menu side=\"start\" menuId=\"main-menu\" contentId=\"main\">\r\n      <ion-header>\r\n        <ion-row class=\"menu-header\">\r\n          <ion-col size=\"4\" class=\"menu-img\">\r\n            <img src=\"assets/user-profile.png\">\r\n          </ion-col>\r\n          <ion-col size=\"8\" class=\"menu-header-user-info\">\r\n            <h3>{{userName}}</h3>\r\n            <p>MMT Institute.</p>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-header>\r\n\r\n      <ion-content style=\"margin: 10px 0;\">\r\n        <ion-list>\r\n\r\n          <ion-col size=\"12\" class=\"padding-0\">\r\n            <!-- <div class=\"motivationl-text\">\r\n              <p>{{daily_motivational_text}}</p>\r\n            </div> -->\r\n            <div>\r\n              <ion-button class=\"credit-btn\">MMT Credit Rs. {{ student_credit_balance}}</ion-button>\r\n            </div>\r\n          </ion-col>\r\n\r\n          <ion-menu-toggle>\r\n            <ion-row class=\"main-class-menu\" (click)=\"GoToDashboard()\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <img src=\"assets/icon/dashboard.svg\">\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>Dashboard</span>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-menu-toggle>\r\n\r\n          <ion-menu-toggle>\r\n            <ion-row class=\"main-class-menu\" (click)=\"GoToReferral()\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <ion-icon name=\"wallet\" class=\"search-circle\"></ion-icon>\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>Refer and Earn</span>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-menu-toggle>\r\n\r\n          <ion-row class=\"main-class-menu\" (click)=\"isRemainder1 === 1 ? isRemainder1 = 0 : isRemainder1 = 1\">\r\n            <ion-col size=\"2\" class=\"profile-icon\">\r\n              <img src=\"assets/icon/packages.svg\">\r\n            </ion-col>\r\n            <ion-col size=\"8\" class=\"profile\">\r\n              <span>Packages</span>\r\n            </ion-col>\r\n            <ion-col size=\"2\" class=\"profile\">\r\n              <ion-icon name=\"add\" class=\"drop-icon\"></ion-icon>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-menu-toggle [hidden]=\"!isRemainder1\">\r\n            <ion-row class=\"main-class-menu padding-left-sub-menu\" (click)=\"GoToSearchPackages()\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <ion-icon name=\"search\" class=\"search-circle\"></ion-icon>\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>Search Packages</span>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row class=\"main-class-menu padding-left-sub-menu\" (click)=\"GoToGroupPackages()\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <ion-icon name=\"server\" class=\"search-circle\"></ion-icon>\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>Group Packages</span>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row class=\"main-class-menu padding-left-sub-menu\" (click)=\"GoToMyPackages()\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <ion-icon name=\"layers\" class=\"search-circle\"></ion-icon>\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>My Packages</span>\r\n              </ion-col>\r\n            </ion-row>\r\n            <!-- <ion-row class=\"main-class-menu padding-left-sub-menu\" (click)=\"GoToFeaturedPackages()\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <ion-icon name=\"layers\" class=\"search-circle\"></ion-icon>\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>Featured Packages</span>\r\n              </ion-col>\r\n            </ion-row> -->\r\n          </ion-menu-toggle>\r\n\r\n          <!-- <ion-menu-toggle>\r\n            <ion-row class=\"main-class-menu\" (click)=\"TestBoard()\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <img src=\"assets/icon/test-board.svg\">\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>My Test Board</span>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-menu-toggle> -->\r\n\r\n          <ion-row class=\"main-class-menu\" (click)=\"isRemainder3 === 1 ? isRemainder3 = 0 : isRemainder3 = 1\">\r\n            <ion-col size=\"2\" class=\"profile-icon\">\r\n              <img src=\"assets/icon/account.svg\">\r\n            </ion-col>\r\n            <ion-col size=\"8\" class=\"profile\">\r\n              <span>Test Board</span>\r\n            </ion-col>\r\n            <ion-col size=\"2\" class=\"profile\">\r\n              <ion-icon name=\"add\" class=\"drop-icon\"></ion-icon>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-menu-toggle [hidden]=\"!isRemainder3\">\r\n            <ion-row class=\"main-class-menu padding-left-sub-menu\" (click)=\"TestBoard()\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <ion-icon name=\"clipboard\" class=\"search-circle\"></ion-icon>\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>My Test Board</span>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row class=\"main-class-menu padding-left-sub-menu\" (click)=\"TestHistory()\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <ion-icon name=\"albums\" class=\"search-circle\"></ion-icon>\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>Test History</span>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-menu-toggle>\r\n\r\n          <ion-menu-toggle>\r\n            <ion-row class=\"main-class-menu\" (click)=\"MyRatings()\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <img src=\"assets/icon/my-rating.svg\">\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>My Rating</span>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-menu-toggle>\r\n\r\n          <ion-menu-toggle>\r\n            <ion-row class=\"main-class-menu\" (click)=\"MyQueries()\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <img src=\"assets/icon/queries.svg\">\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>My Queries</span>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-menu-toggle>\r\n\r\n          <ion-menu-toggle>\r\n            <ion-row class=\"main-class-menu\" (click)=\"MyNotes()\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <img src=\"assets/icon/my-note.svg\">\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>My Notes</span>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-menu-toggle>\r\n\r\n          <ion-menu-toggle>\r\n            <ion-row class=\"main-class-menu\" (click)=\"Myvideos()\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <img src=\"assets/icon/videos.svg\">\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>My Video</span>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-menu-toggle>\r\n\r\n          <!-- <ion-row class=\"main-class-menu\" (click)=\"isRemainder2 === 1 ? isRemainder2 = 0 : isRemainder2 = 1\">\r\n            <ion-col size=\"2\" class=\"profile-icon\">\r\n              <img src=\"assets/icon/account.svg\">\r\n            </ion-col>\r\n            <ion-col size=\"8\" class=\"profile\">\r\n              <span>My Account</span>\r\n            </ion-col>\r\n            <ion-col size=\"2\" class=\"profile\">\r\n              <ion-icon name=\"add\" class=\"drop-icon\"></ion-icon>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-menu-toggle [hidden]=\"!isRemainder2\">\r\n            <ion-row class=\"main-class-menu padding-left-sub-menu\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <img src=\"assets/tab-menu/packages.png\">\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>Account</span>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-menu-toggle> -->\r\n\r\n          <ion-menu-toggle>\r\n            <ion-row class=\"main-class-menu\" (click)=\"logoutAction()\">\r\n              <ion-col size=\"2\" class=\"profile-icon\">\r\n                <img src=\"assets/icon/logout.svg\">\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"profile\">\r\n                <span>Logout</span>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-menu-toggle>\r\n\r\n        </ion-list>\r\n      </ion-content>\r\n    </ion-menu>\r\n    <ion-router-outlet id=\"main\"></ion-router-outlet>\r\n  </ion-split-pane>\r\n</ion-app>");

/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/storage */ "e8h1");
/* harmony import */ var _pages_modal_modal_page__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/modal/modal.page */ "JAFY");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/file-opener/ngx */ "te5A");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/file/ngx */ "FAH8");
/* harmony import */ var _ionic_native_document_viewer_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ionic-native/document-viewer/ngx */ "LfQc");
/* harmony import */ var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ionic-native/social-sharing/ngx */ "/XPu");
/* harmony import */ var _mobiscroll_angular_lite__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @mobiscroll/angular-lite */ "ytlw");
/* harmony import */ var _ionic_native_launch_review_ngx__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ionic-native/launch-review/ngx */ "JaPA");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "m/P+");
/* harmony import */ var _pages_scorecardpopover_scorecardpopover_page__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./pages/scorecardpopover/scorecardpopover.page */ "yjVV");





















let AppModule = class AppModule {
};
AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"], _pages_modal_modal_page__WEBPACK_IMPORTED_MODULE_11__["ModalPage"], _pages_scorecardpopover_scorecardpopover_page__WEBPACK_IMPORTED_MODULE_20__["ScorecardpopoverPage"]],
        entryComponents: [],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"].forRoot(), _ionic_storage__WEBPACK_IMPORTED_MODULE_10__["IonicStorageModule"].forRoot(), _app_routing_module__WEBPACK_IMPORTED_MODULE_9__["AppRoutingModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"], _mobiscroll_angular_lite__WEBPACK_IMPORTED_MODULE_17__["MbscModule"]],
        providers: [
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_5__["UIHelper"],
            _angular_common__WEBPACK_IMPORTED_MODULE_12__["DatePipe"],
            _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_13__["FileOpener"],
            _ionic_native_document_viewer_ngx__WEBPACK_IMPORTED_MODULE_15__["DocumentViewer"],
            _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_14__["File"],
            _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_16__["SocialSharing"],
            _ionic_native_launch_review_ngx__WEBPACK_IMPORTED_MODULE_18__["LaunchReview"],
            _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_19__["InAppBrowser"],
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicRouteStrategy"] }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]],
    })
], AppModule);



/***/ }),

/***/ "fTLw":
/*!*******************************************!*\
  !*** ./src/app/services/event.service.ts ***!
  \*******************************************/
/*! exports provided: EventService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventService", function() { return EventService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "qCKp");



let EventService = class EventService {
    constructor() {
        this.subject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
    }
    sendMessage(text) {
        this.subject.next(text);
    }
    setCreditBalance(data) {
        this.subject.next(data);
    }
    getObservable() {
        return this.subject.asObservable();
    }
};
EventService.ctorParameters = () => [];
EventService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], EventService);



/***/ }),

/***/ "kLfG":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet.entry.js": [
		"dUtr",
		"common",
		0
	],
	"./ion-alert.entry.js": [
		"Q8AI",
		"common",
		1
	],
	"./ion-app_8.entry.js": [
		"hgI1",
		"common",
		2
	],
	"./ion-avatar_3.entry.js": [
		"CfoV",
		"common",
		3
	],
	"./ion-back-button.entry.js": [
		"Nt02",
		"common",
		4
	],
	"./ion-backdrop.entry.js": [
		"Q2Bp",
		5
	],
	"./ion-button_2.entry.js": [
		"0Pbj",
		"common",
		6
	],
	"./ion-card_5.entry.js": [
		"ydQj",
		"common",
		7
	],
	"./ion-checkbox.entry.js": [
		"4fMi",
		"common",
		8
	],
	"./ion-chip.entry.js": [
		"czK9",
		"common",
		9
	],
	"./ion-col_3.entry.js": [
		"/CAe",
		10
	],
	"./ion-datetime_3.entry.js": [
		"WgF3",
		"common",
		11
	],
	"./ion-fab_3.entry.js": [
		"uQcF",
		"common",
		12
	],
	"./ion-img.entry.js": [
		"wHD8",
		13
	],
	"./ion-infinite-scroll_2.entry.js": [
		"2lz6",
		14
	],
	"./ion-input.entry.js": [
		"ercB",
		"common",
		15
	],
	"./ion-item-option_3.entry.js": [
		"MGMP",
		"common",
		16
	],
	"./ion-item_8.entry.js": [
		"9bur",
		"common",
		17
	],
	"./ion-loading.entry.js": [
		"cABk",
		"common",
		18
	],
	"./ion-menu_3.entry.js": [
		"kyFE",
		"common",
		19
	],
	"./ion-modal.entry.js": [
		"TvZU",
		"common",
		20
	],
	"./ion-nav_2.entry.js": [
		"vnES",
		"common",
		21
	],
	"./ion-popover.entry.js": [
		"qCuA",
		"common",
		22
	],
	"./ion-progress-bar.entry.js": [
		"0tOe",
		"common",
		23
	],
	"./ion-radio_2.entry.js": [
		"h11V",
		"common",
		24
	],
	"./ion-range.entry.js": [
		"XGij",
		"common",
		25
	],
	"./ion-refresher_2.entry.js": [
		"nYbb",
		"common",
		26
	],
	"./ion-reorder_2.entry.js": [
		"smMY",
		"common",
		27
	],
	"./ion-ripple-effect.entry.js": [
		"STjf",
		28
	],
	"./ion-route_4.entry.js": [
		"k5eQ",
		"common",
		29
	],
	"./ion-searchbar.entry.js": [
		"OR5t",
		"common",
		30
	],
	"./ion-segment_2.entry.js": [
		"fSgp",
		"common",
		31
	],
	"./ion-select_3.entry.js": [
		"lfGF",
		"common",
		32
	],
	"./ion-slide_2.entry.js": [
		"5xYT",
		33
	],
	"./ion-spinner.entry.js": [
		"nI0H",
		"common",
		34
	],
	"./ion-split-pane.entry.js": [
		"NAQR",
		35
	],
	"./ion-tab-bar_2.entry.js": [
		"knkW",
		"common",
		36
	],
	"./ion-tab_2.entry.js": [
		"TpdJ",
		"common",
		37
	],
	"./ion-text.entry.js": [
		"ISmu",
		"common",
		38
	],
	"./ion-textarea.entry.js": [
		"U7LX",
		"common",
		39
	],
	"./ion-toast.entry.js": [
		"L3sA",
		"common",
		40
	],
	"./ion-toggle.entry.js": [
		"IUOf",
		"common",
		41
	],
	"./ion-virtual-scroll.entry.js": [
		"8Mb5",
		42
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "kLfG";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "lGQG":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./http.service */ "N+K7");
/* harmony import */ var _storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./storage.service */ "n90K");








let AuthService = class AuthService {
    constructor(httpService, storageService, router, http, navCtrl) {
        this.httpService = httpService;
        this.storageService = storageService;
        this.router = router;
        this.http = http;
        this.navCtrl = navCtrl;
    }
    login(postData) {
        return this.httpService.post('login', postData);
    }
    logout() {
        this.storageService.removeStorageItem(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_5__["Constant"].AUTH).then(res => {
            localStorage.removeItem("userToken");
            localStorage.removeItem("course_id");
            localStorage.removeItem("cartCount");
            // this.storageService.removeStorageItem(testDataConstants.TESTDATA);
            // this.router.navigate(['']);
            localStorage.setItem('IsLogin', 'No');
            localStorage.clear();
            this.navCtrl.navigateRoot('login');
        });
    }
    signup(postData) {
        return this.httpService.post('register', postData);
    }
    forgotPassword(postData) {
        const url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_5__["Constant"].apiUrl;
        return this.http.post(url + 'forgotpassword', postData);
    }
};
AuthService.ctorParameters = () => [
    { type: _http_service__WEBPACK_IMPORTED_MODULE_6__["HttpService"] },
    { type: _storage_service__WEBPACK_IMPORTED_MODULE_7__["StorageService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] }
];
AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], AuthService);



/***/ }),

/***/ "lg3r":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/modal/modal.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>Rate Test Evaluation</ion-title>\r\n    <!-- <ion-button size=\"small\" (click)=\"dismiss()\">close</ion-button> -->\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <!-- <ion-item>\r\n    <ion-button size=\"small\" (click)=\"dismiss()\">close</ion-button>\r\n  </ion-item> -->\r\n\r\n  <form #itemForm=\"ngForm\" (ngSubmit)=\"submit_rating(itemForm)\">\r\n  <ion-row>\r\n    <ion-col class=\"padding-0\" size=\"8\" offset=\"2\">\r\n      <div class=\"rate\">\r\n        <input type=\"radio\" id=\"star5\" name=\"ratings\" value=\"5\" [(ngModel)]='postData.ratings'/>\r\n        <label for=\"star5\" title=\"text\"></label>\r\n\r\n        <input type=\"radio\" id=\"star4\" name=\"ratings\" value=\"4\" [(ngModel)]='postData.ratings'/>\r\n        <label for=\"star4\" title=\"text\"></label>\r\n\r\n        <input type=\"radio\" id=\"star3\" name=\"ratings\" value=\"3\" [(ngModel)]='postData.ratings'/>\r\n        <label for=\"star3\" title=\"text\"></label>\r\n\r\n        <input type=\"radio\" id=\"star2\" name=\"ratings\" value=\"2\" [(ngModel)]='postData.ratings'/>\r\n        <label for=\"star2\" title=\"text\"></label>\r\n\r\n        <input type=\"radio\" id=\"star1\" name=\"ratings\" value=\"1\" [(ngModel)]='postData.ratings'/>\r\n        <label for=\"star1\" title=\"text\"></label>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\">\r\n      <ion-item lines=\"none\">\r\n        <ion-textarea placeholder=\"Comment...\" class=\"form-control\" [(ngModel)]='postData.rating_comment' name=\"rating_comment\">\r\n        </ion-textarea>\r\n      </ion-item>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"m-30\">\r\n    <ion-col size=\"6\" class=\"text-center\">\r\n      <button ion-button full class=\"cancel-btn\" (click)=\"dismiss()\">Cancel</button>\r\n    </ion-col>\r\n    <ion-col size=\"6\" class=\"text-center\">\r\n      <button ion-button full class=\"start-exam-btn\" type=\"submit\">Submit</button>\r\n    </ion-col>\r\n  </ion-row>\r\n</form>\r\n</ion-content>\r\n\r\n\r\n");

/***/ }),

/***/ "n88v":
/*!********************************!*\
  !*** ./src/Helper/Constant.ts ***!
  \********************************/
/*! exports provided: Constant */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Constant", function() { return Constant; });
class Constant {
}
Constant.apiUrl = 'http://newdev.makemytest.in/api/';
Constant.adminUrl = 'http://newdev.makemytest.in/';
// public static apiUrl:string = 'http://127.0.0.1:8000/api/';
// public static adminUrl:string = 'http://127.0.0.1:8000/';
Constant.AUTH = 'userData';


/***/ }),

/***/ "n90K":
/*!*********************************************!*\
  !*** ./src/app/services/storage.service.ts ***!
  \*********************************************/
/*! exports provided: StorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorageService", function() { return StorageService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "e8h1");



// import { Plugins } from '@capacitor/core';
// const { Storage } = Plugins;
let StorageService = class StorageService {
    constructor(storage) {
        this.storage = storage;
    }
    //store the value
    store(storageKey, value) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const encryptedValue = JSON.stringify(value);
            const result = yield this.storage.set(storageKey, encryptedValue);
            // console.log('sotrage result',result)
        });
    }
    // get value
    get(storageKey) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const ret = yield this.storage.get(storageKey);
            // console.log('get data',ret)
            return JSON.parse((ret));
        });
    }
    removeStorageItem(storageKey) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.storage.remove(storageKey);
        });
    }
    //   // Clear storage
    //   async clear() {
    //   await Storage.clear();
    //   }
    setParamData(data) {
        this.paramData = data;
    }
    getParamData() {
        return this.paramData;
    }
    setVariable(data) {
        this.isLoadingBool = !!data;
    }
    getVariable() {
        return !!this.isLoadingBool;
    }
};
StorageService.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] }
];
StorageService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], StorageService);



/***/ }),

/***/ "r1+5":
/*!*****************************!*\
  !*** ./src/Helper/Enums.ts ***!
  \*****************************/
/*! exports provided: LOGINTYPE, ROLE, ACTIVITY, NOTIFICATION, TOAST */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LOGINTYPE", function() { return LOGINTYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROLE", function() { return ROLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ACTIVITY", function() { return ACTIVITY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NOTIFICATION", function() { return NOTIFICATION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TOAST", function() { return TOAST; });
var LOGINTYPE;
(function (LOGINTYPE) {
    LOGINTYPE[LOGINTYPE["PARENT"] = 1] = "PARENT";
    LOGINTYPE[LOGINTYPE["TEACHER"] = 2] = "TEACHER";
})(LOGINTYPE || (LOGINTYPE = {}));
var ROLE;
(function (ROLE) {
    ROLE[ROLE["SUPERADMIN"] = 1] = "SUPERADMIN";
    ROLE[ROLE["SCHOOLADMIN"] = 2] = "SCHOOLADMIN";
    ROLE[ROLE["STAFF"] = 3] = "STAFF";
    ROLE[ROLE["PARENT"] = 4] = "PARENT";
})(ROLE || (ROLE = {}));
var ACTIVITY;
(function (ACTIVITY) {
    ACTIVITY[ACTIVITY["PHOTO"] = 1] = "PHOTO";
    ACTIVITY[ACTIVITY["FOOD"] = 2] = "FOOD";
    ACTIVITY[ACTIVITY["LEARNING"] = 3] = "LEARNING";
    ACTIVITY[ACTIVITY["NOTES"] = 4] = "NOTES";
    ACTIVITY[ACTIVITY["NAP"] = 5] = "NAP";
    ACTIVITY[ACTIVITY["POTTY"] = 6] = "POTTY";
})(ACTIVITY || (ACTIVITY = {}));
var NOTIFICATION;
(function (NOTIFICATION) {
    NOTIFICATION[NOTIFICATION["ASSIGNMENT"] = 1] = "ASSIGNMENT";
    NOTIFICATION[NOTIFICATION["EVENT"] = 2] = "EVENT";
    NOTIFICATION[NOTIFICATION["NOTICE"] = 3] = "NOTICE";
})(NOTIFICATION || (NOTIFICATION = {}));
var TOAST;
(function (TOAST) {
    TOAST[TOAST["SUCCESS"] = 1] = "SUCCESS";
    TOAST[TOAST["WARNING"] = 2] = "WARNING";
    TOAST[TOAST["ERROR"] = 3] = "ERROR";
    TOAST[TOAST["INFO"] = 4] = "INFO";
})(TOAST || (TOAST = {}));


/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");



const routes = [
    {
        path: '',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-get-started-get-started-module */ "pages-get-started-get-started-module").then(__webpack_require__.bind(null, /*! ./pages/get-started/get-started.module */ "UUMU")).then(m => m.GetStartedPageModule)
    },
    {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
    },
    {
        path: 'login',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-login-login-module */ "pages-login-login-module").then(__webpack_require__.bind(null, /*! ./pages/login/login.module */ "F4UR")).then(m => m.LoginPageModule)
    },
    {
        path: 'dashboard',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-dashboard-dashboard-module */ "dashboard-dashboard-module").then(__webpack_require__.bind(null, /*! ./pages/dashboard/dashboard.module */ "/2RN")).then(m => m.DashboardPageModule)
    },
    // {
    // path: 'get-started',
    // loadChildren: () => import('./pages/get-started/get-started.module').then( m => m.GetStartedPageModule)
    // },
    {
        path: 'signup',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-signup-signup-module */ "pages-signup-signup-module").then(__webpack_require__.bind(null, /*! ./pages/signup/signup.module */ "UUSl")).then(m => m.SignupPageModule)
    },
    {
        path: 'forget-password',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-forget-password-forget-password-module */ "pages-forget-password-forget-password-module").then(__webpack_require__.bind(null, /*! ./pages/forget-password/forget-password.module */ "m6ud")).then(m => m.ForgetPasswordPageModule)
    },
    {
        path: 'user-profile',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-user-profile-user-profile-module */ "user-profile-user-profile-module").then(__webpack_require__.bind(null, /*! ./pages/user-profile/user-profile.module */ "TBb5")).then(m => m.UserProfilePageModule)
    },
    {
        path: 'settings',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-settings-settings-module */ "pages-settings-settings-module").then(__webpack_require__.bind(null, /*! ./pages/settings/settings.module */ "yPrK")).then(m => m.SettingsPageModule)
    },
    {
        path: 'tabs',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-tabs-tabs-module */ "pages-tabs-tabs-module").then(__webpack_require__.bind(null, /*! ./pages/tabs/tabs.module */ "qiIP")).then(m => m.TabsPageModule)
    },
    {
        path: 'packages',
        loadChildren: () => Promise.all(/*! import() | pages-packages-packages-module */[__webpack_require__.e("common"), __webpack_require__.e("packages-packages-module")]).then(__webpack_require__.bind(null, /*! ./pages/packages/packages.module */ "8D2i")).then(m => m.PackagesPageModule)
    },
    {
        path: 'test-board',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-test-board-test-board-module */ "test-board-test-board-module").then(__webpack_require__.bind(null, /*! ./pages/test-board/test-board.module */ "URWB")).then(m => m.TestBoardPageModule)
    },
    {
        path: 'group-packages',
        loadChildren: () => Promise.all(/*! import() | pages-group-packages-group-packages-module */[__webpack_require__.e("common"), __webpack_require__.e("group-packages-group-packages-module")]).then(__webpack_require__.bind(null, /*! ./pages/group-packages/group-packages.module */ "7Upg")).then(m => m.GroupPackagesPageModule)
    },
    {
        path: 'my-packages',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-my-packages-my-packages-module */ "my-packages-my-packages-module").then(__webpack_require__.bind(null, /*! ./pages/my-packages/my-packages.module */ "pgHy")).then(m => m.MyPackagesPageModule)
    },
    {
        path: 'new-test',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-new-test-new-test-module */ "new-test-new-test-module").then(__webpack_require__.bind(null, /*! ./pages/new-test/new-test.module */ "q7J+")).then(m => m.NewTestPageModule)
    },
    {
        path: 'start-test',
        loadChildren: () => Promise.all(/*! import() | pages-start-test-start-test-module */[__webpack_require__.e("common"), __webpack_require__.e("start-test-start-test-module")]).then(__webpack_require__.bind(null, /*! ./pages/start-test/start-test.module */ "VTbd")).then(m => m.StartTestPageModule)
    },
    {
        path: 'end-test',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-end-test-end-test-module */ "end-test-end-test-module").then(__webpack_require__.bind(null, /*! ./pages/end-test/end-test.module */ "eBAB")).then(m => m.EndTestPageModule)
    },
    {
        path: 'test-history',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-test-history-test-history-module */ "test-history-test-history-module").then(__webpack_require__.bind(null, /*! ./pages/test-history/test-history.module */ "Crn+")).then(m => m.TestHistoryPageModule)
    },
    {
        path: 'test-evaluation',
        loadChildren: () => Promise.all(/*! import() | pages-test-evaluation-test-evaluation-module */[__webpack_require__.e("default~notes-details-notes-details-module~test-evaluation-test-evaluation-module"), __webpack_require__.e("common"), __webpack_require__.e("test-evaluation-test-evaluation-module")]).then(__webpack_require__.bind(null, /*! ./pages/test-evaluation/test-evaluation.module */ "lKpJ")).then(m => m.TestEvaluationPageModule)
    },
    {
        path: 'cart',
        loadChildren: () => Promise.all(/*! import() | pages-cart-cart-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("cart-cart-module")]).then(__webpack_require__.bind(null, /*! ./pages/cart/cart.module */ "sFz8")).then(m => m.CartPageModule)
    },
    {
        path: 'edit-user-profile',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-edit-user-profile-edit-user-profile-module */ "edit-user-profile-edit-user-profile-module").then(__webpack_require__.bind(null, /*! ./pages/edit-user-profile/edit-user-profile.module */ "qRg9")).then(m => m.EditUserProfilePageModule)
    },
    {
        path: 'my-ratings',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-my-ratings-my-ratings-module */ "my-ratings-my-ratings-module").then(__webpack_require__.bind(null, /*! ./pages/my-ratings/my-ratings.module */ "rzzE")).then(m => m.MyRatingsPageModule)
    },
    {
        path: 'my-queries',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-my-queries-my-queries-module */ "my-queries-my-queries-module").then(__webpack_require__.bind(null, /*! ./pages/my-queries/my-queries.module */ "5q53")).then(m => m.MyQueriesPageModule)
    },
    {
        path: 'my-videos',
        loadChildren: () => Promise.all(/*! import() | pages-my-videos-my-videos-module */[__webpack_require__.e("common"), __webpack_require__.e("my-videos-my-videos-module")]).then(__webpack_require__.bind(null, /*! ./pages/my-videos/my-videos.module */ "ix0U")).then(m => m.MyVideosPageModule)
    },
    {
        path: 'notes-details',
        loadChildren: () => Promise.all(/*! import() | pages-notes-details-notes-details-module */[__webpack_require__.e("default~notes-details-notes-details-module~test-evaluation-test-evaluation-module"), __webpack_require__.e("notes-details-notes-details-module")]).then(__webpack_require__.bind(null, /*! ./pages/notes-details/notes-details.module */ "TpMJ")).then(m => m.NotesDetailsPageModule)
    },
    {
        path: 'coupon-popup',
        loadChildren: () => Promise.all(/*! import() | pages-coupon-popup-coupon-popup-module */[__webpack_require__.e("default~cart-cart-module~pages-coupon-popup-coupon-popup-module"), __webpack_require__.e("pages-coupon-popup-coupon-popup-module")]).then(__webpack_require__.bind(null, /*! ./pages/coupon-popup/coupon-popup.module */ "ghvq")).then(m => m.CouponPopupPageModule)
    },
    {
        path: 'refer-and-earn',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-refer-and-earn-refer-and-earn-module */ "refer-and-earn-refer-and-earn-module").then(__webpack_require__.bind(null, /*! ./pages/refer-and-earn/refer-and-earn.module */ "tjE2")).then(m => m.ReferAndEarnPageModule)
    },
    {
        path: 'featured-packages',
        loadChildren: () => __webpack_require__.e(/*! import() | pages-featured-packages-featured-packages-module */ "featured-packages-featured-packages-module").then(__webpack_require__.bind(null, /*! ./pages/featured-packages/featured-packages.module */ "eIpb")).then(m => m.FeaturedPackagesPageModule)
    },
    {
        path: 'pdf-modal',
        loadChildren: () => Promise.all(/*! import() | pages-pdf-modal-pdf-modal-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-pdf-modal-pdf-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/pdf-modal/pdf-modal.module */ "UBgk")).then(m => m.PdfModalPageModule)
    },
    {
        path: 'note-pdf-modal',
        loadChildren: () => Promise.all(/*! import() | pages-note-pdf-modal-note-pdf-modal-module */[__webpack_require__.e("common"), __webpack_require__.e("pages-note-pdf-modal-note-pdf-modal-module")]).then(__webpack_require__.bind(null, /*! ./pages/note-pdf-modal/note-pdf-modal.module */ "RC39")).then(m => m.NotePdfModalPageModule)
    },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"] })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "x7bl":
/*!********************************!*\
  !*** ./src/Helper/UIHelper.ts ***!
  \********************************/
/*! exports provided: UIHelper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UIHelper", function() { return UIHelper; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _Helper_Enums__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Helper/Enums */ "r1+5");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "m/P+");







let UIHelper = class UIHelper {
    constructor(navCtrl, loadingCtrl, alertController, toastCtrl, router, iab, platform) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.alertController = alertController;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.iab = iab;
        this.platform = platform;
        this.toastEnum = _Helper_Enums__WEBPACK_IMPORTED_MODULE_3__["TOAST"];
    }
    Logout() {
    }
    ShowAlert(title, subtitle) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let alert = this.alertController.create({
                header: title,
                subHeader: subtitle,
                buttons: ['Ok']
            });
            (yield alert).present();
        });
    }
    urlAlert(title, subtitle) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let alert = this.alertController.create({
                header: title,
                subHeader: subtitle,
                buttons: [
                    {
                        text: 'Ok',
                        handler: () => {
                            console.log('ok clicked');
                            if (this.platform.is('android')) {
                                console.log('play store link');
                                const browser = this.iab.create("https://play.google.com/store/apps/details?id=com.MakeMyTest&hl=en_IN&gl=US", '_system', { location: 'yes' });
                            }
                            else {
                                // iOS
                                console.log('applie store link');
                                const browser = this.iab.create("itms-apps://itunes.apple.com/app/viphomelink/id1522942035?mt=8", '_system', { location: 'yes' });
                            }
                        }
                    }
                ]
            });
            (yield alert).present();
        });
    }
    presentAlert() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                subHeader: 'Sign Up successful',
                message: 'Please confirm your mail before login.',
                buttons: [
                    {
                        text: 'Ok',
                        handler: () => {
                            this.router.navigate(['/login']);
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    ShowSpinner() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loadingCtrl.create({
                message: `<img src="assets/icon/loader4.gif" class="loader-old" />`,
                // message: `<div class="loader-new" /></div>`,
                spinner: null,
                translucent: true,
                cssClass: 'loading-wrapper',
                backdropDismiss: false
            }).then((res) => {
                res.present();
            });
        });
    }
    ShowSpinner_old() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loadingCtrl.create({
                message: `<img src="assets/icon/loader2.gif" class="loader-old" />`,
                spinner: null,
                translucent: true,
                cssClass: 'text-center',
                backdropDismiss: false
            }).then((res) => {
                res.present();
            });
        });
    }
    HideSpinner() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loadingCtrl.dismiss().then((res) => {
                console.log('Loading dismissed!', res);
            }).catch((error) => {
                console.log('error', error);
            });
        });
    }
    presentLoadingCustom() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                duration: 55000,
                message: '<img src="../assets/imgs/loader.svg" class="img-align"  /> ',
                spinner: null,
                // translucent: true,
                cssClass: 'custom-loader-class',
                backdropDismiss: true
            });
            yield loading.present();
            const { role, data } = yield loading.onDidDismiss();
            console.log('Loading dismissed with role:', role);
        });
    }
    // Show the loader for infinite time
    ShowSpinner1() {
        this.loadingCtrl.create({
            message: '<p>Please wait...</p>',
            cssClass: 'loader-para'
        }).then((res) => {
            res.present();
        });
    }
    // Hide the loader if already created otherwise return error
    HideSpinner1() {
        this.loadingCtrl.dismiss().then((res) => {
            console.log('Loading dismissed!', res);
        }).catch((error) => {
            console.log('error', error);
        });
    }
    playStoreAlert() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                message: 'Do you want to buy this book?',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Ok',
                        handler: () => {
                            console.log('ok clicked');
                            if (this.platform.is('android')) {
                                console.log('play store link');
                                const browser = this.iab.create("https://play.google.com/store/apps/details?id=com.MakeMyTest&hl=en_IN&gl=US", '_system', { location: 'yes' });
                            }
                            else {
                                // iOS
                                console.log('applie store link');
                                const browser = this.iab.create("itms-apps://itunes.apple.com/app/viphomelink/id1522942035?mt=8", '_system', { location: 'yes' });
                            }
                        }
                    }
                ]
            });
            alert.present();
        });
    }
    showToast(message, type) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let cssClassName = 'successToast';
            if (type == this.toastEnum.ERROR) {
                cssClassName = 'errorToast';
            }
            else if (type == this.toastEnum.SUCCESS) {
                cssClassName = 'successToast';
            }
            else if (type == this.toastEnum.WARNING) {
                cssClassName = 'warningToast';
            }
            else if (type == this.toastEnum.INFO) {
                cssClassName = 'infoToast';
            }
            let toast = this.toastCtrl.create({
                message: message,
                duration: 4000,
                position: 'bottom',
                cssClass: cssClassName
            });
            (yield toast).onDidDismiss();
            (yield toast).present();
        });
    }
    getStringBeforeSubstring(parentString, substring) {
        return parentString.substring(0, parentString.indexOf(substring));
    }
    paymentAlert(msg) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                subHeader: 'MMT',
                message: msg,
                buttons: [
                    {
                        text: 'Ok',
                        handler: () => {
                            this.router.navigate(['/tabs']);
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    presentToast(InfoMessage) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: InfoMessage,
                duration: 2000
            });
            toast.present();
        });
    }
};
UIHelper.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ToastController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_5__["InAppBrowser"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"] }
];
UIHelper = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()
], UIHelper);



/***/ }),

/***/ "yjVV":
/*!*****************************************************************!*\
  !*** ./src/app/pages/scorecardpopover/scorecardpopover.page.ts ***!
  \*****************************************************************/
/*! exports provided: ScorecardpopoverPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScorecardpopoverPage", function() { return ScorecardpopoverPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_scorecardpopover_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./scorecardpopover.page.html */ "CegS");
/* harmony import */ var _scorecardpopover_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./scorecardpopover.page.scss */ "AF6X");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");









let ScorecardpopoverPage = class ScorecardpopoverPage {
    constructor(navParams, popovercontroller, http, httpService, loadingController, uihelper) {
        this.navParams = navParams;
        this.popovercontroller = popovercontroller;
        this.http = http;
        this.httpService = httpService;
        this.loadingController = loadingController;
        this.uihelper = uihelper;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpHeaders"]({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
        });
        this.url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_7__["Constant"].apiUrl;
        this.options = { headers: this.headers };
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.uihelper.ShowSpinner();
            this.question_id = this.navParams.data.question_id;
            this.http.get(this.url + 'question-chapters/' + this.question_id, this.options).subscribe((res) => {
                this.uihelper.HideSpinner();
                console.log('popover chapters', res);
                this.chapter_list = res['payload'].chapters;
                console.log('chapter list', this.chapter_list);
            }, (err) => {
                this.uihelper.HideSpinner();
                this.chapter_list = [];
                // this.miscHelper.showAlert("not found!!");
            });
        });
    }
};
ScorecardpopoverPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavParams"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["PopoverController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_6__["HttpService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_8__["UIHelper"] }
];
ScorecardpopoverPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-scorecardpopover',
        template: _raw_loader_scorecardpopover_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_scorecardpopover_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ScorecardpopoverPage);



/***/ }),

/***/ "ynWL":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-header {\n  box-shadow: 0px 0px 3px 1.5px #f1b3b2;\n  padding: 0px;\n}\n\n.menu-header-user-info h3 {\n  font-size: 16px;\n  color: #305F72;\n  margin: 18px 0 0 0;\n  font-weight: 500;\n  text-transform: capitalize;\n}\n\n.menu-header-user-info p {\n  font-size: 14px;\n  margin: 5px 0 0 0;\n  display: block;\n  color: #E2707E;\n  font-weight: 400;\n}\n\n.menu-img img {\n  width: 60px;\n  margin: 5px 15px;\n  text-align: center;\n}\n\n.main-class-menu {\n  padding: 14px 15px 10px;\n  color: #020551;\n  cursor: pointer;\n}\n\n.profile-icon {\n  text-align: center;\n}\n\n.profile-icon img {\n  width: 20px;\n  height: 20px;\n}\n\n.profile span {\n  font-weight: 400;\n  font-size: 15px;\n  color: #455769;\n}\n\n.main-class-menu:hover {\n  background: #FEF0F0;\n  border-left: 4px solid #F27376;\n}\n\n.drop-icon {\n  position: relative;\n  left: 10px;\n  top: 3px;\n  color: #283033;\n}\n\n.padding-left-sub-menu {\n  padding: 10px 15px 5px 50px;\n}\n\n.search-circle {\n  font-size: 23px;\n  color: #455769;\n}\n\n.loading-wrapper {\n  position: absolute !important;\n  top: 0 !important;\n  bottom: 0 !important;\n  left: 0 !important;\n  right: 0 !important;\n  padding: 0 !important;\n  border-radius: 0 !important;\n  display: flex;\n  justify-content: center;\n  align-content: stretch;\n  align-items: center;\n  min-width: 100% !important;\n  max-height: 100% !important;\n  background-color: rgba(0, 0, 0, 0.8) !important;\n}\n\nion-button.credit-btn {\n  --background-activated: #37bf56  !important;\n  --background-focused: #37bf56  !important;\n  --background-hover: #37bf56  !important;\n  --background: #37bf56  !important;\n  border-radius: 10px;\n  font-size: 14px;\n  font-family: \"Lato\";\n  font-weight: 500;\n  width: 80%;\n  margin: 10px 28px;\n  display: block;\n  margin-bottom: 15px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UscUNBQXFDO0VBQ3JDLFlBQVk7QUFDZDs7QUFDRTtFQUNFLGVBQWU7RUFDZixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQiwwQkFBMEI7QUFFOUI7O0FBQUU7RUFDRSxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGNBQWM7RUFDZCxjQUFjO0VBQ2QsZ0JBQWdCO0FBR3BCOztBQURFO0VBQ0UsV0FBVztFQUNYLGdCQUFnQjtFQUNoQixrQkFBa0I7QUFJdEI7O0FBRkU7RUFDRSx1QkFBdUI7RUFDdkIsY0FBYztFQUNkLGVBQWU7QUFLbkI7O0FBSEU7RUFDRSxrQkFBa0I7QUFNdEI7O0FBSkU7RUFDRSxXQUFXO0VBQ1gsWUFBWTtBQU9oQjs7QUFMRTtFQUNFLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsY0FBYztBQVFsQjs7QUFORTtFQUNFLG1CQUFtQjtFQUNuQiw4QkFBOEI7QUFTbEM7O0FBUEU7RUFDRSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLFFBQVE7RUFDUixjQUFjO0FBVWxCOztBQVJFO0VBQ0UsMkJBQTJCO0FBVy9COztBQVRFO0VBQ0UsZUFBZTtFQUNmLGNBQWM7QUFZbEI7O0FBVEU7RUFDRSw2QkFBNkI7RUFDN0IsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQiwyQkFBMkI7RUFLM0IsYUFBYTtFQUtiLHVCQUF1QjtFQUd2QixzQkFBc0I7RUFLdEIsbUJBQW1CO0VBQ25CLDBCQUEwQjtFQUMxQiwyQkFBMkI7RUFDM0IsK0NBQStDO0FBWW5EOztBQVRBO0VBQ0UsMkNBQXVCO0VBQ3ZCLHlDQUFxQjtFQUNyQix1Q0FBbUI7RUFDbkIsaUNBQWE7RUFDYixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsVUFBVTtFQUNWLGlCQUFpQjtFQUNqQixjQUFjO0VBQ2QsbUJBQW1CO0FBWXJCIiwiZmlsZSI6ImFwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1oZWFkZXJ7XHJcbiAgYm94LXNoYWRvdzogMHB4IDBweCAzcHggMS41cHggI2YxYjNiMjtcclxuICBwYWRkaW5nOiAwcHg7XHJcbiAgfVxyXG4gIC5tZW51LWhlYWRlci11c2VyLWluZm8gaDN7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIG1hcmdpbjogMThweCAwIDAgMDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICB9XHJcbiAgLm1lbnUtaGVhZGVyLXVzZXItaW5mbyBwe1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgbWFyZ2luOiA1cHggMCAwIDA7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGNvbG9yOiAjRTI3MDdFO1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICB9XHJcbiAgLm1lbnUtaW1nIGltZ3tcclxuICAgIHdpZHRoOiA2MHB4O1xyXG4gICAgbWFyZ2luOiA1cHggMTVweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9XHJcbiAgLm1haW4tY2xhc3MtbWVudXtcclxuICAgIHBhZGRpbmc6IDE0cHggMTVweCAxMHB4O1xyXG4gICAgY29sb3I6ICMwMjA1NTE7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG4gIC5wcm9maWxlLWljb257XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG4gIC5wcm9maWxlLWljb24gaW1ne1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgfVxyXG4gIC5wcm9maWxlIHNwYW57XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgY29sb3I6ICM0NTU3Njk7XHJcbiAgfVxyXG4gIC5tYWluLWNsYXNzLW1lbnU6aG92ZXJ7XHJcbiAgICBiYWNrZ3JvdW5kOiAjRkVGMEYwO1xyXG4gICAgYm9yZGVyLWxlZnQ6IDRweCBzb2xpZCAjRjI3Mzc2O1xyXG4gIH1cclxuICAuZHJvcC1pY29ue1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbGVmdDogMTBweDtcclxuICAgIHRvcDogM3B4O1xyXG4gICAgY29sb3I6ICMyODMwMzM7XHJcbiAgfVxyXG4gIC5wYWRkaW5nLWxlZnQtc3ViLW1lbnV7XHJcbiAgICBwYWRkaW5nOiAxMHB4IDE1cHggNXB4IDUwcHg7XHJcbiAgfVxyXG4gIC5zZWFyY2gtY2lyY2xle1xyXG4gICAgZm9udC1zaXplOiAyM3B4O1xyXG4gICAgY29sb3I6ICM0NTU3Njk7XHJcbiAgfVxyXG5cclxuICAubG9hZGluZy13cmFwcGVyIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xyXG4gICAgdG9wOiAwICFpbXBvcnRhbnQ7XHJcbiAgICBib3R0b206IDAgIWltcG9ydGFudDtcclxuICAgIGxlZnQ6IDAgIWltcG9ydGFudDtcclxuICAgIHJpZ2h0OiAwICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwICFpbXBvcnRhbnQ7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgIGRpc3BsYXk6IC1tb3otYm94O1xyXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1ib3gtcGFjazogY2VudGVyO1xyXG4gICAgLW1vei1ib3gtcGFjazogY2VudGVyO1xyXG4gICAgLXdlYmtpdC1qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgLXdlYmtpdC1hbGlnbi1jb250ZW50OiBzdHJldGNoO1xyXG4gICAgLW1zLWZsZXgtbGluZS1wYWNrOiBzdHJldGNoO1xyXG4gICAgYWxpZ24tY29udGVudDogc3RyZXRjaDtcclxuICAgIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XHJcbiAgICAtbW96LWJveC1hbGlnbjogY2VudGVyO1xyXG4gICAgLXdlYmtpdC1hbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBtaW4td2lkdGg6IDEwMCUgIWltcG9ydGFudDtcclxuICAgIG1heC1oZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC44KSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5pb24tYnV0dG9uLmNyZWRpdC1idG57XHJcbiAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzM3YmY1NiAgIWltcG9ydGFudDtcclxuICAtLWJhY2tncm91bmQtZm9jdXNlZDogIzM3YmY1NiAgIWltcG9ydGFudDtcclxuICAtLWJhY2tncm91bmQtaG92ZXI6ICMzN2JmNTYgICFpbXBvcnRhbnQ7XHJcbiAgLS1iYWNrZ3JvdW5kOiAjMzdiZjU2ICAhaW1wb3J0YW50O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGZvbnQtZmFtaWx5OiBcIkxhdG9cIjtcclxuICBmb250LXdlaWdodDogNTAwO1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgbWFyZ2luOiAxMHB4IDI4cHg7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxufSJdfQ== */");

/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "a3Wg");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.log(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map