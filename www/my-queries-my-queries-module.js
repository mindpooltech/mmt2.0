(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["my-queries-my-queries-module"],{

/***/ "5D90":
/*!*****************************************************!*\
  !*** ./src/app/pages/my-queries/my-queries.page.ts ***!
  \*****************************************************/
/*! exports provided: MyQueriesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyQueriesPage", function() { return MyQueriesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_my_queries_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./my-queries.page.html */ "rBPZ");
/* harmony import */ var _my_queries_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./my-queries.page.scss */ "P206");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");











let MyQueriesPage = class MyQueriesPage {
    constructor(router, http, httpService, storageService, uihelper, navCtrl) {
        this.router = router;
        this.http = http;
        this.httpService = httpService;
        this.storageService = storageService;
        this.uihelper = uihelper;
        this.navCtrl = navCtrl;
        this.url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__["Constant"].apiUrl;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
        });
        this.options = { headers: this.headers };
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                this.router.events.subscribe((event) => {
                    this.selfPath = '';
                    if (event && event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__["NavigationEnd"] && event.url) {
                        this.selfPath = event.url + '/my-queries';
                    }
                    this.selfPath = this.router.routerState.snapshot.url;
                    this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'my-queries') + 'my-queries';
                });
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    getStringBeforeSubstring(parentString, substring) {
        return parentString.substring(0, parentString.indexOf(substring));
    }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.cartCount = localStorage.getItem('cartCount');
            this.uihelper.ShowSpinner();
            this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__["Constant"].AUTH).then(res => {
                this.userData = res.user_details;
                this.user_id = res.user_details.id;
                this.httpService.afterLoginGet("cart_count", res.user_details.id).subscribe((res) => {
                    this.cartCount = res['payload'].cart_count;
                });
                let data = {
                    user_id: this.user_id
                };
                this.httpService.afterLoginPost('student-test-queries', data).subscribe((res) => {
                    this.queries = res['payload'].studentquery;
                    this.packageList = res['payload'].packages;
                    // console.log('queries',this.queries);
                    this.queries.forEach((value, i) => {
                        this.queries[i]['created_at'] = value.created_at.substring(0, 10);
                    });
                    this.uihelper.HideSpinner();
                }, (err) => {
                    console.log('queries not found');
                    this.uihelper.ShowAlert('', "not found!!");
                });
            });
        });
    }
    getTestData(event) {
        this.package_id = event.target.value;
        this.http.get(this.url + 'package-tests/' + this.package_id, this.options).subscribe((res) => {
            this.package_test = res['payload'];
        });
    }
    selectTest(event) {
        this.test_id = event.target.value;
    }
    selectStatus(event) {
        this.status = event.target.value;
    }
    submit_filter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.uihelper.ShowSpinner();
            let form_data = new FormData();
            form_data.append('package_id', this.package_id ? this.package_id : '');
            form_data.append('test_id', this.test_id ? this.test_id : '');
            form_data.append('test_status', this.status ? this.status : '');
            form_data.append('user_id', this.user_id);
            this.httpService.afterLoginPost('student-test-queries', form_data).subscribe((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                this.queries = res['payload'].studentquery;
                // console.log('filter queries',this.queries); 
                this.uihelper.HideSpinner();
            }), (err) => {
                this.uihelper.HideSpinner();
                console.log('error', err);
            });
        });
    }
    goBack() {
        this.router.navigate(['/tabs']);
    }
    GoToCart() {
        this.router.navigate([this.selfPath + "/cart"]);
    }
    GoToUserProfile() {
        this.router.navigate([this.selfPath + "/user-profile"]);
    }
};
MyQueriesPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_7__["HttpService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_8__["StorageService"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_10__["UIHelper"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] }
];
MyQueriesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-my-queries',
        template: _raw_loader_my_queries_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_my_queries_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MyQueriesPage);



/***/ }),

/***/ "5q53":
/*!*******************************************************!*\
  !*** ./src/app/pages/my-queries/my-queries.module.ts ***!
  \*******************************************************/
/*! exports provided: MyQueriesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyQueriesPageModule", function() { return MyQueriesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _my_queries_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./my-queries-routing.module */ "ArBH");
/* harmony import */ var _my_queries_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./my-queries.page */ "5D90");







let MyQueriesPageModule = class MyQueriesPageModule {
};
MyQueriesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _my_queries_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyQueriesPageRoutingModule"]
        ],
        declarations: [_my_queries_page__WEBPACK_IMPORTED_MODULE_6__["MyQueriesPage"]]
    })
], MyQueriesPageModule);



/***/ }),

/***/ "ArBH":
/*!***************************************************************!*\
  !*** ./src/app/pages/my-queries/my-queries-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: MyQueriesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyQueriesPageRoutingModule", function() { return MyQueriesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _my_queries_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./my-queries.page */ "5D90");




const routes = [
    {
        path: '',
        component: _my_queries_page__WEBPACK_IMPORTED_MODULE_3__["MyQueriesPage"]
    }
];
let MyQueriesPageRoutingModule = class MyQueriesPageRoutingModule {
};
MyQueriesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MyQueriesPageRoutingModule);



/***/ }),

/***/ "P206":
/*!*******************************************************!*\
  !*** ./src/app/pages/my-queries/my-queries.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.back img {\n  width: 40px;\n}\n\n.back {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.header-arrow {\n  padding: 20px 15px !important;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 120px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n}\n\n.packages-header h2 {\n  margin: 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n  position: relative;\n  top: -15px;\n}\n\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\nion-label {\n  margin: 0;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n}\n\n.title-text {\n  padding-left: 10px;\n  font-weight: 500;\n  font-size: 20px;\n  color: #305F72;\n}\n\n.m-10 {\n  margin: 0 10px;\n}\n\n.mt-16 {\n  margin-top: 16px;\n}\n\n.form-control-cart {\n  border: 1px solid #2f3a755c !important;\n  font-size: 14px !important;\n  border-radius: 8px;\n  --color: rgb(48 95 114);\n  font-weight: 400;\n  padding: 0;\n  margin-right: 4px;\n}\n\n.font-size-1 {\n  font-size: 12px;\n  margin: 0;\n}\n\n.test-history-btn button {\n  border: none;\n  padding: 12px 40px;\n  font-size: 15px;\n  font-weight: 400;\n  background-color: #3f5c9b;\n  color: white;\n  border-radius: 8px;\n  outline: none;\n  margin: 10px auto 0;\n  text-align: center;\n  display: block;\n  transition: 0.3s;\n}\n\nion-select {\n  width: 34px;\n  padding: 5px;\n  padding-left: 0;\n}\n\nion-item {\n  --padding-start: 12px;\n  --min-height: 35px;\n}\n\n.mb-0 {\n  margin-bottom: 0px;\n}\n\n.queries-content span {\n  font-size: 15px;\n  color: #696969;\n}\n\n.space-right-left {\n  margin: 0 5px;\n}\n\n.title-text {\n  color: #2a2a2a;\n  font-size: 15px;\n  font-weight: 500;\n  padding-left: 5px;\n}\n\n.test-history-btn button {\n  border: none;\n  padding: 12px 40px;\n  font-size: 15px;\n  font-weight: 400;\n  background-color: #3f5c9b;\n  color: white;\n  border-radius: 8px;\n  outline: none;\n  margin: 10px auto 0;\n  text-align: center;\n  display: block;\n  transition: 0.3s;\n}\n\n.font-size-1 {\n  font-size: 13px;\n  margin: 0;\n}\n\nion-select {\n  width: 34px;\n  padding: 5px;\n  padding-left: 0;\n}\n\nion-item {\n  --padding-start: 12px;\n  --min-height: 35px;\n}\n\n.form-control-cart {\n  border: 1px solid #2f3a755c !important;\n  font-size: 14px !important;\n  border-radius: 8px;\n  --color: rgb(48 95 114);\n  font-weight: 400;\n  padding: 0;\n  margin-right: 4px;\n}\n\n.margin-10 {\n  margin: 0 10px;\n}\n\nbutton {\n  padding: 0;\n}\n\n.mt-16 {\n  margin-top: 16px;\n}\n\n.m-0 {\n  margin: 0px;\n}\n\n.tab-list-view {\n  box-shadow: 0 0 0.875rem 0 rgba(181, 181, 181, 0.56);\n  border-radius: 8px;\n  padding: 20px 10px;\n  margin: 0 10px;\n  margin-bottom: 15px;\n}\n\n.tab-list-content h2 {\n  font-size: 13px;\n  font-weight: 400;\n  margin-top: 0;\n  margin-bottom: 5px;\n}\n\n.tab-list-content h3 strong {\n  font-size: 14px;\n}\n\n.tab-list-content h3 {\n  font-size: 13px;\n  font-weight: 400;\n  margin-top: 0;\n  margin-bottom: 5px;\n}\n\n.tab-list-content a {\n  font-size: 14px;\n  font-weight: 400;\n  color: #51c3d1;\n}\n\n.center-text {\n  display: flex;\n  align-items: center;\n  padding-left: 9px;\n}\n\n.center-text span {\n  font-size: 13px;\n}\n\n.complete-btn {\n  padding: 10px 28px;\n  color: #2a2a2a;\n  font-weight: 500;\n  font-size: 13px;\n  border-radius: 50px !important;\n  box-shadow: none !important;\n  background: #28a745;\n  color: #fff;\n  margin-top: 18px;\n}\n\n.submited-btn {\n  padding: 10px 27px;\n  color: #2a2a2a;\n  font-weight: 400;\n  font-size: 13px;\n  border-radius: 50px !important;\n  box-shadow: none !important;\n  background: #ffc107;\n  color: #fff;\n  margin-top: 18px;\n}\n\n.pending-status {\n  padding: 10px 15px;\n  color: #2a2a2a !important;\n  font-weight: 400;\n  font-size: 13px !important;\n  border-radius: 50px !important;\n  box-shadow: none !important;\n  background: #d7d7d7;\n  margin-top: 5px;\n  text-decoration: none;\n  display: inline-block;\n}\n\n.not_found {\n  padding: 22px;\n  font-size: 20px;\n}\n\n.queries-content img {\n  width: 250px;\n  margin: 0 auto;\n  text-align: center;\n  display: block;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxteS1xdWVyaWVzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFrQjtFQUNsQixXQUFXO0FBQ2Y7O0FBQ0E7RUFDSSxXQUFXO0FBRWY7O0FBQUE7RUFDSSxXQUFXO0FBR2Y7O0FBREE7RUFDSSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixTQUFTO0FBSWI7O0FBRkE7RUFDSSxpQkFBaUI7QUFLckI7O0FBSEE7RUFDSSxrQkFBa0I7QUFNdEI7O0FBSkE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxlQUFlO0VBQ2YsWUFBWTtBQU9oQjs7QUFMQTtFQUNJLGFBQWE7QUFRakI7O0FBTkE7RUFDSSw2QkFBNkI7QUFTakM7O0FBUEE7RUFDSSw0QkFBNEI7RUFDNUIsc0JBQXNCO0VBQ3RCLGFBQWE7RUFDYixXQUFXO0VBQ1gsMkJBQTJCO0VBQzNCLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sNEJBQTRCO0FBVWhDOztBQVJBO0VBQ0ksU0FBUztFQUNULGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsVUFBVTtBQVdkOztBQVRBO0VBQ0ksU0FBUztFQUNULGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsY0FBYztFQUNkLGNBQWM7RUFDZCxnQkFBZ0I7QUFZcEI7O0FBVkE7RUFDSSxTQUFTO0FBYWI7O0FBWEE7RUFDSSxvQkFBZ0I7RUFDaEIsd0JBQW9CO0FBY3hCOztBQVpBO0VBQ0ksa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsY0FBYztBQWVsQjs7QUFiQTtFQUNJLGNBQWM7QUFnQmxCOztBQWRBO0VBQ0ksZ0JBQWU7QUFpQm5COztBQWZBO0VBQ0ksc0NBQXNDO0VBQ3RDLDBCQUEwQjtFQUMxQixrQkFBa0I7RUFDbEIsdUJBQVE7RUFDUixnQkFBZ0I7RUFDaEIsVUFBVTtFQUNWLGlCQUFpQjtBQWtCckI7O0FBaEJBO0VBQ0ksZUFBYztFQUNkLFNBQVE7QUFtQlo7O0FBaEJBO0VBQ0ksWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxnQkFBZ0I7QUFtQnBCOztBQWpCQTtFQUNJLFdBQVU7RUFDVixZQUFXO0VBQ1gsZUFBYztBQW9CbEI7O0FBbEJBO0VBQ0kscUJBQWdCO0VBQ2hCLGtCQUFhO0FBcUJqQjs7QUFuQkE7RUFDSSxrQkFBa0I7QUFzQnRCOztBQXBCQTtFQUNJLGVBQWM7RUFDZCxjQUFhO0FBdUJqQjs7QUFwQkE7RUFDSSxhQUFZO0FBdUJoQjs7QUFyQkE7RUFDSSxjQUFhO0VBQ2IsZUFBYztFQUNkLGdCQUFnQjtFQUNoQixpQkFBaUI7QUF3QnJCOztBQXRCQTtFQUNJLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQix5QkFBeUI7RUFDekIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsZ0JBQWdCO0FBeUJwQjs7QUF2QkE7RUFDSSxlQUFjO0VBQ2QsU0FBUTtBQTBCWjs7QUF4QkE7RUFDSSxXQUFVO0VBQ1YsWUFBVztFQUNYLGVBQWM7QUEyQmxCOztBQXpCQTtFQUNJLHFCQUFnQjtFQUNoQixrQkFBYTtBQTRCakI7O0FBMUJBO0VBQ0ksc0NBQXNDO0VBQ3RDLDBCQUEwQjtFQUMxQixrQkFBa0I7RUFDbEIsdUJBQVE7RUFDUixnQkFBZ0I7RUFDaEIsVUFBVTtFQUNWLGlCQUFpQjtBQTZCckI7O0FBM0JBO0VBQ0ksY0FBYTtBQThCakI7O0FBNUJBO0VBQ0ksVUFBUztBQStCYjs7QUE3QkE7RUFDSSxnQkFBZTtBQWdDbkI7O0FBOUJBO0VBQ0ksV0FBVTtBQWlDZDs7QUEvQkE7RUFDSSxvREFBb0Q7RUFDcEQsa0JBQWtCO0VBQ2xCLGtCQUFpQjtFQUNqQixjQUFhO0VBQ2IsbUJBQWtCO0FBa0N0Qjs7QUFoQ0E7RUFDSSxlQUFjO0VBQ2QsZ0JBQWU7RUFDZixhQUFZO0VBQ1osa0JBQWtCO0FBbUN0Qjs7QUFqQ0E7RUFDSSxlQUFjO0FBb0NsQjs7QUFsQ0E7RUFDSSxlQUFjO0VBQ2QsZ0JBQWU7RUFDZixhQUFZO0VBQ1osa0JBQWtCO0FBcUN0Qjs7QUFuQ0E7RUFDSSxlQUFjO0VBQ2QsZ0JBQWU7RUFDZixjQUFhO0FBc0NqQjs7QUFwQ0E7RUFDSSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLGlCQUFnQjtBQXVDcEI7O0FBckNBO0VBQ0ksZUFBZTtBQXdDbkI7O0FBdENBO0VBQ0ksa0JBQWlCO0VBQ2pCLGNBQWE7RUFDYixnQkFBZ0I7RUFDaEIsZUFBYztFQUNkLDhCQUE0QjtFQUM1QiwyQkFBeUI7RUFDekIsbUJBQW1CO0VBQ25CLFdBQVU7RUFDVixnQkFBZTtBQXlDbkI7O0FBdkNBO0VBQ0ksa0JBQWlCO0VBQ2pCLGNBQWE7RUFDYixnQkFBZ0I7RUFDaEIsZUFBYztFQUNkLDhCQUE0QjtFQUM1QiwyQkFBeUI7RUFDekIsbUJBQW1CO0VBQ25CLFdBQVU7RUFDVixnQkFDSjtBQXlDQTs7QUF4Q0E7RUFDSSxrQkFBaUI7RUFDakIseUJBQXVCO0VBQ3ZCLGdCQUFnQjtFQUNoQiwwQkFBd0I7RUFDeEIsOEJBQTRCO0VBQzVCLDJCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsZUFBYztFQUNkLHFCQUFvQjtFQUNwQixxQkFBcUI7QUEyQ3pCOztBQXpDQTtFQUNJLGFBQWE7RUFDYixlQUFjO0FBNENsQjs7QUExQ0E7RUFDSSxZQUFZO0VBQ1osY0FBYztFQUNkLGtCQUFrQjtFQUNsQixjQUFjO0FBNkNsQiIsImZpbGUiOiJteS1xdWVyaWVzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi51c2VyLWljb24gaW1nIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG59XHJcbi5iYWNrIGltZyB7XHJcbiAgICB3aWR0aDogNDBweDtcclxufVxyXG4uYmFjayB7XHJcbiAgICBmbG9hdDogbGVmdDtcclxufVxyXG4uc2hvcHBpbmctY2FydCBpbWcge1xyXG4gICAgd2lkdGg6IDMwcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IC04cHg7XHJcbn1cclxuLm1lbnUtaWNvbiB7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG4uc2hvcHBpbmctY2FydCB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuLmNhcnQtbnVtYmVyIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAxNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmYmIwMDtcclxuICAgIGhlaWdodDogMjBweDtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGNvbG9yOiAjM2Y1YzliO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgcGFkZGluZzogMXB4O1xyXG59XHJcbmlvbi1oZWFkZXIge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG4uaGVhZGVyLWFycm93IHtcclxuICAgIHBhZGRpbmc6IDIwcHggMTVweCAhaW1wb3J0YW50O1xyXG59XHJcbi50b3AtYmcge1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBoZWlnaHQ6IDEyMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMzBweCAzMHB4O1xyXG59XHJcbi5wYWNrYWdlcy1oZWFkZXIgaDJ7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLTE1cHg7XHJcbn1cclxuLnBhY2thZ2VzLWhlYWRlciBzcGFue1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgY29sb3I6ICMzMDVGNzI7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuaW9uLWxhYmVsIHtcclxuICAgIG1hcmdpbjogMDtcclxufVxyXG5pb24taXRlbSB7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcclxuICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcclxufVxyXG4udGl0bGUtdGV4dHtcclxuICAgIHBhZGRpbmctbGVmdDogMTBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxufVxyXG4ubS0xMHtcclxuICAgIG1hcmdpbjogMCAxMHB4O1xyXG59XHJcbi5tdC0xNntcclxuICAgIG1hcmdpbi10b3A6MTZweDtcclxufVxyXG4uZm9ybS1jb250cm9sLWNhcnQge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgIzJmM2E3NTVjICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgIC0tY29sb3I6IHJnYig0OCA5NSAxMTQpO1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDRweDtcclxufVxyXG4uZm9udC1zaXplLTF7XHJcbiAgICBmb250LXNpemU6MTJweDtcclxuICAgIG1hcmdpbjowO1xyXG59XHJcblxyXG4udGVzdC1oaXN0b3J5LWJ0biBidXR0b257XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBwYWRkaW5nOiAxMnB4IDQwcHg7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzNmNWM5YjtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICBtYXJnaW46IDEwcHggYXV0byAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB0cmFuc2l0aW9uOiAwLjNzO1xyXG59XHJcbmlvbi1zZWxlY3R7XHJcbiAgICB3aWR0aDozNHB4O1xyXG4gICAgcGFkZGluZzo1cHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6MDtcclxufVxyXG5pb24taXRlbXtcclxuICAgIC0tcGFkZGluZy1zdGFydDogMTJweDtcclxuICAgIC0tbWluLWhlaWdodDogMzVweDtcclxufVxyXG4ubWItMHtcclxuICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxufVxyXG4ucXVlcmllcy1jb250ZW50IHNwYW57XHJcbiAgICBmb250LXNpemU6MTVweDtcclxuICAgIGNvbG9yOiM2OTY5Njk7XHJcbn1cclxuXHJcbi5zcGFjZS1yaWdodC1sZWZ0e1xyXG4gICAgbWFyZ2luOjAgNXB4O1xyXG59XHJcbi50aXRsZS10ZXh0e1xyXG4gICAgY29sb3I6IzJhMmEyYTtcclxuICAgIGZvbnQtc2l6ZToxNXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG59XHJcbi50ZXN0LWhpc3RvcnktYnRuIGJ1dHRvbntcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIHBhZGRpbmc6IDEycHggNDBweDtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2Y1YzliO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIG1hcmdpbjogMTBweCBhdXRvIDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHRyYW5zaXRpb246IDAuM3M7XHJcbn1cclxuLmZvbnQtc2l6ZS0xe1xyXG4gICAgZm9udC1zaXplOjEzcHg7XHJcbiAgICBtYXJnaW46MDtcclxufVxyXG5pb24tc2VsZWN0e1xyXG4gICAgd2lkdGg6MzRweDtcclxuICAgIHBhZGRpbmc6NXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OjA7XHJcbn1cclxuaW9uLWl0ZW17XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDEycHg7XHJcbiAgICAtLW1pbi1oZWlnaHQ6IDM1cHg7XHJcbn1cclxuLmZvcm0tY29udHJvbC1jYXJ0IHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICMyZjNhNzU1YyAhaW1wb3J0YW50O1xyXG4gICAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICAtLWNvbG9yOiByZ2IoNDggOTUgMTE0KTtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA0cHg7XHJcbn1cclxuLm1hcmdpbi0xMHtcclxuICAgIG1hcmdpbjowIDEwcHg7XHJcbn1cclxuYnV0dG9ue1xyXG4gICAgcGFkZGluZzowO1xyXG59XHJcbi5tdC0xNntcclxuICAgIG1hcmdpbi10b3A6MTZweDtcclxufVxyXG4ubS0we1xyXG4gICAgbWFyZ2luOjBweDtcclxufVxyXG4udGFiLWxpc3Qtdmlld3tcclxuICAgIGJveC1zaGFkb3c6IDAgMCAwLjg3NXJlbSAwIHJnYmEoMTgxLCAxODEsIDE4MSwgMC41Nik7XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgICBwYWRkaW5nOjIwcHggMTBweDtcclxuICAgIG1hcmdpbjowIDEwcHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOjE1cHg7XHJcbn1cclxuLnRhYi1saXN0LWNvbnRlbnQgaDJ7XHJcbiAgICBmb250LXNpemU6MTNweDtcclxuICAgIGZvbnQtd2VpZ2h0OjQwMDtcclxuICAgIG1hcmdpbi10b3A6MDtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxufVxyXG4udGFiLWxpc3QtY29udGVudCBoMyBzdHJvbmd7XHJcbiAgICBmb250LXNpemU6MTRweDtcclxufVxyXG4udGFiLWxpc3QtY29udGVudCBoM3tcclxuICAgIGZvbnQtc2l6ZToxM3B4O1xyXG4gICAgZm9udC13ZWlnaHQ6NDAwO1xyXG4gICAgbWFyZ2luLXRvcDowO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG59XHJcbi50YWItbGlzdC1jb250ZW50IGF7XHJcbiAgICBmb250LXNpemU6MTRweDtcclxuICAgIGZvbnQtd2VpZ2h0OjQwMDtcclxuICAgIGNvbG9yOiM1MWMzZDE7XHJcbn1cclxuLmNlbnRlci10ZXh0e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nLWxlZnQ6OXB4O1xyXG59XHJcbi5jZW50ZXItdGV4dCBzcGFue1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcbi5jb21wbGV0ZS1idG57XHJcbiAgICBwYWRkaW5nOjEwcHggMjhweDtcclxuICAgIGNvbG9yOiMyYTJhMmE7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgZm9udC1zaXplOjEzcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOjUwcHghaW1wb3J0YW50O1xyXG4gICAgYm94LXNoYWRvdzpub25lIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQ6ICMyOGE3NDU7XHJcbiAgICBjb2xvcjojZmZmO1xyXG4gICAgbWFyZ2luLXRvcDoxOHB4O1xyXG59XHJcbi5zdWJtaXRlZC1idG57XHJcbiAgICBwYWRkaW5nOjEwcHggMjdweDtcclxuICAgIGNvbG9yOiMyYTJhMmE7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgZm9udC1zaXplOjEzcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOjUwcHghaW1wb3J0YW50O1xyXG4gICAgYm94LXNoYWRvdzpub25lIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQ6ICNmZmMxMDc7XHJcbiAgICBjb2xvcjojZmZmO1xyXG4gICAgbWFyZ2luLXRvcDoxOHB4XHJcbn1cclxuLnBlbmRpbmctc3RhdHVze1xyXG4gICAgcGFkZGluZzoxMHB4IDE1cHg7XHJcbiAgICBjb2xvcjojMmEyYTJhIWltcG9ydGFudDtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBmb250LXNpemU6MTNweCFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOjUwcHghaW1wb3J0YW50O1xyXG4gICAgYm94LXNoYWRvdzpub25lIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQ6ICNkN2Q3ZDc7XHJcbiAgICBtYXJnaW4tdG9wOjVweDtcclxuICAgIHRleHQtZGVjb3JhdGlvbjpub25lO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcbi5ub3RfZm91bmR7XHJcbiAgICBwYWRkaW5nOiAyMnB4O1xyXG4gICAgZm9udC1zaXplOjIwcHg7XHJcbn1cclxuLnF1ZXJpZXMtY29udGVudCBpbWd7XHJcbiAgICB3aWR0aDogMjUwcHg7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59Il19 */");

/***/ }),

/***/ "rBPZ":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-queries/my-queries.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <!-- <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button (click)=\"getHome()\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-buttons slot=\"primary\">\r\n      <ion-button>\r\n        <ul class=\"shopping-icon\">\r\n          <li (click)=\"GoToCart()\">\r\n            <img src=\"assets/icon/my-cart-1.png\" class=\"cart\">\r\n            <span class=\"cart_count\">{{cartCount}}</span>\r\n          </li>\r\n          <li routerLink=\"/user-profile\">\r\n            <img src=\"assets/icon/user.png\" >\r\n          </li>\r\n        </ul>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ios title-default hydrated\">My Queries</ion-title>\r\n  </ion-toolbar> -->\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <ion-row style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n    <ion-col size=\"12\" class=\"header-arrow padding-0\">\r\n      <div>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <div class=\"back\" (click)=\"goBack()\">\r\n                <img src=\"assets/back.svg\" alt=\"back\">\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <div class=\"menu-icon\">\r\n              <span class=\"shopping-cart\" (click)=\"GoToCart()\"><img src=\"assets/shopping-cart.svg\" alt=\"shopping\"><span\r\n                  class=\"cart-number\">{{cartCount}}</span></span>\r\n              <span class=\"user-icon\" slot=\"start\" (click)=\"GoToUserProfile()\">\r\n                <img src=\"assets/user-profile.png\" alt=\"user-icon\">\r\n              </span>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <div class=\"packages-header\">\r\n              <h2>My Queries</h2>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"m-10\">\r\n    <ion-col size=\"8\" class=\"padding-0\">\r\n      <h4 class=\"title-text mb-0\">My Queries</h4>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"m-10 mt-16\">\r\n    <ion-col size=\"4\" class=\"padding-0\">\r\n      <ion-list class=\"form-control-cart\">\r\n        <ion-item lines=\"none\" style=\"border-radius: 25px;\">\r\n          <ion-label class=\"text-gray font-size-1\">Packages</ion-label>\r\n          <ion-select value=\"\" okText=\"Ok\" (ionChange)=\"getTestData($event)\">\r\n            <ion-select-option *ngFor=\"let list of packageList\" value=\"{{list.package_id}}\">{{list.package.name}}</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"4\" class=\"padding-0\">\r\n      <ion-list class=\"form-control-cart\">\r\n        <ion-item lines=\"none\" style=\"border-radius: 25px;\">\r\n          <ion-label class=\"text-gray font-size-1\">Test</ion-label>\r\n          <ion-select value=\"\" okText=\"Ok\" (ionChange)=\"selectTest($event)\">\r\n            <ion-select-option *ngFor=\"let tests of package_test\" value=\"{{tests.id}}\">{{tests.title}}</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"4\" class=\"padding-0\">\r\n      <ion-list class=\"form-control-cart\">\r\n        <ion-item lines=\"none\" style=\"border-radius: 25px;\">\r\n          <ion-label class=\"text-gray font-size-1\">All</ion-label>\r\n          <ion-select value=\"\" okText=\"Ok\" (ionChange)=\"selectStatus($event)\">\r\n            <ion-select-option value=\"0\">Posted</ion-select-option>\r\n            <ion-select-option value=\"1\">Replied</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n      </ion-list>\r\n    </ion-col>\r\n\r\n  </ion-row>\r\n\r\n  <ion-row class=\"mt-16\">\r\n    <ion-col size=\"8\" offset=\"2\" class=\"padding-0\">\r\n      <div class=\"test-history-btn\">\r\n        <button ion-button type=\"submit\" (click)=\"submit_filter()\">Search</button>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"mt-16\" *ngIf=\"queries != ''\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"tab-list-view\" *ngFor = \"let query of queries\">\r\n        <ion-row >\r\n          <ion-col size=\"8\" class=\"padding-0\">\r\n            <div class=\"tab-list-content\">\r\n              <h3>\r\n                <strong>Package </strong>\r\n                 - {{query.test.package.name}}\r\n              </h3>\r\n              <h3>\r\n                <strong>Test Name </strong>\r\n                - {{query.test.title}}\r\n              </h3>\r\n              <h3>\r\n                <strong>Course name </strong> \r\n                - {{query.test.package.course.course_name}}\r\n              </h3>\r\n              <h3>\r\n                <strong>My Query </strong> \r\n                - {{query.query}}\r\n              </h3>\r\n              <h3 *ngIf=\"query.reply_comment != null\">\r\n                <strong>Examiner Reply </strong> \r\n                - {{query.reply_comment}}\r\n              </h3>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"4\" class=\"padding-0\">\r\n           <div class=\"tab-list-content\">\r\n            <h3><strong>Posted on</strong></h3>\r\n            <h3><strong>{{query.created_at}}</strong></h3>\r\n           </div>\r\n           <div class=\"complete-btn-main\">\r\n            <button ion-button full class=\"{{query.reply_comment == null ? 'complete-btn' : 'submited-btn'}} \">{{query.reply_comment == null ? 'Posted' : 'Replied'}}</button>\r\n          </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"mt-16\" *ngIf=\"queries == ''\">\r\n    <ion-col size=\"12\" class=\"padding-0\" >\r\n      <div class=\"queries-content\">\r\n        <img src=\"assets/icon/ops-queries.png\" alt=\"no-packages-found\">\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=my-queries-my-queries-module.js.map