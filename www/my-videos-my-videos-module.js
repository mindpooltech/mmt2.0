(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["my-videos-my-videos-module"],{

/***/ "5/lF":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/video-popover/video-popover.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>video-popover</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content> -->\r\n\r\n\r\n<div class=\"popover-scroll\">\r\n  <div class=\"popover-main-holder\">\r\n    <h4 class=\"text-center\">Chapter List</h4>\r\n    <ul class=\"list\" *ngFor=\"let chapters of chapter_list\">\r\n      <li class=\"item\">{{chapters.title}}</li>\r\n    </ul>\r\n  </div>\r\n  </div>");

/***/ }),

/***/ "PBBi":
/*!*****************************************************!*\
  !*** ./src/app/pages/my-videos/my-videos.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("::slotted(ion-label) {\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n\n.user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.back img {\n  width: 40px;\n}\n\n.back {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.header-arrow {\n  padding: 20px 15px !important;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 120px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n}\n\n.packages-header h2 {\n  margin: 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n  position: relative;\n  top: -15px;\n}\n\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\nion-label {\n  margin: 0;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n}\n\n.course-name-heading ion-label {\n  color: #305f72;\n  font-weight: 500;\n  font-size: 18px;\n  margin: 10px 15px;\n}\n\n.padding-0 {\n  padding: 0;\n}\n\n.mt-16 {\n  margin-top: 16px;\n}\n\n.m-0 {\n  margin: 0;\n}\n\n.text-white {\n  color: #fff;\n}\n\n.outer-border {\n  font-size: 14px !important;\n  margin: 10px 20px;\n  --color: rgb(44, 44, 44);\n  font-weight: 400;\n  padding: 0;\n  border: 1px solid #b1bcd4;\n  border-radius: 10px;\n  color: #8a959e;\n}\n\n.tab-list-view {\n  border: 1px solid #b1bcd440;\n  box-shadow: 0px 1px 12px -4px #37242442;\n  border-radius: 20px;\n  padding: 15px 15px;\n  margin: 10px 20px;\n}\n\n.tab-list-content span {\n  display: block;\n  font-size: 14px;\n  font-weight: 400;\n  text-transform: capitalize;\n}\n\n.tab-list-content a {\n  font-size: 13px;\n  font-weight: 600;\n  color: #020551;\n}\n\n.test-count h6 {\n  font-size: 14px;\n  font-weight: 500;\n  margin-bottom: 5px;\n}\n\n.test-count a {\n  color: #f27376;\n  font-size: 15px;\n  text-align: center;\n  margin: 0 auto;\n  display: block;\n  font-weight: 500;\n}\n\n.choose-subject-new {\n  font-size: 16px;\n  padding: 10px;\n}\n\n.tab-list-content h6 {\n  color: #305f72;\n  text-transform: capitalize;\n  padding: 3px 0;\n}\n\n.test-count span {\n  font-size: 18px;\n}\n\n.test-cart {\n  font-size: 15px;\n  font-weight: 400;\n}\n\n.text-right {\n  text-align: right;\n}\n\n.cart-btn {\n  padding: 8px 12px;\n  color: #ffffff;\n  font-weight: 400;\n  font-size: 15px;\n  border-radius: 50px !important;\n  box-shadow: none !important;\n  background: #51C3D1;\n  margin-top: 15px;\n}\n\n.test-count {\n  padding-left: 7px;\n}\n\n.courses-tab h6 {\n  font-weight: 400;\n  font-size: 14px;\n}\n\n.courses-tab {\n  padding-left: 10px;\n}\n\nion-tab-button {\n  --background-color:transparent;\n}\n\nion-tab-bar,\nion-tab-button {\n  background: rgba(0, 0, 0, 0);\n  color: #ffffff;\n  margin: -15px 0;\n}\n\n.active:after {\n  content: \"\";\n  width: 30px;\n  height: 2px;\n  display: block;\n  background: #fff;\n}\n\n.text-left {\n  text-align: left;\n}\n\nion-label {\n  font-weight: 400;\n}\n\nbutton:focus {\n  outline: none;\n}\n\nul.shopping-icon {\n  list-style-type: none;\n}\n\n.menu-icon button {\n  padding-top: 5px;\n}\n\n.menu-icon button:focus {\n  outline: none;\n}\n\n.nav-title h6 {\n  margin: 10px 0 0 0;\n  color: #fff;\n  font-weight: 400;\n  text-align: center;\n}\n\n.cart-btn {\n  padding: 8px 10px;\n  color: #ffffff;\n  font-weight: 400;\n  font-size: 13px;\n  border-radius: 5px !important;\n  box-shadow: none !important;\n  background: #020551;\n  margin-top: 5px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxteS12aWRlb3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBYztFQUNkLGtCQUFpQjtBQUNyQjs7QUFDQTtFQUNJLGtCQUFrQjtFQUNsQixXQUFXO0FBRWY7O0FBQUE7RUFDSSxXQUFXO0FBR2Y7O0FBREE7RUFDSSxXQUFXO0FBSWY7O0FBRkE7RUFDSSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixTQUFTO0FBS2I7O0FBSEE7RUFDSSxpQkFBaUI7QUFNckI7O0FBSkE7RUFDSSxrQkFBa0I7QUFPdEI7O0FBTEE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxlQUFlO0VBQ2YsWUFBWTtBQVFoQjs7QUFOQTtFQUNJLGFBQWE7QUFTakI7O0FBUEE7RUFDSSw2QkFBNkI7QUFVakM7O0FBUkE7RUFDSSw0QkFBNEI7RUFDNUIsc0JBQXNCO0VBQ3RCLGFBQWE7RUFDYixXQUFXO0VBQ1gsMkJBQTJCO0VBQzNCLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sNEJBQTRCO0FBV2hDOztBQVRBO0VBQ0ksU0FBUztFQUNULGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsVUFBVTtBQVlkOztBQVZBO0VBQ0ksU0FBUztFQUNULGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsY0FBYztFQUNkLGNBQWM7RUFDZCxnQkFBZ0I7QUFhcEI7O0FBWEE7RUFDSSxTQUFTO0FBY2I7O0FBWkE7RUFDSSxvQkFBZ0I7RUFDaEIsd0JBQW9CO0FBZXhCOztBQWJBO0VBQ0ksY0FBYztFQUNkLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsaUJBQWlCO0FBZ0JyQjs7QUFkQTtFQUNJLFVBQVM7QUFpQmI7O0FBZkE7RUFDSSxnQkFBZTtBQWtCbkI7O0FBaEJBO0VBQ0ksU0FBUztBQW1CYjs7QUFqQkE7RUFDSSxXQUFVO0FBb0JkOztBQWxCQTtFQUNJLDBCQUEwQjtFQUMxQixpQkFBaUI7RUFDakIsd0JBQVE7RUFDUixnQkFBZ0I7RUFDaEIsVUFBVTtFQUNWLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsY0FBYztBQXFCbEI7O0FBbkJBO0VBQ0ksMkJBQTJCO0VBQzNCLHVDQUF1QztFQUN2QyxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGlCQUFpQjtBQXNCckI7O0FBcEJBO0VBQ0ksY0FBYztFQUNkLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsMEJBQTBCO0FBdUI5Qjs7QUFyQkE7RUFDSSxlQUFjO0VBQ2QsZ0JBQWU7RUFDZixjQUFhO0FBd0JqQjs7QUF0QkE7RUFDSSxlQUFjO0VBQ2QsZ0JBQWU7RUFDZixrQkFBa0I7QUF5QnRCOztBQXZCQTtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCLGNBQWM7RUFDZCxjQUFjO0VBQ2QsZ0JBQWdCO0FBMEJwQjs7QUF4QkE7RUFDSSxlQUFlO0VBQ2YsYUFBYTtBQTJCakI7O0FBekJBO0VBQ0ksY0FBYztFQUNkLDBCQUEwQjtFQUMxQixjQUFjO0FBNEJsQjs7QUExQkE7RUFDSSxlQUFjO0FBNkJsQjs7QUEzQkE7RUFDSSxlQUFjO0VBQ2QsZ0JBQWdCO0FBOEJwQjs7QUE1QkE7RUFDSSxpQkFBaUI7QUErQnJCOztBQTdCQTtFQUNJLGlCQUFnQjtFQUNoQixjQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLGVBQWM7RUFDZCw4QkFBNEI7RUFDNUIsMkJBQXlCO0VBQ3pCLG1CQUFtQjtFQUNuQixnQkFBZTtBQWdDbkI7O0FBOUJBO0VBQ0ksaUJBQWdCO0FBaUNwQjs7QUEvQkE7RUFDSSxnQkFBZTtFQUNmLGVBQWM7QUFrQ2xCOztBQWhDQTtFQUNJLGtCQUFpQjtBQW1DckI7O0FBakNBO0VBQ0ksOEJBQW1CO0FBb0N2Qjs7QUFsQ0E7O0VBRUksNEJBQXlCO0VBQ3pCLGNBQWM7RUFDZCxlQUFjO0FBcUNsQjs7QUFuQ0E7RUFDSSxXQUFVO0VBQ1YsV0FBVTtFQUNWLFdBQVU7RUFDVixjQUFjO0VBQ2QsZ0JBQWdCO0FBc0NwQjs7QUFwQ0E7RUFDSSxnQkFBZ0I7QUF1Q3BCOztBQXJDQTtFQUNJLGdCQUFlO0FBd0NuQjs7QUF0Q0E7RUFDSSxhQUFZO0FBeUNoQjs7QUF2Q0E7RUFDSSxxQkFBcUI7QUEwQ3pCOztBQXhDQTtFQUNJLGdCQUFnQjtBQTJDcEI7O0FBekNBO0VBQ0ksYUFBYTtBQTRDakI7O0FBMUNBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVU7RUFDVixnQkFBZ0I7RUFDaEIsa0JBQWtCO0FBNkN0Qjs7QUEzQ0E7RUFDSSxpQkFBaUI7RUFDakIsY0FBYztFQUNkLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsNkJBQTZCO0VBQzdCLDJCQUEyQjtFQUMzQixtQkFBbUI7RUFDbkIsZUFBZTtBQThDbkIiLCJmaWxlIjoibXktdmlkZW9zLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjo6c2xvdHRlZChpb24tbGFiZWwpIHtcclxuICAgIG1hcmdpbi10b3A6MHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTowcHg7XHJcbn1cclxuLnVzZXItaWNvbiBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgd2lkdGg6IDQwcHg7XHJcbn1cclxuLmJhY2sgaW1nIHtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG59XHJcbi5iYWNrIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IGltZyB7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLThweDtcclxufVxyXG4ubWVudS1pY29uIHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4uY2FydC1udW1iZXIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDE1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZiYjAwO1xyXG4gICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICMzZjVjOWI7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBwYWRkaW5nOiAxcHg7XHJcbn1cclxuaW9uLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcbi5oZWFkZXItYXJyb3cge1xyXG4gICAgcGFkZGluZzogMjBweCAxNXB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLnRvcC1iZyB7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIGhlaWdodDogMTIwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAgMCAzMHB4IDMwcHg7XHJcbn1cclxuLnBhY2thZ2VzLWhlYWRlciBoMntcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAtMTVweDtcclxufVxyXG4ucGFja2FnZXMtaGVhZGVyIHNwYW57XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG5pb24tbGFiZWwge1xyXG4gICAgbWFyZ2luOiAwO1xyXG59XHJcbmlvbi1pdGVtIHtcclxuICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xyXG4gICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xyXG59XHJcbi5jb3Vyc2UtbmFtZS1oZWFkaW5nIGlvbi1sYWJlbHtcclxuICAgIGNvbG9yOiAjMzA1ZjcyO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIG1hcmdpbjogMTBweCAxNXB4O1xyXG59XHJcbi5wYWRkaW5nLTB7XHJcbiAgICBwYWRkaW5nOjA7XHJcbn1cclxuLm10LTE2e1xyXG4gICAgbWFyZ2luLXRvcDoxNnB4O1xyXG59XHJcbi5tLTB7XHJcbiAgICBtYXJnaW46IDA7XHJcbn1cclxuLnRleHQtd2hpdGV7XHJcbiAgICBjb2xvcjojZmZmO1xyXG59XHJcbi5vdXRlci1ib3JkZXJ7XHJcbiAgICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcclxuICAgIG1hcmdpbjogMTBweCAyMHB4O1xyXG4gICAgLS1jb2xvcjogcmdiKDQ0LCA0NCwgNDQpO1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjYjFiY2Q0O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIGNvbG9yOiAjOGE5NTllO1xyXG59XHJcbi50YWItbGlzdC12aWV3e1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2IxYmNkNDQwO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDFweCAxMnB4IC00cHggIzM3MjQyNDQyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIHBhZGRpbmc6IDE1cHggMTVweDtcclxuICAgIG1hcmdpbjogMTBweCAyMHB4O1xyXG59XHJcbi50YWItbGlzdC1jb250ZW50IHNwYW57XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxufVxyXG4udGFiLWxpc3QtY29udGVudCBhe1xyXG4gICAgZm9udC1zaXplOjEzcHg7XHJcbiAgICBmb250LXdlaWdodDo2MDA7XHJcbiAgICBjb2xvcjojMDIwNTUxO1xyXG59XHJcbi50ZXN0LWNvdW50IGg2e1xyXG4gICAgZm9udC1zaXplOjE0cHg7XHJcbiAgICBmb250LXdlaWdodDo1MDA7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbn1cclxuLnRlc3QtY291bnQgYXtcclxuICAgIGNvbG9yOiAjZjI3Mzc2O1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn0gICBcclxuLmNob29zZS1zdWJqZWN0LW5ld3tcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuLnRhYi1saXN0LWNvbnRlbnQgaDZ7XHJcbiAgICBjb2xvcjogIzMwNWY3MjtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG4gICAgcGFkZGluZzogM3B4IDA7XHJcbn1cclxuLnRlc3QtY291bnQgc3BhbntcclxuICAgIGZvbnQtc2l6ZToxOHB4O1xyXG59XHJcbi50ZXN0LWNhcnR7XHJcbiAgICBmb250LXNpemU6MTVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbn1cclxuLnRleHQtcmlnaHR7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG4uY2FydC1idG57XHJcbiAgICBwYWRkaW5nOjhweCAxMnB4O1xyXG4gICAgY29sb3I6I2ZmZmZmZjtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBmb250LXNpemU6MTVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6NTBweCFpbXBvcnRhbnQ7XHJcbiAgICBib3gtc2hhZG93Om5vbmUhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZDogIzUxQzNEMTtcclxuICAgIG1hcmdpbi10b3A6MTVweDtcclxufVxyXG4udGVzdC1jb3VudHtcclxuICAgIHBhZGRpbmctbGVmdDo3cHg7XHJcbn1cclxuLmNvdXJzZXMtdGFiIGg2e1xyXG4gICAgZm9udC13ZWlnaHQ6NDAwO1xyXG4gICAgZm9udC1zaXplOjE0cHg7XHJcbn1cclxuLmNvdXJzZXMtdGFie1xyXG4gICAgcGFkZGluZy1sZWZ0OjEwcHg7XHJcbn1cclxuaW9uLXRhYi1idXR0b257XHJcbiAgICAtLWJhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7XHJcbn1cclxuaW9uLXRhYi1iYXIsXHJcbmlvbi10YWItYnV0dG9uIHtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMCwwLDAsMCk7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIG1hcmdpbjotMTVweCAwO1xyXG59XHJcbi5hY3RpdmU6YWZ0ZXJ7XHJcbiAgICBjb250ZW50OlwiXCI7XHJcbiAgICB3aWR0aDozMHB4O1xyXG4gICAgaGVpZ2h0OjJweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxufVxyXG4udGV4dC1sZWZ0e1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxufVxyXG5pb24tbGFiZWx7XHJcbiAgICBmb250LXdlaWdodDo0MDA7XHJcbn1cclxuYnV0dG9uOmZvY3Vze1xyXG4gICAgb3V0bGluZTpub25lO1xyXG59XHJcbnVsLnNob3BwaW5nLWljb257XHJcbiAgICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XHJcbn1cclxuLm1lbnUtaWNvbiBidXR0b257XHJcbiAgICBwYWRkaW5nLXRvcDogNXB4O1xyXG59XHJcbi5tZW51LWljb24gYnV0dG9uOmZvY3Vze1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxufVxyXG4ubmF2LXRpdGxlIGg2e1xyXG4gICAgbWFyZ2luOiAxMHB4IDAgMCAwO1xyXG4gICAgY29sb3I6I2ZmZjtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLmNhcnQtYnRue1xyXG4gICAgcGFkZGluZzogOHB4IDEwcHg7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHggIWltcG9ydGFudDtcclxuICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQ6ICMwMjA1NTE7XHJcbiAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "SF9P":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/my-videos/my-videos.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button ></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-buttons slot=\"primary\">\r\n      <ion-button>\r\n        <ul class=\"shopping-icon\">\r\n          <!-- <li (click)=\"GoToCart()\">\r\n            <img src=\"assets/icon/my-cart-1.png\" class=\"cart\">\r\n            <span class=\"cart_count\">{{cartCount ? cartCount : '0'}}</span>\r\n          </li> -->\r\n          <!-- <li>\r\n            <img src=\"assets/icon/notification.png\" class=\"cart\">\r\n          </li> -->\r\n          <li routerLink=\"/user-profile\">\r\n            <img src=\"assets/icon/user.png\" >\r\n          </li>\r\n        </ul>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ios title-default hydrated\">Videos</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n<ion-content>\r\n  <ion-row style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n    <ion-col size=\"12\" class=\"header-arrow padding-0\">\r\n      <div>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <div class=\"back\" (click)=\"goBack()\">\r\n                <img src=\"assets/back.svg\" alt=\"back\">\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <div class=\"menu-icon\">\r\n              <span class=\"shopping-cart\" (click)=\"GoToCart()\"><img src=\"assets/shopping-cart.svg\" alt=\"shopping\"><span\r\n                  class=\"cart-number\">{{cartCount}}</span></span>\r\n              <span class=\"user-icon\" slot=\"start\" (click)=\"GoToUserProfile()\">\r\n                <img src=\"assets/user-profile.png\" alt=\"user-icon\">\r\n              </span>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <div class=\"packages-header\">\r\n              <h2>Videos</h2>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"mt-16\">\r\n    <ion-col size=\"12\" class=\"course-name-heading\">\r\n        <ion-label>{{course_name}}</ion-label>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <ion-list class=\"border space-m10 outer-border\">\r\n        <ion-item lines=\"none\">\r\n          <ion-label class=\"m-0 choose-subject-new\">Choose Subject</ion-label>\r\n          <ion-select (ionChange)=\"getVideos($event)\">\r\n            <ion-select-option *ngFor=\"let list of subjectList\" [value]=\"list.id\">{{list.subject_name}}</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n    </ion-list>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row class=\"mt-15 m-10\">\r\n    <ion-col size=\"12\" class=\"padding-0 bottom-space\" *ngFor=\"let video of video_list, let i = index\">\r\n      <div class=\"tab-list-view\" >\r\n        <ion-row >\r\n          <ion-col size=\"4\" class=\"padding-0\">\r\n            <div class=\"tab-list-content\">\r\n              <h6 class=\"m-0\">Description</h6>\r\n              <span class=\"m-0 d-block\">{{video.description}}</span>\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"4\" class=\"padding-0\">\r\n            <div class=\"test-count text-center\">\r\n              <a (click)=\"openPopover(video.id,$event)\" value=\"video.id\">View Chapters</a>\r\n                            \r\n            </div>\r\n          </ion-col>\r\n         \r\n        </ion-row>\r\n        <br>\r\n        <ion-row>\r\n          <!-- <span>video :-</span> -->\r\n          <ion-col class=\"padding-0\">\r\n            <iframe allowfullscreen frameborder=\"0\" height=\"160\" width=\"100%\" [src]=\"video.video_link | safe\">\r\n            </iframe>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row> ");

/***/ }),

/***/ "dE9m":
/*!***********************************************************!*\
  !*** ./src/app/pages/video-popover/video-popover.page.ts ***!
  \***********************************************************/
/*! exports provided: VideoPopoverPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoPopoverPage", function() { return VideoPopoverPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_video_popover_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./video-popover.page.html */ "5/lF");
/* harmony import */ var _video_popover_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./video-popover.page.scss */ "mB4j");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");









let VideoPopoverPage = class VideoPopoverPage {
    constructor(navParams, popovercontroller, http, httpService, uihelper) {
        this.navParams = navParams;
        this.popovercontroller = popovercontroller;
        this.http = http;
        this.httpService = httpService;
        this.uihelper = uihelper;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
        });
        this.url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_7__["Constant"].apiUrl;
        this.options = { headers: this.headers };
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.uihelper.ShowSpinner();
            this.video_id = this.navParams.data.video_id;
            // console.log('this.video_id',this.video_id)
            this.http.get(this.url + 'video-chapters/' + this.video_id, this.options).subscribe((res) => {
                this.chapter_list = res['payload'].chapters;
                // console.log(this.chapter_list)
                this.uihelper.HideSpinner();
            });
        });
    }
};
VideoPopoverPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavParams"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["PopoverController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_6__["HttpService"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_8__["UIHelper"] }
];
VideoPopoverPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-video-popover',
        template: _raw_loader_video_popover_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_video_popover_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], VideoPopoverPage);



/***/ }),

/***/ "erg9":
/*!***************************************************!*\
  !*** ./src/app/pages/my-videos/my-videos.page.ts ***!
  \***************************************************/
/*! exports provided: MyVideosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyVideosPage", function() { return MyVideosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_my_videos_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./my-videos.page.html */ "SF9P");
/* harmony import */ var _my_videos_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./my-videos.page.scss */ "PBBi");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");
/* harmony import */ var _video_popover_video_popover_page__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../video-popover/video-popover.page */ "dE9m");












let MyVideosPage = class MyVideosPage {
    constructor(router, http, httpService, storageService, loadingController, navCtrl, uihelper, popovercontroller) {
        this.router = router;
        this.http = http;
        this.httpService = httpService;
        this.storageService = storageService;
        this.loadingController = loadingController;
        this.navCtrl = navCtrl;
        this.uihelper = uihelper;
        this.popovercontroller = popovercontroller;
        this.course_id = {
            course_id: localStorage.getItem('course_id')
        };
    }
    ngOnInit() {
        try {
            this.router.events.subscribe((event) => {
                this.selfPath = '';
                if (event && event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__["NavigationEnd"] && event.url) {
                    this.selfPath = event.url + '/my-videos';
                }
                this.selfPath = this.router.routerState.snapshot.url;
                this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'my-videos') + 'my-videos';
            });
        }
        catch (e) {
            console.log(e);
        }
    }
    getStringBeforeSubstring(parentString, substring) {
        return parentString.substring(0, parentString.indexOf(substring));
    }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.cartCount = localStorage.getItem('cartCount');
            this.uihelper.ShowAlert('', 'Please select Subject.');
            this.uihelper.ShowSpinner();
            this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__["Constant"].AUTH).then(res => {
                this.userData = res.user_details;
                this.course_name = this.userData.course_name;
                this.institute_id = res.user_details.institute_id;
                this.user_id = res.user_details.id;
                this.httpService.afterLoginGet("cart_count", res.user_details.id).subscribe((res) => {
                    this.cartCount = res['payload'].cart_count;
                });
                this.uihelper.HideSpinner();
            });
            this.httpService.afterLoginPost('subject-list', this.course_id).subscribe((res) => {
                this.subjectList = res.payload;
                // console.log('subjectList',this.subjectList)
                this.uihelper.HideSpinner();
            });
        });
    }
    getVideos(event) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.uihelper.ShowSpinner();
            let data = {
                institute_id: this.institute_id,
                course_id: this.course_id,
                subject_id: event.target.value
            };
            this.httpService.afterLoginPost('subject-wise-videos', data).subscribe((res) => {
                this.video_list = res.payload.videos;
                this.uihelper.HideSpinner();
                // console.log('video_list',this.video_list)
            }, (err) => {
                this.uihelper.HideSpinner();
                this.uihelper.ShowAlert('', "Not Found");
                this.video_list = [];
            });
        });
    }
    openPopover(video_id, ev) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const popover = yield this.popovercontroller.create({
                component: _video_popover_video_popover_page__WEBPACK_IMPORTED_MODULE_11__["VideoPopoverPage"],
                // cssClass: 'my-custom-class',
                componentProps: {
                    "video_id": video_id,
                },
            });
            return yield popover.present();
        });
    }
    goBack() {
        this.router.navigate(['/tabs']);
    }
    GoToCart() {
        this.router.navigate([this.selfPath + "/cart"]);
    }
    GoToUserProfile() {
        this.router.navigate([this.selfPath + "/user-profile"]);
    }
};
MyVideosPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_7__["HttpService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_8__["StorageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_10__["UIHelper"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["PopoverController"] }
];
MyVideosPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-my-videos',
        template: _raw_loader_my_videos_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_my_videos_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MyVideosPage);



/***/ }),

/***/ "ix0U":
/*!*****************************************************!*\
  !*** ./src/app/pages/my-videos/my-videos.module.ts ***!
  \*****************************************************/
/*! exports provided: MyVideosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyVideosPageModule", function() { return MyVideosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _my_videos_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./my-videos-routing.module */ "rglN");
/* harmony import */ var _my_videos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./my-videos.page */ "erg9");
/* harmony import */ var _safe_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../safe.pipe */ "Z2+D");
/* harmony import */ var _video_popover_video_popover_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../video-popover/video-popover.page */ "dE9m");









let MyVideosPageModule = class MyVideosPageModule {
};
MyVideosPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _my_videos_routing_module__WEBPACK_IMPORTED_MODULE_5__["MyVideosPageRoutingModule"]
        ],
        declarations: [_my_videos_page__WEBPACK_IMPORTED_MODULE_6__["MyVideosPage"], _safe_pipe__WEBPACK_IMPORTED_MODULE_7__["SafePipe"], _video_popover_video_popover_page__WEBPACK_IMPORTED_MODULE_8__["VideoPopoverPage"]]
    })
], MyVideosPageModule);



/***/ }),

/***/ "mB4j":
/*!*************************************************************!*\
  !*** ./src/app/pages/video-popover/video-popover.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".popover-main-holder h4 {\n  margin: 0px 0px 10px 0px;\n  padding: 15px 0px 15px 0px;\n  font-size: 16px;\n  color: #ffffff;\n  font-weight: 500;\n  text-align: center;\n  border-bottom: 1px solid #d0d0d0;\n  background-color: #df6a6d;\n}\n\nul.list {\n  margin: 5px 0;\n}\n\n.list li {\n  padding: 7px 0 7px 0;\n  font-size: 15px;\n  color: #2c5869;\n}\n\n.popover-scroll {\n  overflow: scroll;\n  max-height: 300px;\n  min-width: 250px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFx2aWRlby1wb3BvdmVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFNQTtFQUNJLHdCQUF3QjtFQUN4QiwwQkFBMEI7RUFDMUIsZUFBZTtFQUNmLGNBQWM7RUFDZCxnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLGdDQUFnQztFQUNoQyx5QkFBeUI7QUFMN0I7O0FBT0E7RUFDSSxhQUFhO0FBSmpCOztBQU1BO0VBQ0ksb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZixjQUFjO0FBSGxCOztBQU1BO0VBQ0ksZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQixnQkFBZ0I7QUFIcEIiLCJmaWxlIjoidmlkZW8tcG9wb3Zlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBhcHAtcG9wb3ZlcntcclxuLy8gICAgIC0td2lkdGg6MjUwcHghaW1wb3J0YW50O1xyXG4vLyB9XHJcbi5wb3BvdmVyLW1haW4taG9sZGVyIHtcclxuICAgIC8vIHBhZGRpbmc6MTBweCAwO1xyXG59XHJcbi5wb3BvdmVyLW1haW4taG9sZGVyIGg0IHtcclxuICAgIG1hcmdpbjogMHB4IDBweCAxMHB4IDBweDtcclxuICAgIHBhZGRpbmc6IDE1cHggMHB4IDE1cHggMHB4O1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkMGQwZDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGY2YTZkO1xyXG59XHJcbnVsLmxpc3Qge1xyXG4gICAgbWFyZ2luOiA1cHggMDtcclxufVxyXG4ubGlzdCBsaSB7XHJcbiAgICBwYWRkaW5nOiA3cHggMCA3cHggMDtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIGNvbG9yOiAjMmM1ODY5O1xyXG59XHJcblxyXG4ucG9wb3Zlci1zY3JvbGwge1xyXG4gICAgb3ZlcmZsb3c6IHNjcm9sbDtcclxuICAgIG1heC1oZWlnaHQ6IDMwMHB4O1xyXG4gICAgbWluLXdpZHRoOiAyNTBweDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "rglN":
/*!*************************************************************!*\
  !*** ./src/app/pages/my-videos/my-videos-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: MyVideosPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyVideosPageRoutingModule", function() { return MyVideosPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _my_videos_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./my-videos.page */ "erg9");




const routes = [
    {
        path: '',
        component: _my_videos_page__WEBPACK_IMPORTED_MODULE_3__["MyVideosPage"]
    }
];
let MyVideosPageRoutingModule = class MyVideosPageRoutingModule {
};
MyVideosPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MyVideosPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=my-videos-my-videos-module.js.map