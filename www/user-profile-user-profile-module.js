(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-profile-user-profile-module"],{

/***/ "7EoU":
/*!*******************************************************************!*\
  !*** ./src/app/pages/user-profile/user-profile-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: UserProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfilePageRoutingModule", function() { return UserProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _user_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user-profile.page */ "Ang3");




const routes = [
    {
        path: '',
        component: _user_profile_page__WEBPACK_IMPORTED_MODULE_3__["UserProfilePage"]
    }
];
let UserProfilePageRoutingModule = class UserProfilePageRoutingModule {
};
UserProfilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UserProfilePageRoutingModule);



/***/ }),

/***/ "Ang3":
/*!*********************************************************!*\
  !*** ./src/app/pages/user-profile/user-profile.page.ts ***!
  \*********************************************************/
/*! exports provided: UserProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfilePage", function() { return UserProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_user_profile_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./user-profile.page.html */ "z4lj");
/* harmony import */ var _user_profile_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user-profile.page.scss */ "Lglj");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");









let UserProfilePage = class UserProfilePage {
    constructor(navCtrl, router, authService, storageService) {
        this.navCtrl = navCtrl;
        this.router = router;
        this.authService = authService;
        this.storageService = storageService;
    }
    ngOnInit() {
        try {
            this.router.events.subscribe((event) => {
                this.selfPath = '';
                if (event && event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_6__["NavigationEnd"] && event.url) {
                    this.selfPath = event.url + '/user-profile';
                }
                this.selfPath = this.router.routerState.snapshot.url;
                this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'user-profile') + 'user-profile';
            });
        }
        catch (e) {
            console.log(e);
        }
        this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_8__["Constant"].AUTH).then(res => {
            this.userData = res.user_details;
            this.avatar = res.user_details.avatar_location ? src_Helper_Constant__WEBPACK_IMPORTED_MODULE_8__["Constant"].adminUrl + res.user_details.avatar_location : 'assets/user-profile.png';
            // console.log('user data',this.userData);
        });
    }
    getStringBeforeSubstring(parentString, substring) {
        return parentString.substring(0, parentString.indexOf(substring));
    }
    EditUserPage() {
        this.router.navigate([this.selfPath + '/edit-user-profile']);
    }
    goBack() {
        this.navCtrl.pop();
    }
};
UserProfilePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_5__["StorageService"] }
];
UserProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-user-profile',
        template: _raw_loader_user_profile_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_user_profile_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], UserProfilePage);



/***/ }),

/***/ "Lglj":
/*!***********************************************************!*\
  !*** ./src/app/pages/user-profile/user-profile.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.back img {\n  width: 40px;\n}\n\n.back {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.header-arrow {\n  padding: 20px 15px !important;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 120px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n}\n\n.packages-header h2 {\n  margin: 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n  position: relative;\n  top: -15px;\n}\n\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\nion-label {\n  margin: 0;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n}\n\n.m-0 {\n  margin: 0;\n}\n\n.user-profile h5 {\n  font-size: 20px;\n  margin: 5px 0;\n  color: #305f72;\n}\n\n.user-profile p {\n  margin-top: 0px;\n  font-size: 14px;\n}\n\n.padding-0 {\n  padding: 0;\n}\n\n.margin-10 {\n  margin: 0 10px;\n}\n\n.user-details-info {\n  border-bottom: 1px solid #415f7782;\n  text-align: left;\n  margin: 0 5px;\n  padding: 5px 0px;\n}\n\n.user-details-info h5 {\n  font-weight: 500;\n  font-size: 15px;\n  color: #305f72;\n  margin-bottom: 5px;\n}\n\n.user-details-info span {\n  font-weight: 400;\n  font-size: 14px;\n  color: #606060;\n  display: block;\n}\n\n.mt-30 {\n  margin-top: 10px;\n}\n\n.edit-button {\n  border-radius: 8px !important;\n  padding: 13px;\n  color: #ffffff;\n  box-shadow: none !important;\n  margin-top: 35px;\n  margin-bottom: 20px;\n  display: inline-block;\n  --background-activated: #3f5c9b !important;\n  --background-focused: #3f5c9b !important;\n  --background-hover: #3f5c9b !important;\n  background: #3f5c9b !important;\n  font-size: 16px;\n  font-family: \"Lato\";\n  font-weight: 500;\n  width: 160px;\n}\n\nbutton:focus {\n  outline: 0;\n}\n\n.user-profile {\n  margin-top: 20px;\n}\n\n.user-profile img {\n  border-radius: 50%;\n  width: 120px;\n  height: 120px;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFx1c2VyLXByb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7QUFDZjs7QUFDQTtFQUNJLFdBQVc7QUFFZjs7QUFBQTtFQUNJLFdBQVc7QUFHZjs7QUFEQTtFQUNJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFNBQVM7QUFJYjs7QUFGQTtFQUNJLGlCQUFpQjtBQUtyQjs7QUFIQTtFQUNJLGtCQUFrQjtBQU10Qjs7QUFKQTtFQUNJLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGVBQWU7RUFDZixZQUFZO0FBT2hCOztBQUxBO0VBQ0ksYUFBYTtBQVFqQjs7QUFOQTtFQUNJLDZCQUE2QjtBQVNqQzs7QUFQQTtFQUNJLDRCQUE0QjtFQUM1QixzQkFBc0I7RUFDdEIsYUFBYTtFQUNiLFdBQVc7RUFDWCwyQkFBMkI7RUFDM0Isa0JBQWtCO0VBQ2xCLE1BQU07RUFDTiw0QkFBNEI7QUFVaEM7O0FBUkE7RUFDSSxTQUFTO0VBQ1Qsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixVQUFVO0FBV2Q7O0FBVEE7RUFDSSxTQUFTO0VBQ1Qsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixjQUFjO0VBQ2QsY0FBYztFQUNkLGdCQUFnQjtBQVlwQjs7QUFWQTtFQUNJLFNBQVM7QUFhYjs7QUFYQTtFQUNJLG9CQUFnQjtFQUNoQix3QkFBb0I7QUFjeEI7O0FBWkE7RUFDSSxTQUFRO0FBZVo7O0FBYkE7RUFDSSxlQUFlO0VBQ2YsYUFBYTtFQUNiLGNBQWM7QUFnQmxCOztBQWRBO0VBQ0ksZUFBYztFQUNkLGVBQWM7QUFpQmxCOztBQWZBO0VBQ0ksVUFBUztBQWtCYjs7QUFoQkE7RUFDSSxjQUFhO0FBbUJqQjs7QUFqQkE7RUFDSSxrQ0FBa0M7RUFDbEMsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixnQkFBZ0I7QUFvQnBCOztBQWxCQTtFQUNJLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsY0FBYztFQUNkLGtCQUFrQjtBQXFCdEI7O0FBbkJBO0VBQ0ksZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixjQUFjO0VBQ2QsY0FBYztBQXNCbEI7O0FBcEJBO0VBQ0ksZ0JBQWU7QUF1Qm5COztBQXJCQTtFQUNJLDZCQUE2QjtFQUM3QixhQUFhO0VBQ2IsY0FBYztFQUNkLDJCQUEyQjtFQUMzQixnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBQ25CLHFCQUFxQjtFQUNyQiwwQ0FBdUI7RUFDdkIsd0NBQXFCO0VBQ3JCLHNDQUFtQjtFQUNuQiw4QkFBOEI7RUFDOUIsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsWUFBWTtBQXdCaEI7O0FBdEJBO0VBQ0ksVUFBUztBQXlCYjs7QUF0QkE7RUFDSSxnQkFBZ0I7QUF5QnBCOztBQXZCQTtFQUNJLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osYUFBYTtBQTBCakIiLCJmaWxlIjoidXNlci1wcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi51c2VyLWljb24gaW1nIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG59XHJcbi5iYWNrIGltZyB7XHJcbiAgICB3aWR0aDogNDBweDtcclxufVxyXG4uYmFjayB7XHJcbiAgICBmbG9hdDogbGVmdDtcclxufVxyXG4uc2hvcHBpbmctY2FydCBpbWcge1xyXG4gICAgd2lkdGg6IDMwcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IC04cHg7XHJcbn1cclxuLm1lbnUtaWNvbiB7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG4uc2hvcHBpbmctY2FydCB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuLmNhcnQtbnVtYmVyIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAxNXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmYmIwMDtcclxuICAgIGhlaWdodDogMjBweDtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGNvbG9yOiAjM2Y1YzliO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgcGFkZGluZzogMXB4O1xyXG59XHJcbmlvbi1oZWFkZXIge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG4uaGVhZGVyLWFycm93IHtcclxuICAgIHBhZGRpbmc6IDIwcHggMTVweCAhaW1wb3J0YW50O1xyXG59XHJcbi50b3AtYmcge1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBoZWlnaHQ6IDEyMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMzBweCAzMHB4O1xyXG59XHJcbi5wYWNrYWdlcy1oZWFkZXIgaDJ7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLTE1cHg7XHJcbn1cclxuLnBhY2thZ2VzLWhlYWRlciBzcGFue1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgY29sb3I6ICMzMDVGNzI7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbn1cclxuaW9uLWxhYmVsIHtcclxuICAgIG1hcmdpbjogMDtcclxufVxyXG5pb24taXRlbSB7XHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcclxuICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweDtcclxufVxyXG4ubS0we1xyXG4gICAgbWFyZ2luOjA7XHJcbn1cclxuLnVzZXItcHJvZmlsZSBoNXtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIG1hcmdpbjogNXB4IDA7XHJcbiAgICBjb2xvcjogIzMwNWY3MjtcclxufVxyXG4udXNlci1wcm9maWxlIHB7XHJcbiAgICBtYXJnaW4tdG9wOjBweDtcclxuICAgIGZvbnQtc2l6ZToxNHB4O1xyXG59XHJcbi5wYWRkaW5nLTB7XHJcbiAgICBwYWRkaW5nOjA7XHJcbn1cclxuLm1hcmdpbi0xMHtcclxuICAgIG1hcmdpbjowIDEwcHg7XHJcbn1cclxuLnVzZXItZGV0YWlscy1pbmZve1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICM0MTVmNzc4MjtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICBtYXJnaW46IDAgNXB4O1xyXG4gICAgcGFkZGluZzogNXB4IDBweDtcclxufVxyXG4udXNlci1kZXRhaWxzLWluZm8gaDV7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgY29sb3I6ICMzMDVmNzI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbn1cclxuLnVzZXItZGV0YWlscy1pbmZvIHNwYW57XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgY29sb3I6ICM2MDYwNjA7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufVxyXG4ubXQtMzB7XHJcbiAgICBtYXJnaW4tdG9wOjEwcHg7XHJcbn1cclxuLmVkaXQtYnV0dG9ue1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nOiAxM3B4O1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICBtYXJnaW4tdG9wOiAzNXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJhY2tncm91bmQtaG92ZXI6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIGZvbnQtZmFtaWx5OiBcIkxhdG9cIjtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICB3aWR0aDogMTYwcHg7XHJcbn1cclxuYnV0dG9uOmZvY3VzIHtcclxuICAgIG91dGxpbmU6MDtcclxufVxyXG5cclxuLnVzZXItcHJvZmlsZXtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbn1cclxuLnVzZXItcHJvZmlsZSBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgd2lkdGg6IDEyMHB4O1xyXG4gICAgaGVpZ2h0OiAxMjBweDtcclxufSJdfQ== */");

/***/ }),

/***/ "TBb5":
/*!***********************************************************!*\
  !*** ./src/app/pages/user-profile/user-profile.module.ts ***!
  \***********************************************************/
/*! exports provided: UserProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserProfilePageModule", function() { return UserProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _user_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user-profile-routing.module */ "7EoU");
/* harmony import */ var _user_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-profile.page */ "Ang3");







let UserProfilePageModule = class UserProfilePageModule {
};
UserProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _user_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["UserProfilePageRoutingModule"]
        ],
        declarations: [_user_profile_page__WEBPACK_IMPORTED_MODULE_6__["UserProfilePage"]]
    })
], UserProfilePageModule);



/***/ }),

/***/ "z4lj":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/user-profile/user-profile.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n\r\n  <!-- <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\r\n    </ion-buttons>\r\n\r\n    <ion-title>Profile</ion-title>\r\n  </ion-toolbar> -->\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-row style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n    <ion-col size=\"12\" class=\"header-arrow padding-0\">\r\n      <div>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <div class=\"back\" (click)=\"goBack()\">\r\n                <img src=\"assets/back.svg\" alt=\"back\">\r\n            </div>\r\n          </ion-col>\r\n          <!-- <ion-col size=\"6\">\r\n            <div class=\"menu-icon\">\r\n              <span class=\"shopping-cart\"><img src=\"assets/shopping-cart.svg\" alt=\"shopping\"><span\r\n                  class=\"cart-number\">{{cartCount}}</span></span>\r\n              <span class=\"user-icon\" slot=\"start\">\r\n                <img src=\"assets/user-profile.png\" alt=\"user-icon\">\r\n              </span>\r\n            </div>\r\n          </ion-col> -->\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <div class=\"packages-header\">\r\n              <h2>Profile</h2>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"user-profile text-center\">\r\n        <img src=\"{{avatar}}\" alt=\"user-icon\">\r\n        <h5 class=\"m-0\">{{userData?.full_name}}</h5>\r\n        <p>{{userData?.email}}</p>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"mt-30 margin-10\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Mobile Number</h5>\r\n        <span>{{userData?.phone_number}}</span>\r\n      </div>\r\n    </ion-col>\r\n    <ion-col size=\"12\" class=\"mt-30 padding-0\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Alternate Number</h5>\r\n        <span>{{userData?.phone_number2}}</span>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <!-- <ion-row class=\"mt-30 margin-10\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Address 1</h5>\r\n        <span>{{userData?.address1}}</span>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"mt-30 margin-10\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Address 2</h5>\r\n        <span>{{userData?.address2}}</span>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row> -->\r\n\r\n  <ion-row class=\"mt-30 margin-10\">\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Courses Opted For</h5>\r\n        <span>{{userData?.course_name}}</span>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row class=\"mt-30 margin-10\">\r\n    <ion-col size=\"12\" class=\"padding-0\" *ngIf=\"userData?.parents_phone_no != 'null'\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Parents Number</h5>\r\n        <span>{{userData?.parents_phone_no}}</span>\r\n      </div>\r\n    </ion-col>\r\n    <ion-col size=\"12\" class=\"mt-30 padding-0\" *ngIf=\"userData?.parents_email_id != 'null'\">\r\n      <div class=\"user-details-info\">\r\n        <h5 class=\"m-0\">Parents Email</h5>\r\n        <span>{{userData?.parents_email_id}}</span>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row>\r\n    <ion-col>\r\n      <div class=\"text-center\">\r\n        <button ion-button full class=\"edit-button\" type=\"submit\" (click)=\"EditUserPage()\">Edit Info</button>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  </ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=user-profile-user-profile-module.js.map