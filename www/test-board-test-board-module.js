(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["test-board-test-board-module"],{

/***/ "6ZXX":
/*!***********************************************!*\
  !*** ./src/app/pages/popover/popover.page.ts ***!
  \***********************************************/
/*! exports provided: PopoverPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopoverPage", function() { return PopoverPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_popover_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./popover.page.html */ "WEWd");
/* harmony import */ var _popover_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./popover.page.scss */ "BRn6");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");









let PopoverPage = class PopoverPage {
    constructor(navParams, modalController, http, httpService, uihelper) {
        this.navParams = navParams;
        this.modalController = modalController;
        this.http = http;
        this.httpService = httpService;
        this.uihelper = uihelper;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
        });
        this.url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_7__["Constant"].apiUrl;
        this.options = { headers: this.headers };
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // this.uihelper.ShowSpinner();
            // this.test_id = this.navParams.data.test_id
            // this.http.get(this.url + 'test-chapters/' + this.test_id, this.options).subscribe((res)=>{     
            //     // console.log('popover chapters', res)
            //     this.chapter_list = res['payload']
            //     this.uihelper.HideSpinner();
            // });
            this.chapter_list = this.navParams.data.chapter_list;
            console.log('chapter_list', this.chapter_list);
        });
    }
    dismiss() {
        this.modalController.dismiss();
        return false;
    }
};
PopoverPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavParams"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_6__["HttpService"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_8__["UIHelper"] }
];
PopoverPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-popover',
        template: _raw_loader_popover_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_popover_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], PopoverPage);



/***/ }),

/***/ "BRn6":
/*!*************************************************!*\
  !*** ./src/app/pages/popover/popover.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".popover-main-holder {\n  padding: 0;\n}\n\n.popover-main-holder h4 {\n  margin: 20px 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\nul.list {\n  margin: 5px 0;\n}\n\n.list li {\n  padding: 3px 0;\n  font-size: 15px;\n  color: #305F72;\n}\n\n.popover-scroll {\n  overflow: scroll;\n  min-width: 250px;\n}\n\n.user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.back img {\n  width: 40px;\n}\n\n.back {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.header-arrow {\n  padding: 20px 15px;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 100px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n  margin-bottom: 20px;\n}\n\n.packages-header {\n  margin: 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n  padding-top: 40px;\n}\n\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\n.border-all {\n  border: 1px solid #b1bcd4;\n  border-radius: 10px;\n  margin-bottom: 22px;\n  color: #8a959e;\n}\n\n.reg-num {\n  font-size: 15px;\n  font-weight: 300;\n  color: #8a959e;\n  padding-left: 15px;\n}\n\n.space-3 {\n  margin: 25px 20px 0px;\n}\n\nion-label {\n  margin: 0;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n}\n\n.close-popover {\n  position: absolute;\n  right: 10px;\n  font-size: 26px;\n  top: 10px;\n  color: #ffffff;\n  cursor: pointer;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxwb3BvdmVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQTtFQUNJLFVBQVU7QUFGZDs7QUFJQTtFQUNJLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxjQUFjO0VBQ2QsZ0JBQWdCO0FBRHBCOztBQUdBO0VBQ0ksYUFBYTtBQUFqQjs7QUFFQTtFQUNJLGNBQWE7RUFDYixlQUFjO0VBQ2QsY0FBYztBQUNsQjs7QUFFQTtFQUNJLGdCQUFnQjtFQUNoQixnQkFBZ0I7QUFDcEI7O0FBQ0U7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztBQUVmOztBQUFBO0VBQ0ksV0FBVztBQUdmOztBQURBO0VBQ0ksV0FBVztBQUlmOztBQUZBO0VBQ0ksV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsU0FBUztBQUtiOztBQUhBO0VBQ0ksaUJBQWlCO0FBTXJCOztBQUpBO0VBQ0ksa0JBQWtCO0FBT3RCOztBQUxBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIsWUFBWTtFQUNaLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZUFBZTtFQUNmLFlBQVk7QUFRaEI7O0FBTkE7RUFDSSxhQUFhO0FBU2pCOztBQVBBO0VBQ0ksa0JBQWtCO0FBVXRCOztBQVJBO0VBQ0ksNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QixhQUFhO0VBQ2IsV0FBVztFQUNYLDJCQUEyQjtFQUMzQixrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLDRCQUE0QjtFQUM1QixtQkFBbUI7QUFXdkI7O0FBVEE7RUFDSSxTQUFTO0VBQ1Qsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGlCQUFpQjtBQVlyQjs7QUFWQTtFQUNJLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxjQUFjO0VBQ2QsZ0JBQWdCO0FBYXBCOztBQVhBO0VBQ0kseUJBQXlCO0VBQ3pCLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFDbkIsY0FBYztBQWNsQjs7QUFaQTtFQUNJLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGtCQUFrQjtBQWV0Qjs7QUFiQTtFQUNJLHFCQUFxQjtBQWdCekI7O0FBZEE7RUFDSSxTQUFTO0FBaUJiOztBQWZBO0VBQ0ksb0JBQWdCO0VBQ2hCLHdCQUFvQjtBQWtCeEI7O0FBaEJBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxlQUFlO0VBQ2YsU0FBUztFQUNULGNBQWM7RUFDZCxlQUFlO0FBbUJuQiIsImZpbGUiOiJwb3BvdmVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGFwcC1wb3BvdmVye1xyXG4vLyAgICAgLS13aWR0aDoyNTBweCFpbXBvcnRhbnQ7XHJcbi8vIH1cclxuLnBvcG92ZXItbWFpbi1ob2xkZXJ7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG59XHJcbi5wb3BvdmVyLW1haW4taG9sZGVyIGg0e1xyXG4gICAgbWFyZ2luOiAyMHB4IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG51bC5saXN0e1xyXG4gICAgbWFyZ2luOiA1cHggMDtcclxufVxyXG4ubGlzdCBsaXtcclxuICAgIHBhZGRpbmc6M3B4IDA7XHJcbiAgICBmb250LXNpemU6MTVweDtcclxuICAgIGNvbG9yOiAjMzA1RjcyO1xyXG59XHJcblxyXG4ucG9wb3Zlci1zY3JvbGx7XHJcbiAgICBvdmVyZmxvdzogc2Nyb2xsO1xyXG4gICAgbWluLXdpZHRoOiAyNTBweDtcclxuICB9XHJcbiAgLnVzZXItaWNvbiBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgd2lkdGg6IDQwcHg7XHJcbn1cclxuLmJhY2sgaW1nIHtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG59XHJcbi5iYWNrIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IGltZyB7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLThweDtcclxufVxyXG4ubWVudS1pY29uIHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4uY2FydC1udW1iZXIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDE1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZiYjAwO1xyXG4gICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICMzZjVjOWI7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBwYWRkaW5nOiAxcHg7XHJcbn1cclxuaW9uLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcbi5oZWFkZXItYXJyb3cge1xyXG4gICAgcGFkZGluZzogMjBweCAxNXB4O1xyXG59XHJcbi50b3AtYmcge1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMzBweCAzMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG4ucGFja2FnZXMtaGVhZGVye1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogNDBweDtcclxufVxyXG4ucGFja2FnZXMtaGVhZGVyIHNwYW57XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG4uYm9yZGVyLWFsbCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjYjFiY2Q0O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDIycHg7XHJcbiAgICBjb2xvcjogIzhhOTU5ZTtcclxufVxyXG4ucmVnLW51bSB7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBmb250LXdlaWdodDogMzAwO1xyXG4gICAgY29sb3I6ICM4YTk1OWU7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XHJcbn1cclxuLnNwYWNlLTMge1xyXG4gICAgbWFyZ2luOiAyNXB4IDIwcHggMHB4O1xyXG59XHJcbmlvbi1sYWJlbCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbn1cclxuaW9uLWl0ZW0ge1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XHJcbiAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XHJcbn1cclxuLmNsb3NlLXBvcG92ZXJ7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICByaWdodDogMTBweDtcclxuICAgIGZvbnQtc2l6ZTogMjZweDtcclxuICAgIHRvcDogMTBweDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59Il19 */");

/***/ }),

/***/ "MwE2":
/*!*****************************************************!*\
  !*** ./src/app/pages/test-board/test-board.page.ts ***!
  \*****************************************************/
/*! exports provided: TestBoardPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestBoardPage", function() { return TestBoardPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_test_board_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./test-board.page.html */ "g6TN");
/* harmony import */ var _test_board_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./test-board.page.scss */ "on7x");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");
/* harmony import */ var _popover_popover_page__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../popover/popover.page */ "6ZXX");












let TestBoardPage = class TestBoardPage {
    constructor(router, http, httpService, storageService, navCtrl, uihelper, popovercontroller, modalController) {
        this.router = router;
        this.http = http;
        this.httpService = httpService;
        this.storageService = storageService;
        this.navCtrl = navCtrl;
        this.uihelper = uihelper;
        this.popovercontroller = popovercontroller;
        this.modalController = modalController;
        this.url = src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__["Constant"].apiUrl;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('userToken'),
        });
        this.options = { headers: this.headers };
        this.tempObj = [];
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                this.router.events.subscribe((event) => {
                    this.selfPath = '';
                    if (event && event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__["NavigationEnd"] && event.url) {
                        this.selfPath = event.url + '/test-board';
                    }
                    this.selfPath = this.router.routerState.snapshot.url;
                    this.selfPath = this.getStringBeforeSubstring(this.selfPath, 'test-board') + 'test-board';
                });
            }
            catch (e) {
                console.log(e);
            }
        });
    }
    getStringBeforeSubstring(parentString, substring) {
        return parentString.substring(0, parentString.indexOf(substring));
    }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // this.cartCount = localStorage.getItem('cartCount');
            this.uihelper.ShowSpinner();
            this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_9__["Constant"].AUTH).then(res => {
                this.userData = res.user_details;
                let user_id = res.user_details.id;
                this.httpService.afterLoginGet("cart_count", res.user_details.id).subscribe((res) => {
                    this.cartCount = res['payload'].cart_count;
                });
                this.http.get(this.url + 'test-board/' + user_id, this.options).subscribe((res) => {
                    // console.log('test board',res['payload'])        
                    this.testBoardData = res['payload'];
                    this.testBoardPackages = res['payload'].packages;
                    this.totalPackages = this.testBoardPackages.length;
                    this.total_avail_tests = res['payload'].total_avail_tests;
                    this.total_test_pending = res['payload'].total_test_pending;
                    this.avg_percantage = parseFloat("" + res['payload'].avg_percantage.toFixed(1));
                    this.subject_id = res['payload'].subject_id;
                    this.uihelper.HideSpinner();
                    this.tempObj.length = 0;
                    this.testBoardPackages.forEach((value) => {
                        value.package.package_tests.forEach((value2, index) => {
                            const testData = Object.assign({}, value2);
                            testData.name = !index ? value.package.name : '';
                            testData.subject_name = !index ? value.package.subject.subject_name : '';
                            this.tempObj.push(testData);
                        });
                        // console.log('tempObj',this.tempObj)  
                        this.tempObj.forEach((value3, i) => {
                            var _a, _b;
                            if (value3.exam) {
                                if (value3.exam.mcq_marks || value3.exam.theory_marks) {
                                    this.tempObj[i]['marks_obtained'] = parseFloat(((_a = value3.exam) === null || _a === void 0 ? void 0 : _a.mcq_marks) || 0) + parseFloat(((_b = value3.exam) === null || _b === void 0 ? void 0 : _b.theory_marks) || 0);
                                    this.tempObj[i]['marks_obtained'] = this.tempObj[i]['marks_obtained'] < 0 ? '0' : this.tempObj[i]['marks_obtained'];
                                    // console.log('marks_obtained',this.tempObj[i]['marks_obtained']);
                                }
                                else {
                                    this.tempObj[i]['marks_obtained'] = '0';
                                }
                            }
                            else {
                                this.tempObj[i]['marks_obtained'] = 'N/A';
                            }
                        });
                    });
                    // console.log('temp obj check',this.tempObj)
                });
                this.http.get(this.url + "dashboard/" + user_id, this.options).subscribe((res) => {
                    this.dashboard_data = res['payload'];
                    this.daily_motivational_text = this.dashboard_data.daily_motivational_text;
                    this.total_avail_tests = this.dashboard_data.total_avail_tests;
                    this.pendingTestResultCount = this.dashboard_data.pendingTestResultCount;
                    this.totalCompletedTestCount = this.dashboard_data.totalCompletedTestCount;
                    this.totalTestsAverage = this.dashboard_data.totalTestsAverage;
                    this.repliesToPost = this.dashboard_data.repliesToPost;
                });
            });
        });
    }
    openPopover(test_id, ev) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.uihelper.ShowSpinner();
            this.http.get(this.url + 'test-chapters/' + test_id, this.options).subscribe((res) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                let chapter_list = res['payload'];
                // console.log('popover chapters', chapter_list)
                this.uihelper.HideSpinner();
                const popover = yield this.modalController.create({
                    component: _popover_popover_page__WEBPACK_IMPORTED_MODULE_11__["PopoverPage"],
                    componentProps: {
                        "chapter_list": chapter_list,
                    },
                });
                return yield popover.present();
            }));
        });
    }
    takeTest(test_id, total_marks) {
        this.router.navigate([this.selfPath + '/new-test'], { queryParams: { test_id: test_id, total_marks: total_marks } });
    }
    startTestPage(test_id) {
        this.router.navigate([this.selfPath + '/start-test'], { queryParams: { test_id: test_id } });
    }
    GoToHistoryPage() {
        this.router.navigate([this.selfPath + "/test-history"]);
    }
    goBack() {
        this.router.navigate(['/tabs']);
    }
    GoToCart() {
        this.router.navigate([this.selfPath + "/cart"]);
    }
    GoToUserProfile() {
        this.router.navigate([this.selfPath + "/user-profile"]);
    }
};
TestBoardPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_7__["HttpService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_8__["StorageService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_10__["UIHelper"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"] }
];
TestBoardPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-test-board',
        template: _raw_loader_test_board_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_test_board_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], TestBoardPage);



/***/ }),

/***/ "URWB":
/*!*******************************************************!*\
  !*** ./src/app/pages/test-board/test-board.module.ts ***!
  \*******************************************************/
/*! exports provided: TestBoardPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestBoardPageModule", function() { return TestBoardPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _test_board_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./test-board-routing.module */ "ould");
/* harmony import */ var _test_board_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./test-board.page */ "MwE2");
/* harmony import */ var _popover_popover_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../popover/popover.page */ "6ZXX");








let TestBoardPageModule = class TestBoardPageModule {
};
TestBoardPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _test_board_routing_module__WEBPACK_IMPORTED_MODULE_5__["TestBoardPageRoutingModule"]
        ],
        declarations: [_test_board_page__WEBPACK_IMPORTED_MODULE_6__["TestBoardPage"], _popover_popover_page__WEBPACK_IMPORTED_MODULE_7__["PopoverPage"]]
    })
], TestBoardPageModule);



/***/ }),

/***/ "WEWd":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/popover/popover.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>rttryrt</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content> -->\r\n\r\n<div class=\"popover-scroll\">\r\n  <div class=\"popover-main-holder\">\r\n    <div style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n      <ion-icon name=\"close\" class=\"close-popover\" (click)=\"dismiss()\"></ion-icon>\r\n      <h2 class=\"packages-header\">Chapter List</h2>\r\n    </div>\r\n    <ul class=\"list\" *ngFor=\"let chapters of chapter_list\">\r\n      <li class=\"item\">{{chapters.title}}</li>\r\n    </ul>\r\n  </div>\r\n</div>\r\n\r\n\r\n<!-- (click)=\"dismiss()\" -->");

/***/ }),

/***/ "g6TN":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/test-board/test-board.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n     <ion-title >Test Board</ion-title>\r\n   </ion-toolbar>\r\n </ion-header>\r\n \r\n <ion-content>\r\n  <ion-row style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n    <ion-col size=\"12\" class=\"header-arrow padding-0\">\r\n      <div>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <div class=\"back\" (click)=\"goBack()\">\r\n                <img src=\"assets/back.svg\" alt=\"back\">\r\n            </div>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <div class=\"menu-icon\">\r\n              <span class=\"shopping-cart\" (click)=\"GoToCart()\"><img src=\"assets/shopping-cart.svg\" alt=\"shopping\"><span\r\n                  class=\"cart-number\">{{cartCount}}</span></span>\r\n              <span class=\"user-icon\" slot=\"start\" (click)=\"GoToUserProfile()\">\r\n                <img src=\"assets/user-profile.png\" alt=\"user-icon\">\r\n              </span>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <div class=\"packages-header\">\r\n              <h2>Test Board</h2>\r\n              <span>My Test Board</span>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n \r\n   <ion-row class=\"space-right-left\">\r\n\r\n     <ion-col size=\"12\"class=\"padding-0\">\r\n       <ion-row class=\"package-taken bg-purple\">\r\n         <ion-col size=\"3\"><img src=\"assets/test-board-icon/packages-taken.svg\"></ion-col>\r\n         <ion-col size=\"7\"><h6 class=\"margin-0\">Total Packages Taken</h6></ion-col>\r\n         <ion-col size=\"2\"><span>{{totalPackages}}</span></ion-col>\r\n       </ion-row>\r\n     </ion-col>\r\n\r\n     <ion-col size=\"12\"class=\"padding-0\">\r\n      <ion-row class=\"package-taken bg-green\">\r\n        <ion-col size=\"3\"><img src=\"assets/test-board-icon/total-test.svg\"></ion-col>\r\n        <ion-col size=\"7\"><h6 class=\"margin-0\">Total Tests Available</h6></ion-col>\r\n        <ion-col size=\"2\"><span>{{total_avail_tests}}</span></ion-col>\r\n      </ion-row>\r\n    </ion-col>\r\n\r\n    <ion-col size=\"12\"class=\"padding-0\">\r\n     <ion-row class=\"package-taken bg-blue\">\r\n       <ion-col size=\"3\"><img src=\"assets/test-board-icon/test-score.svg\"></ion-col>\r\n       <ion-col size=\"6\"> <h6 class=\"margin-0\">Average Test Score</h6></ion-col>\r\n       <ion-col size=\"3\"><span>{{avg_percantage}} %</span></ion-col>\r\n     </ion-row>\r\n   </ion-col>\r\n\r\n   </ion-row>\r\n \r\n   <ion-row class=\"space-top-16 space-right-left\">\r\n     <ion-col size=\"6\" class=\"padding-0\">\r\n       <div class=\"package-taken-inner\" style=\"background-color: #FAE4F4;\">\r\n         <h6 class=\"margin-0\">Total Packages Taken</h6>\r\n         <span>{{totalPackages}}</span>\r\n       </div>\r\n     </ion-col>\r\n     <ion-col size=\"6\" class=\"padding-0\">\r\n       <div class=\"package-taken-inner\" style=\"background-color: #FFF5E5;\">\r\n         <h6 class=\"margin-0\">Test Results Declared</h6>\r\n         <span>{{totalCompletedTestCount}}</span>\r\n       </div>\r\n     </ion-col>\r\n   </ion-row>\r\n \r\n   <ion-row class=\"space-top-16 space-right-left\">\r\n     <ion-col size=\"6\" class=\"padding-0\">\r\n       <div class=\"package-taken-inner\" style=\"background-color: #F0EBF4;\">\r\n         <h6 class=\"margin-0\">Test Results Pending</h6>\r\n         <span>{{pendingTestResultCount}}</span>\r\n       </div>\r\n     </ion-col>\r\n     <ion-col size=\"6\" class=\"padding-0\">\r\n       <div class=\"package-taken-inner\" style=\"background-color: #FEF3F3;\">\r\n         <h6 class=\"margin-0\">Total Test Pending</h6>\r\n         <span>{{total_test_pending}}</span>\r\n       </div>\r\n     </ion-col>\r\n   </ion-row>\r\n   \r\n \r\n   <ion-row class=\"space-top-16 space-right-left\">\r\n     <ion-col>\r\n       <div class=\"table-responsive table-bg\">\r\n         <table class=\"table table-hover table-bordered text-center\">\r\n           <thead class=\"bg-info-color\">\r\n             <tr>\r\n               <th scope=\"col\">Package Name</th>\r\n               <th scope=\"col\">Tests</th>\r\n               <th scope=\"col\">Subjects</th>\r\n               <th scope=\"col\">Status</th>\r\n               <th scope=\"col\">Total</th>\r\n               <th scope=\"col\">Target</th>\r\n               <th scope=\"col\">Marks Obtained</th>\r\n             </tr>\r\n           </thead>\r\n           <tbody>\r\n             <tr *ngFor=\"let packages of tempObj\">\r\n               <th scope=\"row\" class=\"course-ttl\">{{packages.name}}</th>\r\n               <td>{{packages.title}}</td>\r\n               <td >{{packages.subject_name}} <span style=\"color: rgb(128, 0, 43);\" (click)=\"openPopover(packages.id,$event)\" value=\"packages.id\">View Chapters </span></td>\r\n                <td class=\"completed-td submitted\" *ngIf=\"packages.exam && packages.exam.status == 'submitted'\">Test Submited</td>\r\n               <td class=\"completed-td examcompleted\" *ngIf=\"packages.exam && packages.exam.status == 'completed'\" (click)=\"GoToHistoryPage()\">Test Completed</td>\r\n               <td class=\"completed-td examstart\" *ngIf=\"packages.exam && packages.exam.status == 'started'\" (click)=\"startTestPage(packages.id)\">Test Started</td>\r\n               <td class=\"completed-td taketest\" *ngIf=\"packages.exam == null\" (click)=\"takeTest(packages.id,packages.total_marks)\">Take Test Now</td>\r\n               <td>{{packages.total_marks}}</td>\r\n               <td>{{packages.exam?.target_percent}}</td>\r\n               <td>{{packages.marks_obtained}}</td>\r\n             </tr>\r\n             <!-- <tr>\r\n               <th scope=\"row\"></th>\r\n               <td>Middle Over Test</td>\r\n               <td class=\"completed-td\">Completed</td>\r\n               <td>90%</td>\r\n               <td>60%</td>\r\n             </tr> -->\r\n           </tbody>\r\n         </table>\r\n       </div>\r\n     </ion-col>\r\n   </ion-row>\r\n \r\n   \r\n \r\n </ion-content>\r\n ");

/***/ }),

/***/ "on7x":
/*!*******************************************************!*\
  !*** ./src/app/pages/test-board/test-board.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* width */\n::-webkit-scrollbar {\n  width: 10px;\n  height: 7px;\n}\n/* Track */\n::-webkit-scrollbar-track {\n  background: #f1f1f1;\n}\n/* Handle */\n::-webkit-scrollbar-thumb {\n  background: #3f5c9b;\n  border-radius: 8px;\n}\n/* Handle on hover */\n::-webkit-scrollbar-thumb:hover {\n  background: #51c3d1;\n}\n.title-text {\n  color: #2a2a2a;\n  font-size: 15px;\n  font-weight: 500;\n  padding-left: 5px;\n}\n.space-right-left {\n  margin: 10px 5px 20px 5px;\n}\n.padding-0 {\n  padding: 0;\n}\n.margin-0 {\n  margin: 0px;\n}\n.package-taken {\n  border-radius: 15px !important;\n  padding: 12px;\n  text-align: center;\n  height: 90px;\n  color: #fff;\n  margin: 10px 15px 0;\n}\n.package-taken h6 {\n  font-size: 16px;\n  color: #305F72;\n  text-align: left;\n  padding-top: 20px;\n}\n.package-taken span {\n  font-size: 17px;\n  font-weight: 500;\n  color: #305F72;\n  border-radius: 50%;\n  position: relative;\n  top: 20px;\n}\n.package-taken img {\n  width: 58px;\n  height: 58px;\n  float: left;\n}\n.bg-purple {\n  background-color: #FAE4F4;\n}\n.bg-green {\n  background-color: #FFF5E5;\n}\n.bg-blue {\n  background-color: #E7FFE5;\n}\n.space-top-16 {\n  margin-top: 16px;\n}\n.package-taken-inner {\n  border-radius: 15px !important;\n  padding: 22px 25px;\n  text-align: center;\n  height: 115px;\n  color: #305F72;\n  margin: 0 15px;\n}\n.package-taken-inner h6 {\n  font-sizE: 14px;\n}\n.package-taken-inner span {\n  font-size: 35px;\n  font-weight: 500;\n  display: block;\n  color: #3F5C9B;\n  padding-top: 8px;\n}\n.table-bg {\n  border-radius: 8px 8px 8px 8px;\n  box-shadow: 0 0 0.875rem 0 rgba(181, 181, 181, 0.3);\n  margin-bottom: 25px;\n}\n.table-responsive {\n  display: block;\n  width: 100%;\n  overflow-x: auto;\n  -webkit-overflow-scrolling: touch;\n}\n.bg-info-color {\n  background-color: #F27376;\n  color: #ffffff;\n}\n.table-bordered thead th {\n  border-bottom-width: 2px;\n  font-size: 14px;\n  font-weight: 400;\n}\n.course-ttl {\n  font-size: 13px;\n  font-weight: 500;\n}\n.table > tbody > tr > td {\n  vertical-align: middle;\n  font-size: 13px;\n  font-weight: 500;\n}\n.table td, .table th {\n  padding: 0.55rem;\n}\n.table-bordered, .table-bordered td, .table-bordered th {\n  border: 1px solid #dee2e6;\n}\n.table-responsive > .table-bordered {\n  border: 0;\n  margin: 0;\n  background-color: #ffffff;\n}\n.table-hover tbody tr:hover {\n  color: #495057;\n  background-color: rgba(0, 0, 0, 0.0375);\n}\n.completed-td {\n  color: #00a52f;\n}\n.header-icon {\n  float: right;\n}\nul.shopping-icon {\n  list-style-type: none;\n}\n.menu-icon button {\n  padding-top: 5px;\n}\n.menu-icon button:focus {\n  outline: none;\n}\n.nav-title h6 {\n  margin: 8px 0 0 0;\n  color: #fff;\n  font-weight: 400;\n}\ntable {\n  width: 100%;\n}\n.text-blue {\n  color: #3b7ddd;\n}\n.submitted {\n  color: #fc7b03;\n}\n.examstart {\n  color: #4f8a25;\n}\n.examcompleted {\n  color: #00a52f;\n}\n.taketest {\n  color: #3b7ddd;\n}\n.user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n.back img {\n  width: 40px;\n}\n.back {\n  float: left;\n}\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n.menu-icon {\n  text-align: right;\n}\n.shopping-cart {\n  position: relative;\n}\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\nion-header {\n  display: none;\n}\n.header-arrow {\n  padding: 20px 15px;\n}\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 160px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n}\n.packages-header h2 {\n  margin: 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n}\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFx0ZXN0LWJvYXJkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxVQUFBO0FBQ0E7RUFDSSxXQUFXO0VBQ1gsV0FBVztBQUNmO0FBQ0EsVUFBQTtBQUNBO0VBQ0ksbUJBQW1CO0FBRXZCO0FBQUEsV0FBQTtBQUNBO0VBQ0ksbUJBQW1CO0VBQ25CLGtCQUFrQjtBQUd0QjtBQURBLG9CQUFBO0FBQ0E7RUFDSSxtQkFBbUI7QUFJdkI7QUFGQTtFQUNJLGNBQWE7RUFDYixlQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtBQUtyQjtBQUhBO0VBQ0kseUJBQXdCO0FBTTVCO0FBSkE7RUFDSSxVQUFTO0FBT2I7QUFMQTtFQUNJLFdBQVU7QUFRZDtBQU5BO0VBQ0ksOEJBQThCO0VBQzlCLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsWUFBWTtFQUNaLFdBQVc7RUFDWCxtQkFBbUI7QUFTdkI7QUFQQTtFQUNJLGVBQWU7RUFDZixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtBQVVyQjtBQVBBO0VBQ0ksZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixTQUFTO0FBVWI7QUFSQTtFQUNJLFdBQVc7RUFDWCxZQUFZO0VBQ1osV0FBVztBQVdmO0FBVEE7RUFDSSx5QkFBd0I7QUFZNUI7QUFWQTtFQUNJLHlCQUF3QjtBQWE1QjtBQVhBO0VBQ0UseUJBQXlCO0FBYzNCO0FBWkE7RUFDSSxnQkFBZTtBQWVuQjtBQWJBO0VBQ0ksOEJBQThCO0VBQzlCLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLGNBQWM7RUFDZCxjQUFjO0FBZ0JsQjtBQWRBO0VBQ0ksZUFBYztBQWlCbEI7QUFmQTtFQUNJLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGNBQWM7RUFDZCxnQkFBZ0I7QUFrQnBCO0FBaEJBO0VBQ0ksOEJBQThCO0VBQzlCLG1EQUFtRDtFQUNuRCxtQkFBbUI7QUFtQnZCO0FBakJBO0VBQ0ksY0FBYztFQUNkLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsaUNBQWlDO0FBb0JyQztBQWxCQTtFQUNJLHlCQUF5QjtFQUN6QixjQUFjO0FBcUJsQjtBQW5CQTtFQUNJLHdCQUF3QjtFQUN4QixlQUFlO0VBQ2YsZ0JBQWdCO0FBc0JwQjtBQXBCQTtFQUNJLGVBQWM7RUFDZCxnQkFBZTtBQXVCbkI7QUFyQkE7RUFDSSxzQkFBc0I7RUFDdEIsZUFBZTtFQUNmLGdCQUFnQjtBQXdCcEI7QUF0QkE7RUFDSSxnQkFBZ0I7QUF5QnBCO0FBdkJBO0VBQ0kseUJBQXlCO0FBMEI3QjtBQXhCQTtFQUNJLFNBQVM7RUFDVCxTQUFTO0VBQ1QseUJBQXlCO0FBMkI3QjtBQXpCQTtFQUNJLGNBQWM7RUFDZCx1Q0FBc0M7QUE0QjFDO0FBMUJBO0VBQ0ksY0FBYztBQTZCbEI7QUEzQkE7RUFDSSxZQUFXO0FBOEJmO0FBM0JBO0VBQ0kscUJBQW9CO0FBOEJ4QjtBQTVCQTtFQUNJLGdCQUFnQjtBQStCcEI7QUE3QkE7RUFDSSxhQUFhO0FBZ0NqQjtBQTlCQTtFQUNJLGlCQUFnQjtFQUNoQixXQUFVO0VBQ1YsZ0JBQWdCO0FBaUNwQjtBQS9CQTtFQUNJLFdBQVc7QUFrQ2Y7QUFoQ0E7RUFDSSxjQUFhO0FBbUNqQjtBQWpDQTtFQUNJLGNBQWE7QUFvQ2pCO0FBbENBO0VBQ0ksY0FBYTtBQXFDakI7QUFuQ0E7RUFDSSxjQUFhO0FBc0NqQjtBQXBDQTtFQUNJLGNBQWE7QUF1Q2pCO0FBcENBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7QUF1Q2Y7QUFyQ0E7RUFDSSxXQUFXO0FBd0NmO0FBdENBO0VBQ0ksV0FBVztBQXlDZjtBQXZDQTtFQUNJLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLFNBQVM7QUEwQ2I7QUF4Q0E7RUFDSSxpQkFBaUI7QUEyQ3JCO0FBekNBO0VBQ0ksa0JBQWtCO0FBNEN0QjtBQTFDQTtFQUNJLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLFlBQVk7RUFDWixXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLGVBQWU7RUFDZixZQUFZO0FBNkNoQjtBQTNDQTtFQUNJLGFBQWE7QUE4Q2pCO0FBNUNBO0VBQ0ksa0JBQWtCO0FBK0N0QjtBQTdDQTtFQUNJLDRCQUE0QjtFQUM1QixzQkFBc0I7RUFDdEIsYUFBYTtFQUNiLFdBQVc7RUFDWCwyQkFBMkI7RUFDM0Isa0JBQWtCO0VBQ2xCLE1BQU07RUFDTiw0QkFBNEI7QUFnRGhDO0FBOUNBO0VBQ0ksU0FBUztFQUNULGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsY0FBYztFQUNkLGtCQUFrQjtBQWlEdEI7QUEvQ0E7RUFDSSxTQUFTO0VBQ1Qsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixjQUFjO0VBQ2QsY0FBYztFQUNkLGdCQUFnQjtBQWtEcEIiLCJmaWxlIjoidGVzdC1ib2FyZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiB3aWR0aCAqL1xyXG46Oi13ZWJraXQtc2Nyb2xsYmFyIHtcclxuICAgIHdpZHRoOiAxMHB4O1xyXG4gICAgaGVpZ2h0OiA3cHg7XHJcbn1cclxuLyogVHJhY2sgKi9cclxuOjotd2Via2l0LXNjcm9sbGJhci10cmFjayB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZjFmMWYxO1xyXG59XHJcbi8qIEhhbmRsZSAqL1xyXG46Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICAgIGJhY2tncm91bmQ6ICMzZjVjOWI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbn1cclxuLyogSGFuZGxlIG9uIGhvdmVyICovXHJcbjo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWI6aG92ZXIge1xyXG4gICAgYmFja2dyb3VuZDogIzUxYzNkMTtcclxufVxyXG4udGl0bGUtdGV4dHtcclxuICAgIGNvbG9yOiMyYTJhMmE7XHJcbiAgICBmb250LXNpemU6MTVweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxufVxyXG4uc3BhY2UtcmlnaHQtbGVmdHtcclxuICAgIG1hcmdpbjoxMHB4IDVweCAyMHB4IDVweDtcclxufVxyXG4ucGFkZGluZy0we1xyXG4gICAgcGFkZGluZzowO1xyXG59XHJcbi5tYXJnaW4tMHtcclxuICAgIG1hcmdpbjowcHg7XHJcbn1cclxuLnBhY2thZ2UtdGFrZW57XHJcbiAgICBib3JkZXItcmFkaXVzOiAxNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nOiAxMnB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgaGVpZ2h0OiA5MHB4O1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBtYXJnaW46IDEwcHggMTVweCAwO1xyXG59XHJcbi5wYWNrYWdlLXRha2VuIGg2e1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgY29sb3I6ICMzMDVGNzI7XHJcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgcGFkZGluZy10b3A6IDIwcHg7XHJcbn1cclxuXHJcbi5wYWNrYWdlLXRha2VuIHNwYW4ge1xyXG4gICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGNvbG9yOiAjMzA1RjcyO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAyMHB4O1xyXG59XHJcbi5wYWNrYWdlLXRha2VuIGltZ3tcclxuICAgIHdpZHRoOiA1OHB4O1xyXG4gICAgaGVpZ2h0OiA1OHB4O1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuLmJnLXB1cnBsZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6I0ZBRTRGNDtcclxufVxyXG4uYmctZ3JlZW57XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNGRkY1RTU7XHJcbn1cclxuLmJnLWJsdWV7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI0U3RkZFNTtcclxufVxyXG4uc3BhY2UtdG9wLTE2e1xyXG4gICAgbWFyZ2luLXRvcDoxNnB4O1xyXG59XHJcbi5wYWNrYWdlLXRha2VuLWlubmVye1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTVweCAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZzogMjJweCAyNXB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgaGVpZ2h0OiAxMTVweDtcclxuICAgIGNvbG9yOiAjMzA1RjcyO1xyXG4gICAgbWFyZ2luOiAwIDE1cHg7XHJcbn1cclxuLnBhY2thZ2UtdGFrZW4taW5uZXIgaDZ7XHJcbiAgICBmb250LXNpekU6MTRweDtcclxufVxyXG4ucGFja2FnZS10YWtlbi1pbm5lciBzcGFue1xyXG4gICAgZm9udC1zaXplOiAzNXB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgY29sb3I6ICMzRjVDOUI7XHJcbiAgICBwYWRkaW5nLXRvcDogOHB4O1xyXG59XHJcbi50YWJsZS1iZyB7XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHggOHB4IDhweCA4cHg7XHJcbiAgICBib3gtc2hhZG93OiAwIDAgMC44NzVyZW0gMCByZ2JhKDE4MSwgMTgxLCAxODEsIDAuMyk7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG59XHJcbi50YWJsZS1yZXNwb25zaXZlIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBvdmVyZmxvdy14OiBhdXRvO1xyXG4gICAgLXdlYmtpdC1vdmVyZmxvdy1zY3JvbGxpbmc6IHRvdWNoO1xyXG59XHJcbi5iZy1pbmZvLWNvbG9yIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNGMjczNzY7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxufVxyXG4udGFibGUtYm9yZGVyZWQgdGhlYWQgdGgge1xyXG4gICAgYm9yZGVyLWJvdHRvbS13aWR0aDogMnB4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxufVxyXG4uY291cnNlLXR0bHtcclxuICAgIGZvbnQtc2l6ZToxM3B4O1xyXG4gICAgZm9udC13ZWlnaHQ6NTAwO1xyXG59XHJcbi50YWJsZSA+IHRib2R5ID4gdHIgPiB0ZCB7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG4udGFibGUgdGQsIC50YWJsZSB0aCB7XHJcbiAgICBwYWRkaW5nOiAwLjU1cmVtO1xyXG59XHJcbi50YWJsZS1ib3JkZXJlZCwgLnRhYmxlLWJvcmRlcmVkIHRkLCAudGFibGUtYm9yZGVyZWQgdGgge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2RlZTJlNjtcclxufVxyXG4udGFibGUtcmVzcG9uc2l2ZSA+IC50YWJsZS1ib3JkZXJlZCB7XHJcbiAgICBib3JkZXI6IDA7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG59XHJcbi50YWJsZS1ob3ZlciB0Ym9keSB0cjpob3ZlciB7XHJcbiAgICBjb2xvcjogIzQ5NTA1NztcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgLjAzNzUpO1xyXG59XHJcbi5jb21wbGV0ZWQtdGQge1xyXG4gICAgY29sb3I6ICMwMGE1MmY7XHJcbn1cclxuLmhlYWRlci1pY29ue1xyXG4gICAgZmxvYXQ6cmlnaHQ7XHJcbn1cclxuXHJcbnVsLnNob3BwaW5nLWljb257XHJcbiAgICBsaXN0LXN0eWxlLXR5cGU6bm9uZTtcclxufVxyXG4ubWVudS1pY29uIGJ1dHRvbntcclxuICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbn1cclxuLm1lbnUtaWNvbiBidXR0b246Zm9jdXN7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG59XHJcbi5uYXYtdGl0bGUgaDZ7XHJcbiAgICBtYXJnaW46OHB4IDAgMCAwO1xyXG4gICAgY29sb3I6I2ZmZjtcclxuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbn1cclxudGFibGV7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG4udGV4dC1ibHVle1xyXG4gICAgY29sb3I6IzNiN2RkZDtcclxufVxyXG4uc3VibWl0dGVke1xyXG4gICAgY29sb3I6I2ZjN2IwMztcclxufVxyXG4uZXhhbXN0YXJ0e1xyXG4gICAgY29sb3I6IzRmOGEyNTtcclxufVxyXG4uZXhhbWNvbXBsZXRlZHtcclxuICAgIGNvbG9yOiMwMGE1MmY7XHJcbn1cclxuLnRha2V0ZXN0e1xyXG4gICAgY29sb3I6IzNiN2RkZDtcclxufVxyXG5cclxuLnVzZXItaWNvbiBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgd2lkdGg6IDQwcHg7XHJcbn1cclxuLmJhY2sgaW1nIHtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG59XHJcbi5iYWNrIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IGltZyB7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLThweDtcclxufVxyXG4ubWVudS1pY29uIHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4uY2FydC1udW1iZXIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDE1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZiYjAwO1xyXG4gICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICMzZjVjOWI7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBwYWRkaW5nOiAxcHg7XHJcbn1cclxuaW9uLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcbi5oZWFkZXItYXJyb3cge1xyXG4gICAgcGFkZGluZzogMjBweCAxNXB4O1xyXG59XHJcbi50b3AtYmcge1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBoZWlnaHQ6IDE2MHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMzBweCAzMHB4O1xyXG59XHJcbi5wYWNrYWdlcy1oZWFkZXIgaDJ7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIG1hcmdpbi1ib3R0b206IDVweDtcclxufVxyXG4ucGFja2FnZXMtaGVhZGVyIHNwYW57XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufSJdfQ== */");

/***/ }),

/***/ "ould":
/*!***************************************************************!*\
  !*** ./src/app/pages/test-board/test-board-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: TestBoardPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestBoardPageRoutingModule", function() { return TestBoardPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _test_board_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./test-board.page */ "MwE2");




const routes = [
    {
        path: '',
        component: _test_board_page__WEBPACK_IMPORTED_MODULE_3__["TestBoardPage"]
    }
];
let TestBoardPageRoutingModule = class TestBoardPageRoutingModule {
};
TestBoardPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TestBoardPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=test-board-test-board-module.js.map