(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~cart-cart-module~pages-coupon-popup-coupon-popup-module"],{

/***/ "0DiE":
/*!***********************************************************!*\
  !*** ./src/app/pages/coupon-popup/coupon-popup.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".popover-main-holder {\n  padding: 0;\n}\n\n.popover-main-holder h4 {\n  margin: 20px 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\nul.list {\n  margin: 5px 0;\n}\n\n.list li {\n  padding: 3px 0;\n  font-size: 15px;\n  color: #305F72;\n}\n\n.popover-scroll {\n  overflow: scroll;\n  min-width: 250px;\n}\n\n.user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.back img {\n  width: 40px;\n}\n\n.back {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.header-arrow {\n  padding: 20px 15px;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 100px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n  margin-bottom: 20px;\n}\n\n.packages-header {\n  margin: 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n  padding-top: 40px;\n}\n\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\n.border-all {\n  border: 1px solid #b1bcd4;\n  border-radius: 10px;\n  margin-bottom: 22px;\n  color: #8a959e;\n}\n\n.reg-num {\n  font-size: 15px;\n  font-weight: 300;\n  color: #8a959e;\n  padding-left: 15px;\n}\n\n.space-3 {\n  margin: 25px 20px 0px;\n}\n\nion-label {\n  margin: 0;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n}\n\n.close-popover {\n  position: absolute;\n  right: 10px;\n  font-size: 26px;\n  top: 10px;\n  color: #ffffff;\n  cursor: pointer;\n}\n\n.coupon-input ion-input {\n  border: 1px solid #b1bcd4;\n  border-radius: 10px;\n  color: #000000;\n  --padding-start: 10px;\n}\n\n.start-btn {\n  background: #3f5c9b !important;\n  --background-activated: #3f5c9b !important;\n  --background-focused: #3f5c9b !important;\n  --background-hover: #3f5c9b !important;\n  border-radius: 8px;\n  height: 40px;\n  margin: 0;\n  width: 100%;\n  text-align: center;\n  font-size: 13px;\n  font-weight: 400;\n  float: right;\n  color: #ffffff;\n}\n\n.coupons-availabel h2 {\n  font-size: 16px;\n  padding: 0;\n  margin: 0;\n  padding-bottom: 15px;\n  padding-top: 15px;\n  border-top: 1px solid #8080803d;\n  font-weight: 500;\n  color: #2b5465;\n}\n\n.coupons-details h2 {\n  margin: 0;\n  font-size: 16px;\n  font-weight: 800;\n  color: #3f5c9b;\n}\n\n.coupons-details p {\n  margin: 0;\n  font-size: 14px;\n  font-weight: 500;\n  color: #000000;\n  padding: 6px 0 0 0;\n}\n\n.coupons-details button {\n  font-size: 14px;\n  padding: 10px 25px;\n  border-radius: 8px;\n  background-color: #f27376;\n  color: #ffffff;\n  margin: 5px auto;\n  text-align: center;\n  float: right;\n}\n\n.coupons-details span {\n  font-size: 12px;\n}\n\n.trash-icon {\n  position: absolute;\n  right: 40px;\n  top: 2px;\n  font-size: 30px;\n  color: #ff5c50;\n}\n\n.btn-opacity {\n  opacity: 0.4;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxjb3Vwb24tcG9wdXAucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUdBO0VBQ0ksVUFBVTtBQUZkOztBQUlBO0VBQ0ksY0FBYztFQUNkLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsY0FBYztFQUNkLGNBQWM7RUFDZCxnQkFBZ0I7QUFEcEI7O0FBR0E7RUFDSSxhQUFhO0FBQWpCOztBQUVBO0VBQ0ksY0FBYTtFQUNiLGVBQWM7RUFDZCxjQUFjO0FBQ2xCOztBQUVBO0VBQ0ksZ0JBQWdCO0VBQ2hCLGdCQUFnQjtBQUNwQjs7QUFDRTtFQUNFLGtCQUFrQjtFQUNsQixXQUFXO0FBRWY7O0FBQUE7RUFDSSxXQUFXO0FBR2Y7O0FBREE7RUFDSSxXQUFXO0FBSWY7O0FBRkE7RUFDSSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGtCQUFrQjtFQUNsQixTQUFTO0FBS2I7O0FBSEE7RUFDSSxpQkFBaUI7QUFNckI7O0FBSkE7RUFDSSxrQkFBa0I7QUFPdEI7O0FBTEE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLHlCQUF5QjtFQUN6QixZQUFZO0VBQ1osV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxlQUFlO0VBQ2YsWUFBWTtBQVFoQjs7QUFOQTtFQUNJLGFBQWE7QUFTakI7O0FBUEE7RUFDSSxrQkFBa0I7QUFVdEI7O0FBUkE7RUFDSSw0QkFBNEI7RUFDNUIsc0JBQXNCO0VBQ3RCLGFBQWE7RUFDYixXQUFXO0VBQ1gsMkJBQTJCO0VBQzNCLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sNEJBQTRCO0VBQzVCLG1CQUFtQjtBQVd2Qjs7QUFUQTtFQUNJLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsaUJBQWlCO0FBWXJCOztBQVZBO0VBQ0ksU0FBUztFQUNULGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsY0FBYztFQUNkLGNBQWM7RUFDZCxnQkFBZ0I7QUFhcEI7O0FBWEE7RUFDSSx5QkFBeUI7RUFDekIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixjQUFjO0FBY2xCOztBQVpBO0VBQ0ksZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2Qsa0JBQWtCO0FBZXRCOztBQWJBO0VBQ0kscUJBQXFCO0FBZ0J6Qjs7QUFkQTtFQUNJLFNBQVM7QUFpQmI7O0FBZkE7RUFDSSxvQkFBZ0I7RUFDaEIsd0JBQW9CO0FBa0J4Qjs7QUFoQkE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLGVBQWU7RUFDZixTQUFTO0VBQ1QsY0FBYztFQUNkLGVBQWU7QUFtQm5COztBQWpCQTtFQUNJLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLHFCQUFnQjtBQW9CcEI7O0FBbEJBO0VBQ0ksOEJBQThCO0VBQzlCLDBDQUF1QjtFQUN2Qix3Q0FBcUI7RUFDckIsc0NBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osU0FBUztFQUNULFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osY0FBYztBQXFCbEI7O0FBbkJBO0VBQ0ksZUFBZTtFQUNmLFVBQVU7RUFDVixTQUFTO0VBQ1Qsb0JBQW9CO0VBQ3BCLGlCQUFpQjtFQUNqQiwrQkFBK0I7RUFDL0IsZ0JBQWdCO0VBQ2hCLGNBQWM7QUFzQmxCOztBQXBCQTtFQUNJLFNBQVM7RUFDVCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7QUF1QmxCOztBQXJCQTtFQUNJLFNBQVM7RUFDVCxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxrQkFBa0I7QUF3QnRCOztBQXRCQTtFQUNJLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLHlCQUF5QjtFQUN6QixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQixZQUFZO0FBeUJoQjs7QUF2QkE7RUFDSSxlQUFlO0FBMEJuQjs7QUF4QkE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFFBQVE7RUFDUixlQUFlO0VBQ2YsY0FBYztBQTJCbEI7O0FBekJBO0VBQ0ksWUFBWTtBQTRCaEIiLCJmaWxlIjoiY291cG9uLXBvcHVwLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGFwcC1wb3BvdmVye1xyXG4vLyAgICAgLS13aWR0aDoyNTBweCFpbXBvcnRhbnQ7XHJcbi8vIH1cclxuLnBvcG92ZXItbWFpbi1ob2xkZXJ7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG59XHJcbi5wb3BvdmVyLW1haW4taG9sZGVyIGg0e1xyXG4gICAgbWFyZ2luOiAyMHB4IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG51bC5saXN0e1xyXG4gICAgbWFyZ2luOiA1cHggMDtcclxufVxyXG4ubGlzdCBsaXtcclxuICAgIHBhZGRpbmc6M3B4IDA7XHJcbiAgICBmb250LXNpemU6MTVweDtcclxuICAgIGNvbG9yOiAjMzA1RjcyO1xyXG59XHJcblxyXG4ucG9wb3Zlci1zY3JvbGx7XHJcbiAgICBvdmVyZmxvdzogc2Nyb2xsO1xyXG4gICAgbWluLXdpZHRoOiAyNTBweDtcclxuICB9XHJcbiAgLnVzZXItaWNvbiBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgd2lkdGg6IDQwcHg7XHJcbn1cclxuLmJhY2sgaW1nIHtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG59XHJcbi5iYWNrIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IGltZyB7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLThweDtcclxufVxyXG4ubWVudS1pY29uIHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4uY2FydC1udW1iZXIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDE1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZiYjAwO1xyXG4gICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICMzZjVjOWI7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBwYWRkaW5nOiAxcHg7XHJcbn1cclxuaW9uLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcbi5oZWFkZXItYXJyb3cge1xyXG4gICAgcGFkZGluZzogMjBweCAxNXB4O1xyXG59XHJcbi50b3AtYmcge1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMzBweCAzMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG4ucGFja2FnZXMtaGVhZGVye1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICBwYWRkaW5nLXRvcDogNDBweDtcclxufVxyXG4ucGFja2FnZXMtaGVhZGVyIHNwYW57XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICBjb2xvcjogIzMwNUY3MjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcclxufVxyXG4uYm9yZGVyLWFsbCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjYjFiY2Q0O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDIycHg7XHJcbiAgICBjb2xvcjogIzhhOTU5ZTtcclxufVxyXG4ucmVnLW51bSB7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBmb250LXdlaWdodDogMzAwO1xyXG4gICAgY29sb3I6ICM4YTk1OWU7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XHJcbn1cclxuLnNwYWNlLTMge1xyXG4gICAgbWFyZ2luOiAyNXB4IDIwcHggMHB4O1xyXG59XHJcbmlvbi1sYWJlbCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbn1cclxuaW9uLWl0ZW0ge1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XHJcbiAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XHJcbn1cclxuLmNsb3NlLXBvcG92ZXJ7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICByaWdodDogMTBweDtcclxuICAgIGZvbnQtc2l6ZTogMjZweDtcclxuICAgIHRvcDogMTBweDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbi5jb3Vwb24taW5wdXQgaW9uLWlucHV0e1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2IxYmNkNDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICAgIC0tcGFkZGluZy1zdGFydDogMTBweDtcclxufVxyXG4uc3RhcnQtYnRuIHtcclxuICAgIGJhY2tncm91bmQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJhY2tncm91bmQtaG92ZXI6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG59XHJcbi5jb3Vwb25zLWF2YWlsYWJlbCBoMntcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcclxuICAgIHBhZGRpbmctdG9wOiAxNXB4O1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICM4MDgwODAzZDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogIzJiNTQ2NTtcclxufVxyXG4uY291cG9ucy1kZXRhaWxzIGgye1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcclxuICAgIGNvbG9yOiAjM2Y1YzliO1xyXG59XHJcbi5jb3Vwb25zLWRldGFpbHMgcHtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBjb2xvcjogIzAwMDAwMDtcclxuICAgIHBhZGRpbmc6IDZweCAwIDAgMDtcclxufVxyXG4uY291cG9ucy1kZXRhaWxzIGJ1dHRvbntcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIHBhZGRpbmc6IDEwcHggMjVweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmMjczNzY7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIG1hcmdpbjogNXB4IGF1dG87XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbn1cclxuLmNvdXBvbnMtZGV0YWlscyBzcGFue1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcbi50cmFzaC1pY29ue1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDQwcHg7XHJcbiAgICB0b3A6IDJweDtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIGNvbG9yOiAjZmY1YzUwO1xyXG59XHJcbi5idG4tb3BhY2l0eXtcclxuICAgIG9wYWNpdHk6IDAuNDtcclxufSJdfQ== */");

/***/ }),

/***/ "Fjy8":
/*!*********************************************************!*\
  !*** ./src/app/pages/coupon-popup/coupon-popup.page.ts ***!
  \*********************************************************/
/*! exports provided: CouponPopupPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CouponPopupPage", function() { return CouponPopupPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_coupon_popup_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./coupon-popup.page.html */ "tqVg");
/* harmony import */ var _coupon_popup_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./coupon-popup.page.scss */ "0DiE");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/http.service */ "N+K7");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");
/* harmony import */ var src_Helper_Constant__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/Helper/Constant */ "n88v");
/* harmony import */ var src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/Helper/UIHelper */ "x7bl");









let CouponPopupPage = class CouponPopupPage {
    constructor(modalController, navParams, httpService, storageService, uihelper) {
        this.modalController = modalController;
        this.navParams = navParams;
        this.httpService = httpService;
        this.storageService = storageService;
        this.uihelper = uihelper;
        this.course_id = {
            course_id: localStorage.getItem('course_id')
        };
        this.storageService.get(src_Helper_Constant__WEBPACK_IMPORTED_MODULE_7__["Constant"].AUTH).then(res => {
            this.userData = res.user_details;
            this.user_id = res.user_details.id;
        });
    }
    ngOnInit() {
        this.data = this.navParams.get('data');
        this.uihelper.ShowSpinner();
        this.httpService.afterLoginPost('fetchadmindiscounts', this.data).subscribe((res) => {
            // console.log('fetchadmindiscounts',res)
            this.coupons_added_in_cart_admin = res.payload.coupons_added_in_cart.filter((element) => {
                return element.discount_of == 'admin';
            });
            this.coupons_added_in_cart_institute = res.payload.coupons_added_in_cart.filter((element) => {
                return element.discount_of == 'institute';
            });
            // console.log('coupons_added_in_cart_institute',this.coupons_added_in_cart_institute)
            this.coupons_added_in_cart_student = res.payload.coupons_added_in_cart.filter((element) => {
                return element.discount_of == 'student';
            });
            // console.log('coupons_added_in_cart_student',this.coupons_added_in_cart_student)
            this.coupons_added = res.payload.coupons_added_in_cart.map((element, index) => {
                return element.id;
            });
            // console.log('coupons_added',this.coupons_added)
            this.single_coupon_list = res.payload.discounts_for_single;
            this.group_coupon_list = res.payload.discounts_for_group;
            this.coupon_list = this.single_coupon_list.concat(this.group_coupon_list);
            console.log('final coupon list', this.coupon_list);
            this.uihelper.HideSpinner();
        });
    }
    dismiss() {
        // console.log('dismiss')
        this.modalController.dismiss();
        return false;
    }
    applyCoupon(coupon_code) {
        if (coupon_code == undefined) {
            this.uihelper.HideSpinner();
            this.uihelper.ShowAlert('Coupon Code', "Please Enter Coupon Code");
        }
        else {
            this.uihelper.ShowSpinner();
            let data = new FormData();
            data.append('course_id', this.course_id.course_id);
            data.append('user_id', this.user_id);
            data.append('coupon_code', coupon_code);
            data.append('remove_credit', '0');
            this.httpService.afterLoginPost('applycoupon', data).subscribe((res) => {
                if (res.payload.discounted_of == 'admin_coupon') {
                    this.uihelper.ShowAlert('Cart', "Coupon Code successfully applied..you got the discount of Rs." + res.payload.admin_code_discounted_amt);
                }
                else if (res.payload.discounted_of == 'institute_coupon') {
                    this.uihelper.ShowAlert('Cart', "Coupon Code successfully applied..you got the discount of Rs." + res.payload.admin_code_discounted_amt);
                }
                else {
                    this.uihelper.ShowAlert('Cart', "Coupon Code successfully applied..you got the discount of Rs." + res.payload.student_code_discounted_amt);
                }
                this.modalController.dismiss();
                this.uihelper.HideSpinner();
            }, (err) => {
                this.uihelper.HideSpinner();
                console.log(err);
                this.uihelper.ShowAlert('Apply Coupon', err.error.payload.message);
            });
        }
    }
    deleteCouponCode(coupon_type) {
        this.uihelper.ShowSpinner();
        // let coupon_type = this.coupon_detail.admin_coupon_detail_data.coupon_code_type
        console.log(' delete coupon', coupon_type);
        let data = new FormData();
        data.append('coupontype', coupon_type);
        data.append('student_id', this.user_id);
        this.httpService.afterLoginPost('removecoupon', data).subscribe((res) => {
            this.modalController.dismiss();
            this.uihelper.HideSpinner();
            this.uihelper.ShowAlert('Cart', "Coupon Code Removed");
        }, (err) => {
            this.uihelper.ShowAlert('Apply Coupon', "Something went wrong!! Try Again..");
        });
    }
};
CouponPopupPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavParams"] },
    { type: src_app_services_http_service__WEBPACK_IMPORTED_MODULE_5__["HttpService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_6__["StorageService"] },
    { type: src_Helper_UIHelper__WEBPACK_IMPORTED_MODULE_8__["UIHelper"] }
];
CouponPopupPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-coupon-popup',
        template: _raw_loader_coupon_popup_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_coupon_popup_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], CouponPopupPage);



/***/ }),

/***/ "tqVg":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/coupon-popup/coupon-popup.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<!-- <ion-header>\r\n  <ion-toolbar>\r\n    <ion-title>coupon-popup</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content> -->\r\n\r\n\r\n<div class=\"popover-scroll\">\r\n  <div class=\"popover-main-holder\">\r\n    <div style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n      <ion-icon name=\"close\" class=\"close-popover\" (click)=\"dismiss()\"></ion-icon>\r\n      <h2 class=\"packages-header\">Apply Coupon</h2>\r\n    </div>\r\n    <div>\r\n      <ion-row style=\"margin: 10px;\">\r\n        <ion-col size=\"8\" class=\"coupon-input\">\r\n          <ion-input placeholder=\"Enter coupon code\" required=\"required\" [(ngModel)]=\"couponCodeManual\"></ion-input>\r\n        </ion-col>\r\n        <ion-col size=\"4\">\r\n          <button ion-button class=\"start-btn\" (click)=\"applyCoupon(couponCodeManual)\">Apply</button>\r\n        </ion-col>\r\n      </ion-row>\r\n\r\n      \r\n\r\n      <ion-row style=\"margin: 10px;\">\r\n        <ion-col size=\"12\" class=\"coupons-availabel\">\r\n          <h2>Available coupons</h2>\r\n        </ion-col>\r\n      </ion-row>\r\n\r\n      <ion-row style=\"margin: 10px; border-bottom: dashed 1px #00000030; padding-bottom: 3px;\" *ngFor=\"let coupon of coupon_list, let i = index\">\r\n        <ion-col size=\"9\" class=\"coupons-details\">\r\n          <h2>{{coupon.discount_code}}</h2>\r\n          <!-- <p>20% OFF upto 40INR</p> -->\r\n          <span><b>Package: </b>{{coupon.pkg_info.name}}</span><br>\r\n          <span><b>Desciption: </b>{{coupon.description}}</span>\r\n        </ion-col>\r\n        <ion-col size=\"3\" class=\"coupons-details\">\r\n            <span><ion-icon name=\"trash\" class=\"trash-icon\" (click)=\"deleteCouponCode(coupon.discount_of)\" *ngIf=\"coupons_added.indexOf(coupon.id) != -1\"></ion-icon></span>\r\n\r\n            <button ion-button [disabled]=\"coupons_added_in_cart_admin.length && coupons_added.indexOf(coupon.id) == -1\" *ngIf=\"coupons_added.indexOf(coupon.id) == -1\" (click)=\"applyCoupon(coupon.discount_code)\" [class.btn-opacity]=\"coupons_added_in_cart_admin.length\">Select</button>\r\n\r\n        </ion-col>\r\n      </ion-row>\r\n\r\n      <!-- for institute coupon display start -->\r\n      <div *ngIf=\"coupons_added_in_cart_institute != ''\">\r\n        <ion-row style=\"margin: 10px;\">\r\n          <ion-col size=\"12\" class=\"coupons-availabel\">\r\n            <h2>Institute coupons</h2>\r\n          </ion-col>\r\n        </ion-row>\r\n\r\n        <ion-row style=\"margin: 10px; border-bottom: dashed 1px #00000030; padding-bottom: 3px;\" *ngFor=\"let coupon of coupons_added_in_cart_institute\">\r\n          <ion-col size=\"9\" class=\"coupons-details\">\r\n            <h2>{{coupon.discount_code}}</h2>\r\n            <span><b>Desciption: </b>{{coupon.description}}</span>\r\n          </ion-col>\r\n          <ion-col size=\"3\" class=\"coupons-details\">\r\n              <span><ion-icon name=\"trash\" class=\"trash-icon\" (click)=\"deleteCouponCode(coupon.discount_of)\" ></ion-icon></span>\r\n\r\n\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n      <!-- for institute coupon ends -->\r\n\r\n      <!-- for student referal coupon display start -->\r\n      <div *ngIf=\"coupons_added_in_cart_student != ''\">\r\n        <ion-row style=\"margin: 10px;\">\r\n          <ion-col size=\"12\" class=\"coupons-availabel\">\r\n            <h2>Referal coupons</h2>\r\n          </ion-col>\r\n        </ion-row>\r\n\r\n        <ion-row style=\"margin: 10px; border-bottom: dashed 1px #00000030; padding-bottom: 3px;\" *ngFor=\"let coupon of coupons_added_in_cart_student\">\r\n          <ion-col size=\"9\" class=\"coupons-details\">\r\n            <h2>{{coupon.discount_code}}</h2>\r\n            <span><b>Desciption: </b>{{coupon.description}}</span>\r\n          </ion-col>\r\n          <ion-col size=\"3\" class=\"coupons-details\">\r\n              <span><ion-icon name=\"trash\" class=\"trash-icon\" (click)=\"deleteCouponCode(coupon.discount_of)\" ></ion-icon></span>\r\n\r\n\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n      <!-- for student referal coupon ends -->\r\n\r\n    </div>\r\n    \r\n  </div>\r\n</div>");

/***/ })

}]);
//# sourceMappingURL=default~cart-cart-module~pages-coupon-popup-coupon-popup-module.js.map