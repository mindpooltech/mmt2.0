(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["end-test-end-test-module"],{

/***/ "0XBv":
/*!***********************************************************!*\
  !*** ./src/app/pages/end-test/end-test-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: EndTestPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EndTestPageRoutingModule", function() { return EndTestPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _end_test_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./end-test.page */ "9Uu6");




const routes = [
    {
        path: '',
        component: _end_test_page__WEBPACK_IMPORTED_MODULE_3__["EndTestPage"]
    }
];
let EndTestPageRoutingModule = class EndTestPageRoutingModule {
};
EndTestPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EndTestPageRoutingModule);



/***/ }),

/***/ "6CYe":
/*!***************************************************!*\
  !*** ./src/app/pages/end-test/end-test.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".user-icon img {\n  border-radius: 50%;\n  width: 40px;\n}\n\n.back img {\n  width: 40px;\n}\n\n.back {\n  float: left;\n}\n\n.shopping-cart img {\n  width: 30px;\n  margin-right: 20px;\n  position: relative;\n  top: -8px;\n}\n\n.menu-icon {\n  text-align: right;\n}\n\n.shopping-cart {\n  position: relative;\n}\n\n.cart-number {\n  position: absolute;\n  right: 15px;\n  background-color: #ffbb00;\n  height: 20px;\n  width: 20px;\n  text-align: center;\n  border-radius: 50%;\n  font-weight: 500;\n  color: #3f5c9b;\n  font-size: 14px;\n  padding: 1px;\n}\n\nion-header {\n  display: none;\n}\n\n.header-arrow {\n  padding: 20px 15px;\n}\n\n.top-bg {\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 160px;\n  width: 100%;\n  background-position: center;\n  position: relative;\n  top: 0;\n  border-radius: 0 0 30px 30px;\n}\n\n.packages-header h2 {\n  margin: 50px 0;\n  text-align: center;\n  font-size: 20px;\n  color: #ffffff;\n  margin-bottom: 5px;\n}\n\n.packages-header span {\n  margin: 0;\n  text-align: center;\n  font-size: 18px;\n  color: #305F72;\n  display: block;\n  font-weight: 500;\n}\n\nion-label {\n  margin: 0;\n}\n\nion-item {\n  --padding-start: 0px;\n  --inner-padding-end: 0px;\n}\n\n.end-test {\n  margin: 40px 0 15px;\n}\n\n.end-test-content h3 {\n  font-size: 24px;\n  margin-top: 5px;\n  margin-bottom: 10px;\n  color: #305f72;\n}\n\n.end-test-content p {\n  font-size: 15px;\n  margin-top: 0;\n  margin-bottom: 15px;\n}\n\n.motivation-content {\n  margin: 0 25px 12px;\n  border-radius: 5px;\n}\n\n.exam-content span {\n  font-size: 15px;\n  line-height: 22px;\n  color: #333;\n}\n\n.dashboard-btn {\n  padding: 9px 20px;\n  border-radius: 8px !important;\n  box-shadow: none !important;\n  margin-left: 10px;\n  background: #3f5c9b !important;\n  --background-activated: #3f5c9b !important;\n  --background-focused: #3f5c9b !important;\n  --background-hover: #3f5c9b !important;\n  --border-radius: 8px;\n  height: 42px;\n  margin: 20px auto;\n  width: 170px;\n  text-align: center;\n  font-size: 15px;\n  font-weight: 400;\n  color: #ffffff;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxlbmQtdGVzdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztBQUNmOztBQUNBO0VBQ0ksV0FBVztBQUVmOztBQUFBO0VBQ0ksV0FBVztBQUdmOztBQURBO0VBQ0ksV0FBVztFQUNYLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsU0FBUztBQUliOztBQUZBO0VBQ0ksaUJBQWlCO0FBS3JCOztBQUhBO0VBQ0ksa0JBQWtCO0FBTXRCOztBQUpBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIsWUFBWTtFQUNaLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsZUFBZTtFQUNmLFlBQVk7QUFPaEI7O0FBTEE7RUFDSSxhQUFhO0FBUWpCOztBQU5BO0VBQ0ksa0JBQWtCO0FBU3RCOztBQVBBO0VBQ0ksNEJBQTRCO0VBQzVCLHNCQUFzQjtFQUN0QixhQUFhO0VBQ2IsV0FBVztFQUNYLDJCQUEyQjtFQUMzQixrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLDRCQUE0QjtBQVVoQzs7QUFSQTtFQUNJLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGNBQWM7RUFDZCxrQkFBa0I7QUFXdEI7O0FBVEE7RUFDSSxTQUFTO0VBQ1Qsa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixjQUFjO0VBQ2QsY0FBYztFQUNkLGdCQUFnQjtBQVlwQjs7QUFWQTtFQUNJLFNBQVM7QUFhYjs7QUFYQTtFQUNJLG9CQUFnQjtFQUNoQix3QkFBb0I7QUFjeEI7O0FBWkE7RUFDSSxtQkFBa0I7QUFldEI7O0FBYkE7RUFDSSxlQUFlO0VBQ2YsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixjQUFjO0FBZ0JsQjs7QUFkQTtFQUNJLGVBQWM7RUFDZCxhQUFZO0VBQ1osbUJBQW1CO0FBaUJ2Qjs7QUFmQTtFQUdJLG1CQUFrQjtFQUNsQixrQkFBaUI7QUFnQnJCOztBQWRBO0VBQ0ksZUFBYztFQUNkLGlCQUFpQjtFQUNqQixXQUFVO0FBaUJkOztBQWZBO0VBQ0ksaUJBQWlCO0VBQ2pCLDZCQUE2QjtFQUM3QiwyQkFBMkI7RUFDM0IsaUJBQWlCO0VBQ2pCLDhCQUE4QjtFQUM5QiwwQ0FBdUI7RUFDdkIsd0NBQXFCO0VBQ3JCLHNDQUFtQjtFQUNuQixvQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsY0FBYztBQWtCbEIiLCJmaWxlIjoiZW5kLXRlc3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnVzZXItaWNvbiBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgd2lkdGg6IDQwcHg7XHJcbn1cclxuLmJhY2sgaW1nIHtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG59XHJcbi5iYWNrIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IGltZyB7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIG1hcmdpbi1yaWdodDogMjBweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogLThweDtcclxufVxyXG4ubWVudS1pY29uIHtcclxuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5zaG9wcGluZy1jYXJ0IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4uY2FydC1udW1iZXIge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgcmlnaHQ6IDE1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZiYjAwO1xyXG4gICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgY29sb3I6ICMzZjVjOWI7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBwYWRkaW5nOiAxcHg7XHJcbn1cclxuaW9uLWhlYWRlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcbi5oZWFkZXItYXJyb3cge1xyXG4gICAgcGFkZGluZzogMjBweCAxNXB4O1xyXG59XHJcbi50b3AtYmcge1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBoZWlnaHQ6IDE2MHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMzBweCAzMHB4O1xyXG59XHJcbi5wYWNrYWdlcy1oZWFkZXIgaDJ7XHJcbiAgICBtYXJnaW46IDUwcHggMDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIGNvbG9yOiAjZmZmZmZmO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG59XHJcbi5wYWNrYWdlcy1oZWFkZXIgc3BhbntcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIGNvbG9yOiAjMzA1RjcyO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG59XHJcbmlvbi1sYWJlbCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbn1cclxuaW9uLWl0ZW0ge1xyXG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XHJcbiAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XHJcbn1cclxuLmVuZC10ZXN0e1xyXG4gICAgbWFyZ2luOjQwcHggMCAxNXB4O1xyXG59XHJcbi5lbmQtdGVzdC1jb250ZW50IGgze1xyXG4gICAgZm9udC1zaXplOiAyNHB4O1xyXG4gICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIGNvbG9yOiAjMzA1ZjcyO1xyXG59XHJcbi5lbmQtdGVzdC1jb250ZW50IHB7XHJcbiAgICBmb250LXNpemU6MTVweDtcclxuICAgIG1hcmdpbi10b3A6MDtcclxuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbn1cclxuLm1vdGl2YXRpb24tY29udGVudHtcclxuICAgIC8vIHBhZGRpbmc6MzBweCAyMHB4O1xyXG4gICAgLy8gYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjEyKSAwcHggNHB4IDE2cHg7XHJcbiAgICBtYXJnaW46MCAyNXB4IDEycHg7XHJcbiAgICBib3JkZXItcmFkaXVzOjVweDtcclxufVxyXG4uZXhhbS1jb250ZW50IHNwYW57XHJcbiAgICBmb250LXNpemU6MTVweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAyMnB4O1xyXG4gICAgY29sb3I6IzMzMztcclxufVxyXG4uZGFzaGJvYXJkLWJ0bntcclxuICAgIHBhZGRpbmc6IDlweCAyMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4ICFpbXBvcnRhbnQ7XHJcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgIGJhY2tncm91bmQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAjM2Y1YzliICFpbXBvcnRhbnQ7XHJcbiAgICAtLWJhY2tncm91bmQtaG92ZXI6ICMzZjVjOWIgIWltcG9ydGFudDtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gICAgaGVpZ2h0OiA0MnB4O1xyXG4gICAgbWFyZ2luOiAyMHB4IGF1dG87XHJcbiAgICB3aWR0aDogMTcwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbn0iXX0= */");

/***/ }),

/***/ "9Uu6":
/*!*************************************************!*\
  !*** ./src/app/pages/end-test/end-test.page.ts ***!
  \*************************************************/
/*! exports provided: EndTestPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EndTestPage", function() { return EndTestPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_end_test_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./end-test.page.html */ "K6Cm");
/* harmony import */ var _end_test_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./end-test.page.scss */ "6CYe");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");





let EndTestPage = class EndTestPage {
    constructor(router, activatedRoute) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.activatedRoute.queryParams.subscribe(params => {
            this.price = params["price"];
            this.exist_theory_que = params["exist_theory_que"];
            console.log('exist_theory_que', this.exist_theory_que);
        });
    }
    ngOnInit() {
    }
    goToDashboard() {
        this.router.navigate(['/tabs']);
    }
};
EndTestPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }
];
EndTestPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-end-test',
        template: _raw_loader_end_test_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_end_test_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], EndTestPage);



/***/ }),

/***/ "K6Cm":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/end-test/end-test.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <!-- <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/home\"></ion-back-button>\r\n    </ion-buttons> -->\r\n    <ion-buttons slot=\"primary\">\r\n      <ion-button>\r\n        <ul class=\"shopping-icon\">\r\n          \r\n        </ul>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ios title-default hydrated\">End Test</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n  <ion-row style=\"background-image:url(../assets/packages-header.svg);\" class=\"top-bg\">\r\n    <ion-col size=\"12\" class=\"header-arrow padding-0\">\r\n      <div>\r\n        <ion-row>\r\n          <ion-col size=\"12\">\r\n            <div class=\"packages-header\">\r\n              <h2>End Test</h2>\r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <!-- <div class=\"end-test text-center\">\r\n        <img src=\"assets/icon/end-test.png\">\r\n      </div> -->\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row>\r\n    <ion-col size=\"12\" class=\"padding-0\">\r\n      <div class=\"end-test-content text-center\">\r\n        <h3>Congratulations !!</h3>\r\n        <p> You have completed the test successfully.</p>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row>\r\n    <ion-col class=\"padding-0\">\r\n      <div class=\"motivation-content text-center\">\r\n        <img src=\"assets/icon/motivational-img.png\">\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row>\r\n    <ion-col size=\"10\" offset=\"1\" class=\"padding-0\">\r\n\r\n      <div class=\"exam-content text-center\" *ngIf=\"exist_theory_que == 'false'\">\r\n        <span >Pinch yourself but it’s true 😍 Your test has been evaluated automatically and your result is declared. Click on Below button and Go to My Test History to view your performance.</span>\r\n      </div>\r\n      <div class=\"exam-content text-center\" *ngIf=\"exist_theory_que == 'true'\">        \r\n        <span *ngIf=\"price == '0'\">Free Tests are self-evaluated. You can check the suggested answers in My Test History .</span>\r\n        <span *ngIf=\"price != '0'\">The test answer sheet is submitted for its evaluation with our experts panel. You will receive the results on your dashboard once the evaluation is done.</span>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n  <ion-row>\r\n    <ion-col size=\"8\" offset=\"2\" class=\"padding-0\">\r\n      <div class=\"text-center\">\r\n        <button ion-button full class=\"dashboard-btn\" (click)=\"goToDashboard()\">Go To Dashboard</button>\r\n      </div>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-content>\r\n");

/***/ }),

/***/ "eBAB":
/*!***************************************************!*\
  !*** ./src/app/pages/end-test/end-test.module.ts ***!
  \***************************************************/
/*! exports provided: EndTestPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EndTestPageModule", function() { return EndTestPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _end_test_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./end-test-routing.module */ "0XBv");
/* harmony import */ var _end_test_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./end-test.page */ "9Uu6");







let EndTestPageModule = class EndTestPageModule {
};
EndTestPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _end_test_routing_module__WEBPACK_IMPORTED_MODULE_5__["EndTestPageRoutingModule"]
        ],
        declarations: [_end_test_page__WEBPACK_IMPORTED_MODULE_6__["EndTestPage"]]
    })
], EndTestPageModule);



/***/ })

}]);
//# sourceMappingURL=end-test-end-test-module.js.map